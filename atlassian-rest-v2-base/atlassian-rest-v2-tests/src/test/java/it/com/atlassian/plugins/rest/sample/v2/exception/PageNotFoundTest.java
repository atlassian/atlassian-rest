package it.com.atlassian.plugins.rest.sample.v2.exception;

import java.net.URI;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.Response;

import org.junit.Test;

import com.atlassian.plugins.rest.api.model.Status;
import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static jakarta.ws.rs.core.MediaType.APPLICATION_XML;
import static jakarta.ws.rs.core.Response.Status.NOT_FOUND;
import static org.junit.Assert.assertEquals;

public class PageNotFoundTest {

    @Test
    public void testNotFoundReturns404() {
        final URI baseUri = UriBuilder.create()
                .path("rest")
                .path("exception")
                .path("1")
                .path("mapper")
                .path("somepaththatdoesntexist")
                .build();

        Response response = ClientBuilder.newClient()
                .target(baseUri)
                .request(APPLICATION_XML)
                .get(Response.class);

        assertEquals(NOT_FOUND.getStatusCode(), response.getStatusInfo().getStatusCode());
        Status statusEntity = response.readEntity(Status.class);
        assertEquals(NOT_FOUND.getStatusCode(), statusEntity.getCode());
    }
}
