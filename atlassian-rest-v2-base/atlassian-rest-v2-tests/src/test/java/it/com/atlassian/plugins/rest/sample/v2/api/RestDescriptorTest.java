package it.com.atlassian.plugins.rest.sample.v2.api;

import java.net.URI;
import jakarta.ws.rs.client.ClientBuilder;

import org.junit.Test;

import com.atlassian.plugins.rest.sample.v2.api.ApiResource;
import com.atlassian.plugins.rest.sample.v2.api1.ApiResource1;
import com.atlassian.plugins.rest.sample.v2.api3.ApiResource3;
import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;

public class RestDescriptorTest {

    private static final String API = "api";
    private static final String SUB = "sub";
    public static final String LATEST = "latest";
    private static final URI BASE_URI =
            UriBuilder.create().path("rest").path(API).build();

    @Test
    public void testNoneVersionRestDescriptor() {
        String result =
                ClientBuilder.newClient().target(BASE_URI).path(API).request().get(String.class);

        assertEquals(ApiResource.class.getSimpleName(), result);
    }

    @Test
    public void testLatestVersionOfNoneVersionInRestDescriptor() {
        String result = ClientBuilder.newClient()
                .target(BASE_URI)
                .path(LATEST)
                .path(API)
                .request()
                .get(String.class);

        assertEquals(ApiResource.class.getSimpleName(), result);
    }

    @Test
    public void testSubPathInRestModuleDescriptor() {
        String result = ClientBuilder.newClient()
                .target(BASE_URI)
                .path(SUB)
                .path("1")
                .path(API)
                .request()
                .get(String.class);

        assertEquals(ApiResource1.class.getSimpleName(), result);
    }

    @Test
    public void testSubPath3InRestModuleDescriptor() {
        String result = ClientBuilder.newClient()
                .target(BASE_URI)
                .path(SUB)
                .path("3")
                .path(API)
                .request()
                .get(String.class);

        assertEquals(ApiResource3.class.getSimpleName(), result);
    }

    /**
     * See DCPL-1380
     * The weight of the rest module descriptor 'rest-api' is assigned higher than 'rest-api-sub-path3'
     * so that 'rest-api-sub-path3' filter is picked up instead of 'rest-api' filter.
     * If the weight is not assigned differently, the response will return 404.
     */
    @Test
    public void testLatestVersionOfSubPathInRestModuleDescriptor() {
        String result = ClientBuilder.newClient()
                .target(BASE_URI)
                .path(SUB)
                .path(LATEST)
                .path(API)
                .request()
                .get(String.class);

        assertEquals(ApiResource3.class.getSimpleName(), result);
    }
}
