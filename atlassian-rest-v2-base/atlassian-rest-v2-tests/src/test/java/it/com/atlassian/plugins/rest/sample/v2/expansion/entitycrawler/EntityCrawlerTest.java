package it.com.atlassian.plugins.rest.sample.v2.expansion.entitycrawler;

import java.net.URI;
import java.util.Arrays;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;

import org.junit.Test;

import com.atlassian.plugins.rest.sample.v2.expansion.entitycrawler.ListWrapperParentEntity;
import com.atlassian.plugins.rest.sample.v2.expansion.selfexpanding.SelfExpandingExampleEntity;
import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class EntityCrawlerTest {
    URI baseUrl = UriBuilder.create().path("rest").path("expansion").path("1").build();
    WebTarget webTarget = ClientBuilder.newClient().target(baseUrl).path("crawler");

    @Test
    public void testCustomSelfExpandingExpanderResolver() {
        final SelfExpandingExampleEntity entity = webTarget
                .path("self")
                .queryParam("expand", "selfExpanding")
                .request()
                .get(SelfExpandingExampleEntity.class);
        assertEquals(Arrays.asList("one", "two"), entity.getSelfExpandingList());
    }

    @Test
    public void testCustomListWrapperExpanderResolver() {
        final ListWrapperParentEntity entity = webTarget
                .path("list")
                .queryParam("expand", "listWrapperEntity")
                .request()
                .get(ListWrapperParentEntity.class);
        assertFalse(entity.getListWrapperEntity().getCustomEntityList().isEmpty());
    }

    /**
     * The values of "expand" query parameter fulfill the pattern with no space in between
     */
    @Test
    public void testCustomListWrapperExpanderResolverExpandingTwoExpandableFields() {
        final ListWrapperParentEntity entity = webTarget
                .path("list")
                .queryParam("expand", "listWrapperEntity,listWrapperEntity2")
                .request()
                .get(ListWrapperParentEntity.class);
        assertFalse(entity.getListWrapperEntity().getCustomEntityList().isEmpty());
        assertFalse(entity.getListWrapperEntity2().getCustomEntityList().isEmpty());
    }

    @Test
    public void testCustomListWrapperExpanderResolverExpandingTwoExpandableFieldsWithSpace() {
        final ListWrapperParentEntity entity = webTarget
                .path("list")
                .queryParam("expand", "listWrapperEntity, listWrapperEntity2")
                .request()
                .get(ListWrapperParentEntity.class);
        assertFalse(entity.getListWrapperEntity().getCustomEntityList().isEmpty());
        assertFalse(entity.getListWrapperEntity2().getCustomEntityList().isEmpty());
    }

    @Test
    public void testCustomListWrapperExpanderResolverExpandingTwoExpandableFieldsWithSpaces() {
        final ListWrapperParentEntity entity = webTarget
                .path("list")
                .queryParam("expand", "listWrapperEntity ,%20listWrapperEntity2 ")
                .request()
                .get(ListWrapperParentEntity.class);
        assertFalse(entity.getListWrapperEntity().getCustomEntityList().isEmpty());
        assertFalse(entity.getListWrapperEntity2().getCustomEntityList().isEmpty());
    }
}
