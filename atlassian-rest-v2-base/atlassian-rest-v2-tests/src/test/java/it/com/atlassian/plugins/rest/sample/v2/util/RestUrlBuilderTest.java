package it.com.atlassian.plugins.rest.sample.v2.util;

import java.net.URI;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;

import org.junit.Test;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;

import static com.atlassian.plugins.rest.sample.v2.resource.RestUrlBuilderResource.PARAM;

public class RestUrlBuilderTest {
    private final URI baseUri = UriBuilder.create()
            .path("rest")
            .path("util")
            .path("1")
            .path("restUrlBuilder")
            .build();
    private final Client client = ClientBuilder.newClient();

    @Test
    public void testDummySubResource() {
        String response =
                client.target(baseUri).path("dummySubResource").request().get(String.class);
        assertEquals("http://atlassian.com:1234/foo/dummy/sub", response);
    }

    /**
     * Test {@code RestUilBuilder} can resolve the external exception not in atlassian-rest-v2-plugin,
     * for example, {@code TypeNotInstalledException} from Applinks
     */
    @Test
    public void testDummySubResourceWithExternalException() {
        String response = client.target(baseUri)
                .path("dummySubResourceExternalException")
                .request()
                .get(String.class);
        assertEquals("http://atlassian.com:1234/foo/dummy/externalException", response);
    }

    @Test
    public void testDummySubResourceWithPathParamIncluded() {
        String response = client.target(baseUri)
                .path("dummySubResourceWithPathParam")
                .request()
                .get(String.class);
        assertEquals("http://atlassian.com:1234/foo/dummy/with-path-param/" + PARAM, response);
    }

    @Test
    public void testDummySubResourceWithoutQueryParamIncluded() {
        String response = client.target(baseUri)
                .path("dummySubResourceWithQueryParam")
                .request()
                .get(String.class);
        assertEquals("http://atlassian.com:1234/foo/dummy/with-query-param", response);
    }

    @Test
    public void testDummySubResourceWithArgConstructor() {
        String response = client.target(baseUri)
                .path("dummySubResourceWithArgConstructor")
                .request()
                .get(String.class);
        assertEquals("http://atlassian.com:1234/foo/dummyWithArgConstructor/sub", response);
    }
}
