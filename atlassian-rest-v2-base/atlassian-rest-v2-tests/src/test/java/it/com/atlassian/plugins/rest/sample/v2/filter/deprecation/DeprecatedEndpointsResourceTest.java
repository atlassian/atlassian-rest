package it.com.atlassian.plugins.rest.sample.v2.filter.deprecation;

import java.net.URI;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.junit.Test;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class DeprecatedEndpointsResourceTest {

    private static final String DEPRECATION = "Deprecation";
    private static final String LINK = "Link";

    final URI baseUri =
            UriBuilder.create().path("rest").path("filter").path("1").build();

    @Test
    public void supportedMethodShouldNotHaveDeprecationHeaderTest() {
        final String deprecationHeader = ClientBuilder.newClient()
                .target(baseUri)
                .path("deprecated/supported")
                .request(MediaType.TEXT_PLAIN)
                .get()
                .getHeaderString(DEPRECATION);

        String expectedHeader = null;

        assertEquals(expectedHeader, deprecationHeader);
    }

    @Test
    public void deprecatedMethodShouldHaveDeprecationHeaderTest() {
        final String deprecationHeader = ClientBuilder.newClient()
                .target(baseUri)
                .path("deprecated/deprecated")
                .request(MediaType.TEXT_PLAIN)
                .get()
                .getHeaderString(DEPRECATION);

        String expectedHeader = "@1682524800";

        assertEquals(expectedHeader, deprecationHeader);
    }

    @Test
    public void deprecatedMethodWithWrongDateShouldHaveDeprecationHeaderTest() {
        final String deprecationHeader = ClientBuilder.newClient()
                .target(baseUri)
                .path("deprecated/deprecated-with-wrong-date")
                .request(MediaType.TEXT_PLAIN)
                .get()
                .getHeaderString(DEPRECATION);

        String expectedHeader = "@0"; // default value

        assertEquals(expectedHeader, deprecationHeader);
    }

    @Test
    public void deprecatedMethodWithProvidedLinkTest() {
        Response response = ClientBuilder.newClient()
                .target(baseUri)
                .path("deprecated/deprecated-with-link")
                .request(MediaType.TEXT_PLAIN)
                .get();

        final String since = response.getHeaderString(DEPRECATION);
        final String link = response.getHeaderString(LINK);

        String expectedSince = "@1682524801";
        String expectedLink = "<https://atlassian.com>;rel=\"deprecation\";type=\"text/html\"";

        assertEquals(expectedSince, since);
        assertEquals(expectedLink, link);
    }

    @Test
    public void deprecatedMethodWithProvidedJsonLinkTest() {
        Response response = ClientBuilder.newClient()
                .target(baseUri)
                .path("deprecated/deprecated-with-json-link")
                .request(MediaType.TEXT_PLAIN)
                .get();

        final String since = response.getHeaderString(DEPRECATION);
        final String link = response.getHeaderString(LINK);

        String expectedSince = "@1682524802";
        String expectedLink =
                "<https://atlassian.com/2024-01-01-deprecation.json>;rel=\"deprecation\";type=\"application/json\"";

        assertEquals(expectedSince, since);
        assertEquals(expectedLink, link);
    }

    @Test
    public void deprecatedMethodWithProvidedWrongLinkTest() {
        Response response = ClientBuilder.newClient()
                .target(baseUri)
                .path("deprecated/deprecated-with-wrong-link")
                .request(MediaType.TEXT_PLAIN)
                .get();

        final String since = response.getHeaderString(DEPRECATION);
        final String link = response.getHeaderString(LINK);

        String expectedSince = "@1682524803";

        assertEquals(expectedSince, since);
        assertNull(link);
    }

    @Test
    public void deprecatedMethodWithProvidedWrongLinkTypeTest() {
        Response response = ClientBuilder.newClient()
                .target(baseUri)
                .path("deprecated/deprecated-with-wrong-link-type")
                .request(MediaType.TEXT_PLAIN)
                .get();

        final String since = response.getHeaderString(DEPRECATION);
        final String link = response.getHeaderString(LINK);

        String expectedSince = "@1682524804";

        assertEquals(expectedSince, since);
        assertNull(link);
    }
}
