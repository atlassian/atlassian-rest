package it.com.atlassian.plugins.rest.sample.v2.marshalling.visibility;

import java.net.URI;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;

import org.junit.Test;

import com.atlassian.plugins.rest.sample.v2.marshalling.visibility.NoAnnotationFieldsOnlyPojo;
import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;

/**
 * The visibility test for Property Accessor.
 * The visibility setting can be found in {@code  com.atlassian.plugins.rest.v2.json.ObjectMapperFactory}.
 */
public class PropertyAccessorVisibilityTest {
    final URI baseUri =
            UriBuilder.create().path("rest").path("marshalling").path("1").build();
    final WebTarget target = ClientBuilder.newClient().target(baseUri).path("visibility");

    @Test
    public void testPropertyAccessorVisibilityOfJaxbPojo() {
        String pojo = target.path("xmlRootElementAnnotationOnly")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get(String.class);

        String expectedPojo =
                "{\"publicField\":\"public field\",\"publicBoolean\":true,\"privateFiled\":\"private field\","
                        + "\"privateBoolean\":true}";
        assertEquals(expectedPojo, pojo);
    }

    @Test
    public void testPropertyAccessorVisibilityOfJsonPojo() {
        String pojo = target.path("jsonAutoDetectAnnotationOnly")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get(String.class);

        String expectedPojo =
                "{\"publicField\":\"public field\",\"publicBoolean\":true,\"privateFiled\":\"private field\","
                        + "\"privateBoolean\":true}";
        assertEquals(expectedPojo, pojo);
    }

    @Test
    public void testPropertyAccessorVisibilityOfNoAnnotationPojo() {
        String pojo = target.path("noAnnotationPojo")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get(String.class);

        String expectedPojo =
                "{\"publicField\":\"public field\",\"publicBoolean\":true,\"privateFiled\":\"private field\","
                        + "\"privateBoolean\":true}";
        assertEquals(expectedPojo, pojo);
    }

    /**
     * {@link NoAnnotationFieldsOnlyPojo} has no public getters. As the result, private fields are not visible, only the
     * public fields are visible.
     */
    @Test
    public void testPropertyAccessorVisibilityOfNoAnnotationFieldsOnlyPojo() {
        String pojo = target.path("noAnnotationFieldsOnlyPojo")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get(String.class);

        String expectedPojo = "{\"publicField\":\"public field\",\"publicBoolean\":true}";
        assertEquals(expectedPojo, pojo);
    }
}
