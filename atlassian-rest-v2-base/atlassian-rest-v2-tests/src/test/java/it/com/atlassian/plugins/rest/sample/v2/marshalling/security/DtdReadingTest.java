package it.com.atlassian.plugins.rest.sample.v2.marshalling.security;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URI;
import java.util.concurrent.atomic.AtomicBoolean;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Response;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static jakarta.ws.rs.core.Response.Status.BAD_REQUEST;
import static jakarta.ws.rs.core.Response.Status.OK;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Tests for {@code com.atlassian.plugins.rest.v2.xml.XmlInputFactoryInjectionProvider}.
 * Client attempts to send XXE attack to access server resource created in {@link HttpAttemptDetector}. If the tests fail,
 * it means that the {@code HttpAttemptDetector} detected access from that client.
 */
public class DtdReadingTest {
    private final URI baseUri = UriBuilder.create()
            .path("rest")
            .path("marshalling")
            .path("1")
            .path("dtd")
            .build();
    private final URI baseUriList = UriBuilder.create()
            .path("rest")
            .path("marshalling")
            .path("1")
            .path("dtd")
            .path("list")
            .build();
    private HttpAttemptDetector detector;

    @Before
    public void setupDetector() throws IOException {
        detector = new HttpAttemptDetector();
        Thread serverThread = new Thread(detector);
        serverThread.start();
    }

    @After
    public void stopListenerThread() throws IOException {
        detector.shutdown();
    }

    @Test
    public void externalDtdIsNotRead() {
        String content =
                "<!DOCTYPE x SYSTEM '" + detector.getUrl() + "'><externalPojo><elem>test</elem></externalPojo>";
        Response response = postResource(content, false);
        assertEquals(
                BAD_REQUEST.getStatusCode(), response.getStatus()); // entity is not defined because DTD is not read
        assertFalse("An HTTP request should not be attempted", detector.wasAttempted());
    }

    @Test
    public void externalDtdIsNotReadWhenListParameter() {
        String content = "<!DOCTYPE x SYSTEM '" + detector.getUrl()
                + "'><elemList><externalPojo><elem>test1</elem></externalPojo><externalPojo><elem>test2</elem></externalPojo></elemList>";
        Response response = postResource(content, true);
        assertEquals(OK.getStatusCode(), response.getStatus());
        assertFalse("An HTTP request should not be attempted", detector.wasAttempted());
    }

    @Test
    public void externalGeneralEntityIsNotRead() {
        String content = "<!DOCTYPE x [ <!ENTITY xxe SYSTEM '" + detector.getUrl()
                + "'>]><externalPojo><elem>&e;</elem></externalPojo>";
        Response response = postResource(content, false);
        assertEquals(BAD_REQUEST.getStatusCode(), response.getStatus());
        assertFalse("An HTTP request should not be attempted", detector.wasAttempted());
    }

    @Test
    public void externalGeneralEntityIsNotReadWhenListParameter() {
        String content = "<!DOCTYPE x [<!ENTITY e SYSTEM '" + detector.getUrl()
                + "'>]><elemList><externalPojo><elem>&e;</elem></externalPojo></elemList>";
        Response response = postResource(content, true);
        assertEquals(BAD_REQUEST.getStatusCode(), response.getStatus());
        assertFalse("An HTTP request should not be attempted", detector.wasAttempted());
    }

    @Test
    public void externalParameterEntityIsNotRead() {
        String content = "<!DOCTYPE x [<!ENTITY % pe SYSTEM '" + detector.getUrl()
                + "'> %pe;]><externalPojo><elem>test</elem></externalPojo>";
        Response response = postResource(content, false);
        assertEquals(BAD_REQUEST.getStatusCode(), response.getStatus());
        assertFalse("An HTTP request should not be attempted", detector.wasAttempted());
    }

    @Test
    public void externalParameterEntityIsNotReadWhenListParameter() {
        String content = "<!DOCTYPE x [<!ENTITY % pe SYSTEM '" + detector.getUrl()
                + "'> %pe;]><elemList><externalPojo><elem>test</elem></externalPojo></elemList>";
        Response response = postResource(content, true);
        assertEquals(OK.getStatusCode(), response.getStatus());
        assertFalse("An HTTP request should not be attempted", detector.wasAttempted());
    }

    private Response postResource(String content, boolean withCollectionEntity) {
        final Client client = ClientBuilder.newClient();
        final WebTarget target = withCollectionEntity ? client.target(baseUriList) : client.target(baseUri);
        return target.request().post(Entity.xml(content));
    }

    /**
     * Listen on a socket, flag connection attempts.
     */
    public static class HttpAttemptDetector implements Runnable {
        private final ServerSocket serverSocket;
        private final AtomicBoolean someoneConnected = new AtomicBoolean(false);

        public HttpAttemptDetector() throws IOException {
            serverSocket = new ServerSocket(0);
        }

        public String getUrl() {
            return "http://localhost:" + serverSocket.getLocalPort();
        }

        public void run() {
            while (true) {
                try {
                    Socket socket = serverSocket.accept();
                    someoneConnected.set(true);
                    /* Pretend to be an HTTP server serving up an empty stream of bytes */
                    final InputStream socketInput = socket.getInputStream();
                    final BufferedReader reader = new BufferedReader(new InputStreamReader(socketInput));

                    String str;

                    while ((str = reader.readLine()) != null && !str.isEmpty())
                        ;

                    OutputStream socketOutput = socket.getOutputStream();
                    socketOutput.write("HTTP/1.0 200 OK\r\n\r\n".getBytes());
                    socketOutput.close();
                    socket.close();
                } catch (IOException e) {
                }
            }
        }

        public void shutdown() throws IOException {
            serverSocket.close();
        }

        public boolean wasAttempted() {
            return someoneConnected.get();
        }
    }
}
