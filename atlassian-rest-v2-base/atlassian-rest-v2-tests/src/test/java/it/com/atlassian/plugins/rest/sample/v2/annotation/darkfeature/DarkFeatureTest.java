package it.com.atlassian.plugins.rest.sample.v2.annotation.darkfeature;

import java.net.URI;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Response;

import org.junit.Test;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;

public class DarkFeatureTest {

    private final URI baseUri = UriBuilder.create()
            .path("rest")
            .path("annotations")
            .path("1")
            .path("darkfeature")
            .build();

    @Test
    public void testIfDarkFeatureResourceHasFeatureKeyEnabled() {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(baseUri).path("enabledDarkFeatureKey");
        Response response = target.request().get();
        assertEquals(200, response.getStatus());
        client.close();
    }

    @Test
    public void testIfDarkFeatureResourceHasFeatureKeyNotEnabled() {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(baseUri).path("notEnabledDarkFeatureKey");
        Response response = target.request().get();
        assertEquals(404, response.getStatus());
        client.close();
    }
}
