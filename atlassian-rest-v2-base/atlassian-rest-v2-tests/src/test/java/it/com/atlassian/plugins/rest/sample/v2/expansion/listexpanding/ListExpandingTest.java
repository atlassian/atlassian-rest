package it.com.atlassian.plugins.rest.sample.v2.expansion.listexpanding;

import java.net.URI;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.plugins.rest.sample.v2.expansion.listexpanding.entity.ListExpandingExampleEntity;
import com.atlassian.plugins.rest.v2.expand.resolver.ListEntityExpanderResolver;
import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Test for List expanding functionality that can be resolved by {@link  ListEntityExpanderResolver}.
 */
public class ListExpandingTest {
    private WebTarget webTarget;

    @Before
    public void setUp() {
        URI baseUrl =
                UriBuilder.create().path("rest").path("expansion").path("1").build();
        webTarget = ClientBuilder.newClient().target(baseUrl);
    }

    @Test
    public void listExpandingFieldGetsExpanded() {
        ListExpandingExampleEntity listExpandingExampleEntity = webTarget
                .path("listexpanding")
                .queryParam("expand", "customEntityList")
                .request()
                .get(ListExpandingExampleEntity.class);

        assertNotNull(listExpandingExampleEntity.getCustomEntityList());
        assertEquals(2, listExpandingExampleEntity.getCustomEntityList().size());
        assertNotNull(listExpandingExampleEntity.getCustomEntityList().stream()
                .findFirst()
                .get()
                .getExpandedField());
    }
}
