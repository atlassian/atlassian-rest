package it.com.atlassian.plugins.rest.sample.v2.security.antisniff;

import java.net.URI;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Response;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.junit.Test;
import com.google.common.collect.ImmutableList;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static jakarta.ws.rs.core.Response.Status.NOT_FOUND;
import static jakarta.ws.rs.core.Response.Status.OK;
import static org.junit.Assert.assertEquals;

public class AntiSniffingHeaderCheckTest {
    private final URI baseUri =
            UriBuilder.create().path("rest").path("security").path("1").build();
    private final WebTarget antisniff =
            ClientBuilder.newClient().target(baseUri).path("antisniff");

    @Test
    public void testAnonymousRestResponseContainsAntiSniffingHeader() {
        assertResponseContainsAntiSniffHeader(
                antisniff.path("unrestrictedAccess").request().get(Response.class));
    }

    @Test
    public void testAuthenticatedRestResponseContainsAntiSniffingHeader() {
        assertResponseContainsAntiSniffHeader(antisniff
                .register(HttpAuthenticationFeature.basic("admin", "admin"))
                .path("authenticatedAccess")
                .request()
                .get(Response.class));
    }

    @Test
    public void testErrorPageRestResponseContainsAntiSniffingHeader() {
        Response response = antisniff.path("somepaththatdoesntexist").request().get(Response.class);
        assertResponseContainsAntiSniffHeader(response);
        assertEquals(NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void testAntiSniffHeaderShouldNotBeAddedIfAlreadySet() {
        Response response = antisniff.path("nosniff-container").request().get(Response.class);
        assertEquals(OK.getStatusCode(), response.getStatus());
        assertResponseContainsAntiSniffHeader(response);
    }

    @Test
    public void testAntiSniffHeaderShouldNotBeAddedIfAlreadySetWithinServletResponse() {
        Response response = antisniff.path("nosniff-servlet").request().get(Response.class);
        assertEquals(OK.getStatusCode(), response.getStatus());
        assertResponseContainsAntiSniffHeader(response);
    }

    private void assertResponseContainsAntiSniffHeader(Response response) {
        assertEquals(ImmutableList.of("nosniff"), response.getHeaders().get("X-Content-Type-Options"));
    }
}
