package it.com.atlassian.plugins.rest.sample.v2.annotation.beanparam;

import java.net.URI;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;

import org.junit.Test;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;

public class BeanParamTest {

    @Test
    public void testIfQueryParamsWereConvertedToJavaObject() {
        final URI baseUri =
                UriBuilder.create().path("rest").path("annotations").path("1").build();
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(baseUri)
                .path("books")
                .queryParam("bookName", "test")
                .queryParam("author", "T.Tester");
        String book = target.request().get(String.class);
        assertEquals("BookInput{bookName='test', author='T.Tester'}", book);
        client.close();
    }
}
