package it.com.atlassian.plugins.rest.sample.v2.security.websudo;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;
import java.util.stream.Stream;
import jakarta.ws.rs.NotAuthorizedException;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Cookie;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static jakarta.ws.rs.core.Response.Status.FOUND;
import static jakarta.ws.rs.core.Response.Status.OK;
import static jakarta.ws.rs.core.Response.Status.UNAUTHORIZED;
import static org.junit.Assert.assertEquals;

import static com.atlassian.plugins.rest.sample.v2.security.websudo.WebSudoCheckResource.SUCCESS_MESSAGE;

/**
 * Test class for testing Web Sudo checks.
 */
public class WebSudoCheckTest {

    private static final String SET_COOKIE_HEADER = "Set-Cookie";
    private static final String JSESSIONID_PREFIX = "JSESSIONID=";
    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final String BASIC_AUTH_PREFIX = "Basic ";
    private static final String INVALID_CREDENTIALS = "invalid-username:invalid-password";
    private static final String VALID_CREDENTIALS = "admin:admin";
    private static final String WEB_SUDO_REQUIRED = "webSudoRequired";
    private static final String WEB_SUDO_NOT_REQUIRED = "webSudoNotRequired";

    private final URI baseUri =
            UriBuilder.create().path("rest").path("security").path("1").build();
    private final WebTarget webSudoCheck =
            ClientBuilder.newClient().target(baseUri).path("websudo");

    private final URI BASE_URL =
            UriBuilder.create().path("plugins").path("servlet").path("login").build();

    @Test
    public void testWebSudoNotRequired() {
        assertEquals(
                SUCCESS_MESSAGE,
                webSudoCheck
                        .path(WEB_SUDO_NOT_REQUIRED)
                        .request(MediaType.TEXT_PLAIN)
                        .get(String.class));
    }

    @Test
    public void testWebSudoRequired() {
        try {
            Response response = webSudoCheck
                    .path(WEB_SUDO_REQUIRED)
                    .request(MediaType.TEXT_PLAIN)
                    .get(Response.class);
            assertEquals(UNAUTHORIZED.getStatusCode(), response.getStatus());
        } catch (NotAuthorizedException e) {
            assertEquals("This resource requires WebSudo.", e.getResponse().getEntity());
        }
    }

    @Test
    public void testRequestWithMissingBasicAuth() {
        Response response = webSudoCheck
                .path(WEB_SUDO_REQUIRED)
                .request(MediaType.TEXT_PLAIN)
                .get(Response.class);
        assertEquals(UNAUTHORIZED.getStatusCode(), response.getStatus());
    }

    @Test
    public void testRequestWithInvalidBasicAuth() {
        Response response = webSudoCheck
                .path(WEB_SUDO_REQUIRED)
                .request(MediaType.TEXT_PLAIN)
                .header(
                        AUTHORIZATION_HEADER,
                        BASIC_AUTH_PREFIX
                                + java.util.Base64.getEncoder().encodeToString(INVALID_CREDENTIALS.getBytes()))
                .get(Response.class);
        assertEquals(UNAUTHORIZED.getStatusCode(), response.getStatus());
    }

    @Test
    public void testRequestWithValidBasicAuth() {
        Response response = webSudoCheck
                .path(WEB_SUDO_REQUIRED)
                .request(MediaType.TEXT_PLAIN)
                .header(
                        AUTHORIZATION_HEADER,
                        BASIC_AUTH_PREFIX + java.util.Base64.getEncoder().encodeToString(VALID_CREDENTIALS.getBytes()))
                .get(Response.class);
        assertEquals(OK.getStatusCode(), response.getStatus());
    }

    @Test
    public void testWebSudoWithInvalidBasicAuthAndValidSessionCookie() throws IOException, URISyntaxException {

        // Step 1: Log in to the instance to get a session cookie -> Use sessionId safely
        String sessionId =
                getSessionIdFromLogin().orElseThrow(() -> new IllegalStateException("Session ID should not be null"));

        // Step 2: Make a websudo request with invalid basic auth and the valid session cookie
        // Update with actual endpoint
        Response response = webSudoCheck
                .path(WEB_SUDO_REQUIRED)
                .request(MediaType.TEXT_PLAIN)
                .header(
                        AUTHORIZATION_HEADER,
                        BASIC_AUTH_PREFIX
                                + java.util.Base64.getEncoder().encodeToString(INVALID_CREDENTIALS.getBytes()))
                .cookie(new Cookie("JSESSIONID", sessionId))
                .get();
        // Assert that the response status is 401 Unauthorized
        assertEquals(UNAUTHORIZED.getStatusCode(), response.getStatus());
        response.close();
    }

    private HttpPost createLoginRequest(URI uri) {
        HttpPost post = new HttpPost(uri);
        StringEntity formEntity =
                new StringEntity("os_username=admin&os_password=admin", ContentType.APPLICATION_FORM_URLENCODED);
        post.setEntity(formEntity);
        return post;
    }

    private Optional<String> getSessionIdFromLogin() throws IOException, URISyntaxException {
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            URI loginUri = removeTrailingSlash(BASE_URL);
            HttpPost loginRequest = createLoginRequest(loginUri);
            HttpResponse loginResponse = client.execute(loginRequest);

            // Validate response status is 302
            int statusCode = loginResponse.getStatusLine().getStatusCode();
            assertEquals(FOUND.getStatusCode(), statusCode);

            String sessionId = extractSessionId(loginResponse);
            EntityUtils.consume(loginResponse.getEntity());
            return Optional.ofNullable(sessionId);
        }
    }

    private String extractSessionId(HttpResponse response) {
        return Stream.of(response.getHeaders(SET_COOKIE_HEADER))
                .map(header -> header.getValue().split(";")[0])
                .filter(cookie -> cookie.startsWith(JSESSIONID_PREFIX))
                .map(cookie -> cookie.substring(JSESSIONID_PREFIX.length()))
                .findFirst()
                .orElse(null);
    }

    public static URI removeTrailingSlash(URI uri) throws URISyntaxException {
        String uriString = uri.toString();
        // Check if the URI ends with a slash and remove it if present
        if (uriString.endsWith("/")) {
            uriString = uriString.substring(0, uriString.length() - 1);
        }
        // Return a new URI instance from the modified string
        return new URI(uriString);
    }
}
