package it.com.atlassian.plugins.rest.sample.v2.annotation.suspended;

import java.net.URI;
import jakarta.ws.rs.InternalServerErrorException;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;

import org.junit.Test;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertThrows;

public class SuspendedTest {

    private final URI baseUri =
            UriBuilder.create().path("rest").path("annotations").path("1").build();

    @Test
    public void testIfRequestTargetReturnsException() {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(baseUri).path("async");
        assertThrows(InternalServerErrorException.class, () -> target.request().get(String.class));
        client.close();
    }
}
