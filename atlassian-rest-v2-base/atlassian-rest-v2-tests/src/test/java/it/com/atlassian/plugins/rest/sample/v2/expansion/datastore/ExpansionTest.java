package it.com.atlassian.plugins.rest.sample.v2.expansion.datastore;

import java.net.URI;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.plugins.rest.sample.v2.expansion.datastore.entity.Game;
import com.atlassian.plugins.rest.sample.v2.expansion.datastore.entity.Player;
import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class ExpansionTest {
    private static final int POINTS_SCORED = 513;
    private WebTarget webTarget;

    @Before
    public void setUp() {
        URI baseUrl =
                UriBuilder.create().path("rest").path("expansion").path("1").build();
        webTarget = ClientBuilder.newClient().target(baseUrl);
    }

    @Test
    public void testGetGameWithNonExpandedPlayers() {
        final Game game = webTarget.path("game").request().get(Game.class);
        assertEquals("Rugby", game.getName());
        assertNull(game.getPlayers().getPlayers());
        assertEquals(5, game.getPlayers().getMaxResults());
        assertEquals(10, game.getPlayers().getSize());
        assertNull(game.getPlayers().getStartIndex());
    }

    @Test
    public void testGetGameWithExpandedPlayers() {
        final Game game =
                webTarget.path("game").queryParam("expand", "players").request().get(Game.class);
        assertEquals("Rugby", game.getName());
        assertEquals(5, game.getPlayers().getPlayers().size());
        assertEquals(5, game.getPlayers().getMaxResults());
        assertEquals(10, game.getPlayers().getSize());
        assertEquals(0, game.getPlayers().getStartIndex().intValue());

        assertEquals(0, game.getPlayers().getPlayers().get(0).getId());
        assertEquals(4, game.getPlayers().getPlayers().get(4).getId());
    }

    @Test
    public void testGetGameWithExpandedPlayersAndOffset() {
        final Game game = webTarget
                .path("game")
                .queryParam("expand", "players[3:]")
                .request()
                .get(Game.class);
        assertEquals("Rugby", game.getName());
        assertEquals(5, game.getPlayers().getPlayers().size());
        assertEquals(5, game.getPlayers().getMaxResults());
        assertEquals(10, game.getPlayers().getSize());
        assertEquals(3, game.getPlayers().getStartIndex().intValue());

        assertEquals(3, game.getPlayers().getPlayers().get(0).getId());
        assertEquals(7, game.getPlayers().getPlayers().get(4).getId());
    }

    @Test
    public void testGetGameWithExpandedPlayersAndOffsetOutOfBounds() {
        final Game game = webTarget
                .path("game")
                .queryParam("expand", "players[12:]")
                .request()
                .get(Game.class);
        assertEquals("Rugby", game.getName());
        assertNull(game.getPlayers().getPlayers());
        assertEquals(5, game.getPlayers().getMaxResults());
        assertEquals(10, game.getPlayers().getSize());
        assertNull(game.getPlayers().getStartIndex());
    }

    @Test
    public void testGetPlayerNames() {
        final Player player1 = webTarget.path("player").path("0").request().get(Player.class);
        final Player player2 = webTarget.path("player").path("1").request().get(Player.class);
        assertEquals("Adam Ashley-Cooper", player1.getFullName());
        assertEquals("Matt Giteau", player2.getFullName());
    }

    @Test
    public void testGetPlayerWithExpandedMultipleSubItemsSamePrefix() {
        final Player player = webTarget
                .path("player")
                .path("2")
                .queryParam("expand", "record.subRecord1,record.subRecord2")
                .request()
                .get(Player.class);
        assertNotNull(player.getRecord().getSubRecord1());
        assertNotNull(player.getRecord().getSubRecord2());

        assertEquals(
                Integer.valueOf(POINTS_SCORED / 2),
                player.getRecord().getSubRecord1().getPointsScored());
        assertEquals(
                Integer.valueOf(POINTS_SCORED / 2),
                player.getRecord().getSubRecord2().getPointsScored());
    }

    @Test
    public void testPlayerWithRecordCanBeExpanded() {
        // get without expansion
        Player player = webTarget.path("player").path("2").request().get(Player.class);
        assertNotNull(player.getRecord());
        // now expand it
        player = webTarget
                .path("player")
                .path("2")
                .queryParam("expand", "record")
                .request()
                .get(Player.class);
        assertNotNull(player.getRecord());
        assertEquals((Integer) POINTS_SCORED, player.getRecord().getPointsScored());
    }

    @Test
    public void testPlayerWithNoRecordCanNotBeExpanded() {
        // get without expansion
        Player player = webTarget.path("player").path("1").request().get(Player.class);
        assertNull(player.getRecord());
        // now expand it
        player = webTarget
                .path("player")
                .path("1")
                .queryParam("expand", "record")
                .request()
                .get(Player.class);
        assertNull(player.getRecord());
    }
}
