package it.com.atlassian.plugins.rest.sample.v2.annotation.namebinding;

import java.net.URI;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Response;

import org.junit.Test;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;

public class NameBindingTest {

    private final URI baseUri =
            UriBuilder.create().path("rest").path("annotations").path("1").build();

    @Test
    public void testIfFilteredResourceHasNameBoundFilter() {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(baseUri).path("filtered");
        Response response = target.request().get();
        assertEquals("Yes", response.getHeaders().get("Filter-Applied").get(0));
        client.close();
    }

    @Test
    public void testIfNonFilteredResourceHasNoFilter() {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(baseUri).path("non-filtered");
        Response response = target.request().get();
        assertEquals("No", response.getHeaders().get("Filter-Applied").get(0));
        client.close();
    }
}
