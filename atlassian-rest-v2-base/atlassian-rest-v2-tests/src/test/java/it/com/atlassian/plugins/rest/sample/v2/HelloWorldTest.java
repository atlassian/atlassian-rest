package it.com.atlassian.plugins.rest.sample.v2;

import java.net.URI;
import java.util.Locale;
import jakarta.ws.rs.client.ClientBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.junit.Test;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;

public class HelloWorldTest {

    private static final URI BASE_URI =
            UriBuilder.create().path("rest").path("hw").path("2").build();

    @Test
    public void test() {
        String result = ClientBuilder.newClient()
                .target(BASE_URI)
                .path("hello")
                .request()
                .get(String.class);

        assertEquals("HELLO", result);
    }

    @Test
    public void testAdminUser() {
        ClientConfig config = new ClientConfig(HttpAuthenticationFeature.basic("admin", "admin"));
        String result = ClientBuilder.newClient(config)
                .target(BASE_URI)
                .path("hello")
                .path("michell")
                .request()
                .acceptLanguage(Locale.UK)
                .get(String.class);

        assertEquals("Hello Michell Unlicensed, are you from GB?", result);
    }
}
