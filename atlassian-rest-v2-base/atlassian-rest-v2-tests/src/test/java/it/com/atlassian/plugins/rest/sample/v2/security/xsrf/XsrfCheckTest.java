package it.com.atlassian.plugins.rest.sample.v2.security.xsrf;

import java.net.URI;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Cookie;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.junit.Test;

import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.plugins.rest.sample.v2.security.xsrf.model.JaxbPojo;
import com.atlassian.plugins.rest.v2.security.xsrf.XsrfResourceFilter;
import com.atlassian.plugins.rest.v2.utils.UriBuilder;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenAccessor;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenValidator;

import static org.junit.Assert.assertEquals;

import static com.atlassian.plugins.rest.sample.v2.security.xsrf.XsrfCheckResource.SUCCESS_MESSAGE;
import static com.atlassian.plugins.rest.sample.v2.security.xsrf.XsrfCheckResource.XSRF_PROTECTION_EXCLUDED_RESOURCE;

/**
 * Test class for testing XSRF checks.
 * It tests successful and unsuccessful scenarios for GET, POST, and PUT requests.
 */
public class XsrfCheckTest {
    public static final String CONTENT_TYPE = "Content-Type";
    private final URI baseUri =
            UriBuilder.create().path("rest").path("security").path("1").build();
    private final WebTarget xsrfCheck =
            ClientBuilder.newClient().target(baseUri).path("xsrfCheck");

    /**
     * GET request will not have XSRF check.
     */
    @Test
    public void testGetRequestSuccessWithNoXSRFCheck() {
        assertEquals(SUCCESS_MESSAGE, xsrfCheck.request(MediaType.TEXT_PLAIN).get(String.class));
    }

    /**
     * HEAD request will not have XSRF check.
     */
    @Test
    public void testHeadRequestSuccessWithNoXSRFCheck() {
        assertEquals(200, xsrfCheck.request(MediaType.TEXT_PLAIN).head().getStatus());
    }

    /**
     * OPTIONS request will not have XSRF check.
     */
    @Test
    public void testOptionsRequestSuccessWithNoXSRFCheck() {
        assertEquals(200, xsrfCheck.request(MediaType.TEXT_PLAIN).options().getStatus());
    }

    /**
     * POST request with Content-Type:application/x-www-form-urlencoded will have XSRF check.
     */
    @Test
    public void testContentTypeApplicationFormUrlencodedPostRequestWithXSRFCheckAndBlocked() {
        Response response = xsrfCheck
                .request()
                .header(CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED)
                .post(Entity.form(new Form()));
        assertEquals(403, response.getStatus());
        assertEquals("XSRF check failed", response.readEntity(String.class));
    }

    /**
     * POST request with Content-Type:multipart/form-data will have XSRF check.
     */
    @Test
    public void testContentTypeMultipartFormDataPostRequestWithXSRFCheckAndBlocked() {
        Response response = xsrfCheck
                .request()
                .header(CONTENT_TYPE, MediaType.MULTIPART_FORM_DATA)
                .post(null);
        assertEquals(403, response.getStatus());
        assertEquals("XSRF check failed", response.readEntity(String.class));
    }

    /**
     * POST request with Content-Type:text/plain will have XSRF check.
     */
    @Test
    public void testContentTypeTextPlainPostRequestWithXSRFCheckAndBlocked() {
        Response response =
                xsrfCheck.request().header(CONTENT_TYPE, MediaType.TEXT_PLAIN).post(Entity.text(""));
        assertEquals(403, response.getStatus());
        assertEquals("XSRF check failed", response.readEntity(String.class));
    }

    /**
     * POST request with header X-Atlassian-Token:no-check will have XSRF check and success.
     */
    @Test
    public void testPostRequestWithXSRFCheckAndSuccess() {
        String entity = xsrfCheck
                .request()
                .header(XsrfResourceFilter.TOKEN_HEADER, XsrfResourceFilter.NO_CHECK)
                .post(Entity.form(new Form()))
                .readEntity(String.class);
        assertEquals(SUCCESS_MESSAGE, entity);
    }

    /**
     * POST request with XML entity will have XSRF check and success.
     */
    @Test
    public void testPostXmlEntitySuccess() {
        String entity = xsrfCheck.request().post(Entity.xml(new JaxbPojo())).readEntity(String.class);
        assertEquals(SUCCESS_MESSAGE, entity);
    }

    /**
     * POST request with JSON entity will not have XSRF check and success.
     */
    @Test
    public void testPostJsonEntitySuccess() {
        String entity = xsrfCheck.request().post(Entity.json(new JaxbPojo())).readEntity(String.class);
        assertEquals(SUCCESS_MESSAGE, entity);
    }

    /**
     * POST request with Content-Type:null will have XSRF check.
     */
    @Test
    public void testNullContentTypePostRequestSuccess() {
        Response response = xsrfCheck.request().post(null);
        assertEquals(200, response.getStatus());
    }

    /**
     * POST request with valid atl.xsrf.token token will have XSRF check and success.
     */
    @Test
    public void testPostWithValidXsrfTokenSuccess() {
        final String xsrfToken = "abc123";
        Cookie xsrfCookie = new Cookie(IndependentXsrfTokenAccessor.XSRF_COOKIE_KEY, xsrfToken);
        String entity = xsrfCheck
                .queryParam(IndependentXsrfTokenValidator.XSRF_PARAM_NAME, xsrfToken)
                .request()
                .cookie(xsrfCookie)
                .post(Entity.form(new Form()))
                .readEntity(String.class);
        assertEquals(SUCCESS_MESSAGE, entity);
    }

    /**
     * POST request with invalid atl.xsrf.token token will have XSRF check and blocked.
     */
    @Test
    public void testPostWithInvalidXsrfTokenBlocked() {
        final String xsrfToken = "abc123";
        Cookie xsrfCookie = new Cookie(IndependentXsrfTokenAccessor.XSRF_COOKIE_KEY, xsrfToken);
        Response response = xsrfCheck
                .queryParam(IndependentXsrfTokenValidator.XSRF_PARAM_NAME, "INCORRECT")
                .request()
                .cookie(xsrfCookie)
                .post(Entity.form(new Form()));
        assertEquals(403, response.getStatus());
        assertEquals("XSRF check failed", response.readEntity(String.class));
    }

    /**
     * PUT request with Content-Type:application/x-www-form-urlencoded will have XSRF check not enforced and success.
     */
    @Test
    public void testContentTypeApplicationFormUrlencodedPutRequestWithXSRFCheckNotEnforcedAndSuccess() {
        Response response = xsrfCheck
                .request()
                .header(CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED)
                .put(Entity.form(new Form()));
        assertEquals(200, response.getStatus());
    }

    /**
     * PUT request with Content-Type:multipart/form-data will have XSRF check not enforced and success.
     */
    @Test
    public void testContentTypeMultipartFormDataPutRequestWithXSRFCheckNotEnforcedAndSuccess() {
        Response response = xsrfCheck
                .request()
                .header(CONTENT_TYPE, MediaType.MULTIPART_FORM_DATA)
                .put(Entity.form(new Form()));
        assertEquals(200, response.getStatus());
    }

    /**
     * PUT request with Content-Type:text/plain will have XSRF check not enforced and success.
     */
    @Test
    public void testContentTypeTextPlainPutRequestWithXSRFCheckNotEnforcedAndSuccess() {
        Response response =
                xsrfCheck.request().header(CONTENT_TYPE, MediaType.TEXT_PLAIN).put(Entity.text(""));
        assertEquals(200, response.getStatus());
    }

    /**
     * PUT request with empty Content-Type will have XSRF check not enforced and success.
     */
    @Test
    public void testEmptyContentTypePostRequestWithXSRFCheckNotEnforcedAndSuccess() {
        Response response = xsrfCheck.request().header(CONTENT_TYPE, "").put(Entity.form(new Form()));
        assertEquals(200, response.getStatus());
    }

    /**
     * PUT request with header X-Atlassian-Token:no-check will have XSRF check and success.
     */
    @Test
    public void testPutRequestWithXSRFCheckAndSuccess() {
        String entity = xsrfCheck
                .request()
                .header(XsrfResourceFilter.TOKEN_HEADER, XsrfResourceFilter.NO_CHECK)
                .put(Entity.form(new Form()))
                .readEntity(String.class);
        assertEquals(SUCCESS_MESSAGE, entity);
    }

    /**
     * PUT request with XML entity will have XSRF check and success.
     */
    @Test
    public void testPutXmlEntitySuccess() {
        String entity = xsrfCheck.request().put(Entity.xml(new JaxbPojo())).readEntity(String.class);
        assertEquals(SUCCESS_MESSAGE, entity);
    }

    /**
     * PUT request with JSON entity will have XSRF check and success.
     */
    @Test
    public void testPutJsonEntitySuccess() {
        String entity = xsrfCheck.request().put(Entity.json(new JaxbPojo())).readEntity(String.class);
        assertEquals(SUCCESS_MESSAGE, entity);
    }

    /**
     * PUT request with valid atl.xsrf.token token will have XSRF check and success.
     */
    @Test
    public void testPutWithValidXsrfTokenSuccess() {
        final String xsrfToken = "abc123";
        Cookie xsrfCookie = new Cookie(IndependentXsrfTokenAccessor.XSRF_COOKIE_KEY, xsrfToken);
        String entity = xsrfCheck
                .queryParam(IndependentXsrfTokenValidator.XSRF_PARAM_NAME, xsrfToken)
                .request()
                .cookie(xsrfCookie)
                .put(Entity.form(new Form()))
                .readEntity(String.class);
        assertEquals(SUCCESS_MESSAGE, entity);
    }

    /**
     * PUT request with invalid atl.xsrf.token token will have XSRF check not enforced and success.
     */
    @Test
    public void testPutWithInvalidXsrfTokenWithXSRFCheckNotEnforcedAndSuccess() {
        final String xsrfToken = "abc123";
        Cookie xsrfCookie = new Cookie(IndependentXsrfTokenAccessor.XSRF_COOKIE_KEY, xsrfToken);
        Response response = xsrfCheck
                .queryParam(IndependentXsrfTokenValidator.XSRF_PARAM_NAME, "INCORRECT")
                .request()
                .cookie(xsrfCookie)
                .put(Entity.form(new Form()));
        assertEquals(200, response.getStatus());
    }

    /**
     * DELETE request with Content-Type:application/x-www-form-urlencoded will have XSRF check not enforced and success.
     */
    @Test
    public void testContentTypeApplicationFormUrlencodedDeleteRequestWithXSRFCheckNotEnforcedAndSuccess() {
        Response response = xsrfCheck
                .request()
                .header(CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED)
                .delete();
        assertEquals(200, response.getStatus());
    }

    /**
     * DELETE request with Content-Type:multipart/form-data will have XSRF check not enforced and success.
     */
    @Test
    public void testContentTypeMultipartFormDataDeleteRequestWithXSRFCheckNotEnforcedAndSuccess() {
        Response response = xsrfCheck
                .request()
                .header(CONTENT_TYPE, MediaType.MULTIPART_FORM_DATA)
                .delete();
        assertEquals(200, response.getStatus());
    }

    /**
     * DELETE request with Content-Type:text/plain will have XSRF check not enforced and success.
     */
    @Test
    public void testContentTypeTextPlainDeleteRequestWithXSRFCheckNotEnforcedAndSuccess() {
        Response response =
                xsrfCheck.request().header(CONTENT_TYPE, MediaType.TEXT_PLAIN).delete();
        assertEquals(200, response.getStatus());
    }

    /**
     * DELETE request with null Content-Type will have XSRF check not enforced and success.
     */
    @Test
    public void testEmptyContentTypeDeleteRequestWithXSRFCheckNotEnforcedAndSuccess() {
        Response response = xsrfCheck.request().delete();
        assertEquals(200, response.getStatus());
    }

    /**
     * DELETE request with header X-Atlassian-Token:no-check will have XSRF check and success.
     */
    @Test
    public void testDeleteRequestWithXSRFCheckAndSuccess() {
        String entity = xsrfCheck
                .request()
                .header(XsrfResourceFilter.TOKEN_HEADER, XsrfResourceFilter.NO_CHECK)
                .delete()
                .readEntity(String.class);
        assertEquals(SUCCESS_MESSAGE, entity);
    }

    /**
     * DELETE request with valid atl.xsrf.token token will have XSRF check and success.
     */
    @Test
    public void testDeleteWithValidXsrfTokenSuccess() {
        final String xsrfToken = "abc123";
        Cookie xsrfCookie = new Cookie(IndependentXsrfTokenAccessor.XSRF_COOKIE_KEY, xsrfToken);
        String entity = xsrfCheck
                .queryParam(IndependentXsrfTokenValidator.XSRF_PARAM_NAME, xsrfToken)
                .request()
                .cookie(xsrfCookie)
                .delete()
                .readEntity(String.class);
        assertEquals(SUCCESS_MESSAGE, entity);
    }

    /**
     * DELETE request with invalid atl.xsrf.token token will have XSRF check not enforced and success.
     */
    @Test
    public void testDeleteWithInvalidXsrfTokenWithXSRFCheckNotEnforcedAndSuccess() {
        final String xsrfToken = "abc123";
        Cookie xsrfCookie = new Cookie(IndependentXsrfTokenAccessor.XSRF_COOKIE_KEY, xsrfToken);
        Response response = xsrfCheck
                .queryParam(IndependentXsrfTokenValidator.XSRF_PARAM_NAME, "INCORRECT")
                .request()
                .cookie(xsrfCookie)
                .delete();
        assertEquals(200, response.getStatus());
    }

    /**
     * GET request to {@link XsrfProtectionExcluded} request with will not have XSRF check.
     */
    @Test
    public void testGetToXsrfProtectionExcludedAnnotatedResourceWithNoXSRFCheck() {
        String entity = xsrfCheck
                .path(XSRF_PROTECTION_EXCLUDED_RESOURCE + "/get")
                .request()
                .get()
                .readEntity(String.class);
        assertEquals(SUCCESS_MESSAGE, entity);
    }

    /**
     * OPTIONS request to {@link XsrfProtectionExcluded} request with will not have XSRF check.
     */
    @Test
    public void testOptionsToXsrfProtectionExcludedAnnotatedResourceWithNoXSRFCheck() {
        String entity = xsrfCheck
                .path(XSRF_PROTECTION_EXCLUDED_RESOURCE + "/options")
                .request()
                .options()
                .readEntity(String.class);
        assertEquals(SUCCESS_MESSAGE, entity);
    }

    /**
     * POST request to {@link XsrfProtectionExcluded} request with will not have XSRF check.
     */
    @Test
    public void testPostToXsrfProtectionExcludedAnnotatedResourceWithNoXSRFCheck() {
        String entity = xsrfCheck
                .path(XSRF_PROTECTION_EXCLUDED_RESOURCE + "/post")
                .request()
                .post(Entity.form(new Form()))
                .readEntity(String.class);
        assertEquals(SUCCESS_MESSAGE, entity);
    }

    /**
     * PUT request to {@link XsrfProtectionExcluded} request with will not have XSRF check.
     */
    @Test
    public void testPutToXsrfProtectionExcludedAnnotatedResourceWithNoXSRFCheck() {
        String entity = xsrfCheck
                .path(XSRF_PROTECTION_EXCLUDED_RESOURCE + "/put")
                .request()
                .put(Entity.form(new Form()))
                .readEntity(String.class);
        assertEquals(SUCCESS_MESSAGE, entity);
    }

    /**
     * DELETE request to {@link XsrfProtectionExcluded} request with will not have XSRF check.
     */
    @Test
    public void testDeleteToXsrfProtectionExcludedAnnotatedResourceWithNoXSRFCheck() {
        String entity = xsrfCheck
                .path(XSRF_PROTECTION_EXCLUDED_RESOURCE + "/delete")
                .request()
                .delete()
                .readEntity(String.class);
        assertEquals(SUCCESS_MESSAGE, entity);
    }
}
