package it.com.atlassian.plugins.rest.sample.scanner;

import java.net.URI;
import jakarta.ws.rs.client.ClientBuilder;

import org.junit.Test;

import com.atlassian.plugins.rest.sample.scanner.BaseClass;
import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;

/**
 * Testing {@link BaseClass}
 */
public class JarScannerTest {

    private static final URI BASE_URI =
            UriBuilder.create().path("rest").path("sc").path("1").build();

    @Test
    public void testGetBaseClassExists() {
        String result = ClientBuilder.newClient()
                .target(BASE_URI)
                .path("base")
                .request()
                .get(String.class);

        assertEquals("This is a base class", result);
    }

    @Test
    public void testGetBundledClassExists() {
        String result = ClientBuilder.newClient()
                .target(BASE_URI)
                .path("bundle")
                .request()
                .get(String.class);

        assertEquals("This is a bundled class", result);
    }
}
