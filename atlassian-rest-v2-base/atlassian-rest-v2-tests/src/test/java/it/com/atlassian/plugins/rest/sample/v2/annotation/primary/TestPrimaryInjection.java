package it.com.atlassian.plugins.rest.sample.v2.annotation.primary;

import java.net.URI;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;

import org.junit.Test;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;

public class TestPrimaryInjection {

    @Test
    public void primaryFruitResourceShouldUsePrimaryBean() {
        final URI baseUri = UriBuilder.create()
                .path("rest")
                .path("annotations")
                .path("1")
                .path("primary-fruit")
                .build();
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(baseUri);
        assertEquals("Apple", target.request().get(String.class));
        client.close();
    }
}
