package it.com.atlassian.plugins.rest.sample.v2.interceptor.namebinding.resource;

import java.net.URI;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.MediaType;

import org.junit.Test;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertArrayEquals;

/**
 * This is an example of a resource with interceptors.
 * It demonstrates Name binding, a concept that allows to say to a JAX-RS runtime that a specific filter or
 * interceptor will be executed only for a specific resource method.
 */
public class InterceptorNameBindingExampleResourceTest {
    @Test
    public void interceptorNameBindingResourceTest() {
        final String originalEntity = "Original entity";

        final URI baseUri =
                UriBuilder.create().path("rest").path("interceptor").path("1").build();
        final String[] responseEntity = ClientBuilder.newClient()
                .target(baseUri)
                .path("interceptorNameBinding")
                .request(MediaType.TEXT_PLAIN)
                .post(Entity.text(originalEntity))
                .readEntity(String.class)
                .split("\n");

        final String[] expectedEntity = {
            "Original entity",
            "This is added text from ReaderInterceptorNameBinding2 with Priority = 7000",
            "This is added text from ReaderInterceptorNameBinding1 with Priority = 8000",
            "This is added text from ReaderAndWriterInterceptorNameBinding with Priority = 9000",
            "Entity has been processed in the resource method",
            "This is added text from WriterInterceptorNameBinding1 with Priority = 6000",
            "This is added text from WriterInterceptorNameBinding2 with Priority = 7000",
            "This is added text from ReaderAndWriterInterceptorNameBinding with Priority = 9000"
        };

        assertArrayEquals(expectedEntity, responseEntity);
    }
}
