package it.com.atlassian.plugins.rest.sample.v2.exception;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.core.Response;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SecurityTest {

    private static final String plainLicensedUser = "barney";
    private static final String plainUnlicensedUser = "michell";
    private static final String adminUser = "betty";
    private static final String systemAdminUser = "fred";
    private static final String unknownUser = "unknown";

    @Parameterized.Parameters(name = "{index}: should return {2} when user {1} tries to access {0} resource")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][] {
            {"empty-access", null, 401},
            {"empty-access", plainLicensedUser, 200},
            {"empty-access", plainUnlicensedUser, 403},
            {"empty-access", adminUser, 200},
            {"empty-access", systemAdminUser, 200},
            {"empty-access", unknownUser, 401},
            {"unrestricted-access", null, 200},
            {"unrestricted-access", plainLicensedUser, 200},
            {"unrestricted-access", plainUnlicensedUser, 200},
            {"unrestricted-access", adminUser, 200},
            {"unrestricted-access", systemAdminUser, 200},
            {"unrestricted-access", unknownUser, 401},
            {"anonymous-site-access", null, 401},
            {"anonymous-site-access", plainLicensedUser, 200},
            {"anonymous-site-access", plainUnlicensedUser, 403},
            {"anonymous-site-access", adminUser, 200},
            {"anonymous-site-access", systemAdminUser, 200},
            {"anonymous-site-access", unknownUser, 401},
            {"unlicensed-site-access", null, 401},
            {"unlicensed-site-access", plainLicensedUser, 200},
            {"unlicensed-site-access", plainUnlicensedUser, 403},
            {"unlicensed-site-access", adminUser, 200},
            {"unlicensed-site-access", systemAdminUser, 200},
            {"unlicensed-site-access", unknownUser, 401},
            {"licensed-only", null, 401},
            {"licensed-only", plainLicensedUser, 200},
            {"licensed-only", plainUnlicensedUser, 403},
            {"licensed-only", adminUser, 200},
            {"licensed-only", systemAdminUser, 200},
            {"licensed-only", unknownUser, 401},
            {"admin-only", null, 401},
            {"admin-only", plainLicensedUser, 403},
            {"admin-only", plainUnlicensedUser, 403},
            {"admin-only", adminUser, 200},
            {"admin-only", systemAdminUser, 200},
            {"admin-only", unknownUser, 401},
            {"system-admin-only", null, 401},
            {"system-admin-only", plainLicensedUser, 403},
            {"system-admin-only", plainUnlicensedUser, 403},
            {"system-admin-only", adminUser, 403},
            {"system-admin-only", systemAdminUser, 200},
            {"system-admin-only", unknownUser, 401},
        });
    }

    private final String user;
    private final String resource;
    private final int responseStatusCode;

    public SecurityTest(String resource, String user, int responseStatusCode) {
        this.user = user;
        this.resource = resource;
        this.responseStatusCode = responseStatusCode;
    }

    @Test
    public void securityTest() {
        final URI baseUri = UriBuilder.create()
                .path("rest")
                .path("exception")
                .path("1")
                .path("secured")
                .path(resource)
                .build();

        final Response response = request(baseUri).get(Response.class);

        assertEquals(responseStatusCode, response.getStatus());
    }

    private Invocation.Builder request(URI baseUri) {
        Invocation.Builder builder = ClientBuilder.newClient().target(baseUri).request(APPLICATION_JSON);
        if (user == null) {
            return builder;
        }

        return builder.header("Authorization", basicAuth());
    }

    private String basicAuth() {
        return "Basic " + Base64.getEncoder().encodeToString((user + ":" + user).getBytes(StandardCharsets.UTF_8));
    }
}
