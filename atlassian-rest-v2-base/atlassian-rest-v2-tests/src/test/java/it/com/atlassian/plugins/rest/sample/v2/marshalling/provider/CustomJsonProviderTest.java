package it.com.atlassian.plugins.rest.sample.v2.marshalling.provider;

import java.net.URI;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;

import org.junit.Test;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static jakarta.ws.rs.core.Response.Status.BAD_REQUEST;
import static jakarta.ws.rs.core.Response.Status.OK;
import static org.junit.Assert.assertEquals;

/**
 * REST configures custom JSON provider {@code com.atlassian.plugins.rest.v2.jersey.RestJacksonJaxbJsonProvider} and
 * {@code com.atlassian.plugins.rest.v2.json.ObjectMapperFactory}, that changes the default behaviour.
 */
public class CustomJsonProviderTest {
    final URI baseUri =
            UriBuilder.create().path("rest").path("marshalling").path("1").build();
    final Client client = ClientBuilder.newClient();
    final WebTarget target = client.target(baseUri).path("provider");

    @Test
    public void testIfJaxbTransientPropertyIsNotVisible() {
        assertEquals(
                "{\"xmlAttributeAnnotatedField\":\"visible\"}", target.request().get(String.class));
        client.close();
    }

    @Test
    public void testIfInclusionOfNullablePropertiesIsDisabled() {
        assertEquals("{}", target.path("nullable").request().get(String.class));
        client.close();
    }

    @Test
    public void testIfAdditionalJacksonMoulesAreRegisteredProperly() {
        assertEquals(
                "{\"message\":\"Hello!\",\"date\":[1970,1,1]}",
                target.path("date").request().get(String.class));
        client.close();
    }

    @Test
    public void testIfProperlyMarshalled() {
        assertEquals(
                OK.getStatusCode(),
                target.path("marshall")
                        .request()
                        .put(Entity.json("{\"xmlAttributeAnnotatedField\": \"HELLO\"}"))
                        .getStatus());
    }

    @Test
    public void testEmptyEntityReturns400() {
        assertEquals(
                BAD_REQUEST.getStatusCode(),
                target.path("marshall").request().put(Entity.json("")).getStatus());
    }
}
