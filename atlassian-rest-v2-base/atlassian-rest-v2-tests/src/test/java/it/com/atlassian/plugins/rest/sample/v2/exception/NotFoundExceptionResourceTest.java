package it.com.atlassian.plugins.rest.sample.v2.exception;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.net.URI;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import jakarta.xml.bind.JAXB;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;

import org.junit.Test;

import com.atlassian.plugins.rest.api.model.Status;
import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class NotFoundExceptionResourceTest {
    private static final Unmarshaller STATUS_UNMARSHALLER;

    static {
        try {
            STATUS_UNMARSHALLER = JAXBContext.newInstance(Status.class).createUnmarshaller();
        } catch (JAXBException e) {
            throw new RuntimeException();
        }
    }

    private static final URI NOT_FOUND_EXCEPTION_URI = UriBuilder.create()
            .path("rest")
            .path("exception")
            .path("1")
            .path("notFoundException")
            .build();

    @Test
    public void shouldGetNotFoundExceptionAsXml() throws Exception {
        Response response = ClientBuilder.newClient()
                .target(NOT_FOUND_EXCEPTION_URI)
                .request(MediaType.APPLICATION_XML)
                .get(Response.class);

        assertContentType(response, MediaType.APPLICATION_XML);
        String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                + "<status><status-code>404</status-code>"
                + "<message>HTTP 404 Not Found</message>"
                + "</status>";
        assertXML(response, expected);
    }

    private void assertXML(Response response, String expected) throws JAXBException {
        String xmlString = getEntityAsXML(response);
        Status actualStatus = unmarshallStatus(xmlString);
        Status expectedStatus = unmarshallStatus(expected);

        assertNotNull(xmlString);
        assertEquals(actualStatus, expectedStatus);
        assertEquals("Response status should be not found", 404, response.getStatus());
    }

    private static String getEntityAsXML(Response response) {
        Object entity = response.readEntity(Status.class);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JAXB.marshal(entity, baos);
        return baos.toString();
    }

    private Status unmarshallStatus(String xmlString) throws JAXBException {
        return (Status) STATUS_UNMARSHALLER.unmarshal(new StringReader(xmlString));
    }

    private static void assertContentType(Response response, String mediaType) {
        MultivaluedMap<String, String> headers = response.getStringHeaders();
        assertEquals(1, headers.get("Content-Type").size());
        assertEquals(mediaType, headers.get("Content-Type").get(0));
    }
}
