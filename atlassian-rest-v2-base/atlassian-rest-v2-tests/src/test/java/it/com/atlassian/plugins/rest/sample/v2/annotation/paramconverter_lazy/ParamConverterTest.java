package it.com.atlassian.plugins.rest.sample.v2.annotation.paramconverter_lazy;

import java.net.URI;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

import org.junit.Test;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;

public class ParamConverterTest {

    private final URI baseUri =
            UriBuilder.create().path("rest").path("annotations").path("1").build();

    @Test
    public void testIfFormParamIsRequestedThenParamConverterGetsExecuted() {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(baseUri).path("lazy-param");
        Form form = new Form();
        MultivaluedMap<String, String> formData = form.asMap();
        formData.add("convertable", "Hello from Client");
        Response response = target.request().post(Entity.form(form));
        assertEquals(200, response.getStatus());
        client.close();
    }
}
