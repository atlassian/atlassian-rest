package it.com.atlassian.plugins.rest.sample.v2.annotation.prematching;

import java.net.URI;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;

import org.junit.Test;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;

public class PreMatchingTest {

    private final URI baseUri =
            UriBuilder.create().path("rest").path("annotations").path("1").build();

    @Test
    public void testIfRequestWillRedirectToPreMatchingResource() {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(baseUri).path("initial");
        assertEquals(
                "Message was redirected by PreMatchingFilter", target.request().get(String.class));
        client.close();
    }
}
