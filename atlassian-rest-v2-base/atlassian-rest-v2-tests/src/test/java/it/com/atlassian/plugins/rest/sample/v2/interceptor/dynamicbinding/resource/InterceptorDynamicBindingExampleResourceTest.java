package it.com.atlassian.plugins.rest.sample.v2.interceptor.dynamicbinding.resource;

import java.net.URI;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.MediaType;

import org.junit.Test;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertArrayEquals;

/**
 * This is an example of a resource with interceptors.
 * It demonstrates Dynamic binding, a way how to assign interceptors to the resource methods in a dynamic manner.
 */
public class InterceptorDynamicBindingExampleResourceTest {
    @Test
    public void interceptorDynamicBindingResourceTest() {
        final String entity = "Original entity";

        final URI baseUri =
                UriBuilder.create().path("rest").path("interceptor").path("1").build();
        final String[] responseEntity = ClientBuilder.newClient()
                .target(baseUri)
                .path("interceptorDynamicBinding")
                .request(MediaType.TEXT_PLAIN)
                .post(Entity.text(entity))
                .readEntity(String.class)
                .split("\n");

        final String[] expectedEntity = {
            "Original entity",
            "This is added text from ReaderInterceptor1 with Priority = 4999",
            "This is added text from ReaderInterceptor3OverrideWithoutPriority with Priority = "
                    + "no priority assigned, the default USER priority value = 5000 is used",
            "This is added text from ReaderInterceptor2 with Priority = 5001",
            "This is added text from ReaderInterceptor3 with Priority = 6000",
            "This is added text from ReaderInterceptor3OverridePriority with Priority = 7500",
            "Entity has been processed in the resource method",
            "This is added text from WriterInterceptor3 with Priority = 6000",
            "This is added text from WriterInterceptor2 with Priority = 7000",
            "This is added text from WriterInterceptor1 with Priority = 8000"
        };

        assertArrayEquals(expectedEntity, responseEntity);
    }
}
