package it.com.atlassian.plugins.rest.sample.v2.marshalling.mediatypes;

import java.net.URI;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Test;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Testing support for following media types: "application/xml", "application/json", "application/atom+xml".
 */
public class ResponseTypesTest {
    final URI baseUri =
            UriBuilder.create().path("rest").path("marshalling").path("1").build();
    final Client client = ClientBuilder.newClient();
    final WebTarget target = client.target(baseUri).path("entity");

    @Test
    public void testIfResourceSupportsJsonResponseType() {
        assertEquals(
                "{\"description\":\"Example returned JaxbPojo\",\"value\":123}",
                target.request().accept(MediaType.APPLICATION_JSON).get(String.class));
    }

    @Test
    public void testIfResourceSupportsXmlResponseType() {
        String jaxbPojo = target.request().accept(MediaType.APPLICATION_XML).get(String.class);
        assertTrue(jaxbPojo.contains("<jaxbPojo description=\"Example returned JaxbPojo\" value=\"123\"/>"));
    }

    @Test
    public void testIfResourceSupportsAtomXmlResponseType() {
        String jaxbPojo =
                target.request().accept(MediaType.APPLICATION_ATOM_XML).get(String.class);
        assertTrue(jaxbPojo.contains("<jaxbPojo description=\"Example returned JaxbPojo\" value=\"123\"/>"));
    }

    @Test
    public void testIfJsonCanBeSerializedToPojo() {
        String jsonJaxbPojoJsonString =
                target.request().accept(MediaType.APPLICATION_JSON).get(String.class);
        assertEquals("{\"description\":\"Example returned JaxbPojo\",\"value\":123}", jsonJaxbPojoJsonString);
    }

    @Test
    public void testIfXmlCanBeSerializedToPojo() {
        String jsonJaxbPojoXmlString =
                target.request().accept(MediaType.APPLICATION_XML).get(String.class);
        assertEquals(
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                        + "<jaxbPojo description=\"Example returned "
                        + "JaxbPojo\" value=\"123\"/>",
                jsonJaxbPojoXmlString);
    }

    @Test
    public void testIfResourceAcceptsJsonContentType() {
        Response res =
                target.request().post(Entity.json("{\"description\":\"Example returned JaxbPojo\",\"value\":123}"));
        assertEquals(200, res.getStatus());
    }

    @Test
    public void testIfResourceAcceptsJsonContentTypeJsonObject() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode objectNode = mapper.createObjectNode();
        objectNode.put("description", "JSON type Pojo");
        objectNode.put("value", 321);
        String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(objectNode);
        Response res = target.request().post(Entity.json(jsonString));
        assertEquals(200, res.getStatus());
        String jaxbPojoXmlString = res.readEntity(String.class);
        assertEquals(
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                        + "<jaxbPojo description=\"JSON type Pojo\" "
                        + "value=\"321\"/>",
                jaxbPojoXmlString);
    }

    @Test
    public void fourHundredErrorWhenJsonObjectUnknownProperties() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode objectNode = mapper.createObjectNode();
        objectNode.put("description", "JSON type Pojo");
        objectNode.put("value", 321);
        objectNode.put("type", "jaxb");
        String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(objectNode);
        Response res = target.request().post(Entity.json(jsonString));
        assertEquals(400, res.getStatus()); // Error as the unknownProperty is not handled by JAXB by default for JSON.
        assertThat(res.readEntity(String.class), CoreMatchers.containsString("Unrecognized field \"type\""));
    }

    @Test
    public void testIfResourceReturnsErrorForUnsupportedMedia() {
        Response res = target.request()
                .post(Entity.xml("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                        + "<jaxbPojo description=\"XML type Pojo\" "
                        + "value=\"321\"/>"));
        assertEquals(415, res.getStatus());
        assertEquals("Unsupported Media Type", res.getStatusInfo().getReasonPhrase());
    }

    @After
    public void cleanUp() {
        client.close();
    }
}
