package it.com.atlassian.plugins.rest.sample.v2.annotation.constrainedto;

import java.net.URI;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Response;

import org.junit.Test;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ConstrainedToTest {

    private final URI baseUri =
            UriBuilder.create().path("rest").path("annotations").path("1").build();

    @Test
    public void testIfOnlyServerSideFilterWasRegistered() {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(baseUri).path("constrained-to");
        Response response = target.request().get();
        assertTrue(response.getHeaders().get("RegisteredFilters").contains("ServerSideFilter"));
        assertFalse(response.getHeaders().get("RegisteredFilters").contains("ClientSideFilter"));
        client.close();
    }
}
