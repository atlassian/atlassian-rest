package it.com.atlassian.plugins.rest.sample.v2.filter.global.resource;

import java.net.URI;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.MediaType;

import org.junit.Test;

import com.atlassian.plugins.rest.sample.v2.filter.global.resource.FilterResource;
import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertArrayEquals;

public class FilterResourceTest {

    @Test
    public void filterResourceTest() {
        final URI baseUri =
                UriBuilder.create().path("rest").path("filter").path("1").build();

        final String[] responseHeader = ClientBuilder.newClient()
                .target(baseUri)
                .path("filter")
                .request(MediaType.TEXT_PLAIN)
                .get()
                .getHeaderString(FilterResource.FILTER_HEADER)
                .split(", ");

        String[] expectedHeader = {
            "[" + "This text is added from PreMatchFilter1 with Priority = 8500",
            "This text is added from PreMatchFilter2 with Priority = 9500",
            "This text is added from ContainerRequestFilter1 with Priority = 6000",
            "This text is added from ContainerRequestFilter2 with Priority = 7000",
            "This text is added from FilterResource",
            "This text is added from ContainerResponseFilter2 with Priority = 7000",
            "This text is added from ContainerResponseFilter1 with Priority = 6000" + "]"
        };

        assertArrayEquals(expectedHeader, responseHeader);
    }
}
