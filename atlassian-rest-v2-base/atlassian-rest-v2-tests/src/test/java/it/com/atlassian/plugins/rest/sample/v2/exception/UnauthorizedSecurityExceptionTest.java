package it.com.atlassian.plugins.rest.sample.v2.exception;

import java.net.URI;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.Response;

import org.junit.Test;

import com.atlassian.plugins.rest.api.model.Status;
import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static jakarta.ws.rs.core.MediaType.APPLICATION_XML;
import static jakarta.ws.rs.core.Response.Status.UNAUTHORIZED;
import static org.junit.Assert.assertEquals;

public class UnauthorizedSecurityExceptionTest {

    @Test
    public void testUnauthorizedRequestReturns401() {
        final URI baseUri = UriBuilder.create()
                .path("rest")
                .path("exception")
                .path("1")
                .path("mapper")
                .path("unauthorizedException")
                .build();

        final Response response = ClientBuilder.newClient()
                .target(baseUri)
                .request(APPLICATION_XML)
                .get(Response.class);

        assertEquals(UNAUTHORIZED.getStatusCode(), response.getStatusInfo().getStatusCode());
        Status statusEntity = response.readEntity(Status.class);
        assertEquals(UNAUTHORIZED.getStatusCode(), statusEntity.getCode());
        assertEquals("Client must be authenticated to access this resource.", statusEntity.getMessage());
    }
}
