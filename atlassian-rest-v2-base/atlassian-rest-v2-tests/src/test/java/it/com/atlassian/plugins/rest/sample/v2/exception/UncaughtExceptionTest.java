package it.com.atlassian.plugins.rest.sample.v2.exception;

import java.net.URI;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.json.JSONObject;
import org.junit.Test;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.MediaType.APPLICATION_XML;
import static jakarta.ws.rs.core.MediaType.TEXT_PLAIN;
import static jakarta.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UncaughtExceptionTest {
    private static final String QUERY_PARAM = "message";
    private static final String MESSAGE = "The detailed error message.";
    public static final String NO_STACKTRACE_MESSAGE =
            "Please contact your admin passing attached Log's referral number: ";

    private final URI baseUri = UriBuilder.create()
            .path("rest")
            .path("exception")
            .path("1")
            .path("mapper")
            .path("uncaughtInternalException")
            .build();

    @Test
    public void testUncaughtErrorResponseIsReturnedInXMLWhenXMLIsRequested() {
        final Response response = ClientBuilder.newClient()
                .target(baseUri)
                .queryParam(QUERY_PARAM, MESSAGE)
                .request(APPLICATION_XML)
                .get(Response.class);

        assertEquals(500, response.getStatus());
        assertEquals(APPLICATION_XML, response.getMediaType().toString());

        String responseEntity = response.readEntity(String.class);

        assertTrue(responseEntity.contains("<status-code>500</status-code>"));
        assertTrue(responseEntity.contains("<message>" + MESSAGE + "</message>"));
        assertTrue(responseEntity.contains(NO_STACKTRACE_MESSAGE));
    }

    @Test
    public void testUncaughtErrorResponseIsReturnedInJSONWhenJSONIsRequested() {
        final Response response = ClientBuilder.newClient()
                .target(baseUri)
                .queryParam(QUERY_PARAM, MESSAGE)
                .request(APPLICATION_JSON)
                .get(Response.class);

        assertEquals(500, response.getStatus());
        assertEquals(APPLICATION_JSON, response.getMediaType().toString());

        JSONObject responseEntity = new JSONObject(response.readEntity(String.class));

        assertEquals(MESSAGE, responseEntity.getString("message"));
        assertEquals(INTERNAL_SERVER_ERROR.getStatusCode(), responseEntity.getInt("status-code"));
        assertTrue(responseEntity.getString("stack-trace").contains(NO_STACKTRACE_MESSAGE));
    }

    @Test
    public void testUncaughtErrorResponseIsReturnedInTextWhenTextIsRequested() {
        final Response response = ClientBuilder.newClient()
                .target(baseUri)
                .queryParam(QUERY_PARAM, MESSAGE)
                .request(TEXT_PLAIN)
                .get(Response.class);

        assertEquals(500, response.getStatus());

        assertEquals(MediaType.valueOf("text/plain; charset=utf-8"), response.getMediaType());

        String responseEntity = response.readEntity(String.class);

        assertTrue(responseEntity.contains(MESSAGE));
        assertTrue(responseEntity.contains(String.valueOf(INTERNAL_SERVER_ERROR.getStatusCode())));
        assertTrue(responseEntity.contains(NO_STACKTRACE_MESSAGE));
    }
}
