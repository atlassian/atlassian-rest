package it.com.atlassian.plugins.rest.sample.v2.exception;

import java.net.URI;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.Response;

import org.junit.Test;

import com.atlassian.plugins.rest.api.model.Status;
import com.atlassian.plugins.rest.v2.exception.entity.UncaughtExceptionEntity;
import com.atlassian.plugins.rest.v2.jersey.ResourceConfigFactory;
import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static jakarta.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;
import static jakarta.ws.rs.core.Response.Status.NOT_FOUND;
import static jakarta.ws.rs.core.Response.Status.REQUEST_HEADER_FIELDS_TOO_LARGE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import static com.atlassian.plugins.rest.sample.v2.exception.WebApplicationExceptionResource.INTERNAL_SERVER_ERROR_RESPONSE_BODY;
import static com.atlassian.plugins.rest.sample.v2.exception.WebApplicationExceptionResource.NOT_FOUND_RESPONSE_BODY;
import static com.atlassian.plugins.rest.sample.v2.exception.WebApplicationExceptionResource.NOT_SUPPORTED_STATUS;
import static com.atlassian.plugins.rest.sample.v2.exception.WebApplicationExceptionResource.REQUEST_HEADER_FIELDS_TOO_LARGE_RESPONSE_BODY;
import static com.atlassian.plugins.rest.sample.v2.exception.WebApplicationExceptionResource.UNSUPPORTED_STATUS_RESPONSE_BODY;

/**
 * This is the test class designed to examine the handling of {@link WebApplicationException} by atlassian-rest.
 * {@code ServerProperties.RESPONSE_SET_STATUS_OVER_SEND_ERROR = "jersey.config.server.response.setStatusOverSendError"} is set
 * to {@code true} in {@link ResourceConfigFactory}
 */
public class WebApplicationExceptionTest {
    public static final String STACKTRACE_MESSAGE =
            "Please contact your admin passing attached Log's referral number: ";

    public static final URI BASE_URI = UriBuilder.create()
            .path("rest")
            .path("exception")
            .path("1")
            .path("webApplicationExceptionMapper")
            .build();

    @Test
    public void testNotFoundWithoutBody() {
        final Response response = ClientBuilder.newClient()
                .target(BASE_URI)
                .path("notFoundWithoutBody")
                .request()
                .get(Response.class);

        assertEquals(NOT_FOUND.getStatusCode(), response.getStatusInfo().getStatusCode());
        Status statusEntity = response.readEntity(Status.class);
        assertEquals(NOT_FOUND.getStatusCode(), statusEntity.getCode());
        assertEquals("HTTP 404 Not Found", statusEntity.getMessage());
    }

    /**
     * This is an unknown Jersey issue that ExceptionMapper not catching WebApplicationException with entity.
     * In this case, the response won't be returned as REST standard XML or JSON format.
     * See: <a href="https://github.com/eclipse-ee4j/jersey/issues/3716">related GitHub issue</a>
     */
    @Test
    public void testNotFoundWithBody() {
        final Response response = ClientBuilder.newClient()
                .target(BASE_URI)
                .path("notFoundWithBody")
                .request()
                .get(Response.class);

        assertEquals(NOT_FOUND.getStatusCode(), response.getStatusInfo().getStatusCode());
        assertEquals(NOT_FOUND_RESPONSE_BODY, response.readEntity(String.class));
    }

    @Test
    public void testInternalErrorWithoutBody() {
        final Response response = ClientBuilder.newClient()
                .target(BASE_URI)
                .path("internalErrorWithoutBody")
                .request()
                .get(Response.class);

        assertEquals(
                INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatusInfo().getStatusCode());
        UncaughtExceptionEntity statusEntity = response.readEntity(UncaughtExceptionEntity.class);
        assertEquals(
                INTERNAL_SERVER_ERROR.getStatusCode(), statusEntity.getCode().intValue());
        assertEquals("HTTP 500 Internal Server Error", statusEntity.getMessage());
        assertTrue(statusEntity.getStackTrace().startsWith(STACKTRACE_MESSAGE));
    }

    /**
     * This is an unknown Jersey issue that ExceptionMapper not catching WebApplicationException with entity.
     * In this case, the response won't be returned as REST standard XML or JSON format.
     * See: <a href="https://github.com/eclipse-ee4j/jersey/issues/3716">related GitHub issue</a>
     */
    @Test
    public void testInternalErrorWithBody() {
        final Response response = ClientBuilder.newClient()
                .target(BASE_URI)
                .path("internalErrorWithBody")
                .request()
                .get(Response.class);

        assertEquals(
                INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatusInfo().getStatusCode());
        assertEquals(INTERNAL_SERVER_ERROR_RESPONSE_BODY, response.readEntity(String.class));
    }

    @Test
    public void testSupportedStatusWithoutBody() {
        final Response response = ClientBuilder.newClient()
                .target(BASE_URI)
                .path("supportedStatusWithoutBody")
                .request()
                .get(Response.class);

        assertEquals(
                REQUEST_HEADER_FIELDS_TOO_LARGE.getStatusCode(),
                response.getStatusInfo().getStatusCode());
        Status statusEntity = response.readEntity(Status.class);
        assertEquals(REQUEST_HEADER_FIELDS_TOO_LARGE.getStatusCode(), statusEntity.getCode());
        assertEquals("HTTP 431 Request Header Fields Too Large", statusEntity.getMessage());
    }

    /**
     * This is an unknown Jersey issue that ExceptionMapper not catching WebApplicationException with entity.
     * In this case, the response won't be returned as REST standard XML or JSON format.
     * See: <a href="https://github.com/eclipse-ee4j/jersey/issues/3716">related GitHub issue</a>
     */
    @Test
    public void testSupportedStatusWithBody() {
        final Response response = ClientBuilder.newClient()
                .target(BASE_URI)
                .path("supportedStatusWithBody")
                .request()
                .get(Response.class);

        assertEquals(
                REQUEST_HEADER_FIELDS_TOO_LARGE.getStatusCode(),
                response.getStatusInfo().getStatusCode());
        assertEquals(REQUEST_HEADER_FIELDS_TOO_LARGE_RESPONSE_BODY, response.readEntity(String.class));
    }

    @Test
    public void testNotSupportedStatusWithoutBody() {
        final Response response = ClientBuilder.newClient()
                .target(BASE_URI)
                .path("notSupportedStatusWithoutBody")
                .request()
                .get(Response.class);

        assertEquals(NOT_SUPPORTED_STATUS, response.getStatusInfo().getStatusCode());
        Status statusEntity = response.readEntity(Status.class);
        assertEquals(NOT_SUPPORTED_STATUS, statusEntity.getCode());
        assertTrue(statusEntity.getMessage().contains("HTTP 444"));
    }

    /**
     * This is an unknown Jersey issue that ExceptionMapper not catching WebApplicationException with entity.
     * In this case, the response won't be returned as REST standard XML or JSON format.
     * See: <a href="https://github.com/eclipse-ee4j/jersey/issues/3716">related GitHub issue</a>
     */
    @Test
    public void testNotSupportedStatusWithBody() {
        final Response response = ClientBuilder.newClient()
                .target(BASE_URI)
                .path("notSupportedStatusWithBody")
                .request()
                .get(Response.class);

        assertEquals(NOT_SUPPORTED_STATUS, response.getStatusInfo().getStatusCode());
        assertEquals(UNSUPPORTED_STATUS_RESPONSE_BODY, response.readEntity(String.class));
    }
}
