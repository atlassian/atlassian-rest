package it.com.atlassian.plugins.rest.sample.v2.marshalling.request;

import java.net.URI;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Response;

import org.junit.Test;

import com.atlassian.plugins.rest.sample.v2.marshalling.request.client.ClientServlet;
import com.atlassian.plugins.rest.sample.v2.marshalling.request.server.ServerResource;
import com.atlassian.plugins.rest.v2.utils.UriBuilder;
import com.atlassian.sal.api.net.MarshallingRequestFactory;
import com.atlassian.sal.api.net.Request;

import static org.junit.Assert.assertEquals;

/**
 * The test validates if the {@link MarshallingRequestFactory} that is used in the {@link ClientServlet} will execute
 * the {@link Request} and marshall the request entity to XML, so that the {@link ServerResource} can read it.
 */
public class DefaultMarshallingRequestFactoryTest {
    final URI baseUri =
            UriBuilder.create().path("plugins").path("servlet").path("client").build();
    final Client client = ClientBuilder.newClient();
    final WebTarget target = client.target(baseUri);

    @Test
    public void testIfTheMessageIsSentToServerResource() {
        String message = "Hello ServerResource";
        Response response = target.queryParam("message", message).request().get(Response.class);
        assertEquals(200, response.getStatus());
        assertEquals(message, response.readEntity(String.class));
    }
}
