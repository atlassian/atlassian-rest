package it.com.atlassian.plugins.rest.sample.v2;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Unmarshaller;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.ByteArrayPartSource;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.PartSource;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.junit.Test;

import com.atlassian.plugins.rest.sample.multipart.models.FilePartObject;
import com.atlassian.plugins.rest.sample.multipart.models.FilePartObjects;
import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static jakarta.ws.rs.core.Response.Status.BAD_REQUEST;
import static jakarta.ws.rs.core.Response.Status.REQUEST_ENTITY_TOO_LARGE;
import static java.util.Comparator.comparing;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

public class MultipartResourceTest {

    private static final Comparator<String> nullSafeStringComparator =
            Comparator.nullsFirst(String::compareToIgnoreCase);
    private static final Comparator<FilePartObject> FILE_PART_OBJECT_COMPARATOR = comparing(FilePartObject::isField)
            .thenComparing(FilePartObject::getFieldName, nullSafeStringComparator)
            .thenComparing(FilePartObject::getFilename, nullSafeStringComparator)
            .thenComparing(FilePartObject::getValue, nullSafeStringComparator);

    @Test
    public void testUploadSingleFile() throws Exception {
        FilePartObject filePartObject =
                new FilePartObject("file", false, "filename.txt", "text/plain", "Hello world!!");
        FilePartObject result = (FilePartObject) uploadAndUnmarshalFileParts("single", filePartObject);
        assertThat(result).usingComparator(FILE_PART_OBJECT_COMPARATOR).isEqualTo(filePartObject);
    }

    @Test
    public void testUploadSingleSimpleField() throws Exception {
        FilePartObject filePartObject = new FilePartObject("file", true, null, null, "Hello world!!");
        FilePartObject result = (FilePartObject) uploadAndUnmarshalFileParts("single", filePartObject);
        assertThat(result).usingComparator(FILE_PART_OBJECT_COMPARATOR).isEqualTo(filePartObject);
    }

    @Test
    public void testUploadMultiple() throws Exception {
        FilePartObject filePartObject1 =
                new FilePartObject("file", false, "file1.txt", "text/plain", "Hello first world!");
        FilePartObject filePartObject2 =
                new FilePartObject("file", false, "file2.txt", "text/plain", "Hello second world!");

        FilePartObjects result =
                (FilePartObjects) uploadAndUnmarshalFileParts("multiple", filePartObject1, filePartObject2);

        assertThat(result.getFileParts())
                .usingElementComparator(FILE_PART_OBJECT_COMPARATOR)
                .containsExactlyInAnyOrder(filePartObject1, filePartObject2);
    }

    @Test
    public void testUploadMultipleAsList() throws Exception {
        FilePartObject filePartObject1 =
                new FilePartObject("file", false, "file1.txt", "text/plain", "Hello first world!");
        FilePartObject filePartObject2 =
                new FilePartObject("file", false, "file2.txt", "text/plain", "Hello second world!");

        FilePartObjects result =
                (FilePartObjects) uploadAndUnmarshalFileParts("multipleAsList", filePartObject1, filePartObject2);
        assertThat(result.getFileParts())
                .usingElementComparator(FILE_PART_OBJECT_COMPARATOR)
                .containsExactlyInAnyOrder(filePartObject1, filePartObject2);
    }

    @Test
    public void testUploadMultipleAsSet() throws Exception {
        FilePartObject filePartObject1 =
                new FilePartObject("file", false, "file1.txt", "text/plain", "Hello first world!");
        FilePartObject filePartObject2 =
                new FilePartObject("file", false, "file2.txt", "text/plain", "Hello second world!");

        FilePartObjects result =
                (FilePartObjects) uploadAndUnmarshalFileParts("multipleAsSet", filePartObject1, filePartObject2);
        assertThat(result.getFileParts())
                .usingElementComparator(FILE_PART_OBJECT_COMPARATOR)
                .containsExactlyInAnyOrder(filePartObject1, filePartObject2);
    }

    @Test
    public void testMaxFileSizeUnder() throws Exception {
        FilePartObject filePartObject = new FilePartObject("file", false, "file.txt", "text/plain", "Safe");
        FilePartObjects result = (FilePartObjects) uploadAndUnmarshalFileParts("config", filePartObject);
        assertThat(result.getFileParts())
                .usingElementComparator(FILE_PART_OBJECT_COMPARATOR)
                .containsExactly(filePartObject);
    }

    @Test
    public void testMaxFileSizeExceeded() throws Exception {
        // The config resource is configured for maximum file size of 10 characters
        FilePartObject filePartObject =
                new FilePartObject("file", false, "file.txt", "text/plain", "This is longer than 10 characters");
        PostMethod method = uploadFileParts("config", filePartObject);
        assertThat(method.getStatusCode()).isEqualTo(REQUEST_ENTITY_TOO_LARGE.getStatusCode());
        assertThat(method.getResponseBodyAsString()).contains("10");
    }

    @Test
    public void testMaxSizeExceeded() throws Exception {
        // The config resource is configured for maximum overall request size of 1000 characters.  This should yield
        // 1161 characters due to all the multipart headers
        PostMethod method = uploadFileParts(
                "config",
                new FilePartObject("file", false, "file.txt", "text/plain", "Safe"),
                new FilePartObject("file", false, "file.txt", "text/plain", "Safe"),
                new FilePartObject("file", false, "file.txt", "text/plain", "Safe"),
                new FilePartObject("file", false, "file.txt", "text/plain", "Safe"),
                new FilePartObject("file", false, "file.txt", "text/plain", "Safe"),
                new FilePartObject("file", false, "file.txt", "text/plain", "Safe"));
        assertThat(method.getStatusCode()).isEqualTo(REQUEST_ENTITY_TOO_LARGE.getStatusCode());
        assertThat(method.getResponseBodyAsString()).contains("1000");
    }

    @Test
    public void testInterceptorInvoked() throws Exception {
        FilePartObject filePartObject = new FilePartObject("file", false, "abc.txt", "text/plain", "Upload");
        PostMethod method = uploadFileParts("fileName", filePartObject);
        assertEquals(
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><message><message>abc.txt: Bob</message></message>",
                method.getResponseBodyAsString());
    }

    @Test
    public void uploadWithEncodedNameShouldLeaveFileNameCorrect() throws Exception {
        FilePartObject filePartObject =
                new FilePartObject("file", false, "=?utf-8?B?0YLQtdGB0YI=?=", "text/plain", "Hello first world!");
        FilePartObject result = (FilePartObject) uploadAndUnmarshalFileParts("single", filePartObject);
        assertEquals("тест", result.getFilename());
    }

    @Test
    public void uploadWithIllegalCharacterEncodingShouldResultInClientError() throws Exception {
        PostMethod method = uploadFileParts(
                "single",
                new FilePartObject(
                        "file", false, "=?INVALID-CHARSET?B?SGVsbG8gd29ybGQ=?=", "text/plain", "Hello first world!"));
        assertThat(method.getStatusCode()).isEqualTo(BAD_REQUEST.getStatusCode());
    }

    @Test
    public void testUploadSingleUsingMultipartHandler() throws Exception {
        FilePartObject filePartObject =
                new FilePartObject("file", false, "filename.txt", "text/plain", "Hello first world!");

        FilePartObject result = (FilePartObject) uploadAndUnmarshalFileParts("singleUsingHandler", filePartObject);
        assertThat(result).usingComparator(FILE_PART_OBJECT_COMPARATOR).isEqualTo(filePartObject);
    }

    @Test
    public void testUploadSingleUsingMultipartHandlerFormFilePart() throws Exception {
        FilePartObject filePartObject = new FilePartObject("file", true, null, "text/plain", "Hello first world!");

        FilePartObject result =
                (FilePartObject) uploadAndUnmarshalFileParts("singleUsingHandlerFormFilePart", filePartObject);
        assertThat(result).usingComparator(FILE_PART_OBJECT_COMPARATOR).isEqualTo(filePartObject);
    }

    @Test
    public void testUploadSingleUsingMultipartHandlerFormFileParts() throws Exception {
        FilePartObject filePartObject1 = new FilePartObject("file", true, null, "text/plain", "Hello first world!");
        FilePartObject filePartObject2 = new FilePartObject("file", true, null, "text/plain", "Hello second world!");

        FilePartObjects result = (FilePartObjects)
                uploadAndUnmarshalFileParts("singleUsingHandlerFormFileParts", filePartObject1, filePartObject2);
        assertThat(result.getFileParts())
                .usingElementComparator(FILE_PART_OBJECT_COMPARATOR)
                .containsExactlyInAnyOrder(filePartObject1, filePartObject2);
    }

    @Test
    public void testUploadSingleUsingMultipartHandlerFormDynamicField() throws Exception {
        FilePartObject filePartObject1 = new FilePartObject("dynamic", true, null, "text/plain", "Hello first world!");
        FilePartObject filePartObject2 = new FilePartObject("dynamic", true, null, "text/plain", "Hello second world!");
        FilePartObject filePartObject3 = new FilePartObject("dynamic2", true, null, "text/plain", "Hello third world!");
        FilePartObject filePartObject4 =
                new FilePartObject("dynamic2", true, null, "text/plain", "Hello fourth world!");

        FilePartObjects result = (FilePartObjects) uploadAndUnmarshalFileParts(
                "singleUsingHandlerFormDynamicField",
                filePartObject1,
                filePartObject2,
                filePartObject3,
                filePartObject4);
        assertThat(result.getFileParts())
                .usingElementComparator(FILE_PART_OBJECT_COMPARATOR)
                .containsExactlyInAnyOrder(filePartObject1, filePartObject2, filePartObject3, filePartObject4);
    }

    private Object uploadAndUnmarshalFileParts(String to, FilePartObject... fileParts) throws Exception {
        PostMethod method = uploadFileParts(to, fileParts);
        Unmarshaller unmarshaller = JAXBContext.newInstance(FilePartObject.class, FilePartObjects.class)
                .createUnmarshaller();

        InputStream responseBodyAsStream = method.getResponseBodyAsStream();
        return unmarshaller.unmarshal(responseBodyAsStream);
    }

    private PostMethod uploadFileParts(String to, FilePartObject... fileParts) throws Exception {
        ArrayList<Part> parts = new ArrayList<>();
        for (FilePartObject o : fileParts) {
            if (o.isField()) {
                StringPart stringPart = new StringPart(o.getFieldName(), o.getValue());
                parts.add(stringPart);
            } else {
                PartSource partSource =
                        new ByteArrayPartSource(o.getFilename(), o.getValue().getBytes(StandardCharsets.UTF_8));
                FilePart filePart = new FilePart(
                        o.getFieldName(), partSource, o.getContentType(), StandardCharsets.UTF_8.toString());
                parts.add(filePart);
            }
        }

        final String baseUri = UriBuilder.create()
                .path("rest")
                .path("multipart")
                .path("1")
                .path(to)
                .build()
                .toString();

        PostMethod method = new PostMethod(baseUri);

        MultipartRequestEntity requestEntity =
                new MultipartRequestEntity(parts.toArray(new Part[0]), new HttpMethodParams());
        method.setRequestEntity(requestEntity);

        HttpClient client = new HttpClient();
        client.executeMethod(method);

        return method;
    }
}
