package it.com.atlassian.plugins.rest.sample.v2.security.cors;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URI;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;

import org.junit.Test;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS;
import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.ACCESS_CONTROL_ALLOW_HEADERS;
import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.ACCESS_CONTROL_ALLOW_METHODS;
import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN;
import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.ACCESS_CONTROL_EXPOSE_HEADERS;
import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.ACCESS_CONTROL_MAX_AGE;
import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.ACCESS_CONTROL_REQUEST_HEADERS;
import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.ACCESS_CONTROL_REQUEST_METHOD;
import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.ORIGIN;
import static com.atlassian.plugins.rest.sample.v2.security.cors.SimpleCorsDefaults.ALLOWED_REQUEST_HEADER;
import static com.atlassian.plugins.rest.sample.v2.security.cors.SimpleCorsDefaults.ALLOWED_RESPONSE_HEADER;
import static com.atlassian.plugins.rest.sample.v2.security.cors.SimpleCorsDefaults.CREDENTIALS;
import static com.atlassian.plugins.rest.sample.v2.security.cors.SimpleCorsDefaults.NO_CREDENTIALS;

public class CorsCheckTest {
    private final URI baseUri = UriBuilder.create()
            .path("rest")
            .path("security")
            .path("1")
            .path("cors")
            .build();
    private final WebTarget corsCheck = ClientBuilder.newClient().target(baseUri);

    @Test
    public void testSimpleGetWithOrigin() throws IOException {
        Map<String, String> headers = makeRequest("GET", Collections.singletonMap(ORIGIN.value(), CREDENTIALS));
        assertEquals(CREDENTIALS, headers.get(ACCESS_CONTROL_ALLOW_ORIGIN.value()));
        assertEquals("true", headers.get(ACCESS_CONTROL_ALLOW_CREDENTIALS.value()));
        assertEquals(ALLOWED_RESPONSE_HEADER, headers.get(ACCESS_CONTROL_EXPOSE_HEADERS.value()));
    }

    @Test
    public void testSimpleGetWithNoCredentialsOrigin() throws IOException {
        Map<String, String> headers = makeRequest("GET", Collections.singletonMap(ORIGIN.value(), NO_CREDENTIALS));
        assertEquals(NO_CREDENTIALS, headers.get(ACCESS_CONTROL_ALLOW_ORIGIN.value()));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_CREDENTIALS.value()));
        assertEquals(ALLOWED_RESPONSE_HEADER, headers.get(ACCESS_CONTROL_EXPOSE_HEADERS.value()));
    }

    @Test
    public void testPreflight() throws IOException {
        Map<String, String> headers = makeRequest("OPTIONS", new HashMap<String, String>() {
            {
                put(ORIGIN.value(), CREDENTIALS);
                put(ACCESS_CONTROL_REQUEST_METHOD.value(), "PUT");
            }
        });
        assertEquals(CREDENTIALS, headers.get(ACCESS_CONTROL_ALLOW_ORIGIN.value()));
        assertEquals("true", headers.get(ACCESS_CONTROL_ALLOW_CREDENTIALS.value()));
        assertEquals(String.valueOf(60 * 60), headers.get(ACCESS_CONTROL_MAX_AGE.value()));
        assertEquals("PUT", headers.get(ACCESS_CONTROL_ALLOW_METHODS.value()));
        assertEquals(ALLOWED_REQUEST_HEADER, headers.get(ACCESS_CONTROL_ALLOW_HEADERS.value()));
    }

    @Test
    public void testPreflightWithWrongHeaders() throws Exception {
        Map<String, String> headers = makeRequest("OPTIONS", new HashMap<String, String>() {
            {
                put(ORIGIN.value(), NO_CREDENTIALS);
                put(ACCESS_CONTROL_REQUEST_METHOD.value(), "PUT");
                put(ACCESS_CONTROL_REQUEST_HEADERS.value(), "X-Unexpected-Header");
            }
        });
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_ORIGIN.value()));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_CREDENTIALS.value()));
        assertNull(headers.get(ACCESS_CONTROL_MAX_AGE.value()));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_METHODS.value()));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_HEADERS.value()));
    }

    @Test
    public void testPreflightWithWrongOrigin() throws IOException {
        Map<String, String> headers = makeRequest("OPTIONS", new HashMap<String, String>() {
            {
                put(ORIGIN.value(), "http://www.invalid.com");
                put(ACCESS_CONTROL_REQUEST_METHOD.value(), "PUT");
            }
        });
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_ORIGIN.value()));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_CREDENTIALS.value()));
        assertNull(headers.get(ACCESS_CONTROL_MAX_AGE.value()));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_METHODS.value()));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_HEADERS.value()));
    }

    @Test
    public void testPreflightWithWrongMethod() throws IOException {
        final String uri = UriBuilder.create()
                .path("rest")
                .path("security")
                .path("1")
                .path("cors")
                .path("none")
                .build()
                .toString();
        Map<String, String> headers = makeRequest(uri, "OPTIONS", new HashMap<String, String>() {
            {
                put(ORIGIN.value(), CREDENTIALS);
                put(ACCESS_CONTROL_REQUEST_METHOD.value(), "PUT");
            }
        });
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_ORIGIN.value()));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_CREDENTIALS.value()));
        assertNull(headers.get(ACCESS_CONTROL_MAX_AGE.value()));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_METHODS.value()));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_HEADERS.value()));
    }

    private Map<String, String> makeRequest(String method, Map<String, String> headers) throws IOException {
        return makeRequest(corsCheck.getUri().toString(), method, headers);
    }

    private Map<String, String> makeRequest(String uri, String method, Map<String, String> headers) throws IOException {
        URL url = new URL(uri);
        Socket socket = new Socket(url.getHost(), url.getPort());

        PrintWriter writer = new PrintWriter(new BufferedOutputStream(socket.getOutputStream()));
        writer.write(method + " " + url.getPath() + " HTTP/1.1\r\n");
        for (String key : headers.keySet()) {
            writer.write(key + ": " + headers.get(key) + "\r\n");
        }
        writer.write("Accept: text/html\r\n");
        writer.write("User-Agent: Me\r\n");
        writer.write("Connection: keep-alive\r\n");
        writer.write("Host: 127.0.0.1:" + url.getPort() + " \r\n");
        writer.write("\n\n");
        writer.flush();

        Map<String, String> responseHeaders = new HashMap<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            if ("".equals(line)) {
                socket.close();
                break;
            }
            int pos = line.indexOf(':');
            if (pos > -1) {
                String key = line.substring(0, pos).trim();
                String value = line.substring(pos + 1).trim();
                responseHeaders.put(key, value);
            }
        }

        return responseHeaders;
    }
}
