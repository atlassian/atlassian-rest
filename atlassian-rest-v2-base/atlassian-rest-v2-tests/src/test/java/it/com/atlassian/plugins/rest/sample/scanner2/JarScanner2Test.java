package it.com.atlassian.plugins.rest.sample.scanner2;

import java.net.URI;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.client.ClientBuilder;

import org.junit.Test;

import com.atlassian.plugins.rest.sample.scanner2.BundledClass;
import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

/**
 * Testing {@link BundledClass}
 */
public class JarScanner2Test {

    private static final URI BUNDLE_URI =
            UriBuilder.create().path("rest").path("sc2").path("1").build();

    @Test
    public void testGetBaseClassDoesntExist() {
        NotFoundException actualException = assertThrows(NotFoundException.class, () -> {
            String result = ClientBuilder.newClient()
                    .target(BUNDLE_URI)
                    .path("base")
                    .request()
                    .get(String.class);
        });

        assertTrue(actualException.getMessage().contains("404 Not Found"));
    }

    @Test
    public void testGetBundledClassExists() {
        String result = ClientBuilder.newClient()
                .target(BUNDLE_URI)
                .path("bundle")
                .request()
                .get(String.class);

        assertEquals("This is a bundled class", result);
    }
}
