package it.com.atlassian.plugins.rest.sample.v2.expansion.indexedexpanding;

import java.net.URI;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.plugins.rest.sample.v2.expansion.indexedexpanding.entity.ParentEntity;
import com.atlassian.plugins.rest.v2.expand.resolver.ListEntityExpanderResolver;
import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Test for List expanding functionality that can be resolved by {@link  ListEntityExpanderResolver}.
 */
public class IndexExpandingTest {
    private WebTarget webTarget;

    @Before
    public void setUp() {
        URI baseUrl =
                UriBuilder.create().path("rest").path("expansion").path("1").build();
        webTarget = ClientBuilder.newClient().target(baseUrl);
    }

    @Test
    public void indexedExpandingFieldNotExpanded() {
        ParentEntity parentEntity = webTarget.path("indexedexpanding").request().get(ParentEntity.class);

        assertNotNull(parentEntity);
        assertNotNull(parentEntity.getCustomListEntity());
        assertNull(parentEntity.getCustomListEntity().getItems());
    }

    @Test
    public void indexedExpandingFieldGetsFullExpanded() {
        ParentEntity parentEntity = webTarget
                .path("indexedexpanding")
                .queryParam("expand", "customListEntity")
                .request()
                .get(ParentEntity.class);

        assertNotNull(parentEntity);
        assertNotNull(parentEntity.getCustomListEntity());
        assertNotNull(parentEntity.getCustomListEntity().getItems());
        assertEquals(10, parentEntity.getCustomListEntity().getItems().size());
    }

    @Test
    public void indexedExpandingFieldGetsLastExpanded() {
        ParentEntity parentEntity = webTarget
                .path("indexedexpanding")
                .queryParam("expand", "customListEntity[-1]")
                .request()
                .get(ParentEntity.class);

        assertNotNull(parentEntity);
        assertNotNull(parentEntity.getCustomListEntity());
        assertNotNull(parentEntity.getCustomListEntity().getItems());
        assertEquals(1, parentEntity.getCustomListEntity().getItems().size());
        assertEquals(
                Integer.valueOf(9),
                parentEntity.getCustomListEntity().getItems().stream()
                        .findFirst()
                        .get());
    }

    @Test
    public void indexedExpandingFieldGetsPartialExpanded() {
        ParentEntity parentEntity = webTarget
                .path("indexedexpanding")
                .queryParam("expand", "customListEntity[5:]")
                .request()
                .get(ParentEntity.class);

        assertNotNull(parentEntity);
        assertNotNull(parentEntity.getCustomListEntity());
        assertNotNull(parentEntity.getCustomListEntity().getItems());
        assertEquals(5, parentEntity.getCustomListEntity().getItems().size());
    }

    @Test
    public void indexedExpandingFieldGetsRangeExpanded() {
        ParentEntity parentEntity = webTarget
                .path("indexedexpanding")
                .queryParam("expand", "customListEntity[1:4]")
                .request()
                .get(ParentEntity.class);

        assertNotNull(parentEntity);
        assertNotNull(parentEntity.getCustomListEntity());
        assertNotNull(parentEntity.getCustomListEntity().getItems());
        assertEquals(4, parentEntity.getCustomListEntity().getItems().size());
    }
}
