package it.com.atlassian.plugins.rest.sample.v2.marshalling.requestcontext;

import java.net.URI;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;

import org.junit.Test;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;

/**
 * {@code RequestContextTest} showcases how request context can be accessed by resource with use of {@code @Context}
 * annotation.
 */
public class RequestContextTest {

    @Test
    public void testIfResponseEntityReturnsRequestHeaders() {
        URI baseUri =
                UriBuilder.create().path("rest").path("marshalling").path("1").build();
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(baseUri).path("requestContext");
        String responsePojoString = target.request()
                .header("Custom-Header", "Hello World")
                .header("Content-Type", MediaType.APPLICATION_XML)
                .accept(MediaType.APPLICATION_JSON)
                .get(String.class);
        assertEquals(
                "{\"contentType\":\"application/xml\",\"accept\":\"application/json\",\"custom\":\"Hello World\"}",
                responsePojoString);
        client.close();
    }
}
