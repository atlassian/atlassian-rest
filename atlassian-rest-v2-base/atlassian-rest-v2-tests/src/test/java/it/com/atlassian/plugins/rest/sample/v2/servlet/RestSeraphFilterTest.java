package it.com.atlassian.plugins.rest.sample.v2.servlet;

import java.net.URI;
import java.util.List;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.Cookie;
import jakarta.ws.rs.core.Response;

import org.apache.commons.httpclient.HttpStatus;
import org.junit.Test;

import com.atlassian.plugins.rest.v2.servlet.RestSeraphFilter;
import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class RestSeraphFilterTest {
    private static final URI BASE_URI =
            UriBuilder.create().path("rest").path("servlet").path("1").build();

    @Test
    public void testDefaultAuthTypeAttribute() {
        Response response = ClientBuilder.newClient()
                .target(BASE_URI)
                .path("servlet")
                .path("defaultAuthType")
                .request()
                .get();

        List<Object> authType = response.getHeaders().get(RestSeraphFilter.DEFAULT_ATTRIBUTE);

        assertEquals(200, response.getStatus());
        assertEquals("any", authType.get(0));
    }

    @Test
    public void testAuthTypeWhenInvalidSessionIsPresent() {
        Cookie invalidSession = new Cookie("JSESSIONID", "invalid_session_79BFFD19B79BC312");

        Response response = ClientBuilder.newClient()
                .target(BASE_URI)
                .path("servlet")
                .path("defaultAuthType")
                .request()
                .cookie(invalidSession)
                .get();

        List<Object> authType = response.getHeaders().get(RestSeraphFilter.DEFAULT_ATTRIBUTE);

        assertEquals(HttpStatus.SC_UNAUTHORIZED, response.getStatus());
        assertNull(authType);
    }
}
