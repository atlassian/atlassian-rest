package it.com.atlassian.plugins.rest.sample.v2.marshalling.extensions;

import java.net.URI;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;

import org.junit.Test;

import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

public class ResourceExtensionsTest {
    final URI baseUri =
            UriBuilder.create().path("rest").path("marshalling").path("1").build();
    final Client client = ClientBuilder.newClient();

    @Test
    public void testIfJsonUriSuffixReturnsJsonResponse() {
        final WebTarget target = client.target(baseUri).path("entity.json");
        assertEquals(
                "{\"description\":\"Example returned JaxbPojo\",\"value\":123}",
                target.request().get(String.class));
    }

    @Test
    public void testIfXmlUriSuffixReturnsXmlResponse() {
        final WebTarget target = client.target(baseUri).path("entity.xml");
        String jaxbXmlPojo = target.request().get(String.class);
        assertTrue(jaxbXmlPojo.contains("<jaxbPojo description=\"Example returned JaxbPojo\" value=\"123\"/>"));
    }

    @Test
    public void testIfExcludedResourceWithoutExtensionReturnsJsonResponse() {
        final WebTarget target = client.target(baseUri).path("excluded");
        assertEquals(
                "{\"xmlAttributeAnnotatedField\":\"Hello\"}", target.request().get(String.class));
    }

    @Test
    public void testIfExcludedResourceWithExtensionReturns404Exception() {
        final WebTarget target = client.target(baseUri).path("excluded.json");
        assertThrows(NotFoundException.class, () -> target.request().get(String.class));
    }
}
