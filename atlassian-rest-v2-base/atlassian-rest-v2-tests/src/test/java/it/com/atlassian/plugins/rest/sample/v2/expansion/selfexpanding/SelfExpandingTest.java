package it.com.atlassian.plugins.rest.sample.v2.expansion.selfexpanding;

import java.net.URI;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.plugins.rest.sample.v2.expansion.selfexpanding.SelfExpandingExampleEntity;
import com.atlassian.plugins.rest.v2.utils.UriBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Test for self expanding functionality.
 */
public class SelfExpandingTest {
    private WebTarget webTarget;

    @Before
    public void setUp() {
        URI baseUrl =
                UriBuilder.create().path("rest").path("expansion").path("1").build();
        webTarget = ClientBuilder.newClient().target(baseUrl);
    }

    @Test
    public void selfExpandingFieldNotExpanded() {
        SelfExpandingExampleEntity selfExpandingExampleEntity =
                webTarget.path("selfexpanding").request().get(SelfExpandingExampleEntity.class);

        assertTrue(selfExpandingExampleEntity.getSelfExpandingList().isEmpty());
    }

    @Test
    public void selfExpandingFieldGetsExpanded() {
        SelfExpandingExampleEntity selfExpandingExampleEntity = webTarget
                .path("selfexpanding")
                .queryParam("expand", "selfExpanding")
                .request()
                .get(SelfExpandingExampleEntity.class);

        assertEquals(
                SelfExpandingExampleEntity.SELF_EXPANDING_STRINGS, selfExpandingExampleEntity.getSelfExpandingList());
    }
}
