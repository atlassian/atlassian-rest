package com.atlassian.plugins.rest.v2.utils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class UriBuilder {
    private static final String CONTEXT_SYSTEM_PROPERTY = "context.path";
    private static final String HTTP_PORT_SYSTEM_PROPERTY = "http.port";
    private static final int DEFAULT_HTTP_PORT = 0;

    private static final String CONTEXT;
    private static final int HTTP_PORT;

    static {
        CONTEXT = System.getProperty(CONTEXT_SYSTEM_PROPERTY);
        HTTP_PORT = Integer.parseInt(System.getProperty(HTTP_PORT_SYSTEM_PROPERTY, String.valueOf(DEFAULT_HTTP_PORT)));

        if (CONTEXT == null || HTTP_PORT == DEFAULT_HTTP_PORT) {
            throw new IllegalStateException(CONTEXT_SYSTEM_PROPERTY + " or " + HTTP_PORT_SYSTEM_PROPERTY + " not set!");
        }
    }

    private final List<String> paths = new LinkedList<>();
    private final Map<String, String> params = new HashMap<>();

    private UriBuilder() {}

    public static UriBuilder create() {
        return new UriBuilder();
    }

    public UriBuilder path(String path) {
        paths.add(path);
        return this;
    }

    public URI build() {
        try {
            String query = buildQuery();
            return new URI(
                    "http", null, "localhost", HTTP_PORT, buildPath(), query.isEmpty() ? null : query, buildFragment());
        } catch (URISyntaxException e) {
            // will not happen
            return null;
        }
    }

    private String buildPath() {
        final StringBuilder path = new StringBuilder(paths.size() * 8);
        if (StringUtils.isNotBlank(CONTEXT)) {
            path.append(CONTEXT).append("/");
        }
        for (String element : paths) {
            path.append(element).append("/");
        }
        if (path.charAt(0) != '/') {
            path.insert(0, '/');
        }
        return path.toString();
    }

    private String buildQuery() {
        if (params.isEmpty()) {
            return "";
        }
        final StringBuilder query = new StringBuilder();
        for (Iterator<Map.Entry<String, String>> iterator = params.entrySet().iterator(); iterator.hasNext(); ) {
            Map.Entry<String, String> entry = iterator.next();
            query.append(entry.getKey());
            query.append("=");
            query.append(entry.getValue());
            if (iterator.hasNext()) query.append("&");
        }
        return query.toString();
    }

    private String buildFragment() {
        return null;
    }
}
