package com.atlassian.plugins.rest.api.internal.expand.resolver;

import com.atlassian.plugins.rest.api.expand.expander.EntityExpander;
import com.atlassian.plugins.rest.api.expand.listwrapper.AbstractPagedListWrapper;
import com.atlassian.plugins.rest.api.expand.listwrapper.ListWrapper;
import com.atlassian.plugins.rest.api.expand.resolver.EntityExpanderResolver;

/**
 * An {@link EntityExpanderResolver EntityExpanderResolver} that binds an {@link EntityExpander EntityExpander}
 * to the entity that implements {@link ListWrapper ListWrapper} or extends the
 * {@link AbstractPagedListWrapper}
 */
public interface ListWrapperEntityExpanderResolver extends EntityExpanderResolver {}
