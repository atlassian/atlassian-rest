package com.atlassian.plugins.rest.api.internal.expand.exception;

public class ExpandIndexException extends RuntimeException {
    public ExpandIndexException(Throwable cause) {
        super(cause);
    }

    public ExpandIndexException(String message) {
        super(message);
    }
}
