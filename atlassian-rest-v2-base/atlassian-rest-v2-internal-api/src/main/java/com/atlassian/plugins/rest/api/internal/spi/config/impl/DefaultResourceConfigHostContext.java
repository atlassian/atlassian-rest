package com.atlassian.plugins.rest.api.internal.spi.config.impl;

import java.util.Set;

import com.atlassian.plugins.rest.api.internal.spi.config.ResourceConfigHostContext;

import static java.util.Collections.emptySet;

/**
 * Default implementation of {@code ResourceConfigHostContext}. Does not specify any additional objects
 * to be injected into {@code org.glassfish.jersey.server.ResourceConfig}
 */
public final class DefaultResourceConfigHostContext implements ResourceConfigHostContext {
    public Set<Object> hostProvidedRestContext() {
        return emptySet();
    }
}
