package com.atlassian.plugins.rest.api.internal.expand.listwrapper;

import java.util.LinkedList;
import java.util.List;

import com.atlassian.plugins.rest.api.expand.listwrapper.ListWrapperCallback;

/**
 * Utility class for {@link ListWrapperCallback}.
 */
public class ListWrapperCallBacks {
    public static <T> ListWrapperCallback<T> identity(final List<T> items) {
        return indexes -> items;
    }

    public static <T> ListWrapperCallback<T> ofList(final List<T> items) {
        return ofList(items, Integer.MAX_VALUE);
    }

    /**
     * Returns a range of items to be expanded from the beginning index to maxResults.
     * @param items elements of the list to be expanded
     * @param maxResults predefined limit for elements to be expanded
     * @return subList of elements to be expanded
     * @param <T> custom entity
     */
    public static <T> ListWrapperCallback<T> ofList(final List<T> items, final int maxResults) {
        return indexes -> {
            final List<T> toReturn = new LinkedList<>();
            for (Integer i : indexes.getIndexes(items.size())) {
                if (i < items.size()) {
                    toReturn.add(items.get(i));
                }
                if (toReturn.size() == maxResults) {
                    break;
                }
            }
            return toReturn;
        };
    }
}
