package com.atlassian.plugins.rest.api.internal.spi.config;

import java.util.Set;

/**
 * Interface exists to allow Host Applications to inject objects such as response handlers into
 * {@code org.glassfish.jersey.server.ResourceConfig} during its construction.
 * <p>
 * Host products may do so by exporting an implementation of this interface (atlassian internal visibility scoped)
 */
public interface ResourceConfigHostContext {
    Set<Object> hostProvidedRestContext();
}
