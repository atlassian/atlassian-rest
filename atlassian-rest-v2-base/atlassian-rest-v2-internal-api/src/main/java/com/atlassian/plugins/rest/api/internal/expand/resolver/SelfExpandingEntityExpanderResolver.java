package com.atlassian.plugins.rest.api.internal.expand.resolver;

import com.atlassian.plugins.rest.api.expand.expander.SelfExpanding;
import com.atlassian.plugins.rest.api.expand.resolver.EntityExpanderResolver;

/**
 * An {@code EntityExpanderResolver} which binds {@code SelfExpandingExpander} to the expanded entity.
 * It's recommended to use {@link SelfExpanding} for expansion instead of instantiating the {@code SelfExpandingEntityExpanderResolver}.
 */
public interface SelfExpandingEntityExpanderResolver extends EntityExpanderResolver {}
