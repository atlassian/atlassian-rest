package com.atlassian.plugins.rest.api.expand.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Annotation for the expanded entity method, in which user can define their expansion logic for
 * list entities with indexing.
 * The method needs to be of type void and have exactly one parameter of type Indexes, i.e.:
 * <pre>
 * &#064;ExpandConstraint
 * public void expandList(Indexes indexes){
 *       // define logic of elements retrieval
 * }
 * </pre>
 */
@Retention(RUNTIME)
@Target(METHOD)
@Documented
public @interface IndexedExpand {}
