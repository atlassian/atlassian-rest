package com.atlassian.plugins.rest.api.security.exception;

/**
 * Exception thrown when a client tries to access a resources that requires authentication and the client is not authenticated.
 *
 * @see com.atlassian.plugins.rest.api.security.annotation.AnonymousSiteAccess
 */
public class AuthenticationRequiredException extends AccessDeniedException {
    public AuthenticationRequiredException() {
        super("Client must be authenticated to access this resource.", UNAUTHORIZED);
    }
}
