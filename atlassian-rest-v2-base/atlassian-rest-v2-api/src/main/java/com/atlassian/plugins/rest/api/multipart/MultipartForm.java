package com.atlassian.plugins.rest.api.multipart;

import java.util.Collection;
import java.util.Set;

/**
 * A parsed multipart form
 *
 * @since 2.4
 */
public interface MultipartForm {
    /**
     * Get the first file part for the given field name
     *
     * @param field The field name
     * @return The first file part, or null if none was found
     */
    FilePart getFilePart(String field);

    /**
     * Gets file parts for the given field name
     *
     * @param field The field name
     * @return All file parts, or empty collection if none were found
     */
    Collection<FilePart> getFileParts(String field);

    /**
     * Get all form field names
     *
     * @since 8.3
     */
    Set<String> getFieldNames();
}
