package com.atlassian.plugins.rest.api.util;

import java.net.URI;
import java.util.function.Consumer;

/**
 * @since 2.2
 */
public interface RestUrlBuilder {

    /**
     * Generates {@link URI} to the REST resource. Path is constructed from the `baseUrl`,
     * class and method {@link jakarta.ws.rs.Path} including {@link jakarta.ws.rs.PathParam} passed to the method as arguments.
     *
     * @param baseUri       used to generate URI to the resource
     * @param resourceClass class representing REST resource
     * @param consumer      lambda calling resource method to which {@link URI} will be generated
     * @return {@link URI} to the provided resource
     */
    <T> URI getUrlFor(URI baseUri, Class<T> resourceClass, Consumer<T> consumer);
}
