package com.atlassian.plugins.rest.api.deprecation.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks the annotated method as deprecated and adds 'Deprecation' HTTP Header.
 * <p>
 * The annotation should support:
 * - since ISO-9601 date
 * - an optional link
 * <p>
 * The ultimate output is going to be HTTP headers, such as
 * Deprecation: @1688169599
 * Link: <<a href="https://developer.example.com/deprecation">...</a>>;
 * rel="deprecation"; type="text/html"
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.PACKAGE})
public @interface DeprecatedEndpoint {
    /**
     * Returns the date (in ISO-8601 date format) in which the annotated element became deprecated.
     * If correct format isn't passed(in case of DateTimeParseException), It will send @0 default value which is 1970-01-01T00:00:00Z
     *
     * @return the ISO-8601 date string (eg. 2023-04-26T16:00:00Z)
     * @since 8.1.0
     */
    String since();

    /**
     * Returns the link to provide more information about deprecation and it's alternative of usages.
     * The default value is the empty string.
     *
     * @return the link string
     * @since 8.1.0
     */
    String link() default "";

    /**
     * Returns the type of the link to automate the reading or parsing mechanism
     * The default value is the empty string.
     *
     * @return the linkType string
     * @since 8.1.0
     */
    String linkType() default "text/html";
}
