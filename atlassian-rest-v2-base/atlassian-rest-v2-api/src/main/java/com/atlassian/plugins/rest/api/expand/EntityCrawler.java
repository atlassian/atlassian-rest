package com.atlassian.plugins.rest.api.expand;

import com.atlassian.plugins.rest.api.expand.parameter.ExpandParameter;
import com.atlassian.plugins.rest.api.expand.resolver.EntityExpanderResolver;

/**
 * Crawls into expandable entity and expands fields defined in ExpandParameter as to be expanded. The {@code EntityCrawler}
 * is used by {@code ExpandFilter} which is run internally in the REST plugin. Therefore, it does not need to be implemented
 * in any other plugin.
 */
public interface EntityCrawler {

    void crawl(Object entity, ExpandParameter expandParameter, EntityExpanderResolver expanderResolver);
}
