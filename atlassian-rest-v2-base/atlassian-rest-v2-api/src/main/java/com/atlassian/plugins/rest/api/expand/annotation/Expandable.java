package com.atlassian.plugins.rest.api.expand.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * This is an annotation to design field on REST resources that can be expanded.
 */
@Retention(RUNTIME)
@Target(FIELD)
@Documented
public @interface Expandable {
    /**
     * The value to match with the expansion parameter to determine whether the given field should be expanded.
     *
     * @return the parameter value to match for expansion.
     */
    String value() default "";
}
