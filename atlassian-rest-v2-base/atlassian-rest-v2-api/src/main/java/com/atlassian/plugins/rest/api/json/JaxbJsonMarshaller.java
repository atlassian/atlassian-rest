package com.atlassian.plugins.rest.api.json;

/**
 * Utility service that will allow clients to marshall a Jackson or Jaxb bean to Json using the same configuration that the REST
 * module uses internally to create Json.
 *
 * @since v1.0.2
 */
public interface JaxbJsonMarshaller {
    /**
     * Given a jaxbBean this method will return a JSON string.
     *
     * @param jaxbBean the bean to be converted to JSON
     * @return a JSON string
     * @throws JsonMarshallingException Thrown if an error occurs during the conversion
     * @since 1.1
     */
    String marshal(Object jaxbBean) throws JsonMarshallingException;
}
