package com.atlassian.plugins.rest.api.expand.expander;

/**
 * Represents a JAXB rest data object that is expandable and is capable of
 * expanding itself. It does not need an accompanying
 * {@link EntityExpander}.
 * In order to use it, the expanded entity can implement {@code SelfExpanding}
 * or instantiate the {@code SelfExpanding} as a field object.
 *
 * @author Erik van Zijst
 * @since v1.0.7
 */
public interface SelfExpanding {
    /**
     * Instructs the self-expanding rest data entity to expand itself.
     */
    void expand();
}
