package com.atlassian.plugins.rest.api.security.exception;

/**
 * An authorization exception indicates that a user cannot access the resource.
 */
public class AuthorizationException extends AccessDeniedException {

    public AuthorizationException(final String message) {
        super(message, FORBIDDEN);
    }
}
