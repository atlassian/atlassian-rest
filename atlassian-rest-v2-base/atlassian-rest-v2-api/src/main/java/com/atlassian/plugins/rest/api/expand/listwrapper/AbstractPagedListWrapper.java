package com.atlassian.plugins.rest.api.expand.listwrapper;

import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;

import static jakarta.xml.bind.annotation.XmlAccessType.FIELD;

/**
 * An abstract list wrapper that provides support for indexing information:
 * <ul>
 * <li>size</li>
 * <li>max-results</li>
 * <li>start-index</li>
 * </ul>
 *
 * @param <T> the type of element in the list to wrap.
 */
@XmlRootElement
@XmlAccessorType(FIELD)
public abstract class AbstractPagedListWrapper<T> implements ListWrapper<T> {
    @XmlAttribute
    private final int size;

    @XmlAttribute(name = "max-results")
    private final int maxResults;

    @XmlAttribute(name = "start-index")
    private Integer startIndex;

    // this is for JAXB
    private AbstractPagedListWrapper() {
        size = 0;
        maxResults = 0;
    }

    protected AbstractPagedListWrapper(int size, int maxResults) {
        this.size = size;
        this.maxResults = maxResults;
    }

    public Integer getStartIndex() {
        return startIndex;
    }

    public int getSize() {
        return size;
    }

    public int getMaxResults() {
        return maxResults;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public final ListWrapperCallback<T> getCallback() {
        return indexes -> {
            final int minIndex = indexes.getMinIndex(size);
            if (minIndex != -1) {
                setStartIndex(minIndex);
            }
            return getPagingCallback().getItems(indexes);
        };
    }

    /**
     * Gets the call back that will actually "retrieve" the necessary element to populate the List. Size, index and max
     * result attributes are already set during instantiation of the expanded entity. Once the callback is called, the
     * attributes will be already initialised.
     *
     * @return the call back that does all the work.
     */
    public abstract ListWrapperCallback<T> getPagingCallback();
}
