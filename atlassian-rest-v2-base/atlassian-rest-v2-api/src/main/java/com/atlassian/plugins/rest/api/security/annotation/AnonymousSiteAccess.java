package com.atlassian.plugins.rest.api.security.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>Permits resource access if at least ONE of the following criteria is met:</p>
 * <ul>
 * <li>Is current user UNauthenticated AND is anonymous access enabled for site</li>
 * <li>Is current user authenticated AND is limited unlicensed access enabled for site</li>
 * <li>Is current user authenticated AND assigned a product license</li>
 * </ul>
 *
 * @since 6.3.0
 */
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.PACKAGE})
@Retention(RetentionPolicy.RUNTIME)
public @interface AnonymousSiteAccess {}
