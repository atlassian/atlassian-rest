package com.atlassian.plugins.rest.api.security.exception;

import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;

/**
 * Thrown when an XSRF check fails
 *
 * @since 2.4
 */
public class XsrfCheckFailedException extends WebApplicationException {
    public XsrfCheckFailedException() {
        this(Response.Status.FORBIDDEN);
    }

    public XsrfCheckFailedException(Response.Status status) {
        super(Response.status(status).entity("XSRF check failed").build());
    }
}
