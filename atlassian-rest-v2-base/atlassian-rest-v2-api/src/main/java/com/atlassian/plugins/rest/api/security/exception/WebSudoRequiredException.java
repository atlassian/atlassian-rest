package com.atlassian.plugins.rest.api.security.exception;

public final class WebSudoRequiredException extends AccessDeniedException {
    public WebSudoRequiredException(final String message) {
        super(message, UNAUTHORIZED);
    }
}
