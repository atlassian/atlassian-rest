package com.atlassian.plugins.rest.api.multipart;

/**
 * Configuration for multipart config
 *
 * @since 2.4
 */
public interface MultipartConfig {
    long NO_LIMIT = -1;
    long DEFAULT_REQUEST_PART_LIMIT = 1000;

    /**
     * Get the max file size, where -1 indicates no limit
     *
     * @return max file size
     */
    default long getMaxFileSize() {
        return NO_LIMIT;
    }

    /**
     * Get the max request size, where -1 indicates no limit
     *
     * @return max request size
     */
    default long getMaxSize() {
        return NO_LIMIT;
    }

    /**
     * Get the max file count, where -1 indicates no limit
     *
     * @return max file count
     */
    default long getMaxFileCount() {
        return DEFAULT_REQUEST_PART_LIMIT;
    }
}
