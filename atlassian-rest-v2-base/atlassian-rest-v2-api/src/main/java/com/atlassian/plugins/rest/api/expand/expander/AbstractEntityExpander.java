package com.atlassian.plugins.rest.api.expand.expander;

import com.atlassian.plugins.rest.api.expand.EntityCrawler;
import com.atlassian.plugins.rest.api.expand.ExpandContext;
import com.atlassian.plugins.rest.api.expand.resolver.EntityExpanderResolver;

/**
 * Abstract class for entity expansion that can perform basic and recursive expansion.
 */
public abstract class AbstractEntityExpander<T> implements EntityExpander<T> {
    public final T expand(
            ExpandContext<T> context, EntityExpanderResolver expanderResolver, EntityCrawler entityCrawler) {
        final T expandedEntity = expandInternal(context.getEntity());
        if (!context.getEntityExpandParameter().isEmpty()) {
            entityCrawler.crawl(
                    expandedEntity,
                    context.getEntityExpandParameter().getExpandParameter(context.getExpandable()),
                    expanderResolver);
        }
        return expandedEntity;
    }

    protected abstract T expandInternal(T entity);
}
