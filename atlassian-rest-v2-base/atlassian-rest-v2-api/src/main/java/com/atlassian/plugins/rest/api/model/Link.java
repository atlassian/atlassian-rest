package com.atlassian.plugins.rest.api.model;

import java.net.URI;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;

import static java.util.Objects.requireNonNull;

/**
 * Represents a link to a given entity. The {@link #href}
 *
 * @since 1.0
 */
@XmlRootElement
public class Link {
    @XmlAttribute
    private final URI href;

    @XmlAttribute
    private final String type;

    @XmlAttribute
    private final String rel;

    // For JAXB's usage
    private Link() {
        this.href = null;
        this.rel = null;
        this.type = null;
    }

    private Link(URI href, String rel, String type) {
        this.href = requireNonNull(href);
        this.rel = requireNonNull(rel);
        this.type = type;
    }

    /**
     * Creates a link using the specified uri and rel values
     *
     * @param uri the {@link URI} to use for the link URI,
     * @param rel the rel attribute of the link.
     * @return a link
     */
    public static Link link(URI uri, String rel) {
        return new Link(uri, rel, null);
    }

    /**
     * Creates a link using the given URI builder to build the URI.
     *
     * @param uri  the {@link URI} to use for the link URI,
     * @param rel  the rel attribute of the link.
     * @param type the type attribute of the link.
     * @return a link
     */
    public static Link link(URI uri, String rel, String type) {
        return new Link(uri, rel, type);
    }

    /**
     * Creates a link using the given URI builder to build the URI.
     * The {@code rel} attribute of the link is set to {@code self}.
     *
     * @param uri the {@link URI} to use for the link URI.
     * @return a link
     */
    public static Link self(URI uri) {
        return link(uri, "self");
    }

    /**
     * Creates a link using the given URI builder to build the URI.
     * The {@code rel} attribute of the link is set to {@code edit}.
     *
     * @param uri the {@link URI} to use for the link URI.
     * @return a link
     */
    public static Link edit(URI uri) {
        return link(uri, "edit");
    }

    /**
     * Creates a link using the given URI builder to build the URI.
     * The {@code rel} attribute of the link is set to {@code add}.
     *
     * @param uri the {@link URI} to use for the link URI.
     * @return a link
     */
    public static Link add(URI uri) {
        return link(uri, "add");
    }

    /**
     * Creates a link using the given URI builder to build the URI.
     * The {@code rel} attribute of the link is set to {@code delete}.
     *
     * @param uri the {@link URI} to use for the link URI.
     * @return a link
     */
    public static Link delete(URI uri) {
        return link(uri, "delete");
    }

    public URI getHref() {
        return href;
    }

    public String getRel() {
        return rel;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((href == null) ? 0 : href.hashCode());
        result = prime * result + ((rel == null) ? 0 : rel.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        Link other = (Link) obj;
        if (href == null) {
            if (other.href != null) {
                return false;
            }
        } else if (!href.equals(other.href)) {
            return false;
        }

        if (rel == null) {
            return other.rel == null;
        } else {
            return rel.equals(other.rel);
        }
    }

    @Override
    public String toString() {
        return "Link{" + "href=" + href + ", type='" + type + '\'' + ", rel='" + rel + '\'' + '}';
    }
}
