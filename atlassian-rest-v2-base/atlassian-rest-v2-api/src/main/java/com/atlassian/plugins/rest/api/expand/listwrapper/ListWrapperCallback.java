package com.atlassian.plugins.rest.api.expand.listwrapper;

import java.util.List;

import com.atlassian.plugins.rest.api.expand.parameter.Indexes;

/**
 * Callback function of a {@link ListWrapper}.
 * @param <T>
 */
public interface ListWrapperCallback<T> {
    /**
     * Retrieves all items of the expanded entity.
     * @param indexes - a range of elements that are to be retrieved for later expansion
     * @return List of items to expand
     */
    List<T> getItems(Indexes indexes);
}
