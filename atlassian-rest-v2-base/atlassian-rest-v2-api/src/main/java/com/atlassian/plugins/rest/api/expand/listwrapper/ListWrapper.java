package com.atlassian.plugins.rest.api.expand.listwrapper;

/**
 * Expander interface which provides functionality for expanding Lists with indexing.
 * Consider using {@link AbstractPagedListWrapper} for any list expansion with indexing.
 * @param <T>
 */
public interface ListWrapper<T> {
    ListWrapperCallback<T> getCallback();
}
