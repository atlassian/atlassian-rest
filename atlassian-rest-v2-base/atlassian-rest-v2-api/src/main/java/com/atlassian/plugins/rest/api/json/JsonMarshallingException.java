package com.atlassian.plugins.rest.api.json;

/**
 * Wrapper for the {@link java.io.IOException} in the {@link JaxbJsonMarshaller#marshal(Object)} method.
 */
public class JsonMarshallingException extends RuntimeException {

    public JsonMarshallingException() {}

    public JsonMarshallingException(String message) {
        super(message);
    }

    public JsonMarshallingException(String message, Throwable cause) {
        super(message, cause);
    }

    public JsonMarshallingException(Throwable cause) {
        super(cause);
    }
}
