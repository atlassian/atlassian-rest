package com.atlassian.plugins.rest.api.model;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import jakarta.ws.rs.core.CacheControl;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Variant;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
import jakarta.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import static jakarta.ws.rs.core.MediaType.APPLICATION_XML;
import static java.util.Objects.requireNonNull;

/**
 * Status entity for those responses that don't have any other entity body.
 */
@XmlRootElement
public class Status {
    /**
     * This is the plugin that exposes the REST api
     */
    @XmlElement
    private final Plugin plugin;

    /**
     * The HTTP response code, 200 for ok, 404 for not found, etc.
     * See <a href="http://www.w3.org/Protocols/rfc2616/rfc2616.html">RFC 2616</a> for more information on those response codes.
     */
    @XmlElement(name = "status-code")
    private final Integer code;

    /**
     * The plugin specific response code. This is used to differentiate possible error code for example.
     */
    @XmlElement(name = "sub-code")
    private final Integer subCode;

    /**
     * A humane readable message for the given status.
     */
    @XmlElement
    private final String message;

    /**
     * the eTag.
     * <p>
     * See <a href="http://www.w3.org/Protocols/rfc2616/rfc2616.html">RFC 2616</a> for more information about ETag.
     */
    @XmlElement(name = "etag")
    private final String eTag;

    /**
     * Resource that have been updated during this request.
     */
    @XmlElementWrapper(name = "resources-created")
    @XmlElement(name = "link")
    @JsonProperty(value = "resources-created")
    private final Collection<Link> resourcesCreated;

    /**
     * Resource that have been updated during this request.
     */
    @XmlElementWrapper(name = "resources-updated")
    @XmlElement(name = "link")
    @JsonProperty(value = "resources-updated")
    private final Collection<Link> resourcesUpdated;

    // For JAXB's usage
    private Status() {
        this.plugin = null;
        this.code = -1;
        this.subCode = -1;
        this.message = null;
        this.eTag = null;
        this.resourcesCreated = null;
        this.resourcesUpdated = null;
    }

    private Status(
            Plugin plugin,
            Integer code,
            Integer subCode,
            String message,
            String eTag,
            Collection<Link> resourcesCreated,
            Collection<Link> resourcesUpdated) {
        this.plugin = plugin;
        this.code = code;
        this.subCode = subCode;
        this.message = message;
        this.eTag = eTag;
        this.resourcesCreated = resourcesCreated;
        this.resourcesUpdated = resourcesUpdated;
    }

    public static StatusResponseBuilder fromStatusCode(int statusCode) {
        return new StatusResponseBuilder(Response.Status.fromStatusCode(statusCode));
    }

    public static StatusResponseBuilder ok() {
        return new StatusResponseBuilder(Response.Status.OK);
    }

    public static StatusResponseBuilder notFound() {
        return new StatusResponseBuilder(Response.Status.NOT_FOUND);
    }

    public static StatusResponseBuilder error() {
        // errors are not cached
        return new StatusResponseBuilder(Response.Status.INTERNAL_SERVER_ERROR)
                .noCache()
                .noStore();
    }

    public static StatusResponseBuilder badRequest() {
        // errors are not cached
        return new StatusResponseBuilder(Response.Status.BAD_REQUEST).noCache().noStore();
    }

    public static StatusResponseBuilder forbidden() {
        return new StatusResponseBuilder(Response.Status.FORBIDDEN);
    }

    public static StatusResponseBuilder unauthorized() {
        return new StatusResponseBuilder(Response.Status.UNAUTHORIZED);
    }

    public static StatusResponseBuilder created(Link link) {
        return new StatusResponseBuilder(Response.Status.CREATED).created(requireNonNull(link));
    }

    public static StatusResponseBuilder conflict() {
        return new StatusResponseBuilder(Response.Status.CONFLICT);
    }

    public Plugin getPlugin() {
        return plugin;
    }

    public int getCode() {
        if (code != null) {
            return code;
        } else {
            return -1;
        }
    }

    public int getSubCode() {
        if (subCode != null) {
            return subCode;
        } else {
            return -1;
        }
    }

    public String getMessage() {
        return message;
    }

    public String getETag() {
        return eTag;
    }

    public Collection<Link> getResourcesCreated() {
        if (resourcesCreated == null) {
            return null;
        } else {
            return Collections.unmodifiableCollection(resourcesCreated);
        }
    }

    public Collection<Link> getResourcesUpdated() {
        if (resourcesUpdated == null) {
            return null;
        } else {
            return Collections.unmodifiableCollection(resourcesUpdated);
        }
    }

    @XmlRootElement
    public static class Plugin {
        @XmlAttribute
        private final String key;

        @XmlAttribute
        private final String version;

        // For JAXB's usage
        private Plugin() {
            this.key = null;
            this.version = null;
        }

        public Plugin(String key, String version) {
            this.key = requireNonNull(key, "key can't be null");
            this.version = requireNonNull(version, "version can't be null");
        }

        public String getKey() {
            return key;
        }

        public String getVersion() {
            return version;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((key == null) ? 0 : key.hashCode());
            result = prime * result + ((version == null) ? 0 : version.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }

            Plugin other = (Plugin) obj;
            if (key == null) {
                if (other.key != null) {
                    return false;
                }
            } else if (!key.equals(other.key)) {
                return false;
            }

            if (version == null) {
                return other.version == null;
            } else {
                return version.equals(other.version);
            }
        }
    }

    public static class StatusResponseBuilder {
        private final CacheControl cacheControl;
        private final Response.Status status;
        private String eTag;
        private Plugin plugin;
        private String message;
        private List<Link> created;
        private List<Link> updated;

        private StatusResponseBuilder(Response.Status status) {
            this(status, new CacheControl());
        }

        private StatusResponseBuilder(Response.Status status, CacheControl cacheControl) {
            this.status = requireNonNull(status, "status can't be null");
            this.cacheControl = requireNonNull(cacheControl, "cacheControl can't be null");
        }

        public StatusResponseBuilder plugin(String key, String version) {
            plugin = new Plugin(key, version);
            return this;
        }

        public StatusResponseBuilder message(String message) {
            this.message = message;
            return this;
        }

        public StatusResponseBuilder tag(String eTag) {
            this.eTag = eTag;
            return this;
        }

        public StatusResponseBuilder noCache() {
            cacheControl.setNoCache(true);
            return this;
        }

        public StatusResponseBuilder noStore() {
            cacheControl.setNoStore(true);
            return this;
        }

        public Status build() {
            return new Status(plugin, status.getStatusCode(), null, message, eTag, created, updated);
        }

        public Response response() {
            return responseBuilder().build();
        }

        public Response.ResponseBuilder responseBuilder() {
            final Response.ResponseBuilder builder = Response.status(status)
                    .cacheControl(cacheControl)
                    .tag(eTag)
                    .entity(build())
                    .type(APPLICATION_XML);

            final List<Link> c = getCreated();
            final List<Link> u = getUpdated();
            if (c.size() == 1 && u.isEmpty()) {
                builder.location(c.get(0).getHref());
            } else if (u.size() == 1 && c.isEmpty()) {
                builder.location(u.get(0).getHref());
            }
            return builder;
        }

        public StatusResponseBuilder created(final Link link) {
            getCreated().add(link);
            return this;
        }

        public StatusResponseBuilder updated(final Link link) {
            getUpdated().add(link);
            return this;
        }

        private List<Link> getCreated() {
            if (created == null) {
                created = new LinkedList<>();
            }
            return created;
        }

        private List<Link> getUpdated() {
            if (updated == null) {
                updated = new LinkedList<>();
            }
            return updated;
        }
    }

    /**
     * These are the media types that a Status can be represented as.
     */
    private static final List<Variant> POSSIBLE_VARIANTS = Variant.mediaTypes(
                    MediaType.APPLICATION_XML_TYPE, MediaType.APPLICATION_JSON_TYPE)
            .add()
            .build();

    public static MediaType variantFor(Request request) {
        Variant v = request.selectVariant(POSSIBLE_VARIANTS);
        if (v == null) {
            v = POSSIBLE_VARIANTS.get(0);
        }

        return v.getMediaType();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Status status = (Status) o;
        return Objects.equals(plugin, status.plugin)
                && Objects.equals(code, status.code)
                && Objects.equals(subCode, status.subCode)
                && Objects.equals(message, status.message)
                && Objects.equals(eTag, status.eTag)
                && Objects.equals(resourcesCreated, status.resourcesCreated)
                && Objects.equals(resourcesUpdated, status.resourcesUpdated);
    }

    @Override
    public int hashCode() {
        return Objects.hash(plugin, code, subCode, message, eTag, resourcesCreated, resourcesUpdated);
    }

    @Override
    public String toString() {
        return "Status{" + "plugin="
                + plugin + ", code="
                + code + ", subCode="
                + subCode + ", message='"
                + message + '\'' + ", eTag='"
                + eTag + '\'' + ", resourcesCreated="
                + resourcesCreated + ", resourcesUpdated="
                + resourcesUpdated + '}';
    }
}
