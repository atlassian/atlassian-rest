package com.atlassian.plugins.rest.api.expand.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.atlassian.plugins.rest.api.expand.expander.EntityExpander;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Annotation available for types to declare which is their {@link EntityExpander}
 */
@Retention(RUNTIME)
@Target(TYPE)
@Documented
public @interface Expander {
    Class<? extends EntityExpander<?>> value();
}
