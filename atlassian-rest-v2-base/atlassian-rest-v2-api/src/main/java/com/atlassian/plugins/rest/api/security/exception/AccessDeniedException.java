package com.atlassian.plugins.rest.api.security.exception;

public class AccessDeniedException extends SecurityException {

    protected static final int UNAUTHORIZED = 401;
    protected static final int FORBIDDEN = 403;

    private final int httpResponseStatusCode;

    public AccessDeniedException(String message, int httpResponseStatusCode) {
        super(message);
        this.httpResponseStatusCode = httpResponseStatusCode;
    }

    public int getHttpResponseStatusCode() {
        return httpResponseStatusCode;
    }
}
