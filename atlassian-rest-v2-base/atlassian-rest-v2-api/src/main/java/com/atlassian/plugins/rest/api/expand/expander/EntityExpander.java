package com.atlassian.plugins.rest.api.expand.expander;

import com.atlassian.plugins.rest.api.expand.EntityCrawler;
import com.atlassian.plugins.rest.api.expand.ExpandContext;
import com.atlassian.plugins.rest.api.expand.resolver.EntityExpanderResolver;

/**
 * Interface for expanding entities.
 * <p>
 * For basic and recursive expansion consider implementing {@link AbstractEntityExpander}
 *
 * @param <T> the type of entity to expand.
 */
public interface EntityExpander<T> {
    /**
     * @param context          the current entity context
     * @param entityCrawler    the entity crawler to be used for recursive expansion.
     * @param expanderResolver the resolver for finding further expander when doing recursive expansion.
     * @return the expanded entity. This can be a completely different object (of the same type). This MUST NOT be {@code null}.
     */
    T expand(ExpandContext<T> context, EntityExpanderResolver expanderResolver, EntityCrawler entityCrawler);
}
