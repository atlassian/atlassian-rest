package com.atlassian.rest.test.jar;

public class ClassWithAnnotatedMethod {
    @AnAnnotation
    public void aMethod() {
        // Empty method to test class with an annotation
    }
}
