package com.atlassian.rest.test.jar4;

import com.atlassian.rest.test.jar.AnAnnotation;

public class BundledClassWithAnnotatedMethod {
    @AnAnnotation
    public void aMethod() {
        // Empty method to test class with an annotation
    }
}
