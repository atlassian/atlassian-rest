package com.atlassian.plugins.rest.processors;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Optional;
import javax.annotation.Nonnull;
import jakarta.ws.rs.HttpMethod;
import jakarta.ws.rs.Path;

import io.swagger.v3.core.util.PathUtils;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;

import static java.util.Objects.requireNonNull;

class EnricherUtils {

    @Nonnull
    static String getRootPath(@Nonnull Class<?> clazz) {
        String rootPath =
                addLeadingSlashIfMissing(requireNonNull(clazz, "clazz").getDeclaredAnnotation(Path.class));
        // for some resources they declare the rootPath as "/" or if it ends with "/" we end up doubling up
        // as the getMethodPath will always return a string with a leading"/". Thus, if the rootPath ends with a "/"
        // we need to trim it off.
        if (rootPath.endsWith("/")) {
            return rootPath.substring(0, rootPath.length() - 1);
        }
        return rootPath;
    }

    static String addLeadingSlashIfMissing(Path path) {
        if (path == null) {
            return "";
        }

        String parsedPath = PathUtils.parsePath(path.value(), new HashMap<>());
        return parsedPath.startsWith("/") ? parsedPath : "/" + parsedPath;
    }

    @Nonnull
    static Optional<HttpMethod> getHttpMethod(@Nonnull Method method) {
        requireNonNull(method, "method");

        if (method.isAnnotationPresent(Hidden.class)) {
            return Optional.empty();
        }

        io.swagger.v3.oas.annotations.Operation operation =
                method.getAnnotation(io.swagger.v3.oas.annotations.Operation.class);
        // filter out hidden operations upfront
        if (operation != null && operation.hidden()) {
            return Optional.empty();
        }

        return Arrays.stream(method.getDeclaredAnnotations())
                .filter(a -> a.annotationType().getAnnotation(HttpMethod.class) != null)
                .findFirst()
                .map(a -> a.annotationType().getAnnotation(HttpMethod.class));
    }

    @Nonnull
    static Optional<Operation> getOperation(
            @Nonnull OpenAPI openAPI, HttpMethod httpMethod, @Nonnull String methodPath, boolean pathItemRequired) {
        requireNonNull(openAPI, "openAPI");
        requireNonNull(methodPath, "methodPath");

        if (openAPI.getPaths() == null) {
            return Optional.empty();
        }

        PathItem item = openAPI.getPaths().get(methodPath);

        if (item == null) {
            if (!pathItemRequired) {
                return Optional.empty();
            }
            throw new IllegalStateException(String.format(
                    "[%s %s]: No PathItem found for method path, please check "
                            + "if the method/class has some unexpected annotation that might need to be handled properly",
                    httpMethod.value(), methodPath));
        }
        return httpMethod == null ? Optional.ofNullable(item.getGet()) : Optional.of(getOperation(httpMethod, item));
    }

    private static Operation getOperation(HttpMethod httpMethod, PathItem item) {
        return getOperation(httpMethod.value(), item);
    }

    private static Operation getOperation(String httpMethod, PathItem item) {
        switch (httpMethod.toUpperCase()) {
            case "DELETE":
                return item.getDelete();
            case "GET":
                return item.getGet();
            case "HEAD":
                return item.getHead();
            case "OPTIONS":
                return item.getOptions();
            case "POST":
                return item.getPost();
            case "PUT":
                return item.getPut();
            case "PATCH":
                return item.getPatch();
            case "TRACE":
                return item.getTrace();
            default:
                throw new IllegalArgumentException(
                        String.format("[%s]: HttpMethod is not supported yet by the Enricher", httpMethod));
        }
    }

    @Nonnull
    static String getMethodPath(@Nonnull Method method) {
        return addLeadingSlashIfMissing(requireNonNull(method, "method").getAnnotation(Path.class));
    }
}
