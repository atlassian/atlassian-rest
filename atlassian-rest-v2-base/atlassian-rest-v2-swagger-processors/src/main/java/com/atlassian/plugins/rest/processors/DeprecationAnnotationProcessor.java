package com.atlassian.plugins.rest.processors;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import jakarta.ws.rs.Path;

import io.swagger.v3.jaxrs2.Reader;
import io.swagger.v3.oas.integration.api.OpenAPIConfiguration;
import io.swagger.v3.oas.integration.api.OpenApiReader;
import io.swagger.v3.oas.models.OpenAPI;

import com.atlassian.plugins.rest.api.deprecation.annotation.DeprecatedEndpoint;

import static com.atlassian.plugins.rest.processors.EnricherUtils.getHttpMethod;
import static com.atlassian.plugins.rest.processors.EnricherUtils.getMethodPath;
import static com.atlassian.plugins.rest.processors.EnricherUtils.getOperation;
import static com.atlassian.plugins.rest.processors.EnricherUtils.getRootPath;

/**
 * Implements OpenApiReader to support default io.swagger.core.v3:swagger-maven-plugin version,
 * it can be added in readerClass list to process multiple enrichers together
 * BiConsumer to support atlassian-forked version com.atlassian.bitbucket.server.swagger:swagger-maven-plugin
 */
public class DeprecationAnnotationProcessor extends Reader
        implements OpenApiReader, BiConsumer<OpenAPI, Set<Class<?>>> {

    @Override
    public void setConfiguration(OpenAPIConfiguration openApiConfiguration) {
        super.setConfiguration(openApiConfiguration);
    }

    @Override
    public OpenAPI read(Set<Class<?>> classes, Map<String, Object> resources) {
        OpenAPI openAPI = super.read(classes, resources);
        process(openAPI, classes);
        return openAPI;
    }

    @Override
    public void accept(OpenAPI openAPI, Set<Class<?>> classes) {
        process(openAPI, classes);
    }

    private void process(OpenAPI openAPI, Set<Class<?>> classes) {
        try {
            classes.forEach(cls -> {
                String rootPath = getRootPath(cls);
                Arrays.stream(cls.getMethods())
                        .filter(method -> method.isAnnotationPresent(Path.class)
                                || getHttpMethod(method).isPresent())
                        .filter(this::isDeprecatedEndpointPresent)
                        .forEach(method -> getHttpMethod(method)
                                .map(value -> getOperation(openAPI, value, rootPath + getMethodPath(method), false))
                                .orElseGet(() -> getOperation(openAPI, null, rootPath + getMethodPath(method), false))
                                .ifPresent(o -> o.setDeprecated(true)));
            });
        } catch (Exception e) {
            // Upgrade exceptions to Errors to ensure build failure if processing fails.
            throw new Error(e);
        }
    }

    private boolean isDeprecatedEndpointPresent(Method method) {
        return method.isAnnotationPresent(DeprecatedEndpoint.class)
                || method.getDeclaringClass().isAnnotationPresent(DeprecatedEndpoint.class)
                || (method.getDeclaringClass().getPackage() != null
                        && method.getDeclaringClass().getPackage().isAnnotationPresent(DeprecatedEndpoint.class));
    }
}
