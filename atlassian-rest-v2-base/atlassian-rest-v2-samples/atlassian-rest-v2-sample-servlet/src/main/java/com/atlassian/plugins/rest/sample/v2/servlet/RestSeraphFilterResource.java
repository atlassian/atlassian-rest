package com.atlassian.plugins.rest.sample.v2.servlet;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.atlassian.seraph.auth.AuthType;

@Path("/servlet")
public class RestSeraphFilterResource {

    @Context
    private HttpHeaders httpHeaders;

    @GET
    @Produces("text/plain")
    @Path("/defaultAuthType")
    @UnrestrictedAccess
    public Response getDefaultAuthTypeAttribute(@Context HttpServletRequest request) {
        return Response.status(200)
                .header(AuthType.DEFAULT_ATTRIBUTE, request.getAttribute(AuthType.DEFAULT_ATTRIBUTE))
                .build();
    }
}
