package com.atlassian.plugins.rest.sample.v2.security.xsrf.model;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class JaxbPojo {

    @XmlAttribute
    String description;

    @XmlAttribute
    Integer value;

    public JaxbPojo(String description, Integer value) {
        this.description = description;
        this.value = value;
    }

    /**
     * This constructor isn't used by any code, but JAXB requires any
     * representation class to have a no-args constructor.
     */
    public JaxbPojo() {}

    public String getDescription() {
        return description;
    }

    public Integer getValue() {
        return value;
    }
}
