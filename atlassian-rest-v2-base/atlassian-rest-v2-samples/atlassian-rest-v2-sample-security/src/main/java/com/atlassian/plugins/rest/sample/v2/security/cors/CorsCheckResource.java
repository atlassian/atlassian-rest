package com.atlassian.plugins.rest.sample.v2.security.cors;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;

import com.atlassian.plugins.rest.api.security.annotation.CorsAllowed;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * Example resource for CORS check testing.
 */
@Path("/cors")
@UnrestrictedAccess
public class CorsCheckResource {
    public static final String SUCCESS_MESSAGE = "Request succeeded";

    @GET
    @Path("none")
    public String getWithNoHeaders() {
        return SUCCESS_MESSAGE;
    }

    @GET
    @CorsAllowed
    public String getWithHeaders() {
        return SUCCESS_MESSAGE;
    }

    @POST
    @Path("none")
    public String postWithNoHeaders() {
        return SUCCESS_MESSAGE;
    }

    @POST
    @CorsAllowed
    public String postWithHeaders() {
        return SUCCESS_MESSAGE;
    }

    @PUT
    @Path("none")
    public String putWithNoHeaders() {
        return SUCCESS_MESSAGE;
    }

    @PUT
    @CorsAllowed
    public String putWithHeaders() {
        return SUCCESS_MESSAGE;
    }
}
