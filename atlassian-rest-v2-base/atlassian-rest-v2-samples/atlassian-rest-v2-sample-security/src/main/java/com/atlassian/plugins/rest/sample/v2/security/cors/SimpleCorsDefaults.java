package com.atlassian.plugins.rest.sample.v2.security.cors;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import com.atlassian.plugins.rest.api.internal.security.cors.CorsDefaults;

import static java.util.Collections.singleton;
import static java.util.Collections.unmodifiableList;

/**
 * A basic implementation of {@link CorsDefaults} purely for testing purposes.
 * The implementation needs to be exported as an OSGI to be imported by REST.
 * See: {@link com.atlassian.plugins.rest.sample.v2.security.spring.SpringBeans#exportCorsDefaults()}
 */
public class SimpleCorsDefaults implements CorsDefaults {
    public static final String CREDENTIALS = "http://rest-v2.credentials.test.com";
    public static final String NO_CREDENTIALS = "http://rest-v2.nocredentials.test.com";
    public static final String ALLOWED_REQUEST_HEADER = "X-Rest-V2-Custom-Header";
    public static final String ALLOWED_RESPONSE_HEADER = "X-REST-V2-Response-Header";

    private static final Collection<String> WHITELIST = unmodifiableList(Arrays.asList(CREDENTIALS, NO_CREDENTIALS));

    public boolean allowsCredentials(String uri) {
        return CREDENTIALS.equals(uri);
    }

    public boolean allowsOrigin(String uri) {
        return WHITELIST.contains(uri);
    }

    public Set<String> getAllowedRequestHeaders(String uri) {
        return singleton(ALLOWED_REQUEST_HEADER);
    }

    public Set<String> getAllowedResponseHeaders(String uri) {
        return singleton(ALLOWED_RESPONSE_HEADER);
    }
}
