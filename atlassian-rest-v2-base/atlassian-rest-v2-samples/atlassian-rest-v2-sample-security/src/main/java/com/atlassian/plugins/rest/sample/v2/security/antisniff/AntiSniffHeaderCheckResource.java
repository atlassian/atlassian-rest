package com.atlassian.plugins.rest.sample.v2.security.antisniff;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

@Path("/antisniff")
public class AntiSniffHeaderCheckResource {

    public static final String ANTI_SNIFFING_HEADER_NAME = "X-Content-Type-Options";
    public static final String ANTI_SNIFFING_HEADER_VALUE = "nosniff";

    @GET
    @Path("/unrestrictedAccess")
    @UnrestrictedAccess
    public String getUnrestrictedAccess() {
        return "unrestrictedAccess";
    }

    @GET
    @Path("/authenticatedAccess")
    public String getAuthenticatedAccess() {
        return "authenticatedAccess";
    }

    @GET
    @Path("/nosniff-container")
    @UnrestrictedAccess
    public Response getResponseWithNoSniffAddedToContainerResponse() {
        return Response.status(200)
                .header(ANTI_SNIFFING_HEADER_NAME, ANTI_SNIFFING_HEADER_VALUE)
                .build();
    }

    @GET
    @Path("/nosniff-servlet")
    @UnrestrictedAccess
    public Response getResponseWithNoSniffAddedToServletResponse(@Context HttpServletResponse response) {
        response.setHeader(ANTI_SNIFFING_HEADER_NAME, ANTI_SNIFFING_HEADER_VALUE);
        return Response.status(200).build();
    }
}
