package com.atlassian.plugins.rest.sample.v2.security.websudo;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.atlassian.sal.api.websudo.WebSudoNotRequired;
import com.atlassian.sal.api.websudo.WebSudoRequired;

/**
 * Sample resource for testing web sudo checks.
 */
@Path("/websudo")
@UnrestrictedAccess
public class WebSudoCheckResource {

    public static final String SUCCESS_MESSAGE = "Request succeeded";

    @GET
    @Path("/webSudoNotRequired")
    @WebSudoNotRequired
    public String getWebSudoNotRequired() {
        return SUCCESS_MESSAGE;
    }

    @GET
    @Path("/webSudoRequired")
    @WebSudoRequired
    public String getWebSudoRequired() {
        return SUCCESS_MESSAGE;
    }
}
