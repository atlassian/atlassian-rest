package com.atlassian.plugins.rest.sample.v2.security.spring;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.osgi.framework.ServiceRegistration;

import com.atlassian.plugins.rest.api.internal.security.cors.CorsDefaults;
import com.atlassian.plugins.rest.sample.v2.security.cors.SimpleCorsDefaults;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;

@Configuration
public class SpringBeans {

    @Bean
    public FactoryBean<ServiceRegistration> exportCorsDefaults() {
        return exportOsgiService(new SimpleCorsDefaults(), as(CorsDefaults.class));
    }
}
