package com.atlassian.plugins.rest.sample.v2.security.xsrf;

import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.OPTIONS;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;

import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * Sample resource for testing XSRF checks.
 */
@Path("/xsrfCheck")
@UnrestrictedAccess
public class XsrfCheckResource {

    public static final String SUCCESS_MESSAGE = "Request succeeded";
    public static final String XSRF_PROTECTION_EXCLUDED_RESOURCE = "xsrfProtectionExcludedResource";

    @GET
    public String getXsrfMessage() {
        return SUCCESS_MESSAGE;
    }

    @OPTIONS
    public String optionsXsrfMessage() {
        return SUCCESS_MESSAGE;
    }

    @POST
    public String postXsrfMessage() {
        return SUCCESS_MESSAGE;
    }

    @PUT
    public String putXsrfMessage() {
        return SUCCESS_MESSAGE;
    }

    @DELETE
    public String deleteXsrfMessage() {
        return SUCCESS_MESSAGE;
    }

    @Path(XSRF_PROTECTION_EXCLUDED_RESOURCE + "/get")
    @GET
    @XsrfProtectionExcluded
    public String getXsrfMessageXsrfProtectionExcluded() {
        return SUCCESS_MESSAGE;
    }

    @Path(XSRF_PROTECTION_EXCLUDED_RESOURCE + "/options")
    @OPTIONS
    @XsrfProtectionExcluded
    public String optionsXsrfMessageXsrfProtectionExcluded() {
        return SUCCESS_MESSAGE;
    }

    @Path(XSRF_PROTECTION_EXCLUDED_RESOURCE + "/post")
    @POST
    @XsrfProtectionExcluded
    public String postXsrfMessageXsrfProtectionExcluded() {
        return SUCCESS_MESSAGE;
    }

    @Path(XSRF_PROTECTION_EXCLUDED_RESOURCE + "/put")
    @PUT
    @XsrfProtectionExcluded
    public String putXsrfMessageXsrfProtectionExcluded() {
        return SUCCESS_MESSAGE;
    }

    @Path(XSRF_PROTECTION_EXCLUDED_RESOURCE + "/delete")
    @DELETE
    @XsrfProtectionExcluded
    public String deleteXsrfMessageXsrfProtectionExcluded() {
        return SUCCESS_MESSAGE;
    }
}
