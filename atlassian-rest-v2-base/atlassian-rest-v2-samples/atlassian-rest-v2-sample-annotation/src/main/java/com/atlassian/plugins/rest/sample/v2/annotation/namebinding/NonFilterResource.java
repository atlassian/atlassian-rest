package com.atlassian.plugins.rest.sample.v2.annotation.namebinding;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * Example resource shows what is the expected response header when the
 * {@link NameBoundFilter NameBoundFilter} is not applied.
 */
@Path("non-filtered")
public class NonFilterResource {

    @GET
    @UnrestrictedAccess
    public Response getFilteredResponse() {
        return Response.ok().header("Filter-Applied", "No").build();
    }
}
