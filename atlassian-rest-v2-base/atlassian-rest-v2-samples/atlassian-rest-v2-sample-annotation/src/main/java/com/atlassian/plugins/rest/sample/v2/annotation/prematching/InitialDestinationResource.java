package com.atlassian.plugins.rest.sample.v2.annotation.prematching;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * The resource to which client is sending a request. However, the final destination of that request will be altered by
 * {@link  PostMatchingDestinationFilter PostMatchingDestinationFilter}.
 */
@Path("initial")
public class InitialDestinationResource {

    @GET
    @UnrestrictedAccess
    public String getMessage() {
        return "Message arrived to initial destination";
    }
}
