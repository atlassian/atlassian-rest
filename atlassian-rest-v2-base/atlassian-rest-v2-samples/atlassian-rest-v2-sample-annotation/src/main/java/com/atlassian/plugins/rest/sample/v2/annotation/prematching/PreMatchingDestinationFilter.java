package com.atlassian.plugins.rest.sample.v2.annotation.prematching;

import java.io.IOException;
import java.net.URI;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.PreMatching;
import jakarta.ws.rs.ext.Provider;

/**
 * Once registered, the server-side filter will alter the destination of incoming request. The initial destination to
 * {@link InitialDestinationResource InitialDestinationResource} will be
 * redirected to {@link PreMatchingDestinationResource PreMatchingDestinationResource}.
 * This behaviour is possible due to {@link jakarta.ws.rs.container.PreMatching @PreMatching} annotation imposed on the filter.
 * As the name hints {@code @PreMatching} annotation causes the filters to be executed before the request matching process is started.
 */
@Provider
@PreMatching
public class PreMatchingDestinationFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        if (isRequestPathToInitialResource(requestContext)) {
            try {
                requestContext.setRequestUri(
                        URI.create(requestContext.getUriInfo().getBaseUri().toString() + "pre-matching"));
            } catch (IllegalStateException ignored) {
            }
        }
    }

    private boolean isRequestPathToInitialResource(ContainerRequestContext requestContext) {
        return requestContext.getUriInfo().getRequestUri().toString().contains("initial");
    }
}
