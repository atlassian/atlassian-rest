package com.atlassian.plugins.rest.sample.v2.annotation.beanparam;

import jakarta.ws.rs.QueryParam;

/**
 * Example Pojo to be sent by client.
 */
public class BookInput {

    @QueryParam("bookName")
    private String bookName;

    @QueryParam("author")
    private String author;

    @Override
    public String toString() {
        return "BookInput{" + "bookName='" + bookName + '\'' + ", author='" + author + '\'' + '}';
    }
}
