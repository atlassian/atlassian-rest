package com.atlassian.plugins.rest.sample.v2.annotation.prematching;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * The resource to which the {@link  PostMatchingDestinationFilter PostMatchingDestinationFilter}
 * redirects a request. The request will never reach this resource for the reasons explained in {@code
 * PostMatchingDestinationFilter}.
 */
@Path("post-matching")
public class PostMatchingDestinationResource {

    @GET
    @UnrestrictedAccess
    public String getMessage() {
        return "Message was redirected by PostMatchingFilter";
    }
}
