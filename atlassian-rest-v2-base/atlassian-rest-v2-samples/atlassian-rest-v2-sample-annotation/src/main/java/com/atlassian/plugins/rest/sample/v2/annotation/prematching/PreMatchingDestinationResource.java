package com.atlassian.plugins.rest.sample.v2.annotation.prematching;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 *  The resource to which the {@link  PreMatchingDestinationFilter PreMatchingDestinationFilter}
 *  redirects a request. Once this filter is executed, client will receive response from {@code getMessage()} method.
 */
@Path("pre-matching")
public class PreMatchingDestinationResource {

    @GET
    @UnrestrictedAccess
    public String getMessage() {
        return "Message was redirected by PreMatchingFilter";
    }
}
