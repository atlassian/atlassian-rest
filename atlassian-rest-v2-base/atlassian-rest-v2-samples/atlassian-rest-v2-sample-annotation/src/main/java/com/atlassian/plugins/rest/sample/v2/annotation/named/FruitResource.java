package com.atlassian.plugins.rest.sample.v2.annotation.named;

import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.atlassian.plugins.rest.sample.v2.annotation.named.beans.Fruit;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * A resource with 2 different beans injected in the constructor. The beans are of the same type {@link Fruit}, so in order to
 * differentiate between the instances, a {@link Named} annotation is applied to the injectees. The value of the annotation
 * corresponds to the name of the bean.
 */
@Path("fruit")
@Produces(APPLICATION_JSON)
public class FruitResource {
    private final Fruit apple;
    private final Fruit orange;

    @Inject
    public FruitResource(@Named("apple") Fruit apple, @Named("orange") Fruit orange) {
        this.apple = apple;
        this.orange = orange;
    }

    @GET
    @Path("apple")
    @UnrestrictedAccess
    public String getApple() {
        return apple.toString();
    }

    @GET
    @Path("orange")
    @UnrestrictedAccess
    public String getOrange() {
        return orange.toString();
    }
}
