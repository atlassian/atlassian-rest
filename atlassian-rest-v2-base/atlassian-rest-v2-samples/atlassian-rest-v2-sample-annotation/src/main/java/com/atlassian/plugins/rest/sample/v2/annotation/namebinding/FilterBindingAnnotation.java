package com.atlassian.plugins.rest.sample.v2.annotation.namebinding;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import jakarta.ws.rs.NameBinding;

/**
 * When the annotation is imposed on a filter and a resource method, it bounds the filter to that method.
 */
@Retention(RetentionPolicy.RUNTIME)
@NameBinding
public @interface FilterBindingAnnotation {}
