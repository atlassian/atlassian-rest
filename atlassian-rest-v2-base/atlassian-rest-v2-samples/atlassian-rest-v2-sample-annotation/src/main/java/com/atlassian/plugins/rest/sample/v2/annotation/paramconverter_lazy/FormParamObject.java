package com.atlassian.plugins.rest.sample.v2.annotation.paramconverter_lazy;

/**
 * The target type of custom {@code ParamConverter}.
 */
public class FormParamObject {

    private final String message;

    public FormParamObject(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return getMessage();
    }
}
