package com.atlassian.plugins.rest.sample.v2.annotation.primary;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.atlassian.plugins.rest.sample.v2.annotation.named.beans.Fruit;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("primary-fruit")
@Produces(APPLICATION_JSON)
public class PrimaryFruitResource {

    private final Fruit primaryFruit;

    // The injection point is ambiguous - 2 of that type are registered in Spring context.
    // Spring should provide only `Apple` entity, based on the `@Primary` definition.
    @Inject
    public PrimaryFruitResource(Fruit primaryFruit) {
        this.primaryFruit = primaryFruit;
    }

    @GET
    @UnrestrictedAccess
    public String getPrimaryFruit() {
        return primaryFruit.toString();
    }
}
