package com.atlassian.plugins.rest.sample.v2.annotation.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.atlassian.plugins.rest.sample.v2.annotation.named.beans.Apple;
import com.atlassian.plugins.rest.sample.v2.annotation.named.beans.Fruit;
import com.atlassian.plugins.rest.sample.v2.annotation.named.beans.Orange;

@Configuration
public class SpringBeans {
    @Bean
    @Primary
    public Fruit apple() {
        return new Apple();
    }

    @Bean
    public Fruit orange() {
        return new Orange();
    }
}
