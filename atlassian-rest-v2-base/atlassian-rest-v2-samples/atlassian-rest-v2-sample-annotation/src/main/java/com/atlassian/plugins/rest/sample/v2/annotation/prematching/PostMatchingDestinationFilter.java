package com.atlassian.plugins.rest.sample.v2.annotation.prematching;

import java.io.IOException;
import java.net.URI;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.ext.Provider;

/**
 * The server-side {@code PostMatchingDestinationFilter} tries to redirect incoming requests to
 * {@link PostMatchingDestinationResource PostMatchingDestinationResource}.
 * However the filter is not annotated with {@link jakarta.ws.rs.container.PreMatching @PreMatching} annotation, therefore it is
 * treated as a post-matching filter, therefore an effort to redirect any request to another resource will end with {@code IllegalStateException}.
 */
@Provider
public class PostMatchingDestinationFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        if (isRequestPathToInitialResource(requestContext)) {
            try {
                requestContext.setRequestUri(
                        URI.create(requestContext.getUriInfo().getBaseUri().toString() + "post-matching"));
            } catch (IllegalStateException ignored) {
            }
        }
    }

    private boolean isRequestPathToInitialResource(ContainerRequestContext requestContext) {
        return requestContext.getUriInfo().getRequestUri().toString().contains("initial");
    }
}
