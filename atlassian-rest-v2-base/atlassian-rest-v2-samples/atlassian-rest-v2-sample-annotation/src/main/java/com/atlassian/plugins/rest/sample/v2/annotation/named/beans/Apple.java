package com.atlassian.plugins.rest.sample.v2.annotation.named.beans;

public class Apple implements Fruit {
    @Override
    public String toString() {
        return "Apple";
    }
}
