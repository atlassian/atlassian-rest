package com.atlassian.plugins.rest.sample.v2.annotation.namebinding;

import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.ext.Provider;

/**
 * The custom {@code FilterBindingAnnotation} should be applied to the target filter.
 * In this example the server-side filter alters response headers to confirm the execution against {@code FilteredResource} method.
 */
@Provider
@FilterBindingAnnotation
public class NameBoundFilter implements ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        responseContext.getHeaders().putSingle("Filter-Applied", "Yes");
    }
}
