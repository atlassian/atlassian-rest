package com.atlassian.plugins.rest.sample.v2.annotation.constrainedto;

import java.io.IOException;
import jakarta.ws.rs.ConstrainedTo;
import jakarta.ws.rs.RuntimeType;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.ext.Provider;

/**
 * This filter annotated {@link jakarta.ws.rs.ConstrainedTo} will be registered on a server side. If it were to be
 * registered on client side, the filter would be ignored and fail to register.
 */
@Provider
@ConstrainedTo(RuntimeType.SERVER)
public class ServerSideFilter implements ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
            throws IOException {
        responseContext.getHeaders().putSingle("RegisteredFilters", "ServerSideFilter");
    }
}
