package com.atlassian.plugins.rest.sample.v2.annotation.darkfeature;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

import com.atlassian.plugins.rest.api.darkfeature.RequiresDarkFeature;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * An example resource that includes {@link RequiresDarkFeature} annotation methods with enabled and not enabled dark feature
 * keys.
 * The dark feature keys are defined in the system property with prefix of 'atlassian.darkfeature'.
 */
@Path("/darkfeature")
@UnrestrictedAccess
public class DarkFeatureResource {
    public static final String FEATURE_KEY = "enabledDarkFeatureKey";
    public static final String NOT_FEATURE_KEY = "notEnabledDarkFeatureKey";

    @GET
    @Path("/enabledDarkFeatureKey")
    @RequiresDarkFeature(FEATURE_KEY)
    public Response validAnnotatedMethod() {
        return Response.ok().build();
    }

    @GET
    @Path("/notEnabledDarkFeatureKey")
    @RequiresDarkFeature(NOT_FEATURE_KEY)
    public Response nonValidAnnotatedMethod() {
        return Response.ok().build();
    }
}
