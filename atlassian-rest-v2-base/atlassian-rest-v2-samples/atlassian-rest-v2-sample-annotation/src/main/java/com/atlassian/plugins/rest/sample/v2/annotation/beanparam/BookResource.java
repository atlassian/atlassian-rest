package com.atlassian.plugins.rest.sample.v2.annotation.beanparam;

import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * {@link jakarta.ws.rs.BeanParam @BeanParam} annotation injects a java class {@link BookInput BookInput}
 * into method parameter {@code newBook}, of which property methods or fields are annotated with any parameter-type annotation,
 * f.e. {@link jakarta.ws.rs.QueryParam @QueryParam} or {@link jakarta.ws.rs.FormParam FormParam}.
 */
@Path("/books")
public class BookResource {

    @GET
    @UnrestrictedAccess
    public String createBook(@BeanParam BookInput newBook) {
        return newBook.toString();
    }
}
