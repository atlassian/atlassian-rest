package com.atlassian.plugins.rest.sample.v2.annotation.constrainedto;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * A resource to which a client sends a request, in order to verify which filter was executed.
 * Only {@link ServerSideFilter ServerSideFilter} will be registered
 * and executed in the server runtime, therefore a response will only be executed against the {@code ServerSideFilter}.
 */
@Path("constrained-to")
public class SampleResource {

    @GET
    @UnrestrictedAccess
    public Response getFilteredResponse() {
        return Response.ok().build();
    }
}
