package com.atlassian.plugins.rest.sample.v2.annotation.constrainedto;

import java.io.IOException;
import jakarta.ws.rs.ConstrainedTo;
import jakarta.ws.rs.RuntimeType;
import jakarta.ws.rs.client.ClientRequestContext;
import jakarta.ws.rs.client.ClientResponseContext;
import jakarta.ws.rs.client.ClientResponseFilter;
import jakarta.ws.rs.ext.Provider;

/**
 * The filter makes acknowledgement of its existence to client by adding an element to header.
 * This filter will not registered as a server-side provider. The {@link jakarta.ws.rs.ConstrainedTo} restricts the filter to only
 * client-side runtime.
 */
@Provider
@ConstrainedTo(RuntimeType.CLIENT)
public class ClientSideFilter implements ClientResponseFilter {

    @Override
    public void filter(ClientRequestContext requestContext, ClientResponseContext responseContext) throws IOException {
        responseContext.getHeaders().add("RegisteredFilters", "ClientSideFilter");
    }
}
