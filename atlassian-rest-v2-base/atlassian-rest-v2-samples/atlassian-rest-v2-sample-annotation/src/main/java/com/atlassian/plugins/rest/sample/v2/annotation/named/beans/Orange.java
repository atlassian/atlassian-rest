package com.atlassian.plugins.rest.sample.v2.annotation.named.beans;

public class Orange implements Fruit {
    @Override
    public String toString() {
        return "Orange";
    }
}
