package com.atlassian.plugins.rest.sample.v2.annotation.namebinding;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * A resource with applied {@link NameBoundFilter NameBoundFilter} to
 * {@code getFilteredResponse()} method.
 * A {@link FilterBindingAnnotation @FilterBindingAnnotation} imposed on
 * {@code getFilteredResponse()} method binds the {@code NameBoundFilter} to the {@code FilteredResource}.
 *
 */
@Path("filtered")
public class FilteredResource {

    @GET
    @FilterBindingAnnotation
    @UnrestrictedAccess
    public Response getFilteredResponse() {
        return Response.ok().header("Filter-Applied", "No").build();
    }
}
