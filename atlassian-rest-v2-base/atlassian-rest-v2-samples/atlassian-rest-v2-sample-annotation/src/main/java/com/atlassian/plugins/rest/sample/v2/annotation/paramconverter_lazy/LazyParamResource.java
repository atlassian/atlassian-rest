package com.atlassian.plugins.rest.sample.v2.annotation.paramconverter_lazy;

import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * A {@code ParamConverter} is used to inject a parameter (of String type) into a method argument {@code paramObject}.
 * The {@code paramObject} needs to be annotated with {@link jakarta.ws.rs.DefaultValue @DefaultValue}, otherwise the lazy
 * conversion will be no use.
 */
@Path("/lazy-param")
public class LazyParamResource {

    @POST
    @UnrestrictedAccess
    @XsrfProtectionExcluded
    public Response getConvertedParam(
            @FormParam("convertable") @DefaultValue("Hello from DefaultValue") FormParamObject paramObject) {
        return Response.ok().build();
    }
}
