package com.atlassian.plugins.rest.sample.v2.annotation.suspended;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.container.AsyncResponse;
import jakarta.ws.rs.container.Suspended;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * Resource that implements asynchronous processing, involves suspension of the original request thread and initiation
 * of request processing on a different thread, thereby freeing up the original server side thread to accept other incoming requests.
 * {@link jakarta.ws.rs.container.Suspended @Suspended} annotation instructs the container to inject an instance of {@link jakarta.ws.rs.container.AsyncResponse AsyncResponse}
 * and invoke the method asynchronously.
 * Unfortunately sending request to async resource causes Exception in server, because
 * async processing is not supported by seraph-trusted-apps-filter plugin.
 */
@Path("async")
public class SimpleAsyncResource {

    @GET
    @UnrestrictedAccess
    public void longRunning1(@Suspended AsyncResponse response) {
        new Thread(() -> response.resume("Hello from async resource!")).start();
    }
}
