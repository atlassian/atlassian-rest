package com.atlassian.plugins.rest.sample.v2.annotation.paramconverter_lazy;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import jakarta.ws.rs.ext.ParamConverter;
import jakarta.ws.rs.ext.ParamConverterProvider;
import jakarta.ws.rs.ext.Provider;

/**
 * Factory pattern for creating an instance of {@link jakarta.ws.rs.ext.ParamConverter ParamConverter}.
 * The {@code ParamConverter} converts String value to the target type and vice versa.
 */
@Provider
@ParamConverter.Lazy
public class CustomParameterConverterProvider implements ParamConverterProvider {

    @Override
    public <T> ParamConverter<T> getConverter(Class<T> rawType, Type genericType, Annotation[] annotations) {
        if (rawType.getName().equals(FormParamObject.class.getName())) {
            return new ParamConverter<T>() {

                @Override
                public T fromString(String value) {
                    FormParamObject convertedValue = new FormParamObject(value);
                    return (T) convertedValue;
                }

                @Override
                public String toString(T value) {
                    if (value == null) {
                        return null;
                    }
                    return value.toString();
                }
            };
        }
        return null;
    }
}
