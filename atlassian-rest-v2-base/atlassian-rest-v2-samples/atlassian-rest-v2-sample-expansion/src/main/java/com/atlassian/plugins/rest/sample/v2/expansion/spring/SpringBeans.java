package com.atlassian.plugins.rest.sample.v2.expansion.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.atlassian.plugins.rest.api.expand.EntityCrawler;
import com.atlassian.plugins.rest.api.internal.expand.resolver.ListWrapperEntityExpanderResolver;
import com.atlassian.plugins.rest.api.internal.expand.resolver.SelfExpandingEntityExpanderResolver;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class SpringBeans {

    @Bean
    public EntityCrawler importEntityCrawler() {
        return importOsgiService(EntityCrawler.class);
    }

    @Bean
    public ListWrapperEntityExpanderResolver importListWrapperEntityExpanderResolver() {
        return importOsgiService(ListWrapperEntityExpanderResolver.class);
    }

    @Bean
    public SelfExpandingEntityExpanderResolver importSelfExpandingEntityExpanderResolver() {
        return importOsgiService(SelfExpandingEntityExpanderResolver.class);
    }
}
