package com.atlassian.plugins.rest.sample.v2.expansion.indexedexpanding.entity;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;

import com.atlassian.plugins.rest.api.expand.annotation.IndexedExpand;
import com.atlassian.plugins.rest.api.expand.parameter.Indexes;

public class CustomListEntity {
    @XmlElement
    private List<Integer> items;

    @XmlTransient
    private final List<Integer> expandedList = IntStream.range(0, 10).boxed().collect(Collectors.toList());

    /*
       Use indexes to define a range of entity you want to expand.
    */
    @IndexedExpand
    public void expandList(Indexes indexes) {
        // Define logic to expand the entity
        items = new LinkedList<>();
        for (Integer i : indexes.getIndexes(expandedList.size())) {
            if (i < expandedList.size()) {
                items.add(expandedList.get(i));
            }
        }
    }

    public List<Integer> getItems() {
        return items;
    }

    public CustomListEntity() {
        // empty
    }
}
