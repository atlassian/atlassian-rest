package com.atlassian.plugins.rest.sample.v2.expansion.entitycrawler;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

import com.atlassian.plugins.rest.api.expand.annotation.Expandable;

@XmlRootElement
public class ListWrapperParentEntity {
    @Expandable
    @XmlElement
    ListWrapperExampleEntity listWrapperEntity;

    @Expandable
    @XmlElement
    ListWrapperExampleEntity listWrapperEntity2;

    public ListWrapperParentEntity() {
        this.listWrapperEntity = new ListWrapperExampleEntity(2, 2);
        this.listWrapperEntity2 = new ListWrapperExampleEntity(2, 2);
    }

    public ListWrapperExampleEntity getListWrapperEntity() {
        return listWrapperEntity;
    }

    public ListWrapperExampleEntity getListWrapperEntity2() {
        return listWrapperEntity2;
    }
}
