package com.atlassian.plugins.rest.sample.v2.expansion.listexpanding.expander;

import com.atlassian.plugins.rest.api.expand.expander.AbstractEntityExpander;
import com.atlassian.plugins.rest.sample.v2.expansion.listexpanding.entity.CustomEntity;

public class CustomEntityExpander extends AbstractEntityExpander<CustomEntity> {
    protected CustomEntity expandInternal(CustomEntity entity) {
        entity.setExpandedField("Mimic the field is expended.");
        return entity;
    }
}
