package com.atlassian.plugins.rest.sample.v2.expansion.entitycrawler;

import java.util.Arrays;
import java.util.List;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

import com.atlassian.plugins.rest.api.expand.listwrapper.AbstractPagedListWrapper;
import com.atlassian.plugins.rest.api.expand.listwrapper.ListWrapperCallback;
import com.atlassian.plugins.rest.api.internal.expand.listwrapper.ListWrapperCallBacks;
import com.atlassian.plugins.rest.sample.v2.expansion.listexpanding.entity.CustomEntity;

@XmlRootElement
public class ListWrapperExampleEntity extends AbstractPagedListWrapper<CustomEntity> {
    @XmlElement
    List<CustomEntity> customEntityList;

    protected ListWrapperExampleEntity(int size, int maxResults) {
        super(size, maxResults);
    }

    public ListWrapperExampleEntity() {
        super(0, 0);
    }

    @Override
    public ListWrapperCallback<CustomEntity> getPagingCallback() {
        List<CustomEntity> items = Arrays.asList(new CustomEntity(), new CustomEntity());
        return ListWrapperCallBacks.ofList(items, getMaxResults());
    }

    public List<CustomEntity> getCustomEntityList() {
        return customEntityList;
    }
}
