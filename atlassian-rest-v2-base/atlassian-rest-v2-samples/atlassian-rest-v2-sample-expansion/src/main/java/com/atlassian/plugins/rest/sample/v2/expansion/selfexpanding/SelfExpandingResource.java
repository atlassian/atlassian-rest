package com.atlassian.plugins.rest.sample.v2.expansion.selfexpanding;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * Resource for testing expand functionality.
 */
@Path("/selfexpanding")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class SelfExpandingResource {
    @GET
    @UnrestrictedAccess
    public SelfExpandingExampleEntity get() {
        return new SelfExpandingExampleEntity();
    }
}
