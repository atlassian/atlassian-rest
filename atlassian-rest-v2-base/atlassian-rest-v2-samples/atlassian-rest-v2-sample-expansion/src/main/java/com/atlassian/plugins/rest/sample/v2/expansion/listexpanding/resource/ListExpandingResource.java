package com.atlassian.plugins.rest.sample.v2.expansion.listexpanding.resource;

import java.util.Arrays;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.atlassian.plugins.rest.sample.v2.expansion.listexpanding.entity.CustomEntity;
import com.atlassian.plugins.rest.sample.v2.expansion.listexpanding.entity.ListExpandingExampleEntity;

/**
 * The resource example demonstrate the List can be resolved by `com.atlassian.plugins.rest.v2.expand.resolver.ListEntityExpanderResolver`.
 */
@Path("/listexpanding")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class ListExpandingResource {
    @GET
    @UnrestrictedAccess
    public ListExpandingExampleEntity get() {
        return new ListExpandingExampleEntity(Arrays.asList(new CustomEntity(), new CustomEntity()));
    }
}
