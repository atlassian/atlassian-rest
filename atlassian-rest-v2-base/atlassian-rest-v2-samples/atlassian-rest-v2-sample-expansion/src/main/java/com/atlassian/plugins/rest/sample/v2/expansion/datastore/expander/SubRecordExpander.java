package com.atlassian.plugins.rest.sample.v2.expansion.datastore.expander;

import com.atlassian.plugins.rest.api.expand.expander.AbstractEntityExpander;
import com.atlassian.plugins.rest.sample.v2.expansion.datastore.entity.SubRecord;
import com.atlassian.plugins.rest.sample.v2.expansion.datastore.resource.DataStore;

/**
 * Expands a {com.atlassian.plugins.rest.sample.expansion.entity.SubRecord} by asking the
 * {@link DataStore} to perform the database queries (fake).
 */
public class SubRecordExpander extends AbstractEntityExpander<SubRecord> {
    protected SubRecord expandInternal(SubRecord entity) {
        if (entity != null && entity.getPlayerRecord() != null) {
            return DataStore.getInstance().getSubRecord(entity.getPlayerRecord());
        }
        return entity;
    }
}
