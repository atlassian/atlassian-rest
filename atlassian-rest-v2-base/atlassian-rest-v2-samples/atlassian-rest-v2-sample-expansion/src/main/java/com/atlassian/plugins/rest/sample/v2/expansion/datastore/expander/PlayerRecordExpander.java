package com.atlassian.plugins.rest.sample.v2.expansion.datastore.expander;

import com.atlassian.plugins.rest.api.expand.expander.AbstractEntityExpander;
import com.atlassian.plugins.rest.sample.v2.expansion.datastore.entity.PlayerRecord;
import com.atlassian.plugins.rest.sample.v2.expansion.datastore.resource.DataStore;

/**
 * Expands a {@link PlayerRecord} by asking the {@link DataStore} to perform the database queries.
 */
public class PlayerRecordExpander extends AbstractEntityExpander<PlayerRecord> {
    protected PlayerRecord expandInternal(PlayerRecord entity) {
        if (entity != null && entity.getPlayer() != null)
            return DataStore.getInstance().getPlayerRecord(entity.getPlayer());
        else return entity;
    }
}
