package com.atlassian.plugins.rest.sample.v2.expansion.datastore.entity;

import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;

import com.atlassian.plugins.rest.api.expand.annotation.Expandable;
import com.atlassian.plugins.rest.api.expand.annotation.Expander;
import com.atlassian.plugins.rest.sample.v2.expansion.datastore.expander.PlayerRecordExpander;

import static jakarta.xml.bind.annotation.XmlAccessType.FIELD;

/**
 * The total points scoring record for a player.
 */
@XmlRootElement
@XmlAccessorType(FIELD)
@Expander(PlayerRecordExpander.class)
public class PlayerRecord {
    /**
     * Creates an empty record which is used to inform the client that a record is available without
     * performing the cost of calculating it.  If the client requires the record it can expand this field.
     *
     * @param player
     * @return
     */
    public static PlayerRecord emptyRecord(Player player) {
        return new PlayerRecord(player);
    }

    @XmlAttribute
    private String expand;

    @XmlElement
    private Integer pointsScored;

    @XmlTransient
    private Player player;

    @Expandable
    @XmlElement
    private SubRecord subRecord1;

    @Expandable
    @XmlElement
    private SubRecord subRecord2;

    public PlayerRecord() {}

    public PlayerRecord(Player player) {
        this.player = player;
    }

    public PlayerRecord(int pointsScored) {
        this.pointsScored = pointsScored;
    }

    public Integer getPointsScored() {
        return pointsScored;
    }

    public void setPointsScored(Integer pointsScored) {
        this.pointsScored = pointsScored;
    }

    public void setSubRecord1(final SubRecord subRecord1) {
        this.subRecord1 = subRecord1;
    }

    public SubRecord getSubRecord1() {
        return subRecord1;
    }

    public void setSubRecord2(final SubRecord subRecord2) {
        this.subRecord2 = subRecord2;
    }

    public SubRecord getSubRecord2() {
        return subRecord2;
    }

    public Player getPlayer() {
        return player;
    }
}
