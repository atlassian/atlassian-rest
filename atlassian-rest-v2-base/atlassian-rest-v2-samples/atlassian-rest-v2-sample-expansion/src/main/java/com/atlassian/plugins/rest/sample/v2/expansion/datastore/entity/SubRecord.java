package com.atlassian.plugins.rest.sample.v2.expansion.datastore.entity;

import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;

import com.atlassian.plugins.rest.api.expand.annotation.Expander;
import com.atlassian.plugins.rest.sample.v2.expansion.datastore.expander.SubRecordExpander;

import static jakarta.xml.bind.annotation.XmlAccessType.FIELD;

/**
 * The total points scoring record for a player.
 */
@XmlRootElement
@XmlAccessorType(FIELD)
@Expander(SubRecordExpander.class)
public class SubRecord {

    @XmlElement
    private Integer pointsScored;

    @XmlTransient
    private PlayerRecord playerRecord;

    public static SubRecord emptySubRecord(PlayerRecord playerRecord) {
        return new SubRecord(playerRecord);
    }

    public SubRecord() {}

    public SubRecord(final PlayerRecord playerRecord) {
        this.playerRecord = playerRecord;
    }

    public SubRecord(int pointsScored) {
        this.pointsScored = pointsScored;
    }

    public Integer getPointsScored() {
        return pointsScored;
    }

    public void setPointsScored(Integer pointsScored) {
        this.pointsScored = pointsScored;
    }

    public PlayerRecord getPlayerRecord() {
        return playerRecord;
    }
}
