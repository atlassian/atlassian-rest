package com.atlassian.plugins.rest.sample.v2.expansion.datastore.entity;

import java.util.List;
import jakarta.ws.rs.core.UriBuilder;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;

import com.atlassian.plugins.rest.api.expand.listwrapper.AbstractPagedListWrapper;
import com.atlassian.plugins.rest.api.expand.listwrapper.ListWrapperCallback;
import com.atlassian.plugins.rest.api.internal.expand.listwrapper.ListWrapperCallBacks;
import com.atlassian.plugins.rest.sample.v2.expansion.datastore.resource.DataStore;

import static jakarta.xml.bind.annotation.XmlAccessType.FIELD;

@XmlRootElement
@XmlAccessorType(FIELD)
public class Players extends AbstractPagedListWrapper<Player> {
    @XmlElement(name = "player")
    private List<Player> playersList;

    @XmlTransient
    private UriBuilder uriBuilder;

    // for JAXB
    private Players() {
        super(0, 0);
    }

    public Players(int size, int maxItems, UriBuilder uriBuilder) {
        super(size, maxItems);
        this.uriBuilder = uriBuilder;
    }

    public ListWrapperCallback<Player> getPagingCallback() {
        return ListWrapperCallBacks.ofList(DataStore.getInstance().getPlayers(uriBuilder), getMaxResults());
    }

    public List<Player> getPlayers() {
        return playersList;
    }
}
