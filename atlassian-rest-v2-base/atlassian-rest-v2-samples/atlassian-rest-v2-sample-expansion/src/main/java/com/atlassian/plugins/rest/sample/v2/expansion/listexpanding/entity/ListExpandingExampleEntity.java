package com.atlassian.plugins.rest.sample.v2.expansion.listexpanding.entity;

import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

import com.atlassian.plugins.rest.api.expand.annotation.Expandable;

import static jakarta.xml.bind.annotation.XmlAccessType.FIELD;

@XmlRootElement
@XmlAccessorType(FIELD)
public class ListExpandingExampleEntity {
    @Expandable
    private List<CustomEntity> customEntityList;

    public ListExpandingExampleEntity() {
        // empty
    }

    public ListExpandingExampleEntity(List<CustomEntity> recordList) {
        this.customEntityList = recordList;
    }

    public List<CustomEntity> getCustomEntityList() {
        return customEntityList;
    }

    public void setCustomEntityList(List<CustomEntity> customEntityList) {
        this.customEntityList = customEntityList;
    }
}
