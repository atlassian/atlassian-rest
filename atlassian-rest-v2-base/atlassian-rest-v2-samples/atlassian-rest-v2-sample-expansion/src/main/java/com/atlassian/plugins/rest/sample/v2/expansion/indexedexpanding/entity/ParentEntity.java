package com.atlassian.plugins.rest.sample.v2.expansion.indexedexpanding.entity;

import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;

import com.atlassian.plugins.rest.api.expand.annotation.Expandable;

import static jakarta.xml.bind.annotation.XmlAccessType.FIELD;

@XmlAccessorType(FIELD)
public class ParentEntity {
    @XmlElement
    @Expandable
    private CustomListEntity customListEntity;

    public CustomListEntity getCustomListEntity() {
        return customListEntity;
    }

    public void setCustomListEntity(CustomListEntity customListEntity) {
        this.customListEntity = customListEntity;
    }

    public ParentEntity() {
        // empty
    }

    public ParentEntity(CustomListEntity customListEntity) {
        this.customListEntity = customListEntity;
    }
}
