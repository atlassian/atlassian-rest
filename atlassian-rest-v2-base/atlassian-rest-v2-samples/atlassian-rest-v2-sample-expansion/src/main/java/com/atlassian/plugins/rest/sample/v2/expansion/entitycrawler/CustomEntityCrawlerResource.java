package com.atlassian.plugins.rest.sample.v2.expansion.entitycrawler;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;

import com.atlassian.plugins.rest.api.expand.EntityCrawler;
import com.atlassian.plugins.rest.api.expand.expander.AbstractEntityExpander;
import com.atlassian.plugins.rest.api.expand.expander.SelfExpanding;
import com.atlassian.plugins.rest.api.expand.listwrapper.AbstractPagedListWrapper;
import com.atlassian.plugins.rest.api.expand.resolver.EntityExpanderResolver;
import com.atlassian.plugins.rest.api.internal.expand.parameter.DefaultExpandParameter;
import com.atlassian.plugins.rest.api.internal.expand.resolver.ListWrapperEntityExpanderResolver;
import com.atlassian.plugins.rest.api.internal.expand.resolver.SelfExpandingEntityExpanderResolver;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.atlassian.plugins.rest.sample.v2.expansion.selfexpanding.SelfExpandingExampleEntity;

/**
 * The example resource showcases the ability for {@link EntityCrawler}, {@link SelfExpandingEntityExpanderResolver} and
 * {@link ListWrapperEntityExpanderResolver} to be injected into the application's context.
 * This example was created specifically for atlassian products. It is not recommended to use {@link EntityCrawler}
 * or {@link EntityExpanderResolver} directly!
 * Rather if you want to simply expand a custom entity, refer to these expanders: {@link AbstractEntityExpander},
 * {@link AbstractPagedListWrapper}, {@link SelfExpanding}.
 */
@Path("crawler")
public class CustomEntityCrawlerResource {
    private final EntityCrawler entityCrawler;
    private final ListWrapperEntityExpanderResolver listWrapperEntityExpanderResolver;
    private final SelfExpandingEntityExpanderResolver selfExpandingEntityExpanderResolver;

    @Inject
    public CustomEntityCrawlerResource(
            EntityCrawler entityCrawler,
            ListWrapperEntityExpanderResolver listWrapperEntityExpanderResolver,
            SelfExpandingEntityExpanderResolver selfExpandingEntityExpanderResolver) {
        this.entityCrawler = entityCrawler;
        this.listWrapperEntityExpanderResolver = listWrapperEntityExpanderResolver;
        this.selfExpandingEntityExpanderResolver = selfExpandingEntityExpanderResolver;
    }

    @GET
    @Path("self")
    @UnrestrictedAccess
    public SelfExpandingExampleEntity getSelfExpandingExampleEntity(@Context UriInfo uriInfo) {
        SelfExpandingExampleEntity entity = new SelfExpandingExampleEntity();
        entityCrawler.crawl(
                entity,
                new DefaultExpandParameter(uriInfo.getQueryParameters().get("expand")),
                selfExpandingEntityExpanderResolver);
        return entity;
    }

    @GET
    @Path("list")
    @UnrestrictedAccess
    public ListWrapperParentEntity getListWrapperEntityExpanderResolver(@Context UriInfo uriInfo) {
        ListWrapperParentEntity entity = new ListWrapperParentEntity();
        entityCrawler.crawl(
                entity,
                new DefaultExpandParameter(uriInfo.getQueryParameters().get("expand")),
                listWrapperEntityExpanderResolver);
        return entity;
    }
}
