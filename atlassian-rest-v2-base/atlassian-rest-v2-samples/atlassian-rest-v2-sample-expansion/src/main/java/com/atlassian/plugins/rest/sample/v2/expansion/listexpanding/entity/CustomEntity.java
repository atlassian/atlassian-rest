package com.atlassian.plugins.rest.sample.v2.expansion.listexpanding.entity;

import jakarta.xml.bind.annotation.XmlAccessorType;

import com.atlassian.plugins.rest.api.expand.annotation.Expander;
import com.atlassian.plugins.rest.sample.v2.expansion.listexpanding.expander.CustomEntityExpander;

import static jakarta.xml.bind.annotation.XmlAccessType.FIELD;

@Expander(CustomEntityExpander.class)
@XmlAccessorType(FIELD)
public class CustomEntity {
    private String expandedField;

    public CustomEntity() {
        // empty
    }

    public void setExpandedField(String expandedField) {
        this.expandedField = expandedField;
    }

    public String getExpandedField() {
        return expandedField;
    }
}
