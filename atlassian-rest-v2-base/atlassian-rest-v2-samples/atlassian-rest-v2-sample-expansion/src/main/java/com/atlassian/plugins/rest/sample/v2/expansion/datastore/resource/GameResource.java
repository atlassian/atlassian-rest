package com.atlassian.plugins.rest.sample.v2.expansion.datastore.resource;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.MediaType.APPLICATION_XML;

/**
 * Resource representing a Game such as Rugby, Football (and I mean soccer), etc.
 */
@Path("/game")
@Consumes({APPLICATION_XML, APPLICATION_JSON})
@Produces({APPLICATION_XML, APPLICATION_JSON})
@UnrestrictedAccess
public class GameResource {
    @Context
    private UriInfo uriInfo;

    @GET
    public Response getGame() {
        return Response.ok(DataStore.getInstance().getGame("rugby", PlayerResource.getPlayerUriBuilder(uriInfo)))
                .build();
    }
}
