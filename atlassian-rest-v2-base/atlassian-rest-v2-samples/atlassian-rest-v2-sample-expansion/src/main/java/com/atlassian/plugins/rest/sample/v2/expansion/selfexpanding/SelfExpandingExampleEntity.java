package com.atlassian.plugins.rest.sample.v2.expansion.selfexpanding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;

import com.atlassian.plugins.rest.api.expand.annotation.Expandable;
import com.atlassian.plugins.rest.api.expand.expander.SelfExpanding;

/**
 * Representation bean used to test expando stuff.
 */
@XmlRootElement(name = "selfExpandingExample")
public class SelfExpandingExampleEntity {
    /**
     * The value to expect when expansion is performed.
     */
    public static final List<String> SELF_EXPANDING_STRINGS = Collections.unmodifiableList(Arrays.asList("one", "two"));

    /**
     * An expandable field. Will get expanded by the framework if requested.
     */
    @XmlAttribute
    private List<String> selfExpandingList = new ArrayList<>();

    /**
     * This expander takes care of populating the selfExpanding field when requested.
     */
    @Expandable
    @XmlTransient
    private final SelfExpanding selfExpanding = () -> selfExpandingList = new ArrayList<>(SELF_EXPANDING_STRINGS);

    public SelfExpandingExampleEntity() {
        // empty
    }

    public List<String> getSelfExpandingList() {
        return selfExpandingList;
    }
}
