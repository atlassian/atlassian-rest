package com.atlassian.plugins.rest.sample.v2.expansion.datastore.resource;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriBuilder;
import jakarta.ws.rs.core.UriInfo;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.MediaType.APPLICATION_XML;

/**
 * A resource used to show details of a player and optionally an expandable player record element.  The
 * {@link DataStore} will provide data of two player, one which has a player record and which doesn't.
 * <p>
 * When viewing player with the record the client has the option to expand it.
 */
@Path("/player")
@Consumes({APPLICATION_XML, APPLICATION_JSON})
@Produces({APPLICATION_XML, APPLICATION_JSON})
@UnrestrictedAccess
public class PlayerResource {
    @Context
    private UriInfo uriInfo;

    @GET
    @Path("/{id}")
    public Response getPlayer(@PathParam("id") Integer id) {
        return Response.ok(DataStore.getInstance().getPlayer(id, getPlayerUriBuilder(uriInfo)))
                .build();
    }

    static UriBuilder getPlayerUriBuilder(UriInfo uriInfo) {
        return uriInfo.getBaseUriBuilder().path("player").path("{id}");
    }
}
