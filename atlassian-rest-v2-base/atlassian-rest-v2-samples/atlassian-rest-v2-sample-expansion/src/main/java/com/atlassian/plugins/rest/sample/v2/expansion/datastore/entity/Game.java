package com.atlassian.plugins.rest.sample.v2.expansion.datastore.entity;

import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

import com.atlassian.plugins.rest.api.expand.annotation.Expandable;

import static jakarta.xml.bind.annotation.XmlAccessType.FIELD;

@XmlRootElement
@XmlAccessorType(FIELD)
public class Game {
    @XmlAttribute
    private String name;

    @XmlElement
    @Expandable
    private Players players;

    private Game() {}

    public Game(String name) {
        this.name = name;
    }

    public void setPlayers(Players players) {
        this.players = players;
    }

    public String getName() {
        return name;
    }

    public Players getPlayers() {
        return players;
    }
}
