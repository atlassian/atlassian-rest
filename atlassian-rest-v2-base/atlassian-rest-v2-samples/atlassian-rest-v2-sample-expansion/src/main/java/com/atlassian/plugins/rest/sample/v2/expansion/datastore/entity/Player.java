package com.atlassian.plugins.rest.sample.v2.expansion.datastore.entity;

import jakarta.ws.rs.core.UriBuilder;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

import com.atlassian.plugins.rest.api.expand.annotation.Expandable;
import com.atlassian.plugins.rest.api.model.Link;

import static jakarta.xml.bind.annotation.XmlAccessType.FIELD;

/**
 * Details about a player and a link to their record if available.
 */
@XmlRootElement
@XmlAccessorType(FIELD)
public class Player {
    @XmlAttribute
    private String expand;

    @XmlAttribute
    private int id;

    @XmlElement
    private String fullName;

    @XmlElement
    private Link link;

    @Expandable
    @XmlElement
    private PlayerRecord record;

    public Player() {}

    public Player(int id, String fullName, UriBuilder builder) {
        this.id = id;
        this.fullName = fullName;
        this.link = Link.self(builder.build(this.id));
    }

    public Player(int id, UriBuilder builder) {
        this.id = id;
        this.link = Link.self(builder.build(this.id));
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setRecord(PlayerRecord record) {
        this.record = record;
    }

    public PlayerRecord getRecord() {
        return record;
    }
}
