package com.atlassian.plugins.rest.sample.v2.expansion.indexedexpanding.resource;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.atlassian.plugins.rest.sample.v2.expansion.indexedexpanding.entity.CustomListEntity;
import com.atlassian.plugins.rest.sample.v2.expansion.indexedexpanding.entity.ParentEntity;

/**
 * The resource example demonstrate the List can be resolved by `com.atlassian.plugins.rest.v2.expand.resolver.ListEntityExpanderResolver`.
 */
@Path("/indexedexpanding")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class IndexedExpandingResource {
    @GET
    @UnrestrictedAccess
    public ParentEntity get() {
        return new ParentEntity(new CustomListEntity());
    }
}
