package com.atlassian.plugins.rest.sample.v2;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

/**
 * This resource serves paths that start with /example as per the {@link Path Path("/hello")} annotation.
 * The resource class is used for testing the Jersey generated WADL for each REST endpoint.
 */
@Path("/example")
public class WadlGenerationResource {

    @GET
    public String hello() {
        return "HELLO";
    }
}
