package com.atlassian.plugins.rest.sample.v2.api1;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

@Path("/api")
public class ApiResource1 {
    @GET
    @UnrestrictedAccess
    public String getClassName() {
        return this.getClass().getSimpleName();
    }
}
