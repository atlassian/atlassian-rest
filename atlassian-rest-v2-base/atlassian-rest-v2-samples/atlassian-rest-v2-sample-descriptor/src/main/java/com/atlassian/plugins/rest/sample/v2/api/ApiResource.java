package com.atlassian.plugins.rest.sample.v2.api;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

@Path("/api")
public class ApiResource {
    @GET
    @UnrestrictedAccess
    public String getClassName() {
        return this.getClass().getSimpleName();
    }
}
