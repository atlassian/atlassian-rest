package com.atlassian.plugins.rest.sample.scanner2;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

@Path("bundle")
@UnrestrictedAccess
public class BundledClass {
    @GET
    public String helloFromBundled() {
        return "This is a bundled class";
    }
}
