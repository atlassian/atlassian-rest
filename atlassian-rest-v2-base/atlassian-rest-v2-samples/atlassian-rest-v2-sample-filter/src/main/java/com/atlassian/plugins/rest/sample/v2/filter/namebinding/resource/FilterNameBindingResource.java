package com.atlassian.plugins.rest.sample.v2.filter.namebinding.resource;

import java.util.List;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.atlassian.plugins.rest.sample.v2.filter.namebinding.annotation.FilterNameBinding1;
import com.atlassian.plugins.rest.sample.v2.filter.namebinding.annotation.FilterNameBinding2;

/**
 * This resource class demonstrates the usage of {@link jakarta.ws.rs.NameBinding}
 * annotations for binding filters to the resource method.
 *
 * <p> The entire class is bound to the filter associated with
 * {@link FilterNameBinding1}. This annotation is a custom name binding
 * annotation that indicates which filter(s) should be applied when a resource
 * method is called.
 *
 * <p> The {@code filter} resource method of this class is additionally bound to
 * the filter associated with {@link FilterNameBinding2}.
 * Similar to {@link FilterNameBinding1}, this is also a custom name binding
 * annotation that signals the application of a specific filter when the method
 * is invoked.
 *
 * <p> In regard to the execution order of the method itself, after all
 * {@link ContainerRequestFilter container request filters} have been executed,
 * the method (in this case, {@code filter}) is invoked. Any logic present in
 * this method will be executed at this stage. Once the method execution is
 * finished and a response is being prepared, the {@link ContainerResponseFilter
 * container response filters} are called. Thus, the order of execution is as
 * follows:
 * {@link ContainerRequestFilter} -&gt; Resource method (e.g., {@code filter}) -&gt; {@link ContainerResponseFilter}.
 */
@Path("/filterNameBinding")
@FilterNameBinding1
public class FilterNameBindingResource {

    public static final String FILTER_HEADER = "Filter-Header";

    @GET
    @UnrestrictedAccess
    @FilterNameBinding2
    public String filter(@Context HttpHeaders httpHeaders) {
        List<String> headers = httpHeaders.getRequestHeaders().get(FILTER_HEADER);
        String message = "This text is added from " + this.getClass().getSimpleName();
        headers.add(message);
        return "FilterResource has been processed.";
    }
}
