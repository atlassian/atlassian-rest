package com.atlassian.plugins.rest.sample.v2.filter.dynamicbinding.filters;

import jakarta.annotation.Priority;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;

import com.atlassian.plugins.rest.sample.v2.filter.global.resource.FilterResource;
import com.atlassian.plugins.rest.sample.v2.filter.util.FilterTextAppenderUtil;

/**
 * This class implements the {@link jakarta.ws.rs.container.ContainerRequestFilter} interface to provide
 * custom behavior during the container request filter processing.
 *
 * <p> The {@link javax.annotation.Priority @Priority} annotation is used to control the order of execution of
 * filters. Container request filters with lower numbers are executed first. In this case, the priority
 * is set to {@link jakarta.ws.rs.Priorities#USER}+900 (which results in 5900), indicating its position
 * in the execution chain.
 *
 * <p> The {@code filter} method is overridden to provide the custom behavior. In this method, a text is appended
 * to the header and then it proceeds to the next filter in the chain.
 */
@Priority(Priorities.USER + 900)
public class ContainerRequestDynamicBindingFilter2 implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext) {
        requestContext
                .getHeaders()
                .add(FilterResource.FILTER_HEADER, FilterTextAppenderUtil.createText(this.getClass()));
    }
}
