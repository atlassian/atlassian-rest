package com.atlassian.plugins.rest.sample.v2.filter.util;

import jakarta.annotation.Priority;

public class FilterTextAppenderUtil {

    public static String createText(Class<?> clazz) {
        Priority annotation = clazz.getAnnotation(Priority.class);
        if (annotation == null) {
            return "This text is added from " + clazz.getSimpleName()
                    + " with NO PRIORITY assigned - the default USER priority value = 5000 is used";
        } else {
            return "This text is added from " + clazz.getSimpleName() + " with Priority = " + annotation.value();
        }
    }
}
