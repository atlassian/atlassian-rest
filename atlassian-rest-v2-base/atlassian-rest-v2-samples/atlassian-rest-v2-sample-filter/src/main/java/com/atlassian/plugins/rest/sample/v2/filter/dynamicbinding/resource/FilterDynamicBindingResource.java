package com.atlassian.plugins.rest.sample.v2.filter.dynamicbinding.resource;

import java.util.List;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.atlassian.plugins.rest.sample.v2.filter.dynamicbinding.annotation.FilterDynamicBinding;
import com.atlassian.plugins.rest.sample.v2.filter.dynamicbinding.filters.ContainerRequestDynamicBindingFilter1;
import com.atlassian.plugins.rest.sample.v2.filter.dynamicbinding.filters.ContainerRequestDynamicBindingFilter2;
import com.atlassian.plugins.rest.sample.v2.filter.dynamicbinding.filters.ContainerResponseDynamicBindingFilter1;
import com.atlassian.plugins.rest.sample.v2.filter.dynamicbinding.filters.ContainerResponseDynamicBindingFilter2;

/**
 * This resource class demonstrates the use of {@link FilterDynamicBinding} annotation on a class
 * and a method within the class to bind the filters with the resource method through Dynamic Binding.
 *
 * <p> The class is bound to several {@link ContainerRequestFilter} and {@link ContainerResponseFilter} instances.
 * The container request filters are invoked before the resource method is executed, and they are used to manipulate
 * incoming requests. The container response filters are invoked after the resource method is executed, and they
 * are used to manipulate outgoing responses.
 *
 * <p> The order of execution of the filters is determined by the {@link javax.annotation.Priority @Priority}
 * annotation.
 *
 * @see FilterDynamicBinding
 */
@Path("/filterDynamicBinding")
@FilterDynamicBinding({ContainerRequestDynamicBindingFilter1.class, ContainerResponseDynamicBindingFilter1.class})
public class FilterDynamicBindingResource {

    public static final String FILTER_HEADER = "Filter-Header";

    @GET
    @UnrestrictedAccess
    @FilterDynamicBinding({ContainerRequestDynamicBindingFilter2.class, ContainerResponseDynamicBindingFilter2.class})
    public String filter(@Context HttpHeaders httpHeaders) {
        List<String> headers = httpHeaders.getRequestHeaders().get(FILTER_HEADER);
        String message = "This text is added from " + this.getClass().getSimpleName();
        headers.add(message);
        return "FilterResource has been processed.";
    }
}
