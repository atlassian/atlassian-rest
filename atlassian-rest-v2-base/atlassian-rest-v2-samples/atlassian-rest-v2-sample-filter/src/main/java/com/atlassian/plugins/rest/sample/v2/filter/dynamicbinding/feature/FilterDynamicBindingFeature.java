package com.atlassian.plugins.rest.sample.v2.filter.dynamicbinding.feature;

import java.util.stream.Stream;
import jakarta.ws.rs.container.DynamicFeature;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.FeatureContext;
import jakarta.ws.rs.ext.Provider;

import com.atlassian.plugins.rest.sample.v2.filter.dynamicbinding.annotation.FilterDynamicBinding;

/**
 * This class represents a feature which configures the filters for resources.
 * It checks for the presence of the {@link FilterDynamicBinding} annotation at the method and class
 * levels and then register the classes.
 *
 * <p> The class is annotated with {@link jakarta.ws.rs.ext.Provider @Provider}, which enables
 * Atlassian REST plugin to discover and automatically register the filter.
 */
@Provider
public class FilterDynamicBindingFeature implements DynamicFeature {

    @Override
    public void configure(ResourceInfo resourceInfo, FeatureContext context) {
        registerFilters(resourceInfo.getResourceMethod().getAnnotation(FilterDynamicBinding.class), context);
        registerFilters(resourceInfo.getResourceClass().getAnnotation(FilterDynamicBinding.class), context);
    }

    private void registerFilters(FilterDynamicBinding annotation, FeatureContext context) {
        if (annotation != null) {
            Stream.of(annotation.value()).forEach(context::register);
        }
    }
}
