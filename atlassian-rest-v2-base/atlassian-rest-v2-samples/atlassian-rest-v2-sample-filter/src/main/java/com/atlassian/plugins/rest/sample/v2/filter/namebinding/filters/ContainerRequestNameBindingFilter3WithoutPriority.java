package com.atlassian.plugins.rest.sample.v2.filter.namebinding.filters;

import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.ext.Provider;

import com.atlassian.plugins.rest.sample.v2.filter.global.resource.FilterResource;
import com.atlassian.plugins.rest.sample.v2.filter.namebinding.annotation.FilterNameBinding2;
import com.atlassian.plugins.rest.sample.v2.filter.util.FilterTextAppenderUtil;

/**
 * This class demonstrates the use of the {@link FilterNameBinding2} annotation, implementing the
 * {@link jakarta.ws.rs.container.ContainerRequestFilter} interface to provide custom behavior during the
 * container request filter processing.
 *
 * <p> The class uses {@link jakarta.ws.rs.ext.Provider @Provider} annotation, which enables Atlassian REST plugin
 * to discover and automatically register the filter.
 *
 * <p> When the {@link javax.annotation.Priority @Priority} annotation is absent on a component, the system uses
 * a default priority value. According to the Jakarta EE specification, this default value is
 * {@link jakarta.ws.rs.Priorities#USER}.
 *
 * <p> The {@code filter} method is overridden to provide the custom behavior. In this method, a text is appended
 * to the header and then it proceeds to the next filter in the chain.
 */
@Provider
@FilterNameBinding2
public class ContainerRequestNameBindingFilter3WithoutPriority implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext) {
        requestContext
                .getHeaders()
                .add(FilterResource.FILTER_HEADER, FilterTextAppenderUtil.createText(this.getClass()));
    }
}
