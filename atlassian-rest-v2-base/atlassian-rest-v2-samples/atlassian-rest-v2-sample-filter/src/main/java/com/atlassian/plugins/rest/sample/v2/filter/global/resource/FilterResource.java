package com.atlassian.plugins.rest.sample.v2.filter.global.resource;

import java.util.List;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.container.PreMatching;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * This resource class demonstrates how global filters can be matched with resource method.
 *
 * <p> All filters under {@link com.atlassian.plugins.rest.sample.v2.filter.global.filters}
 * are registered globally.
 *
 * <p> The order of execution of the filters is determined by the
 * {@link javax.annotation.Priority @Priority} annotation assigned to each filter.
 *
 * <p> The order of execution is as follows:
 * <ul>
 *   <li>{@link PreMatching} filters</li>
 *   <li>{@link ContainerRequestFilter} filters</li>
 *   <li>Resource method (e.g., {@code filter})</li>
 *   <li>{@link ContainerResponseFilter} filters</li>
 * </ul>
 */
@Path("/filter")
public class FilterResource {

    public static final String FILTER_HEADER = "Filter-Header";

    @GET
    @UnrestrictedAccess
    public String filter(@Context HttpHeaders httpHeaders) {
        List<String> headers = httpHeaders.getRequestHeaders().get(FILTER_HEADER);
        String message = "This text is added from " + this.getClass().getSimpleName();
        headers.add(message);
        return "FilterResource has been processed.";
    }
}
