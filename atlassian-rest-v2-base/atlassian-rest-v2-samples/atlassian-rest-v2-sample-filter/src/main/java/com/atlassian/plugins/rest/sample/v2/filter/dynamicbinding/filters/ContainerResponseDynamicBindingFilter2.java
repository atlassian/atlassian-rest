package com.atlassian.plugins.rest.sample.v2.filter.dynamicbinding.filters;

import java.util.List;
import jakarta.annotation.Priority;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;

import com.atlassian.plugins.rest.sample.v2.filter.global.resource.FilterResource;
import com.atlassian.plugins.rest.sample.v2.filter.util.FilterTextAppenderUtil;

/**
 * This class implements the {@link jakarta.ws.rs.container.ContainerResponseFilter} interface to provide
 * custom behavior during the container response filter processing.
 *
 * <p> The {@link javax.annotation.Priority @Priority} annotation is used to control the order of execution of
 * filters. Container response filters with higher numbers are executed first. In this case, the priority
 * is set to {@link jakarta.ws.rs.Priorities#USER}+900 (which results in 5900), indicating its position
 * in the execution chain.
 *
 * <p> The {@code filter} method is overridden to provide the custom behavior. In this method, a text is appended
 * to the header and then it proceeds to the next filter in the chain.
 */
@Priority(Priorities.USER + 900)
public class ContainerResponseDynamicBindingFilter2 implements ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        List<String> headers = (List<String>)
                responseContext.getHeaders().get(FilterResource.FILTER_HEADER).get(0);
        headers.add(FilterTextAppenderUtil.createText(this.getClass()));
    }
}
