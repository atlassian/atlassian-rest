package com.atlassian.plugins.rest.sample.v2.filter.deprecation;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import com.atlassian.plugins.rest.api.deprecation.annotation.DeprecatedEndpoint;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * This resource class demonstrates how {@link com.atlassian.plugins.rest.api.deprecation.annotation.DeprecatedEndpoint}
 * annotation can be used on methods.
 */
@Path("/deprecated")
@Produces({MediaType.TEXT_PLAIN})
@UnrestrictedAccess
public class DeprecatedEndpointsResource {

    @GET
    @Path("supported")
    public Response someSupportedMethod() {
        return Response.ok().build();
    }

    @GET
    @Deprecated
    @Path("java-deprecated")
    public Response someJavaDeprecatedMethod() {
        return Response.ok().build();
    }

    @GET
    @Path("deprecated")
    @DeprecatedEndpoint(since = "2023-04-26T16:00:00Z")
    public Response someDeprecatedMethod() {
        return Response.ok().build();
    }

    @GET
    @Path("deprecated-with-wrong-date")
    @DeprecatedEndpoint(since = "wrong-date")
    public Response deprecatedMethodWithWrongDate() {
        return Response.ok().build();
    }

    @GET
    @Path("deprecated-with-link")
    @DeprecatedEndpoint(since = "2023-04-26T16:00:01Z", link = "https://atlassian.com")
    public Response anotherDeprecatedMethod() {
        return Response.ok().build();
    }

    @GET
    @Path("deprecated-with-json-link")
    @DeprecatedEndpoint(
            since = "2023-04-26T16:00:02Z",
            link = "https://atlassian.com/2024-01-01-deprecation.json",
            linkType = "application/json")
    public Response deprecatedMethodWithJsonLink() {
        return Response.ok().build();
    }

    @GET
    @Path("deprecated-with-wrong-link")
    @DeprecatedEndpoint(since = "2023-04-26T16:00:03Z", link = "htt://atlassian.com/blah")
    public Response deprecatedMethodWithWrongLink() {
        return Response.ok().build();
    }

    @GET
    @Path("deprecated-with-wrong-link-type")
    @DeprecatedEndpoint(since = "2023-04-26T16:00:04Z", link = "https://atlassian.com/2024-01-01-deprecation.json")
    public Response deprecatedMethodWithWrongLinkType() {
        return Response.ok().build();
    }
}
