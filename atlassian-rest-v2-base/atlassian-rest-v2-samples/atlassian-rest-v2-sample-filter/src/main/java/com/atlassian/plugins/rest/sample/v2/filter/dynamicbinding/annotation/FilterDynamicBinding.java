package com.atlassian.plugins.rest.sample.v2.filter.dynamicbinding.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is used for dynamic binding of filters.
 * It can be applied to methods, and classes (types).
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface FilterDynamicBinding {
    Class<?>[] value();
}
