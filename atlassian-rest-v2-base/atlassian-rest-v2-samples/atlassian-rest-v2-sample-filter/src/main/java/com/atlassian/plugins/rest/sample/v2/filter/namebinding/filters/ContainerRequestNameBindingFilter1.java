package com.atlassian.plugins.rest.sample.v2.filter.namebinding.filters;

import jakarta.annotation.Priority;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.ext.Provider;

import com.atlassian.plugins.rest.sample.v2.filter.global.resource.FilterResource;
import com.atlassian.plugins.rest.sample.v2.filter.namebinding.annotation.FilterNameBinding1;
import com.atlassian.plugins.rest.sample.v2.filter.util.FilterTextAppenderUtil;

/**
 * This class demonstrates the use of the {@link FilterNameBinding1} annotation, implementing the
 * {@link jakarta.ws.rs.container.ContainerRequestFilter} interface to provide custom behavior during the
 * container request filter processing.
 *
 * <p> The class uses {@link jakarta.ws.rs.ext.Provider @Provider} annotation, which enables Atlassian REST plugin
 * to discover and automatically register the filter.
 *
 * <p> The {@link javax.annotation.Priority @Priority} annotation is used to control the order of execution of
 * filters. Container request filters with lower numbers are executed first. In this case, the priority
 * is set to {@link jakarta.ws.rs.Priorities#USER}+800 (which results in 5800), indicating its position
 * in the execution chain.
 *
 * <p> The {@code filter} method is overridden to provide the custom behavior. In this method, a text is appended
 * to the header and then it proceeds to the next filter in the chain.
 */
@Priority(5800)
@Provider
@FilterNameBinding1
public class ContainerRequestNameBindingFilter1 implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext) {
        requestContext
                .getHeaders()
                .add(FilterResource.FILTER_HEADER, FilterTextAppenderUtil.createText(this.getClass()));
    }
}
