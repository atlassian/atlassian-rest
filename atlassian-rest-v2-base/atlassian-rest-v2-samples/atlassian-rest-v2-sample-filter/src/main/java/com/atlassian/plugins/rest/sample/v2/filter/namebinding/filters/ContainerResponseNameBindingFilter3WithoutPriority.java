package com.atlassian.plugins.rest.sample.v2.filter.namebinding.filters;

import java.util.List;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.ext.Provider;

import com.atlassian.plugins.rest.sample.v2.filter.global.resource.FilterResource;
import com.atlassian.plugins.rest.sample.v2.filter.namebinding.annotation.FilterNameBinding2;
import com.atlassian.plugins.rest.sample.v2.filter.util.FilterTextAppenderUtil;

/**
 * This class demonstrates the use of the {@link FilterNameBinding2} annotation, implementing the
 * {@link jakarta.ws.rs.container.ContainerResponseFilter} interface to provide custom behavior during the
 * container response filter processing.
 *
 * <p> The class uses {@link jakarta.ws.rs.ext.Provider @Provider} annotation, which enables Atlassian REST plugin
 * to discover and automatically register the filter.
 *
 * <p> When the {@link javax.annotation.Priority @Priority} annotation is absent on a component, the system uses
 * a default priority value. According to the Jakarta EE specification, this default value is
 * {@link jakarta.ws.rs.Priorities#USER}.
 *
 * <p> The {@code filter} method is overridden to provide the custom behavior. In this method, a text is appended
 * to the header and then it proceeds to the next filter in the chain.
 */
@Provider
@FilterNameBinding2
public class ContainerResponseNameBindingFilter3WithoutPriority implements ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        List<String> headers = (List<String>)
                responseContext.getHeaders().get(FilterResource.FILTER_HEADER).get(0);
        headers.add(FilterTextAppenderUtil.createText(this.getClass()));
    }
}
