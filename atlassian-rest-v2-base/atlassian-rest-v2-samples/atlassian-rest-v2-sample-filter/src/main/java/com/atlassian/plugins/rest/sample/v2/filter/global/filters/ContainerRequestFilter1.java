package com.atlassian.plugins.rest.sample.v2.filter.global.filters;

import jakarta.annotation.Priority;
import jakarta.ws.rs.NameBinding;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.DynamicFeature;
import jakarta.ws.rs.ext.Provider;

import com.atlassian.plugins.rest.sample.v2.filter.dynamicbinding.resource.FilterDynamicBindingResource;
import com.atlassian.plugins.rest.sample.v2.filter.global.resource.FilterResource;
import com.atlassian.plugins.rest.sample.v2.filter.util.FilterTextAppenderUtil;

/**
 * This class implements the {@link jakarta.ws.rs.container.ContainerRequestFilter} interface to provide
 * custom behavior during the container request filter processing.
 *
 * <p> The scope of this filter is global which means this filter will be executed for all resource methods.
 * Alternative approach is to bind resource method to specific filters.
 * In order to achieve that we may use {@link NameBinding} or {@link DynamicFeature}.
 * Name binding example {@link FilterDynamicBindingResource}.
 * Dynamic binding example {@link FilterDynamicBindingResource}.
 *
 * <p> The class uses {@link jakarta.ws.rs.ext.Provider @Provider} annotation, which enables Atlassian REST plugin
 * to discover and automatically register the filter.
 *
 * <p> The {@link javax.annotation.Priority @Priority} annotation is used to control the order of execution of
 * filters. Container request filters with lower numbers are executed first. In this case, the priority
 * is set to {@link jakarta.ws.rs.Priorities#USER}+1000 (which results in 6000), indicating its position
 * in the execution chain.
 *
 * <p> The {@code filter} method is overridden to provide the custom behavior. In this method, a text is appended
 * to the header and then it proceeds to the next filter in the chain.
 */
@Priority(Priorities.USER + 1000)
@Provider
public class ContainerRequestFilter1 implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext) {
        requestContext
                .getHeaders()
                .add(FilterResource.FILTER_HEADER, FilterTextAppenderUtil.createText(this.getClass()));
    }
}
