package com.atlassian.plugins.rest.sample.v2.filter.namebinding.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import jakarta.ws.rs.NameBinding;

/**
 * This annotation is used to create a name binding between a filter and a class
 * or a method. This name binding annotation can be used to associate filters
 * with resource classes or methods.
 *
 * <p> {@link NameBinding} is used to create custom annotations which can be
 * used to bind JAX-RS filters and interceptors to specific resource classes and
 * methods. By applying {@link NameBinding} to this custom annotation, we enable
 * it to be used for binding filters to the resources.
 *
 * @see jakarta.ws.rs.NameBinding
 */
@NameBinding
@Retention(RetentionPolicy.RUNTIME)
public @interface FilterNameBinding1 {}
