package com.atlassian.plugins.rest.sample.v2.exception;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.MediaType.APPLICATION_XML;
import static jakarta.ws.rs.core.MediaType.TEXT_PLAIN;

/**
 * This is an example resource class used to test {@link WebApplicationException}.
 */
@Produces({APPLICATION_XML, APPLICATION_JSON, TEXT_PLAIN})
@Path("/webApplicationExceptionMapper")
@UnrestrictedAccess
public class WebApplicationExceptionResource {

    public static final String NOT_FOUND_RESPONSE_BODY = "Not Found response body";
    public static final String INTERNAL_SERVER_ERROR_RESPONSE_BODY = "Internal Server Error response body";
    public static final String REQUEST_HEADER_FIELDS_TOO_LARGE_RESPONSE_BODY =
            "Request Header Fields Too Large response body";
    public static final String UNSUPPORTED_STATUS_RESPONSE_BODY = "Unsupported status response body";
    public static final int NOT_SUPPORTED_STATUS = 444;

    @GET
    @Path("/notFoundWithoutBody")
    public Response notFoundWithoutBody() {
        throw new WebApplicationException(
                (Response.status(Response.Status.NOT_FOUND).build()));
    }

    @GET
    @Path("/notFoundWithBody")
    public Response notFoundWithBody() {
        throw new WebApplicationException((Response.status(Response.Status.NOT_FOUND)
                .entity(NOT_FOUND_RESPONSE_BODY)
                .build()));
    }

    @GET
    @Path("/internalErrorWithoutBody")
    public Response internalErrorWithoutBody() {
        throw new WebApplicationException(
                (Response.status(Response.Status.INTERNAL_SERVER_ERROR).build()));
    }

    @GET
    @Path("/internalErrorWithBody")
    public Response internalErrorWithBody() {
        throw new WebApplicationException((Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(INTERNAL_SERVER_ERROR_RESPONSE_BODY)
                .build()));
    }

    @GET
    @Path("/supportedStatusWithoutBody")
    public Response supportedStatusWithoutBody() {
        throw new WebApplicationException((Response.status(Response.Status.REQUEST_HEADER_FIELDS_TOO_LARGE)
                .build()));
    }

    @GET
    @Path("/supportedStatusWithBody")
    public Response supportedStatusWithBody() {
        throw new WebApplicationException((Response.status(Response.Status.REQUEST_HEADER_FIELDS_TOO_LARGE)
                .entity(REQUEST_HEADER_FIELDS_TOO_LARGE_RESPONSE_BODY)
                .build()));
    }

    @GET
    @Path("/notSupportedStatusWithoutBody")
    public Response notSupportedStatusWithoutBody() {
        throw new WebApplicationException(Response.status(NOT_SUPPORTED_STATUS).build());
    }

    @GET
    @Path("/notSupportedStatusWithBody")
    public Response notSupportedStatusWithBody() {
        throw new WebApplicationException(Response.status(NOT_SUPPORTED_STATUS)
                .entity(UNSUPPORTED_STATUS_RESPONSE_BODY)
                .build());
    }
}
