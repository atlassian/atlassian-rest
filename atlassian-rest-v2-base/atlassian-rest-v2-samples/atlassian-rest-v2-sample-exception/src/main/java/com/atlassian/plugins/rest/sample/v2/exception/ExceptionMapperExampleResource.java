package com.atlassian.plugins.rest.sample.v2.exception;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;

import com.atlassian.plugins.rest.api.security.annotation.AdminOnly;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.MediaType.APPLICATION_XML;
import static jakarta.ws.rs.core.MediaType.TEXT_PLAIN;

/**
 * This is an example resource class used to test different exception mappers.
 */
@Produces({APPLICATION_XML, APPLICATION_JSON, TEXT_PLAIN})
@Path("/mapper")
public class ExceptionMapperExampleResource {
    @GET
    @Path("/uncaughtInternalException")
    @UnrestrictedAccess
    public Object unrestrictedAccess(@QueryParam("message") String message) {
        throw new IllegalStateException(message);
    }

    @GET
    @Path("/unauthorizedException")
    @AdminOnly
    public Object adminOnly(@QueryParam("message") String message) {
        throw new IllegalStateException(message);
    }
}
