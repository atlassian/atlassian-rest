package com.atlassian.plugins.rest.sample.v2.exception;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

@Path("notFoundException")
public class NotFoundExceptionResource {

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @UnrestrictedAccess
    public String throwNotFoundException() {
        throw new NotFoundException();
    }
}
