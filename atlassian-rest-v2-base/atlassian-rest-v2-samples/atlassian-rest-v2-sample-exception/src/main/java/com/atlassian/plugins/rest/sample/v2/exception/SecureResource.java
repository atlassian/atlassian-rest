package com.atlassian.plugins.rest.sample.v2.exception;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;

import com.atlassian.plugins.rest.api.security.annotation.AdminOnly;
import com.atlassian.plugins.rest.api.security.annotation.AnonymousSiteAccess;
import com.atlassian.plugins.rest.api.security.annotation.LicensedOnly;
import com.atlassian.plugins.rest.api.security.annotation.SystemAdminOnly;
import com.atlassian.plugins.rest.api.security.annotation.UnlicensedSiteAccess;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.MediaType.APPLICATION_XML;
import static jakarta.ws.rs.core.MediaType.TEXT_PLAIN;

@Produces({APPLICATION_XML, APPLICATION_JSON, TEXT_PLAIN})
@Path("/secured")
public class SecureResource {

    @GET
    @Path("/empty-access")
    public String emptyAccess() {
        return "empty access";
    }

    @GET
    @Path("/unrestricted-access")
    @UnrestrictedAccess
    public String unrestrictedAccess() {
        return "unrestricted access";
    }

    @GET
    @Path("/anonymous-site-access")
    @AnonymousSiteAccess
    public String anonymousSiteAccess() {
        return "anonymous site access";
    }

    @GET
    @Path("/unlicensed-site-access")
    @UnlicensedSiteAccess
    public String unlicensedSiteAccess() {
        return "unlicensed site access";
    }

    @GET
    @Path("/licensed-only")
    @LicensedOnly
    public String licensedOnly() {
        return "licensed only";
    }

    @GET
    @Path("/admin-only")
    @AdminOnly
    public String adminOnly() {
        return "admin only";
    }

    @GET
    @Path("/system-admin-only")
    @SystemAdminOnly
    public String systemAdminOnly() {
        return "system admin only";
    }
}
