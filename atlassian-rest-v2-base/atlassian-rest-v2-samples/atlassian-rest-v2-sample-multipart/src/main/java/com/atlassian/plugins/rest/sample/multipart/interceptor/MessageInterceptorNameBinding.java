package com.atlassian.plugins.rest.sample.multipart.interceptor;

import java.io.IOException;
import jakarta.inject.Inject;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.ext.Provider;
import jakarta.ws.rs.ext.WriterInterceptor;
import jakarta.ws.rs.ext.WriterInterceptorContext;

import com.atlassian.plugins.rest.sample.multipart.models.Message;

@Provider
@MessageInterceptor
public class MessageInterceptorNameBinding implements WriterInterceptor {

    @Inject
    private NameProviderService nameProviderService;

    @Override
    public void aroundWriteTo(WriterInterceptorContext context) throws IOException, WebApplicationException {
        final Message message = (Message) context.getEntity();
        message.setMessage(message.getMessage() + ": " + nameProviderService.getName());
        context.proceed();
    }
}
