package com.atlassian.plugins.rest.sample.multipart.models;

import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Message {
    private String message;

    public Message() {}

    public Message(String value) {
        this.message = value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
