package com.atlassian.plugins.rest.sample.multipart;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.atlassian.plugins.rest.sample.multipart.interceptor.NameProviderService;

@Configuration
public class SpringBeans {

    @Bean
    public NameProviderService nameProviderService() {
        return new NameProviderService();
    }
}
