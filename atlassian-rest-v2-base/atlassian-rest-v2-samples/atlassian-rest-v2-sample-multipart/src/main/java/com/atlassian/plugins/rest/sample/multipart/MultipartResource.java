package com.atlassian.plugins.rest.sample.multipart;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Context;

import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.plugins.rest.api.multipart.FilePart;
import com.atlassian.plugins.rest.api.multipart.MultipartConfig;
import com.atlassian.plugins.rest.api.multipart.MultipartConfigClass;
import com.atlassian.plugins.rest.api.multipart.MultipartForm;
import com.atlassian.plugins.rest.api.multipart.MultipartFormParam;
import com.atlassian.plugins.rest.api.multipart.MultipartHandler;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.atlassian.plugins.rest.sample.multipart.interceptor.MessageInterceptor;
import com.atlassian.plugins.rest.sample.multipart.models.FilePartObject;
import com.atlassian.plugins.rest.sample.multipart.models.FilePartObjects;
import com.atlassian.plugins.rest.sample.multipart.models.Message;

@Path("/")
@UnrestrictedAccess
public class MultipartResource {

    private static final String FILE_FIELD_NAME = "file";

    @POST
    @Path("single")
    @XsrfProtectionExcluded
    public FilePartObject showMultipartSingle(@MultipartFormParam(FILE_FIELD_NAME) FilePart filePart) {
        return createFilePartObject(FILE_FIELD_NAME, filePart);
    }

    @POST
    @Path("multiple")
    @XsrfProtectionExcluded
    public FilePartObjects showMultipartMultiple(@MultipartFormParam(FILE_FIELD_NAME) Collection<FilePart> fileParts) {
        return createFilePartObjects(FILE_FIELD_NAME, fileParts);
    }

    @POST
    @Path("multipleAsList")
    @XsrfProtectionExcluded
    public FilePartObjects showMultipartMultipleAsList(@MultipartFormParam(FILE_FIELD_NAME) List<FilePart> fileParts) {
        return createFilePartObjects(FILE_FIELD_NAME, fileParts);
    }

    @POST
    @Path("multipleAsSet")
    @XsrfProtectionExcluded
    public FilePartObjects showMultipartMultipleAsSet(@MultipartFormParam(FILE_FIELD_NAME) Set<FilePart> fileParts) {
        return createFilePartObjects(FILE_FIELD_NAME, fileParts);
    }

    @POST
    @Path("config")
    @MultipartConfigClass(MultipartResource.SmallMultipartConfig.class)
    @XsrfProtectionExcluded
    public FilePartObjects showMultipartConfig(@MultipartFormParam(FILE_FIELD_NAME) Collection<FilePart> fileParts) {
        return createFilePartObjects(FILE_FIELD_NAME, fileParts);
    }

    @POST
    @Path("fileName")
    @MessageInterceptor
    @XsrfProtectionExcluded
    public Message returnFileName(@MultipartFormParam(FILE_FIELD_NAME) FilePart filePart) {
        return new Message(filePart.getName());
    }

    @POST
    @Path("/singleUsingHandler")
    @XsrfProtectionExcluded
    public FilePartObject showMultipartSingleUsingHandler(
            @Context MultipartHandler multipartHandler, @Context HttpServletRequest request) {
        FilePart filePart = multipartHandler.getFilePart(request, FILE_FIELD_NAME);
        return createFilePartObject(FILE_FIELD_NAME, filePart);
    }

    @POST
    @Path("/singleUsingHandlerFormFilePart")
    @XsrfProtectionExcluded
    public FilePartObject showMultipartSingleUsingHandlerFormFilePart(
            @Context MultipartHandler multipartHandler, @Context HttpServletRequest request) {
        MultipartForm form = multipartHandler.getForm(request);
        FilePart filePart = form.getFilePart(FILE_FIELD_NAME);
        return createFilePartObject(FILE_FIELD_NAME, filePart);
    }

    @POST
    @Path("/singleUsingHandlerFormFileParts")
    @XsrfProtectionExcluded
    public FilePartObjects showMultipartSingleUsingHandlerFormFileParts(
            @Context MultipartHandler multipartHandler, @Context HttpServletRequest request) {
        MultipartForm form = multipartHandler.getForm(request);
        Collection<FilePart> fileParts = form.getFileParts(FILE_FIELD_NAME);
        return createFilePartObjects(FILE_FIELD_NAME, fileParts);
    }

    @POST
    @Path("/singleUsingHandlerFormDynamicField")
    @XsrfProtectionExcluded
    public FilePartObjects showMultipartSingleUsingHandlerFormDynamicField(
            @Context MultipartHandler multipartHandler, @Context HttpServletRequest request) {
        MultipartForm form = multipartHandler.getForm(request);
        List<FilePartObject> filePartObjectList = new ArrayList<>();
        for (String fieldName : form.getFieldNames()) {
            Collection<FilePart> fileParts = form.getFileParts(fieldName);
            FilePartObjects filePartObjectsForField = createFilePartObjects(fieldName, fileParts);
            filePartObjectList.addAll(filePartObjectsForField.getFileParts());
        }
        return new FilePartObjects(filePartObjectList);
    }

    public static class SmallMultipartConfig implements MultipartConfig {

        @Override
        public long getMaxFileSize() {
            return 10;
        }

        @Override
        public long getMaxSize() {
            return 1000;
        }

        @Override
        public long getMaxFileCount() {
            return 8;
        }
    }

    private FilePartObjects createFilePartObjects(String fieldName, Collection<FilePart> fileParts) {
        return new FilePartObjects(fileParts.stream()
                .map(filePart -> createFilePartObject(fieldName, filePart))
                .toList());
    }

    private FilePartObject createFilePartObject(String fieldName, FilePart filePart) {
        return new FilePartObject(
                fieldName, filePart.isFormField(), filePart.getName(), filePart.getContentType(), filePart.getValue());
    }
}
