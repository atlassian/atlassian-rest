package com.atlassian.plugins.rest.sample.multipart.models;

import java.util.Collection;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FilePartObjects {
    @XmlElement(name = "filePartObject")
    private Collection<FilePartObject> fileParts;

    public FilePartObjects() {}

    public FilePartObjects(final Collection<FilePartObject> fileParts) {
        this.fileParts = fileParts;
    }

    public Collection<FilePartObject> getFileParts() {
        return fileParts;
    }
}
