package com.atlassian.plugins.rest.sample.v2.swagger.deprecation;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.parser.OpenAPIV3Parser;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DeprecationEndpointSwaggerGenerationTest {

    private OpenAPI generatedOpenAPI;

    @Before
    public void setUp() throws Exception {

        String generatedSwaggerPath = String.format(
                "%s/%s.json",
                System.getProperty("swagger.generated.location"), System.getProperty("swagger.generated.filename"));
        String generatedJson = new String(Files.readAllBytes(Paths.get(generatedSwaggerPath)));
        generatedOpenAPI =
                new OpenAPIV3Parser().readContents(generatedJson, null, null).getOpenAPI();
    }

    @Test
    public void shouldDeprecatedSetWhenAnnotatedAtMethodsTest() {
        Optional<Boolean> actualDeprecated = Optional.ofNullable(generatedOpenAPI
                .getPaths()
                .get("/deprecated-methods/deprecated")
                .getGet()
                .getDeprecated());
        assertTrue(actualDeprecated.orElse(false));
    }

    @Test
    public void shouldDeprecatedSetWhenJavaAnnotatedTest() {
        Optional<Boolean> actualDeprecated = Optional.ofNullable(generatedOpenAPI
                .getPaths()
                .get("/deprecated-methods/java-deprecated")
                .getGet()
                .getDeprecated());
        assertTrue(actualDeprecated.orElse(false));
    }

    @Test
    public void shouldNotDeprecatedSetWhenOverriddenMethodNotAnnotatedTest() {
        Optional<Boolean> actualDeprecated = Optional.ofNullable(generatedOpenAPI
                .getPaths()
                .get("/deprecated-methods/non-deprecated-overridden")
                .getGet()
                .getDeprecated());
        assertFalse(actualDeprecated.orElse(false));
    }

    @Test
    public void shouldDeprecatedSetWhenAnnotatedAtClassTest() {
        Optional<Boolean> actualDeprecated = Optional.ofNullable(generatedOpenAPI
                .getPaths()
                .get("/deprecated-class/deprecated")
                .getGet()
                .getDeprecated());
        assertTrue(actualDeprecated.orElse(false));
    }

    @Test
    public void shouldDeprecatedSetWhenAnnotatedAtPackageTest() {
        Optional<Boolean> actualDeprecated = Optional.ofNullable(generatedOpenAPI
                .getPaths()
                .get("/deprecated-package/deprecated")
                .getGet()
                .getDeprecated());
        assertTrue(actualDeprecated.orElse(false));
    }
}
