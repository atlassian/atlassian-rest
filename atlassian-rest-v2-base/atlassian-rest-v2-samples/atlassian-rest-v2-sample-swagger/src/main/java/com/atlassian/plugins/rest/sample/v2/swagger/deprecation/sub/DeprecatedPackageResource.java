package com.atlassian.plugins.rest.sample.v2.swagger.deprecation.sub;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

import com.atlassian.plugins.rest.api.deprecation.annotation.DeprecatedEndpoint;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * This resource class demonstrates how {@link DeprecatedEndpoint}
 * annotation can be used on package level.
 */
@Path("/deprecated-package")
@Produces({MediaType.TEXT_PLAIN})
@UnrestrictedAccess
@OpenAPIDefinition(
        info =
                @Info(
                        title = "Deprecated endpoint resources",
                        version = "8.1.0",
                        description = "REST endpoint for annotating with new @DeprecatedEndpoint"))
public class DeprecatedPackageResource {

    @GET
    @Path("deprecated")
    public Response someDeprecatedMethod() {
        return Response.ok().build();
    }
}
