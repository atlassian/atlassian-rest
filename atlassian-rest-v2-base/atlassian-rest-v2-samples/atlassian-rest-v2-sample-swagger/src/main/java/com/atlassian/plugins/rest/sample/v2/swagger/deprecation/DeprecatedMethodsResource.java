package com.atlassian.plugins.rest.sample.v2.swagger.deprecation;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

import com.atlassian.plugins.rest.api.deprecation.annotation.DeprecatedEndpoint;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * This resource class demonstrates how {@link com.atlassian.plugins.rest.api.deprecation.annotation.DeprecatedEndpoint}
 * annotation can be used on methods.
 */
@Path("/deprecated-methods")
@Produces({MediaType.TEXT_PLAIN})
@UnrestrictedAccess
@OpenAPIDefinition(
        info =
                @Info(
                        title = "Deprecated endpoint resources",
                        version = "8.1.0",
                        description = "REST endpoint for annotating with new @DeprecatedEndpoint"))
public class DeprecatedMethodsResource {

    @GET
    @Path("java-deprecated")
    @Deprecated
    public Response someJavaDeprecatedMethod() {
        return Response.ok().build();
    }

    @GET
    @Path("deprecated")
    @DeprecatedEndpoint(since = "2023-04-26T16:00:00Z")
    public Response someDeprecatedMethod() {
        return Response.ok().build();
    }

    @GET
    @Path("non-deprecated-overridden")
    public Response someDeprecatedMethod(String id) {
        return Response.ok().build();
    }
}
