package com.atlassian.plugins.rest.sample.v2.marshalling.request.entity;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ClientMessagePojo {

    @XmlAttribute
    private String message;

    public ClientMessagePojo(String message) {
        this.message = message;
    }

    public ClientMessagePojo() {}

    public String getMessage() {
        return message;
    }
}
