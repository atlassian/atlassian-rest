package com.atlassian.plugins.rest.sample.v2.marshalling.requestcontext;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ResponsePojo {
    @XmlAttribute
    private String contentType;

    @XmlAttribute
    private String accept;

    @XmlAttribute
    private String custom;

    public ResponsePojo(String contentType, String accept, String custom) {
        this.contentType = contentType;
        this.accept = accept;
        this.custom = custom;
    }

    public ResponsePojo() {}

    public String getContentType() {
        return contentType;
    }

    public String getAccept() {
        return accept;
    }

    public String getCustom() {
        return custom;
    }
}
