package com.atlassian.plugins.rest.sample.v2.marshalling.visibility;

/**
 * Example POJO with no annotations from {@link jakarta.xml.bind.annotation} and {@link com.fasterxml.jackson.annotation} on the
 * class in order to test the visibility of public fields.
 */
public class NoAnnotationFieldsOnlyPojo {
    public String publicField;
    public boolean publicBoolean;
    private String privateFiled;
    private boolean privateBoolean;
}
