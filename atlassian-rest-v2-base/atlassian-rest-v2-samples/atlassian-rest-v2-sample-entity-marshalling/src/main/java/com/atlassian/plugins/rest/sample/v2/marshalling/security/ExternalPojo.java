package com.atlassian.plugins.rest.sample.v2.marshalling.security;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ExternalPojo {
    @XmlElement
    private String elem;

    public ExternalPojo() {}
}
