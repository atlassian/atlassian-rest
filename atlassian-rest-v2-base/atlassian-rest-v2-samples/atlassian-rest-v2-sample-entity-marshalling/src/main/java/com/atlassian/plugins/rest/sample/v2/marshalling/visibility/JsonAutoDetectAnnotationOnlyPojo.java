package com.atlassian.plugins.rest.sample.v2.marshalling.visibility;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

/**
 * Example POJO with {@link JsonAutoDetect} annotation on the class in order to test property accessor visibility.
 */
@JsonAutoDetect
public class JsonAutoDetectAnnotationOnlyPojo {
    public String publicField;
    public boolean publicBoolean;
    private String privateFiled;
    private boolean privateBoolean;

    public JsonAutoDetectAnnotationOnlyPojo(
            String publicField, boolean publicBoolean, String privateFiled, boolean privateBoolean) {
        this.publicField = publicField;
        this.publicBoolean = publicBoolean;
        this.privateFiled = privateFiled;
        this.privateBoolean = privateBoolean;
    }

    public String getPrivateFiled() {
        return privateFiled;
    }

    public void setPrivateFiled(String privateFiled) {
        this.privateFiled = privateFiled;
    }

    public boolean isPrivateBoolean() {
        return privateBoolean;
    }

    public void setPrivateBoolean(boolean privateBoolean) {
        this.privateBoolean = privateBoolean;
    }
}
