package com.atlassian.plugins.rest.sample.v2.marshalling.visibility;

/**
 * Example POJO with no annotations from {@link jakarta.xml.bind.annotation} and {@link com.fasterxml.jackson.annotation} on the
 * class in order to test property accessor visibility.
 */
public class NoAnnotationPojo {
    public String publicField;
    public boolean publicBoolean;
    private String privateFiled;
    private boolean privateBoolean;

    public NoAnnotationPojo(String publicField, boolean publicBoolean, String privateFiled, boolean privateBoolean) {
        this.publicField = publicField;
        this.publicBoolean = publicBoolean;
        this.privateFiled = privateFiled;
        this.privateBoolean = privateBoolean;
    }

    public String getPrivateFiled() {
        return privateFiled;
    }

    public void setPrivateFiled(String privateFiled) {
        this.privateFiled = privateFiled;
    }

    public boolean isPrivateBoolean() {
        return privateBoolean;
    }

    public void setPrivateBoolean(boolean privateBoolean) {
        this.privateBoolean = privateBoolean;
    }
}
