package com.atlassian.plugins.rest.sample.v2.marshalling.extensions;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.atlassian.plugins.rest.sample.v2.marshalling.provider.SimplePojo;

import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * Request with "excluded.json" URI to {@code ExcludeResource} will not produce json response but a {@code HttpNotFound}
 * exception, because this resource path has been excluded from ExtensionJerseyFilter.
 */
@Path("excluded")
@Produces(APPLICATION_JSON)
public class ExcludedResource {
    @GET
    @UnrestrictedAccess
    public SimplePojo getSimplePojo() {
        return new SimplePojo("Hello", "World");
    }
}
