package com.atlassian.plugins.rest.sample.v2.marshalling.visibility;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

@Path("visibility")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PropertyAccessorVisibilityResource {

    public static final String PUBLIC_FIELD = "public field";
    public static final String PRIVATE_FIELD = "private field";

    @GET
    @UnrestrictedAccess
    @Path("xmlRootElementAnnotationOnly")
    public XmlRootElementAnnotationOnlyPojo getXmlRootElementAnnotationOnlyPojo() {
        return new XmlRootElementAnnotationOnlyPojo(PUBLIC_FIELD, true, PRIVATE_FIELD, true);
    }

    @GET
    @UnrestrictedAccess
    @Path("jsonAutoDetectAnnotationOnly")
    public JsonAutoDetectAnnotationOnlyPojo getJsonAutoDetectAnnotationOnlyPojo() {
        return new JsonAutoDetectAnnotationOnlyPojo(PUBLIC_FIELD, true, PRIVATE_FIELD, true);
    }

    @GET
    @UnrestrictedAccess
    @Path("noAnnotationPojo")
    public NoAnnotationPojo getNoAnnotationPojo() {
        return new NoAnnotationPojo(PUBLIC_FIELD, true, PRIVATE_FIELD, true);
    }

    @GET
    @UnrestrictedAccess
    @Path("noAnnotationFieldsOnlyPojo")
    public NoAnnotationFieldsOnlyPojo getNoAnnotationFieldsOnlyPojo() {
        NoAnnotationFieldsOnlyPojo noAnnotationPublicFieldsOnlyPojo = new NoAnnotationFieldsOnlyPojo();
        noAnnotationPublicFieldsOnlyPojo.publicField = PUBLIC_FIELD;
        noAnnotationPublicFieldsOnlyPojo.publicBoolean = true;
        return noAnnotationPublicFieldsOnlyPojo;
    }
}
