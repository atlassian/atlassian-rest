package com.atlassian.plugins.rest.sample.v2.marshalling.request.provider;

import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyWriter;
import jakarta.ws.rs.ext.Provider;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

import com.atlassian.plugins.rest.sample.v2.marshalling.request.client.ClientServlet;
import com.atlassian.plugins.rest.sample.v2.marshalling.request.entity.ClientMessagePojo;
import com.atlassian.plugins.rest.sample.v2.marshalling.request.entity.ServerMessagePojo;
import com.atlassian.plugins.rest.sample.v2.marshalling.request.server.ServerResource;

/**
 * The {@link MessageBodyWriter} for {@link ClientMessagePojo} that is sent from the {@link ClientServlet}.
 * The provider converts the {@link ClientMessagePojo} pojo into the {@link ServerMessagePojo}, that is an entity consumed
 * by the {@link ServerResource}. The provider must be annotated with {@link Provider}.
 */
@Provider
@Produces(MediaType.APPLICATION_XML)
public class ClientToServerMessagePojoProvider implements MessageBodyWriter<ClientMessagePojo> {

    @Override
    public boolean isWriteable(Class type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return ClientMessagePojo.class.isAssignableFrom(type);
    }

    @Override
    public void writeTo(
            ClientMessagePojo clientMessagePojo,
            Class<?> type,
            Type genericType,
            Annotation[] annotations,
            MediaType mediaType,
            MultivaluedMap<String, Object> httpHeaders,
            OutputStream entityStream)
            throws WebApplicationException {
        ServerMessagePojo serverMessagePojo = new ServerMessagePojo(clientMessagePojo.getMessage());
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ServerMessagePojo.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.marshal(serverMessagePojo, entityStream);
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }
}
