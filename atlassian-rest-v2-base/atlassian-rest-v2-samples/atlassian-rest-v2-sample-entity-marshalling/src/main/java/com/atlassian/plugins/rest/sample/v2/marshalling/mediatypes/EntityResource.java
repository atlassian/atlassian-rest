package com.atlassian.plugins.rest.sample.v2.marshalling.mediatypes;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

@Path("entity")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML})
public class EntityResource {

    @GET
    @UnrestrictedAccess
    public JaxbPojo getJaxbPojo() {
        return new JaxbPojo("Example returned JaxbPojo", 123);
    }

    /**
     * This resource method is limited to accept json media type.
     */
    @POST
    @UnrestrictedAccess
    @Consumes(MediaType.APPLICATION_JSON)
    public JaxbPojo postJsonTypeOnlyJaxbPojo(JaxbPojo jaxbPojo) {
        return jaxbPojo;
    }

    /**
     * This resource method is limited to accept XML media type.
     */
    @Path("/xml")
    @POST
    @UnrestrictedAccess
    @Consumes(MediaType.APPLICATION_XML)
    public JaxbPojo postXmlOnlyJaxbPojo(JaxbPojo jaxbPojo) {
        return jaxbPojo;
    }
}
