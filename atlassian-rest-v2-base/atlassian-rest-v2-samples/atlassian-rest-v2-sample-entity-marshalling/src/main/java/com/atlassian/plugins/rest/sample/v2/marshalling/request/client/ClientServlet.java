package com.atlassian.plugins.rest.sample.v2.marshalling.request.client;

import java.io.IOException;
import jakarta.inject.Inject;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;

import com.atlassian.plugins.rest.sample.v2.marshalling.request.entity.ClientMessagePojo;
import com.atlassian.plugins.rest.sample.v2.marshalling.request.server.ServerResource;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.net.MarshallingRequestFactory;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.ResponseException;

/**
 * The example showcases a usage of the {@link MarshallingRequestFactory}, to send requests with an entity in the request payload.
 * The plugin doesn't need to register any providers to be able to marshall the entity to any basic media type, for example:
 * "application/json", "application/xml".
 */
public class ClientServlet extends HttpServlet {

    private final MarshallingRequestFactory jerseyRequestFactory;
    private final ApplicationProperties applicationProperties;

    @Inject
    public ClientServlet(MarshallingRequestFactory jerseyRequestFactory, ApplicationProperties applicationProperties) {
        this.jerseyRequestFactory = jerseyRequestFactory;
        this.applicationProperties = applicationProperties;
    }

    /**
     * The {@link ClientServlet} sends a POST request to the {@link ServerResource}, using the {@link MarshallingRequestFactory}
     * * from SAL.
     *
     * @param req  an {@link HttpServletRequest} object that
     *             contains the request the client has made
     *             of the servlet
     * @param resp an {@link HttpServletResponse} object that
     *             contains the response the servlet sends
     *             to the client
     */
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) {
        Request jerseyRequest = jerseyRequestFactory.createRequest(
                Request.MethodType.POST,
                applicationProperties.getBaseUrl(UrlMode.ABSOLUTE) + "/rest/marshalling/1/server");
        jerseyRequest.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML);
        jerseyRequest.addHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_XML);
        jerseyRequest.setEntity(new ClientMessagePojo(req.getParameter("message")));
        try {
            String response = jerseyRequest.execute();
            resp.setContentType("text/plain");
            resp.getWriter().write(response);
        } catch (ResponseException | IOException e) {
            throw new RuntimeException(e);
        }
    }
}
