package com.atlassian.plugins.rest.sample.v2.marshalling.provider;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;

/**
 * Example presents pojo with 2 fields with different Jaxb annotations.
 * The expected outcome is that public fields and private fields with getters will be marshalled into jackson
 * entity.
 * The filed with {@link XmlTransient} will not be marshalled.
 */
@XmlRootElement
public class SimplePojo {

    @XmlAttribute
    private String xmlAttributeAnnotatedField;

    @XmlTransient
    private String xmlTransientAnnotatedField;

    public SimplePojo(String xmlAttributeAnnotatedField, String xmlTransientAnnotatedField) {
        this.xmlAttributeAnnotatedField = xmlAttributeAnnotatedField;
        this.xmlTransientAnnotatedField = xmlTransientAnnotatedField;
    }

    public SimplePojo() {}

    public String getXmlAttributeAnnotatedField() {
        return xmlAttributeAnnotatedField;
    }

    public String getXmlTransientAnnotatedField() {
        return xmlTransientAnnotatedField;
    }

    public void setXmlAttributeAnnotatedField(String xmlAttributeAnnotatedField) {
        this.xmlAttributeAnnotatedField = xmlAttributeAnnotatedField;
    }

    public void setXmlTransientAnnotatedField(String xmlTransientAnnotatedField) {
        this.xmlTransientAnnotatedField = xmlTransientAnnotatedField;
    }
}
