package com.atlassian.plugins.rest.sample.v2.marshalling.request.server;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.atlassian.plugins.rest.sample.v2.marshalling.request.entity.ServerMessagePojo;

@Path("server")
@Produces(MediaType.APPLICATION_XML)
@Consumes(MediaType.APPLICATION_XML)
public class ServerResource {

    @POST
    @UnrestrictedAccess
    public String postJsonEntityPojo(ServerMessagePojo serverMessagePojo) {
        return serverMessagePojo.getMessage();
    }
}
