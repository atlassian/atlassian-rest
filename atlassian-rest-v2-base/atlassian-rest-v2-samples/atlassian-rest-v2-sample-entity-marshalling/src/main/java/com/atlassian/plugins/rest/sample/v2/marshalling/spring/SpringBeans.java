package com.atlassian.plugins.rest.sample.v2.marshalling.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.net.MarshallingRequestFactory;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class SpringBeans {
    @Bean
    public MarshallingRequestFactory importMarshallingRequestFactory() {
        return importOsgiService(MarshallingRequestFactory.class);
    }

    @Bean
    public ApplicationProperties applicationProperties() {
        return importOsgiService(ApplicationProperties.class);
    }
}
