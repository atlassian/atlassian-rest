package com.atlassian.plugins.rest.sample.v2.marshalling.request.entity;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ServerMessagePojo {

    @XmlAttribute
    private String message;

    public ServerMessagePojo(String message) {
        this.message = message;
    }

    public ServerMessagePojo() {}

    public String getMessage() {
        return message;
    }
}
