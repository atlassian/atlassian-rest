package com.atlassian.plugins.rest.sample.v2.marshalling.requestcontext;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * {@code SimpleResource} fetches all resources from the incoming request through {@code @Context} annotation
 */
@Path("requestContext")
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class SimpleResource {

    @GET
    @UnrestrictedAccess
    public ResponsePojo getRequestContext(@Context HttpHeaders headers) {
        return new ResponsePojo(
                headers.getHeaderString("Content-Type"),
                headers.getHeaderString("Accept"),
                headers.getHeaderString("Custom-Header"));
    }
}
