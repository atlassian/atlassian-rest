package com.atlassian.plugins.rest.sample.v2.marshalling.security;

import java.util.List;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

@Path("dtd")
@Consumes(MediaType.APPLICATION_XML)
@Produces(MediaType.APPLICATION_XML)
public class DtdExampleResource {

    @POST
    @UnrestrictedAccess
    public Response postEntity(ExternalPojo external) {
        return Response.ok().build();
    }

    @POST
    @Path("list")
    @UnrestrictedAccess
    public Response postEntityList(List<ExternalPojo> elemList) {
        return Response.ok().build();
    }
}
