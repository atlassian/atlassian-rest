package com.atlassian.plugins.rest.sample.v2.marshalling.provider;

import java.time.LocalDate;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

import static jakarta.ws.rs.core.Response.Status.NOT_ACCEPTABLE;

@Path("provider")
@Produces({MediaType.APPLICATION_JSON})
public class CustomJsonProviderResource {

    @GET
    @UnrestrictedAccess
    public SimplePojo getSimplePojo() {
        return new SimplePojo("visible", "not visible");
    }

    /**
     * Jackson includes only non-null properties into marshalling.
     *
     * @return empty {@code SimplePojo} bean
     */
    @GET
    @UnrestrictedAccess
    @Path("nullable")
    public SimplePojo getNullablePojo() {
        return new SimplePojo(null, "not visible");
    }

    @GET
    @UnrestrictedAccess
    @Path("date")
    public PojoWithDate getSomeDates() {
        return new PojoWithDate("Hello!", LocalDate.of(1970, 1, 1));
    }

    @PUT
    @UnrestrictedAccess
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("marshall")
    public Response checkMarshalling(SimplePojo pojo) {
        if ("HELLO".equals(pojo.getXmlAttributeAnnotatedField())) {
            return Response.ok().build();
        }

        return Response.status(NOT_ACCEPTABLE).build();
    }

    @XmlRootElement
    public static class PojoWithDate {

        @XmlAttribute
        private String message;

        @XmlAttribute
        private LocalDate date;

        public PojoWithDate() {}

        public PojoWithDate(String message, LocalDate date) {
            this.message = message;
            this.date = date;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public LocalDate getDate() {
            return date;
        }

        public void setDate(LocalDate date) {
            this.date = date;
        }
    }
}
