package com.atlassian.plugins.rest.sample.v2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.atlassian.sal.api.user.UserManager;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class SpringBeans {

    @Bean
    public UserManager importUserManager() {
        return importOsgiService(UserManager.class);
    }
}
