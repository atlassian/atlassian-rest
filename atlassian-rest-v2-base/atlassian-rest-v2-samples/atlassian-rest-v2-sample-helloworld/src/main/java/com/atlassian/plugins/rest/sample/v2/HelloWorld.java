package com.atlassian.plugins.rest.sample.v2;

import java.util.List;
import java.util.Locale;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;

import com.atlassian.plugins.rest.api.security.annotation.AdminOnly;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;

/**
 * This resource serves paths that start with /hello as per the {@link Path Path("/hello")} annotation.
 * The {@link UnrestrictedAccess} ensures the resource is accessible for user who have not authenticated.
 */
@Path("/hello")
public class HelloWorld {

    private final UserManager userManager;

    @Context
    private HttpHeaders httpHeaders;

    @Inject
    public HelloWorld(UserManager userManager) {
        this.userManager = userManager;
    }

    @GET
    @UnrestrictedAccess
    public String hello() {
        return "HELLO";
    }

    /*
     * This method explained:
     * <ul>
     * <li>{@link GET}, it accepts HTTP GET requests</li>
     * <li>{@link Produces Produces("text/plain")}, the HTTP response content type will be {@code text/plain}</li>
     * <li>{@link Path Path("{name}")}, this method will serve path like /hello/<name> where name can be any valid path element. It will be then passed as a parameter to the parameter annotated with
     * {@link PathParam PathParam("name")}</li>
     * </ul>
     */
    @GET
    @Path("{name}")
    @AdminOnly
    public Response sayHelloTo(@PathParam("name") String name) {
        List<Locale> languages = httpHeaders.getAcceptableLanguages();
        UserProfile userProfile = userManager.getUserProfile(name);

        if (userProfile != null) {
            Locale locale = languages.stream().findFirst().orElse(Locale.US);
            return Response.ok(
                            String.format("Hello %s, are you from %s?", userProfile.getFullName(), locale.getCountry()))
                    .build();
        }

        return Response.status(404, "user not found").build();
    }
}
