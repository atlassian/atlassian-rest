package com.atlassian.plugins.rest.sample.v2.interceptor.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import jakarta.annotation.Priority;
import jakarta.ws.rs.ext.ReaderInterceptorContext;
import jakarta.ws.rs.ext.WriterInterceptorContext;

import org.apache.commons.io.IOUtils;

public class InterceptorTextAppendingUtil {

    private InterceptorTextAppendingUtil() {}

    public static final String ENTITY_MESSAGE = "\nEntity has been processed in the resource method";

    public static void appendText(Class<?> thisClass, ReaderInterceptorContext context) throws IOException {
        Priority annotation = thisClass.getAnnotation(Priority.class);

        InputStream originalInputStream = context.getInputStream();
        String content = IOUtils.toString(originalInputStream, StandardCharsets.UTF_8);

        content = appendContent(content, thisClass.getSimpleName(), annotation);

        context.setInputStream(new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8)));
    }

    public static void appendText(Class<?> thisClass, WriterInterceptorContext context) {
        Priority annotation = thisClass.getAnnotation(Priority.class);

        String content = context.getEntity().toString();
        content = appendContent(content, thisClass.getSimpleName(), annotation);

        context.setEntity(content);
    }

    private static String appendContent(String content, String className, Priority annotation) {
        String priorityValue;
        if (annotation == null) {
            priorityValue = "no priority assigned, the default USER priority value = 5000 is used";
        } else {
            priorityValue = String.valueOf(annotation.value());
        }

        return content + String.format("%nThis is added text from %s with Priority = %s", className, priorityValue);
    }
}
