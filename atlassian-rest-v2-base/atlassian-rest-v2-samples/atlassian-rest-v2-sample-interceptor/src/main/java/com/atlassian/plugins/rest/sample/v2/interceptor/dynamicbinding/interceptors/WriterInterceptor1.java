package com.atlassian.plugins.rest.sample.v2.interceptor.dynamicbinding.interceptors;

import java.io.IOException;
import jakarta.annotation.Priority;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.ext.WriterInterceptor;
import jakarta.ws.rs.ext.WriterInterceptorContext;

import com.atlassian.plugins.rest.sample.v2.interceptor.util.InterceptorTextAppendingUtil;

/**
 * This class implements the {@link WriterInterceptor} interface to provide custom behavior
 * during the marshalling of a response's entity stream.
 * <p>
 * The {@link javax.annotation.Priority @Priority} annotation is used to control the order of execution of
 * interceptors. Interceptors with lower numbers are executed first. In this case, the priority is set to 8000,
 * indicating its position in the execution chain.
 * <p>
 * The {@code aroundWriteTo} method is overridden to provide the custom behavior. In this method, a text is appended
 * to the response context and then it proceeds to the next interceptor in the chain.
 */
@Priority(8000)
public class WriterInterceptor1 implements WriterInterceptor {

    @Override
    public void aroundWriteTo(WriterInterceptorContext context) throws IOException, WebApplicationException {
        InterceptorTextAppendingUtil.appendText(this.getClass(), context);

        context.proceed();
    }
}
