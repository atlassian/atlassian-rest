package com.atlassian.plugins.rest.sample.v2.interceptor.dynamicbinding.interceptors;

import jakarta.annotation.Priority;
import jakarta.ws.rs.Priorities;

/**
 * This class extends {@link ReaderInterceptor3} and overrides its {@link javax.annotation.Priority @Priority} value.
 * <p>
 * The {@link javax.annotation.Priority @Priority} annotation is used to control the order of execution of
 * interceptors, with lower numbers being executed first. In this case, the priority is set to
 * {@link Priorities#USER} + 2500
 * (7500), which is higher than the Priority value of the original {@link ReaderInterceptor3}
 * ({@link Priorities#USER} + 1000).
 * Therefore, this class will be executed after {@link ReaderInterceptor3}
 * <p>
 * By extending {@link ReaderInterceptor3} and setting a new {@link javax.annotation.Priority @Priority},
 * we can change the execution order of the interceptor chain without modifying the original interceptor.
 */
@Priority(Priorities.USER + 2500)
public class ReaderInterceptor3OverridePriority extends ReaderInterceptor3 {}
