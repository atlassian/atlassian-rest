package com.atlassian.plugins.rest.sample.v2.interceptor.namebinding.interceptors;

import java.io.IOException;
import jakarta.annotation.Priority;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.ext.Provider;
import jakarta.ws.rs.ext.ReaderInterceptor;
import jakarta.ws.rs.ext.ReaderInterceptorContext;
import jakarta.ws.rs.ext.WriterInterceptor;
import jakarta.ws.rs.ext.WriterInterceptorContext;

import com.atlassian.plugins.rest.sample.v2.interceptor.namebinding.annotation.InterceptorNameBinding1;
import com.atlassian.plugins.rest.sample.v2.interceptor.util.InterceptorTextAppendingUtil;

/**
 * This class demonstrates the use of the {@code InterceptorNameBinding1} annotation for binding
 * interceptors to resources. It implements both {@link ReaderInterceptor} and {@link WriterInterceptor}
 * interfaces to provide custom behavior during the unmarshalling of a request's entity stream and
 * the marshalling of a response's entity stream.
 *
 * <p>
 * The class uses {@link jakarta.ws.rs.ext.Provider @Provider} annotation, which enables Atlassian REST plugin to
 * discover and automatically register the interceptor.
 * </p>
 *
 * <p>
 * The {@link javax.annotation.Priority @Priority} annotation is used to control the order of execution
 * of interceptors. Here, the priority is set to {@link Priorities#USER} + 4000, indicating its position in the
 * execution chain.
 * </p>
 *
 * <p>
 * The {@code aroundReadFrom} method is overridden to provide custom behavior for reading an entity.
 * The {@code aroundWriteTo} method is overridden for writing an entity. In these methods, a text is
 * appended to the context and then it proceeds to the next interceptor in the chain.
 * </p>
 */
@Priority(Priorities.USER + 4000)
@Provider
@InterceptorNameBinding1
public class ReaderAndWriterInterceptorNameBinding implements ReaderInterceptor, WriterInterceptor {

    @Override
    public Object aroundReadFrom(ReaderInterceptorContext context) throws IOException, WebApplicationException {
        InterceptorTextAppendingUtil.appendText(this.getClass(), context);

        return context.proceed();
    }

    @Override
    public void aroundWriteTo(WriterInterceptorContext context) throws IOException, WebApplicationException {
        InterceptorTextAppendingUtil.appendText(this.getClass(), context);

        context.proceed();
    }
}
