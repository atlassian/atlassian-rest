package com.atlassian.plugins.rest.sample.v2.interceptor.dynamicbinding.interceptors;

import java.io.IOException;
import jakarta.annotation.Priority;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.ext.ReaderInterceptor;
import jakarta.ws.rs.ext.ReaderInterceptorContext;

import com.atlassian.plugins.rest.sample.v2.interceptor.util.InterceptorTextAppendingUtil;

/**
 * This class implements the {@link ReaderInterceptor} interface to provide custom behavior
 * during the unmarshalling of a request's entity stream.
 * <p>
 * The {@link javax.annotation.Priority @Priority} annotation is used to control the order of execution of
 * interceptors. Interceptors with lower numbers are executed first. In this case, the priority is set to
 * {@link Priorities#USER}+1 (5001),
 * indicating its position in the execution chain.
 * <p>
 * The {@code aroundReadFrom} method is overridden to provide the custom behavior. In this method, a text is appended
 * to the request context and then it proceeds to the next interceptor in the chain.
 */
@Priority(Priorities.USER + 1)
public class ReaderInterceptor2 implements ReaderInterceptor {

    @Override
    public Object aroundReadFrom(ReaderInterceptorContext context) throws IOException, WebApplicationException {
        InterceptorTextAppendingUtil.appendText(this.getClass(), context);

        return context.proceed();
    }
}
