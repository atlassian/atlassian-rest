package com.atlassian.plugins.rest.sample.v2.interceptor.namebinding.resource;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.MediaType;

import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.atlassian.plugins.rest.sample.v2.interceptor.namebinding.annotation.InterceptorNameBinding1;
import com.atlassian.plugins.rest.sample.v2.interceptor.namebinding.annotation.InterceptorNameBinding2;

import static com.atlassian.plugins.rest.sample.v2.interceptor.util.InterceptorTextAppendingUtil.ENTITY_MESSAGE;

/**
 * This resource class demonstrates the usage of Name Binding annotations for binding interceptors to the resource
 * method.
 * <p>
 * The entire class is bound to the interceptor associated with {@code @InterceptorNameBinding1}. This annotation is
 * a custom
 * Name Binding annotation that indicates which interceptor(s) should be applied when a resource method is called.
 * </p>
 * <p>
 * The {@code entity} method is additionally bound to the interceptor associated with {@code @InterceptorNameBinding2}.
 * Similar to {@code @InterceptorNameBinding1}, this is also a custom Name Binding annotation that signals the
 * application
 * of a specific interceptor when the method is invoked.
 * </p>
 * <p>
 * In regard to the execution order of the method itself, after all {@code ReaderInterceptor}s have been executed,
 * the method
 * (in this case, {@code entity}) is invoked. Any logic present in this method will be executed at this stage. Once
 * the method
 * execution is finished and a response is being prepared, the {@code WriterInterceptor} is called. Thus, the order
 * of execution
 * is as follows: {@code ReaderInterceptor} -&gt; Resource method (e.g., {@code entity}) -&gt; {@code WriterInterceptor}.
 * <p>
 * This execution order allows for pre-processing before the method execution with {@code ReaderInterceptor} (such as
 * authentication or logging), handling the specific business logic in the method itself, and then post-processing
 * the response
 * with {@code WriterInterceptor} (such as response modification or additional logging).
 * </p>
 */
@Path("/interceptorNameBinding")
@InterceptorNameBinding1
public class InterceptorNameBindingExampleResource {
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @UnrestrictedAccess
    @XsrfProtectionExcluded
    @InterceptorNameBinding2
    public String entity(String entity) {
        return entity + ENTITY_MESSAGE;
    }
}
