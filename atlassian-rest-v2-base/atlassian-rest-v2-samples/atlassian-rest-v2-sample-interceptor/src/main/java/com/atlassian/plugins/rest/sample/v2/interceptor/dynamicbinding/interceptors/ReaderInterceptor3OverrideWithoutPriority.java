package com.atlassian.plugins.rest.sample.v2.interceptor.dynamicbinding.interceptors;

import jakarta.ws.rs.Priorities;

/**
 * This class extends {@link ReaderInterceptor3} without specifying a {@link javax.annotation.Priority @Priority} value.
 * <p>
 * When the {@link javax.annotation.Priority @Priority} annotation is absent on a component,
 * the system uses a default priority value. According to the Jakarta EE specification, this default value
 * is {@link Priorities#USER}.
 * <p>
 * Thus, even though {@link ReaderInterceptor3} may have a priority value assigned, this subclass will
 * have a priority of 5000, illustrating that the priority value is not inherited from the superclass.
 */
public class ReaderInterceptor3OverrideWithoutPriority extends ReaderInterceptor3 {}
