/**
 * This class demonstrates the use of {@code InterceptorDynamicBinding} annotation
 * on a package to bind the interceptors with the resource method through Dynamic Binding.
 */
@InterceptorDynamicBinding({ReaderInterceptor1.class, WriterInterceptor2.class})
package com.atlassian.plugins.rest.sample.v2.interceptor.dynamicbinding.resource;

import com.atlassian.plugins.rest.sample.v2.interceptor.dynamicbinding.annotation.InterceptorDynamicBinding;
import com.atlassian.plugins.rest.sample.v2.interceptor.dynamicbinding.interceptors.ReaderInterceptor1;
import com.atlassian.plugins.rest.sample.v2.interceptor.dynamicbinding.interceptors.WriterInterceptor2;
