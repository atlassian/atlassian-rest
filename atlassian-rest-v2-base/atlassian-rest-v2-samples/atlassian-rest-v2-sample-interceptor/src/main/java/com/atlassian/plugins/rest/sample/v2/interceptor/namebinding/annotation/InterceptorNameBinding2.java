package com.atlassian.plugins.rest.sample.v2.interceptor.namebinding.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import jakarta.ws.rs.NameBinding;

/**
 * This annotation is used to create a name binding between an interceptor and a class or a method.
 * This name binding annotation can be used to associate interceptors with resource classes or methods.
 *
 * <p>
 * {@code @NameBinding} is used to create custom annotations which can then be used to bind
 * JAX-RS filters and interceptors to specific resource classes and methods. By applying {@code @NameBinding}
 * to this custom annotation ({@code InterceptorNameBinding2}), we enable it to be used for binding
 * interceptors to the resources.
 * </p>
 *
 * <p>
 * The application of {@code InterceptorNameBinding2} on a method or class ensures that the interceptor,
 * associated with this name binding, will intercept the execution of that method or class. This selective
 * binding provides a greater control and flexibility over the application of interceptors.
 * </p>
 *
 * <p>
 * For example, a method annotated with {@code @InterceptorNameBinding2} will be intercepted by the
 * interceptor associated with this name binding.
 * </p>
 *
 * @see jakarta.ws.rs.NameBinding
 */
@NameBinding
@Retention(RetentionPolicy.RUNTIME)
public @interface InterceptorNameBinding2 {}
