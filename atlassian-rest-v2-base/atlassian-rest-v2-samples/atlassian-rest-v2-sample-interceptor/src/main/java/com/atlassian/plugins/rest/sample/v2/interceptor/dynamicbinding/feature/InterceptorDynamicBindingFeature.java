package com.atlassian.plugins.rest.sample.v2.interceptor.dynamicbinding.feature;

import java.util.stream.Stream;
import jakarta.ws.rs.container.DynamicFeature;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.FeatureContext;
import jakarta.ws.rs.ext.Provider;

import com.atlassian.plugins.rest.sample.v2.interceptor.dynamicbinding.annotation.InterceptorDynamicBinding;

/**
 * This class represents a feature which configures the interceptors for resources.
 * It checks for the presence of the {@link InterceptorDynamicBinding} annotation at the method, class, and package
 * levels and then register the classes.
 *
 * <p>
 * The class is annotated with {@link jakarta.ws.rs.ext.Provider @Provider}, which enables
 * Atlassian REST plugin to discover and automatically register the interceptor.
 */
@Provider
public class InterceptorDynamicBindingFeature implements DynamicFeature {

    @Override
    public void configure(ResourceInfo resourceInfo, FeatureContext context) {
        // Check annotation at method, class and package level
        getInterceptorsFromAnnotation(
                resourceInfo.getResourceMethod().getAnnotation(InterceptorDynamicBinding.class), context);
        getInterceptorsFromAnnotation(
                resourceInfo.getResourceClass().getAnnotation(InterceptorDynamicBinding.class), context);
        getInterceptorsFromAnnotation(
                resourceInfo.getResourceClass().getPackage().getAnnotation(InterceptorDynamicBinding.class), context);
    }

    private void getInterceptorsFromAnnotation(InterceptorDynamicBinding annotation, FeatureContext context) {
        if (annotation != null) {
            Stream.of(annotation.value()).forEach(context::register);
        }
    }
}
