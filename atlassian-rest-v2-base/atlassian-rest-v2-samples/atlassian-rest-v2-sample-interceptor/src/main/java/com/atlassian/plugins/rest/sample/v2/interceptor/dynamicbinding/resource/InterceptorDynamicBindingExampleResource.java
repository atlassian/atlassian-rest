package com.atlassian.plugins.rest.sample.v2.interceptor.dynamicbinding.resource;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.MediaType;

import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.atlassian.plugins.rest.sample.v2.interceptor.dynamicbinding.annotation.InterceptorDynamicBinding;
import com.atlassian.plugins.rest.sample.v2.interceptor.dynamicbinding.interceptors.ReaderInterceptor2;
import com.atlassian.plugins.rest.sample.v2.interceptor.dynamicbinding.interceptors.ReaderInterceptor3;
import com.atlassian.plugins.rest.sample.v2.interceptor.dynamicbinding.interceptors.ReaderInterceptor3OverridePriority;
import com.atlassian.plugins.rest.sample.v2.interceptor.dynamicbinding.interceptors.ReaderInterceptor3OverrideWithoutPriority;
import com.atlassian.plugins.rest.sample.v2.interceptor.dynamicbinding.interceptors.WriterInterceptor1;
import com.atlassian.plugins.rest.sample.v2.interceptor.dynamicbinding.interceptors.WriterInterceptor3;

import static com.atlassian.plugins.rest.sample.v2.interceptor.util.InterceptorTextAppendingUtil.ENTITY_MESSAGE;

/**
 * This resource class demonstrates the use of {@code InterceptorDynamicBinding} annotation
 * on a class and a method within the class to bind the interceptors with the resource method through Dynamic Binding.
 *
 * <p>
 * The class is bound to several {@link jakarta.ws.rs.ext.ReaderInterceptor} and
 * {@link jakarta.ws.rs.ext.WriterInterceptor} instances.
 * The {@code ReaderInterceptor}s are invoked before the method is executed, and they are used to manipulate
 * incoming requests. The {@code WriterInterceptor}s are invoked after the method is executed, and they are used
 * to manipulate outgoing responses.
 * </p>
 *
 * <p>
 * The order of execution of the interceptors is determined by the {@link javax.annotation.Priority @Priority}
 * annotation. Interceptors with lower numbers are executed first.
 * </p>
 *
 * @see InterceptorDynamicBinding
 */
@Path("/interceptorDynamicBinding")
@InterceptorDynamicBinding({
    ReaderInterceptor2.class,
    ReaderInterceptor3OverridePriority.class,
    ReaderInterceptor3OverrideWithoutPriority.class,
    WriterInterceptor3.class
})
public class InterceptorDynamicBindingExampleResource {
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @UnrestrictedAccess
    @XsrfProtectionExcluded
    @InterceptorDynamicBinding({ReaderInterceptor3.class, WriterInterceptor1.class})
    public String entity(String entity) {
        return entity + ENTITY_MESSAGE;
    }
}
