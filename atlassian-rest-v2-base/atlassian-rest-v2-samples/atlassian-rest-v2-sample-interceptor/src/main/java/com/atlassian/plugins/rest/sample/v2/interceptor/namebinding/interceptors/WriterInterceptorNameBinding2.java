package com.atlassian.plugins.rest.sample.v2.interceptor.namebinding.interceptors;

import java.io.IOException;
import jakarta.annotation.Priority;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.ext.Provider;
import jakarta.ws.rs.ext.WriterInterceptor;
import jakarta.ws.rs.ext.WriterInterceptorContext;

import com.atlassian.plugins.rest.sample.v2.interceptor.namebinding.annotation.InterceptorNameBinding2;
import com.atlassian.plugins.rest.sample.v2.interceptor.util.InterceptorTextAppendingUtil;

/**
 * This class demonstrates the use of the {@code InterceptorNameBinding2} annotation, implementing the
 * {@link WriterInterceptor} interface to provide custom behavior during the marshalling of a response's entity stream.
 *
 * <p>
 * The class uses {@link jakarta.ws.rs.ext.Provider @Provider} annotation, which enables Atlassian REST plugin to
 * discover and automatically register the interceptor.
 * </p>
 *
 * <p>
 * The {@link javax.annotation.Priority @Priority} annotation is used to control the order of execution of
 * interceptors. In this case, the priority is set to {@link Priorities#USER} + 2000, indicating its position in the
 * execution chain.
 * </p>
 *
 * <p>
 * The {@code aroundWriteTo} method is overridden to provide custom behavior during the writing of an entity.
 * In this method, a text is appended to the context, and it then proceeds to the next interceptor in the chain.
 * </p>
 */
@Priority(Priorities.USER + 2000)
@Provider
@InterceptorNameBinding2
public class WriterInterceptorNameBinding2 implements WriterInterceptor {

    @Override
    public void aroundWriteTo(WriterInterceptorContext context) throws IOException, WebApplicationException {
        InterceptorTextAppendingUtil.appendText(this.getClass(), context);

        context.proceed();
    }
}
