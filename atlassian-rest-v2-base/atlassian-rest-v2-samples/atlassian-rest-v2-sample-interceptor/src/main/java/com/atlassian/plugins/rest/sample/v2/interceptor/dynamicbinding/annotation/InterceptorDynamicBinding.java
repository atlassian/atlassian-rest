package com.atlassian.plugins.rest.sample.v2.interceptor.dynamicbinding.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is used for dynamic binding of interceptors.
 * It can be applied to methods, classes (types), and packages.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.PACKAGE})
public @interface InterceptorDynamicBinding {
    Class<?>[] value();
}
