package com.atlassian.plugins.rest.sample.v2.resource;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

import static java.util.Objects.requireNonNull;

/**
 * This resource is used to validate the URL generation capabilities of
 * {@link com.atlassian.plugins.rest.api.util.RestUrlBuilder}.
 * It specifically tests the ability of the RestUrlBuilder to resolve classes that have constructors which
 * require arguments.
 */
@Path("/dummyWithArgConstructor")
@UnrestrictedAccess
public class DummyResourceWithArgConstructor {
    private String arg;
    private Runnable arbitraryType;

    private DummyResourceWithArgConstructor() {}

    /**
     * The parameters are used to verify that the {@code RestUrlBuilder} can resolve classes that require
     * arguments defined in {@code com.atlassian.plugins.rest.v2.util.urlbuilder.ByteBuddyProxyCreator#createEmptyValue(java
     * .lang.Class)} upon the proxy initialization.
     * For unsupported type, null will be returned.
     */
    public DummyResourceWithArgConstructor(final String string, final Runnable arbitraryInterface) {
        requireNonNull(string, "string can't be null");
        requireNonNull(arbitraryInterface, "arbitraryInterface can't be null");
        this.arg = string;
        this.arbitraryType = arbitraryInterface;
    }

    /**
     * A sub-resource used for testing the URL generation functionality.
     */
    @Path("/sub")
    public Response subResource() throws BadRequestException {
        return null;
    }
}
