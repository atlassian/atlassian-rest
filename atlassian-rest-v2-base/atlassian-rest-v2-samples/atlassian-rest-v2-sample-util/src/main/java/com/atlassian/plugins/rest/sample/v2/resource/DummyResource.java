package com.atlassian.plugins.rest.sample.v2.resource;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;

import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * A resource used for testing the URL generation functionality of
 * {@link com.atlassian.plugins.rest.api.util.RestUrlBuilder}.
 */
@Path("/dummy")
@UnrestrictedAccess
public class DummyResource {
    /**
     * A sub-resource used for testing the URL generation functionality.
     */
    @Path("/sub")
    public Response subResource() throws BadRequestException {
        return null;
    }

    /**
     * A sub-resource used for testing the URL generation functionality.
     * {@code TypeNotInstalledException} is used an "external" exception type that will force atlassian-rest to resolve the class
     * correctly
     */
    @Path("/externalException")
    public Response subResourceWithExternalException() throws TypeNotInstalledException {
        return null;
    }

    /**
     * A sub-resource used for testing the URL generation functionality with path parameter.
     */
    @Path("/with-path-param/{param}")
    public Response subResourceWithPathParam(@PathParam("param") String param) {
        return null;
    }

    /**
     * A sub-resource used for testing the URL generation functionality with query parameter.
     */
    @Path("/with-query-param")
    public Response subResourceWithQueryParam(@QueryParam("param") String param) {
        return null;
    }
}
