package com.atlassian.plugins.rest.sample.v2.resource;

import java.net.URI;
import java.net.URISyntaxException;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;

import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.atlassian.plugins.rest.api.util.RestUrlBuilder;

/**
 * This is an example resource to demonstrate how to use {@link RestUrlBuilder} with difference use cases.
 * Each resource of this class uses {@link RestUrlBuilder} to build the url of the resource from {@link DummyResource}.
 */
@Path("/restUrlBuilder")
@UnrestrictedAccess
public class RestUrlBuilderResource {
    public static final String PARAM = "param";
    public static final String BASE_URI = "http://atlassian.com:1234/foo";

    @Inject
    private RestUrlBuilder restUrlBuilder;

    @GET
    @Path("/dummySubResource")
    @Produces("text/plain")
    public String getUrlForDummySubResource() throws URISyntaxException {
        return restUrlBuilder
                .getUrlFor(new URI(BASE_URI), DummyResource.class, DummyResource::subResource)
                .toString();
    }

    @GET
    @Path("/dummySubResourceExternalException")
    @Produces("text/plain")
    public String getUrlForDummySubResourceWithExternalException() throws URISyntaxException {
        return restUrlBuilder
                .getUrlFor(new URI(BASE_URI), DummyResource.class, dummyResource -> {
                    try {
                        dummyResource.subResourceWithExternalException();
                    } catch (TypeNotInstalledException e) {
                        throw new RuntimeException(e);
                    }
                })
                .toString();
    }

    @GET
    @Path("/dummySubResourceWithPathParam")
    @Produces("text/plain")
    public String getUrlForDummySubResourceWithPathParam() throws URISyntaxException {
        return restUrlBuilder
                .getUrlFor(new URI(BASE_URI), DummyResource.class, res -> res.subResourceWithPathParam(PARAM))
                .toString();
    }

    @GET
    @Path("/dummySubResourceWithQueryParam")
    @Produces("text/plain")
    public String getUrlForDummySubResourceWithQueryParam() throws URISyntaxException {
        return restUrlBuilder
                .getUrlFor(new URI(BASE_URI), DummyResource.class, res -> res.subResourceWithQueryParam(PARAM))
                .toString();
    }

    @GET
    @Path("/dummySubResourceWithArgConstructor")
    @Produces("text/plain")
    public String getUrlForDummySubResourceWithArgConstructor() throws URISyntaxException {
        return restUrlBuilder
                .getUrlFor(
                        new URI(BASE_URI),
                        DummyResourceWithArgConstructor.class,
                        DummyResourceWithArgConstructor::subResource)
                .toString();
    }
}
