package com.atlassian.plugins.rest.sample.scanner;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

@Path("base")
@UnrestrictedAccess
public class BaseClass {
    @GET
    public String helloFromBaseClass() {
        return "This is a base class";
    }
}
