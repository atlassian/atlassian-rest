package com.atlassian.plugins.rest.v2.security.websudo.packageannotationrequired;

import com.atlassian.sal.api.websudo.WebSudoNotRequired;

public class MethodWebSudoNotRequiredAnnotation {
    @WebSudoNotRequired
    public void aMethod() {}
}
