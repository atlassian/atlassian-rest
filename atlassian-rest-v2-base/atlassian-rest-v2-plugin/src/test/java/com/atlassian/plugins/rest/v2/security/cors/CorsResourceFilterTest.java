package com.atlassian.plugins.rest.v2.security.cors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugins.rest.api.internal.security.cors.CorsDefaults;
import com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN;
import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.CORS_PREFLIGHT_REQUESTED;
import static com.atlassian.plugins.rest.v2.security.cors.CorsResourceFilter.CORS_PREFLIGHT_FAILED;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CorsResourceFilterTest {

    @Mock
    private CorsDefaultService corsDefaultService;

    @Mock
    private CorsDefaults corsDefaults;

    @Mock
    private CorsDefaults corsDefaults2;

    @Mock
    private ContainerResponseContext responseContext;

    private CorsResourceFilter corsResourceFilter;

    @Before
    public void setUp() {
        when(corsDefaultService.getCorsDefaults())
                .thenReturn(new ArrayList<>(Arrays.asList(corsDefaults, corsDefaults2)));
        corsResourceFilter = new CorsResourceFilter("GET", corsDefaultService);
    }

    @Mock
    private ContainerRequestContext requestContext;

    @Test
    public void testSimplePreflightForGet() throws IOException {
        String origin = "http://localhost";
        when(requestContext.getPropertyNames()).thenReturn(Collections.singletonList(CORS_PREFLIGHT_REQUESTED));
        when(requestContext.getProperty(CORS_PREFLIGHT_REQUESTED)).thenReturn("true");
        when(corsDefaults.allowsOrigin(origin)).thenReturn(true);
        MultivaluedMap<String, String> requestHeaders = new MultivaluedHashMap<>();
        requestHeaders.put("Access-Control-Request-Method", Collections.singletonList("GET"));
        requestHeaders.put("Origin", Collections.singletonList(origin));
        when(requestContext.getHeaders()).thenReturn(requestHeaders);
        when(requestContext.getHeaderString("Access-Control-Request-Method")).thenReturn("GET");
        when(requestContext.getHeaderString("Origin")).thenReturn(origin);

        corsResourceFilter.filter(requestContext);

        Response response = getResponse();
        assertEquals(response.getStringHeaders().getFirst("Access-Control-Allow-Origin"), origin);
    }

    @Test
    public void testPreflightSucceedsWhenOneCorsDefaultsAllowsOrigin() throws IOException {
        String origin = "http://localhost";
        execPreflightWithTwoCorsDefaults(origin);

        Response response = getResponse();
        assertEquals(response.getStringHeaders().getFirst("Access-Control-Allow-Origin"), origin);
    }

    private Response getResponse() {
        ArgumentCaptor<Response> argumentCaptor = ArgumentCaptor.forClass(Response.class);
        verify(requestContext).abortWith(argumentCaptor.capture());
        Response response = argumentCaptor.getValue();
        assertEquals(Status.OK.getStatusCode(), response.getStatus());
        return response;
    }

    @Test
    public void testSecondCorsDefaultsIsNotHitIfDoesntAllowOrigin() throws IOException {
        String origin = "http://localhost";
        execPreflightWithTwoCorsDefaults(origin);
        verify(corsDefaults2, never()).allowsCredentials(any());
        verify(corsDefaults2, never()).getAllowedRequestHeaders(any());
        verify(corsDefaults2, never()).getAllowedResponseHeaders(any());
    }

    @Test
    public void relativeOriginUriIsNotAllowed() throws IOException {
        execPreflightWithTwoCorsDefaults("/not-absolute.com");

        assertNullAllowedOriginHeader();
    }

    @Test
    public void opaqueOriginUriIsNotAllowed() throws IOException {
        execPreflightWithTwoCorsDefaults("opaque:test.com");

        assertNullAllowedOriginHeader();
    }

    @Test
    public void nullOriginIsNotAllowed() throws IOException {
        execPreflightWithTwoCorsDefaults("null");

        assertNullAllowedOriginHeader();
    }

    private void assertNullAllowedOriginHeader() {
        Response response = getResponse();
        assertNull(response.getStringHeaders().get(ACCESS_CONTROL_ALLOW_ORIGIN.value()));
    }

    @Test
    public void testSimplePreflightForGetWrongOrigin() throws IOException {
        String origin = "http://localhost";
        when(requestContext.getPropertyNames()).thenReturn(Collections.singletonList(CORS_PREFLIGHT_REQUESTED));
        when(requestContext.getProperty(CORS_PREFLIGHT_REQUESTED)).thenReturn("true");
        when(corsDefaults.allowsOrigin(origin)).thenReturn(false);
        when(requestContext.getHeaderString("Access-Control-Request-Method")).thenReturn("GET");
        when(requestContext.getHeaderString("Origin")).thenReturn(origin);

        execBadPreflight();

        assertFailedPreflight();
    }

    @Test
    public void testSimplePreflightForGetWrongMethod() throws IOException {
        String origin = "http://localhost";
        when(requestContext.getPropertyNames()).thenReturn(Collections.singletonList(CORS_PREFLIGHT_REQUESTED));
        when(requestContext.getProperty(CORS_PREFLIGHT_REQUESTED)).thenReturn("true");
        when(corsDefaults.allowsOrigin(origin)).thenReturn(false);
        when(requestContext.getHeaderString("Access-Control-Request-Method")).thenReturn("POST");
        when(requestContext.getHeaderString("Origin")).thenReturn(origin);
        when(requestContext.getProperty(CORS_PREFLIGHT_FAILED)).thenReturn("true");

        execBadPreflight();

        assertFailedPreflight();
    }

    @Test
    public void testSimplePreflightForGetWrongHeaders() throws IOException {
        String origin = "http://localhost";
        when(requestContext.getPropertyNames()).thenReturn(Collections.singletonList(CORS_PREFLIGHT_REQUESTED));
        when(requestContext.getProperty(CORS_PREFLIGHT_REQUESTED)).thenReturn("true");
        when(corsDefaults.allowsOrigin(origin)).thenReturn(true);
        when(corsDefaults.getAllowedRequestHeaders(origin)).thenReturn(Collections.singleton("Foo-Header"));
        MultivaluedMap<String, String> requestHeaders = new MultivaluedHashMap<>();
        requestHeaders.put("Access-Control-Request-Method", Collections.singletonList("GET"));
        requestHeaders.put("Access-Control-Request-Headers", Collections.singletonList("Bar-Header"));
        requestHeaders.put("Origin", Collections.singletonList(origin));
        when(requestContext.getHeaders()).thenReturn(requestHeaders);
        when(requestContext.getHeaderString("Access-Control-Request-Method")).thenReturn("GET");
        when(requestContext.getHeaderString("Access-Control-Request-Headers")).thenReturn("Bar-Header");
        when(requestContext.getHeaderString("Origin")).thenReturn(origin);
        when(requestContext.getProperty(CORS_PREFLIGHT_FAILED)).thenReturn("true");

        execBadPreflight();

        assertFailedPreflight();
    }

    @Test
    public void testSimpleGet() throws IOException {
        String origin = "http://localhost";
        when(requestContext.getMethod()).thenReturn("GET");
        when(corsDefaults.allowsOrigin(origin)).thenReturn(true);
        MultivaluedMap<String, String> requestHeaders = new MultivaluedHashMap<>();
        requestHeaders.put("Origin", Collections.singletonList(origin));
        when(requestContext.getHeaders()).thenReturn(requestHeaders);
        when(requestContext.getHeaderString("Origin")).thenReturn(origin);
        when(responseContext.getHeaders()).thenReturn(new MultivaluedHashMap<>());
        MultivaluedMap<String, Object> headers = execNoPreflightWithHeaders();
        assertEquals(headers.getFirst("Access-Control-Allow-Origin"), origin);
    }

    @Test
    public void testSimpleGetWhenOneCorsDefaultsAllowsOrigin() throws IOException {
        String origin = "http://localhost";
        MultivaluedMap<String, Object> headers = execNoPreflightWithHeadersForTwoCorsDefaults(origin);
        assertEquals(headers.getFirst("Access-Control-Allow-Origin"), origin);
    }

    @Test
    public void testSecondCorsDefaultIsNotCalledWhenItDoesntAllowOrigin() throws IOException {
        String origin = "http://localhost";
        execNoPreflightWithHeadersForTwoCorsDefaults(origin);
        verify(corsDefaults2, never()).allowsCredentials(any());
        verify(corsDefaults2, never()).getAllowedRequestHeaders(any());
        verify(corsDefaults2, never()).getAllowedResponseHeaders(any());
    }

    @Test
    public void testSimpleGetWrongOrigin() throws IOException {
        String origin = "https://localhost";
        when(requestContext.getMethod()).thenReturn("GET");
        when(corsDefaults.allowsOrigin(origin)).thenReturn(true);
        when(requestContext.getHeaderString("Origin")).thenReturn("https://foo.com");
        execNoPreflightNoHeaders();
    }

    @Test
    public void testSimpleGetNoOrigin() throws IOException {
        when(requestContext.getMethod()).thenReturn("GET");
        execNoPreflightNoHeaders();
    }

    @Test
    public void testSimplePreflightWithMultipleRequestHeaderValues() throws IOException {
        execPreflightWithRequestHeaders(Collections.singletonList("foo-header,bar-header"));
        verify(requestContext).setProperty("Cors-Preflight-Succeeded", "true");
    }

    @Test
    public void simplePreflightWithMixedCasingRequestHeaderValues() throws IOException {
        execPreflightWithRequestHeaders(
                Collections.singletonList("fOO-header,bar-header"),
                Collections.unmodifiableSet(new HashSet<>(Arrays.asList("Foo-Header", "Bar-Header"))));
        verify(requestContext).setProperty("Cors-Preflight-Succeeded", "true");
    }

    @Test
    public void testSimplePreflightWithMultipleRequestHeaders() throws IOException {
        execPreflightWithRequestHeaders(Arrays.asList("foo-header", "bar-header"));
        verify(requestContext).setProperty("Cors-Preflight-Succeeded", "true");
    }

    @Test
    public void testSimplePreflightWithSpacesInRequestHeaders() throws IOException {
        execPreflightWithRequestHeaders(Collections.singletonList(" foo-header, bar-header "));
        verify(requestContext).setProperty("Cors-Preflight-Succeeded", "true");
    }

    @Test
    public void preflightWithMultipleRequestHeadersHasExpectedAccessControlAllowHeaders() throws IOException {
        final String headerName = CorsHeaders.ACCESS_CONTROL_ALLOW_HEADERS.value();
        Set<String> values = Collections.unmodifiableSet(new HashSet<>(Arrays.asList("foo-header", "bar-header")));
        execPreflightWithRequestHeaders(Collections.emptyList(), values);

        MultivaluedMap<String, Object> headers = getResponse().getHeaders();
        assertEquals(1, headers.get(headerName).size());
        assertEquals(
                values,
                new HashSet<>(
                        Arrays.asList(headers.getFirst(headerName).toString().split(", "))));
    }

    @Test
    public void corsResponseHasExpectedAccessControlExposeHeaders() throws IOException {
        final String origin = "http://localhost";
        final String headerName = CorsHeaders.ACCESS_CONTROL_EXPOSE_HEADERS.value();
        Set<String> values = Collections.unmodifiableSet(new HashSet<>(Arrays.asList("foo-header", "bar-header")));

        when(corsDefaults.getAllowedResponseHeaders(origin)).thenReturn(values);
        when(corsDefaults.allowsOrigin(origin)).thenReturn(true);
        when(requestContext.getHeaderString("Origin")).thenReturn(origin);
        when(responseContext.getHeaders()).thenReturn(new MultivaluedHashMap<>());

        MultivaluedMap<String, Object> headers = execNoPreflightWithHeaders();
        assertEquals(1, headers.get(headerName).size());
        assertEquals(
                values,
                new HashSet<>(
                        Arrays.asList(headers.getFirst(headerName).toString().split(", "))));
    }

    private MultivaluedMap<String, Object> execNoPreflightWithHeaders() throws IOException {
        corsResourceFilter.filter(requestContext);
        corsResourceFilter.filter(requestContext, responseContext);
        return responseContext.getHeaders();
    }

    private void execNoPreflightNoHeaders() throws IOException {
        corsResourceFilter.filter(requestContext);
        corsResourceFilter.filter(requestContext, responseContext);
        verify(responseContext, never()).getHeaders();
    }

    private void execPreflightWithRequestHeaders(List<String> headers, Set<String> allowedHeaders) throws IOException {
        String origin = "http://localhost";
        when(requestContext.getPropertyNames()).thenReturn(Collections.singletonList(CORS_PREFLIGHT_REQUESTED));
        when(requestContext.getProperty(CORS_PREFLIGHT_REQUESTED)).thenReturn("true");
        when(corsDefaults.allowsOrigin(origin)).thenReturn(true);
        when(corsDefaults.getAllowedRequestHeaders(origin)).thenReturn(allowedHeaders);

        MultivaluedMap<String, String> requestHeaders = new MultivaluedHashMap<>();
        requestHeaders.put("Access-Control-Request-Method", Collections.singletonList("GET"));
        requestHeaders.put("Access-Control-Request-Headers", headers);
        requestHeaders.put("Origin", Collections.singletonList(origin));
        when(requestContext.getHeaders()).thenReturn(requestHeaders);
        when(requestContext.getHeaderString("Access-Control-Request-Method")).thenReturn("GET");
        when(requestContext.getHeaderString("Access-Control-Request-Headers")).thenReturn(String.join(", ", headers));
        when(requestContext.getHeaderString("Origin")).thenReturn(origin);
        corsResourceFilter.filter(requestContext);
    }

    private void execPreflightWithRequestHeaders(List<String> headers) throws IOException {
        execPreflightWithRequestHeaders(
                headers, Collections.unmodifiableSet(new HashSet<>(Arrays.asList("foo-header", "bar-header"))));
    }

    private void execBadPreflight() throws IOException {
        corsResourceFilter.filter(requestContext);
        corsResourceFilter.filter(requestContext, responseContext);
    }

    private void assertFailedPreflight() {
        verify(requestContext).setProperty(CORS_PREFLIGHT_FAILED, "true");
        Response response = getResponse();
        assertNull(response.getEntity());
        assertTrue(response.getMetadata().isEmpty());
    }

    private void execPreflightWithTwoCorsDefaults(String origin) throws IOException {
        when(requestContext.getPropertyNames()).thenReturn(Collections.singletonList(CORS_PREFLIGHT_REQUESTED));
        when(requestContext.getProperty(CORS_PREFLIGHT_REQUESTED)).thenReturn("true");
        when(corsDefaults.allowsOrigin(origin)).thenReturn(true);
        when(corsDefaults2.allowsOrigin(origin)).thenReturn(false);
        MultivaluedMap<String, String> requestHeaders = new MultivaluedHashMap<>();
        requestHeaders.put("Access-Control-Request-Method", Collections.singletonList("GET"));
        requestHeaders.put("Origin", Collections.singletonList(origin));
        when(requestContext.getHeaders()).thenReturn(requestHeaders);
        when(requestContext.getHeaderString("Access-Control-Request-Method")).thenReturn("GET");
        when(requestContext.getHeaderString("Origin")).thenReturn(origin);
        corsResourceFilter.filter(requestContext);
    }

    private MultivaluedMap<String, Object> execNoPreflightWithHeadersForTwoCorsDefaults(String origin)
            throws IOException {
        when(requestContext.getMethod()).thenReturn("GET");
        when(corsDefaults.allowsOrigin(origin)).thenReturn(true);
        when(corsDefaults2.allowsOrigin(origin)).thenReturn(false);
        MultivaluedMap<String, String> requestHeaders = new MultivaluedHashMap<>();
        requestHeaders.put("Origin", Collections.singletonList(origin));
        when(requestContext.getHeaders()).thenReturn(requestHeaders);
        when(requestContext.getHeaderString("Origin")).thenReturn(origin);
        when(responseContext.getHeaders()).thenReturn(new MultivaluedHashMap<>());
        return execNoPreflightWithHeaders();
    }
}
