package com.atlassian.plugins.rest.v2.security.websudo.packageannotationrequired;

public class ClassProtectedByPackageAnnotation {
    public void aMethod() {}
}
