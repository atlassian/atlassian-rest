package com.atlassian.plugins.rest.v2.sal;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import jakarta.validation.constraints.NotNull;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.GenericEntity;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyWriter;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlRootElement;

import org.glassfish.jersey.message.internal.MessageBodyFactory;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

public class MarshallingEntityHandlerTest {

    /**
     * @throws IOException see REST-172
     */
    @Test
    public void testMarshall() throws IOException {
        MarshallingEntityHandler marshallingEntityHandler = new MarshallingEntityHandler(Collections.emptySet());
        final String text = "Some \n\ntext";
        AnEntity entity = new AnEntity();
        entity.setText(text);
        String marshalledEntity =
                marshallingEntityHandler.marshall(entity, MediaType.APPLICATION_XML_TYPE, StandardCharsets.UTF_8);
        assertEquals(
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><anEntity><text>" + text
                        + "</text></anEntity>",
                marshalledEntity);
    }

    @Test
    public void testMarshallSupportForGenericEntity() throws IOException {
        MarshallingEntityHandler marshallingEntityHandler = new MarshallingEntityHandler(Collections.emptySet());
        final String text = "Some \n\ntext";
        AnEntity entity = new AnEntity();
        entity.setText(text);
        GenericEntity<AnEntity> genericEntity =
                new GenericEntity<>(entity, entity.getClass().getGenericSuperclass());
        String marshalledEntity = marshallingEntityHandler.marshall(
                genericEntity, MediaType.APPLICATION_XML_TYPE, StandardCharsets.UTF_8);
        assertEquals(
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><anEntity><text>" + text
                        + "</text></anEntity>",
                marshalledEntity);
    }

    @Test
    public void testJerseyEntityHandlerSupportForAllBasicMediaTypes() {
        List<MediaType> pojoMediaTypes =
                Arrays.asList(MediaType.APPLICATION_XML_TYPE, MediaType.APPLICATION_JSON_TYPE, MediaType.WILDCARD_TYPE);
        MarshallingEntityHandler marshallingEntityHandler = new MarshallingEntityHandler(Collections.emptySet());
        MessageBodyFactory messageBodyFactory = marshallingEntityHandler.getMessageBodyFactory();
        pojoMediaTypes.forEach(mediaType -> {
            assertSupportForMediaType(messageBodyFactory, AnEntity.class, mediaType);
        });
        assertSupportForMediaType(messageBodyFactory, String.class, MediaType.TEXT_PLAIN_TYPE);
        assertSupportForMediaType(messageBodyFactory, byte[].class, MediaType.APPLICATION_OCTET_STREAM_TYPE);
        assertSupportForMediaType(messageBodyFactory, MultivaluedMap.class, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
    }

    @Test
    public void testJerseyEntityHandlerThrowsExceptionWhenWriterOrReaderNotFound() {
        MarshallingEntityHandler marshallingEntityHandler = new MarshallingEntityHandler(Collections.emptySet());
        assertThrows(
                RuntimeException.class,
                () -> marshallingEntityHandler.marshall(
                        AnEntity.class, MediaType.APPLICATION_OCTET_STREAM_TYPE, Charset.defaultCharset()));
        assertThrows(
                RuntimeException.class,
                () -> marshallingEntityHandler.unmarshall(
                        AnEntity.class,
                        MediaType.APPLICATION_OCTET_STREAM_TYPE,
                        new ByteArrayInputStream(new byte[0]),
                        Collections.emptyMap()));
    }

    @Test
    public void testCustomProvidersRegistering() {
        Set<Class<?>> customProviders = new HashSet<>();
        customProviders.add(CustomMessageBodyWriter.class);
        MarshallingEntityHandler marshallingEntityHandler = new MarshallingEntityHandler(customProviders);
        MessageBodyFactory messageBodyFactory = marshallingEntityHandler.getMessageBodyFactory();
        List<MessageBodyWriter> writerList = getAllMessageBodyWriters(messageBodyFactory);
        assertTrue(containsMbwOfClass(writerList, CustomMessageBodyWriter.class));
    }

    private boolean containsMbwOfClass(List<MessageBodyWriter> writerList, Class<?> cls) {
        return writerList.stream().anyMatch(writer -> cls.isAssignableFrom(writer.getClass()));
    }

    private List<MessageBodyWriter> getAllMessageBodyWriters(MessageBodyFactory messageBodyFactory) {
        Map<MediaType, List<MessageBodyWriter>> writerMap =
                messageBodyFactory.getWriters(MediaType.APPLICATION_XML_TYPE);
        List<MessageBodyWriter> writerList = new LinkedList<>(writerMap.get(MediaType.APPLICATION_XML_TYPE));
        writerList.addAll(writerMap.get(MediaType.WILDCARD_TYPE));
        return writerList;
    }

    private void assertSupportForMediaType(MessageBodyFactory messageBodyFactory, Class<?> cls, MediaType mediaType) {
        assertNotNull(messageBodyFactory.getMessageBodyWriter(cls, cls, new Annotation[0], mediaType));
        assertNotNull(messageBodyFactory.getMessageBodyReader(cls, cls, new Annotation[0], mediaType));
    }

    @XmlRootElement
    public static class AnEntity {
        @NotNull
        private String text;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }

    @Produces("application/xml")
    private static class CustomMessageBodyWriter implements MessageBodyWriter<AnEntity> {

        @Override
        public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
            return type == AnEntity.class;
        }

        @Override
        public void writeTo(
                AnEntity anEntity,
                Class<?> type,
                Type genericType,
                Annotation[] annotations,
                MediaType mediaType,
                MultivaluedMap<String, Object> httpHeaders,
                OutputStream entityStream)
                throws IOException, WebApplicationException {
            JAXBContext jaxbContext;
            try {
                jaxbContext = JAXBContext.newInstance(type);
                Marshaller entityMarshaller = jaxbContext.createMarshaller();
                entityMarshaller.marshal(anEntity, entityStream);
            } catch (JAXBException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
