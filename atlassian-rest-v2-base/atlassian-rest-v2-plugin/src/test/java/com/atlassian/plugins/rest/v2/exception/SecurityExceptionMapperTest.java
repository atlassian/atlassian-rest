package com.atlassian.plugins.rest.v2.exception;

import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugins.rest.api.security.exception.AuthenticationRequiredException;
import com.atlassian.plugins.rest.api.security.exception.AuthorizationException;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class SecurityExceptionMapperTest {
    private SecurityExceptionMapper securityExceptionMapper;

    @Mock
    private Request request;

    @Before
    public void setupMocks() {
        securityExceptionMapper = new SecurityExceptionMapper();
        securityExceptionMapper.request = request;
    }

    @Test
    public void defaultMapToUnauthorized() {
        SecurityException securityException = Mockito.mock(SecurityException.class);
        Response response = securityExceptionMapper.toResponse(securityException);
        assertEquals("Response status should be unauthorized", 401, response.getStatus());
    }

    @Test
    public void shouldMapAuthorisationException() {
        SecurityException securityException = new AuthorizationException("");
        Response response = securityExceptionMapper.toResponse(securityException);
        assertEquals("Response status should be unauthorized", 403, response.getStatus());
    }

    @Test
    public void shouldMapAuthenticationRequiredException() {
        SecurityException securityException = new AuthenticationRequiredException();
        Response response = securityExceptionMapper.toResponse(securityException);
        assertEquals("Response status should be unauthorized", 401, response.getStatus());
    }
}
