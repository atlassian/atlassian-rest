package com.atlassian.plugins.rest.v2.sal;

import java.io.InputStream;
import java.util.Map;
import jakarta.ws.rs.core.MediaType;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.Plugin;
import com.atlassian.sal.api.net.Response;

import static jakarta.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static jakarta.ws.rs.core.MediaType.APPLICATION_XML_TYPE;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(value = MockitoJUnitRunner.class)
public class MarshallingResponseTest {

    @Mock
    private Response delegateResponse;

    @Mock
    private Plugin plugin;

    @Mock
    private MarshallingEntityHandler marshallingEntityHandler;

    @Mock
    private InputStream responseStream;

    @Mock
    private MyEntity entity;

    @Test
    public void testContentTypeForUnmarshallingDefaultsToApplicationXmlWhenNoHeaderPresent() throws Exception {
        verifyUnmarshalling(null, APPLICATION_XML_TYPE);
    }

    @Test
    public void testContentTypeForUnmarshallingIsTakenFromHeader() throws Exception {
        verifyUnmarshalling(APPLICATION_JSON, APPLICATION_JSON_TYPE);
    }

    private void verifyUnmarshalling(
            final String responseContentTypeHeader, final MediaType expectedUnmarshalledContentType) throws Exception {
        MarshallingResponse marshallingResponse =
                new MarshallingResponse(delegateResponse, marshallingEntityHandler, plugin);
        final Map<String, String> responseHeaders = singletonMap("key1", "value1");

        doReturn(responseStream).when(delegateResponse).getResponseBodyAsStream();
        doReturn(getClass().getClassLoader()).when(plugin).getClassLoader();
        doReturn(entity).when(marshallingEntityHandler).unmarshall(any(), any(), any(), any());
        doReturn(responseHeaders).when(delegateResponse).getHeaders();
        doReturn(responseContentTypeHeader).when(delegateResponse).getHeader(CONTENT_TYPE);

        final MyEntity result = marshallingResponse.getEntity(MyEntity.class);
        verify(marshallingEntityHandler)
                .unmarshall(
                        MyEntity.class,
                        expectedUnmarshalledContentType,
                        responseStream,
                        singletonMap("key1", singletonList("value1")));
        assertEquals(result, entity);
    }

    private interface MyEntity {}
}
