package com.atlassian.plugins.rest.v2.darkfeature;

import java.io.IOException;
import jakarta.ws.rs.NotFoundException;

import org.junit.Test;

import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.doReturn;

public class DarkFeatureResourceFilterTest extends AbstractDarkFeatureTestResource {
    @Test
    public void testFilterWithNoAnnotations() throws IOException, NoSuchMethodException {
        setMethodAnnotated(false);
        setResourceAnnotated(false);

        darkFeatureResourceFilter.filter(containerRequestContext);
    }

    @Test
    public void testFilterWithResourceAnnotationAndFeatureEnabled() throws IOException, NoSuchMethodException {
        setFeatureEnabled(FEATURE_KEY, true);
        setResourceAnnotated(true);
        setMethodAnnotated(false);

        darkFeatureResourceFilter.filter(containerRequestContext);
    }

    @Test
    public void testFilterWithResourceAnnotationAndFeatureDisabled() throws NoSuchMethodException {
        setFeatureEnabled(NOT_FEATURE_KEY, false);
        setResourceAnnotated(true);
        setMethodAnnotated(false);

        assertThrows(NotFoundException.class, () -> darkFeatureResourceFilter.filter(containerRequestContext));
    }

    @Test
    public void testFilterWithMethodAnnotationAndFeatureEnabled() throws IOException, NoSuchMethodException {
        setFeatureEnabled(FEATURE_KEY, true);
        setResourceAnnotated(false);
        setMethodAnnotated(true);

        darkFeatureResourceFilter.filter(containerRequestContext);
    }

    @Test
    public void testFilterWithMethodAnnotationAndFeatureDisabled() throws NoSuchMethodException {
        setFeatureEnabled(NOT_FEATURE_KEY, false);
        setResourceAnnotated(false);
        setMethodAnnotated(true);

        assertThrows(NotFoundException.class, () -> darkFeatureResourceFilter.filter(containerRequestContext));
    }

    @Test
    public void testFilterWithMultipleKeysRequiredAndAllEnabled() throws IOException, NoSuchMethodException {
        Class<?> testMockedClass = TestMockedMultipleAnnotatedClass.class;
        doReturn(testMockedClass).when(resourceInfo).getResourceClass();
        setMethodAnnotated(false);
        setFeatureEnabled(FEATURE_KEY, true);
        setFeatureEnabled(FEATURE_KEY_2, true);

        darkFeatureResourceFilter.filter(containerRequestContext);
    }

    @Test
    public void testFilterWithMultipleKeysRequiredAndNotAllEnabled() throws NoSuchMethodException {
        Class<?> testMockedClass = TestMockedMultipleAnnotatedClass.class;
        doReturn(testMockedClass).when(resourceInfo).getResourceClass();
        setMethodAnnotated(false);
        setFeatureEnabled(FEATURE_KEY, true);
        setFeatureEnabled(FEATURE_KEY_2, false);

        assertThrows(NotFoundException.class, () -> darkFeatureResourceFilter.filter(containerRequestContext));
    }
}
