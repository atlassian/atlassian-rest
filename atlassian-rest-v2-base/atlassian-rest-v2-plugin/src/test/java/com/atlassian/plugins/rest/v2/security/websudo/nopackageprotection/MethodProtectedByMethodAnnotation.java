package com.atlassian.plugins.rest.v2.security.websudo.nopackageprotection;

import com.atlassian.sal.api.websudo.WebSudoRequired;

public class MethodProtectedByMethodAnnotation {

    public void aMethod() {}

    @WebSudoRequired
    public void bMethod() {}
}
