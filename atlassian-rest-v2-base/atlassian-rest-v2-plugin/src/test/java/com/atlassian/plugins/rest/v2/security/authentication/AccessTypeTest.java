package com.atlassian.plugins.rest.v2.security.authentication;

import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

import com.atlassian.annotations.security.AdminOnly;
import com.atlassian.annotations.security.AnonymousSiteAccess;
import com.atlassian.annotations.security.SystemAdminOnly;
import com.atlassian.annotations.security.UnlicensedSiteAccess;
import com.atlassian.annotations.security.UnrestrictedAccess;

import static org.junit.Assert.assertEquals;

/**
 * Unit test class for {@link AccessType}
 * <p>
 *
 * @since 6.3.0
 */
public class AccessTypeTest {
    /**
     * Scenario: Resource has SystemAdminOnly annotation
     * Precondition: Resource method has no annotation
     * Expectation: SystemAdminOnly annotation will be used
     */
    @Test
    public void filterWhenResourceHasSystemAdminOnlyAnnotation() {
        assertEquals(
                AccessType.SYSTEM_ADMIN_ONLY, AccessType.getAccessType(TestSystemAdminOnlyMockedClass.class, null));
    }

    /**
     * Scenario: Resource has no annotation
     * Precondition: Resource method has SystemAdminOnly annotation
     * Expectation: SystemAdminOnly annotation will be used
     */
    @Test
    public void filterWhenMethodHasSystemAdminOnlyAnnotation() throws NoSuchMethodException {
        assertEquals(
                AccessType.SYSTEM_ADMIN_ONLY,
                AccessType.getAccessType(
                        TestMockedClass.class, this.getClass().getMethod("mockedSystemAdminOnlyMethod")));
    }

    /**
     * Scenario: Resource has AdminOnly annotation
     * Precondition: Resource method has no annotation
     * Expectation: AdminOnly annotation will be used
     */
    @Test
    public void filterWhenResourceHasAdminOnlyAnnotation() {
        assertEquals(AccessType.ADMIN_ONLY, AccessType.getAccessType(TestAdminOnlyMockedClass.class, null));
    }

    /**
     * Scenario: Resource has no annotation
     * Precondition: Resource method has AdminOnly annotation
     * Expectation: AdminOnly annotation will be used
     */
    @Test
    public void filterWhenMethodHasAdminOnlyAnnotation() throws NoSuchMethodException {
        assertEquals(
                AccessType.ADMIN_ONLY,
                AccessType.getAccessType(TestMockedClass.class, this.getClass().getMethod("mockedAdminOnlyMethod")));
    }

    /**
     * Scenario: Resource has no annotation
     * Precondition: Resource method has no annotation
     * Expectation: No annotation will be used
     */
    @Test
    public void filterWhenResourceAndMethodHasNoAnnotation() throws NoSuchMethodException {
        assertEquals(
                AccessType.EMPTY,
                AccessType.getAccessType(TestMockedClass.class, this.getClass().getMethod("mockedMethod")));
    }

    /**
     * Scenario: Resource has AnonymousSiteAccess annotation
     * Precondition: Resource method has no annotation
     * Expectation: AnonymousSiteAccess annotation will be used
     */
    @Test
    public void filterWhenResourceHasAnonymousSiteAccessAnnotation() {
        assertEquals(
                AccessType.ANONYMOUS_SITE_ACCESS,
                AccessType.getAccessType(TestAnonymousSiteAccessMockedClass.class, null));
    }

    /**
     * Scenario: Resource has no annotation
     * Precondition: Resource method has AnonymousSiteAccess annotation
     * Expectation: AnonymousSiteAccess annotation will be used
     */
    @Test
    public void filterWhenMethodHasAnonymousSiteAccessAnnotation() throws NoSuchMethodException {
        assertEquals(
                AccessType.ANONYMOUS_SITE_ACCESS,
                AccessType.getAccessType(
                        TestMockedClass.class, this.getClass().getMethod("mockedAnonymousSiteAccessMethod")));
    }

    /**
     * Scenario: Resource has UnlicensedSiteAccess annotation
     * Precondition: Resource method has no annotation
     * Expectation: UnlicensedSiteAccess annotation will be used
     */
    @Test
    public void filterWhenResourceHasUnlicensedSiteAccessAnnotation() {
        assertEquals(
                AccessType.UNLICENSED_SITE_ACCESS,
                AccessType.getAccessType(TestUnlicensedSiteAccessMockedClass.class, null));
    }

    /**
     * Scenario: Resource has no annotation
     * Precondition: Resource method has UnlicensedSiteAccess annotation
     * Expectation: UnlicensedSiteAccess annotation will be used
     */
    @Test
    public void filterWhenMethodHasUnlicensedSiteAccessAnnotation() throws NoSuchMethodException {
        assertEquals(
                AccessType.UNLICENSED_SITE_ACCESS,
                AccessType.getAccessType(
                        TestMockedClass.class, this.getClass().getMethod("mockedUnlicensedSiteAccessMethod")));
    }

    /**
     * Scenario: Resource has AnonymousSiteAccess annotation
     * Precondition: Resource method has SystemAdminOnly annotation
     * Expectation: SystemAdminOnly annotation will be used
     */
    @Test
    public void filterWhenResourceHasAnonymousSiteAccessAnnotationButMethodHasSystemAdminOnlyAnnotation()
            throws NoSuchMethodException {
        assertEquals(
                AccessType.SYSTEM_ADMIN_ONLY,
                AccessType.getAccessType(
                        TestAnonymousSiteAccessMockedClass.class,
                        this.getClass().getMethod("mockedSystemAdminOnlyMethod")));
    }

    /**
     * Scenario: Resource has AnonymousSiteAccess annotation
     * Precondition: Resource method has AdminOnly annotation
     * Expectation: AdminOnly annotation will be used
     */
    @Test
    public void filterWhenResourceHasAnonymousSiteAccessAnnotationButMethodHasAdminOnlyAnnotation()
            throws NoSuchMethodException {
        assertEquals(
                AccessType.ADMIN_ONLY,
                AccessType.getAccessType(
                        TestAnonymousSiteAccessMockedClass.class,
                        this.getClass().getMethod("mockedAdminOnlyMethod")));
    }

    /**
     * Scenario: Resource has AnonymousSiteAccess annotation
     * Precondition: Resource method has UnlicensedSiteAccess annotation
     * Expectation: UnlicensedSiteAccess annotation will be used
     */
    @Test
    public void filterWhenResourceHasAnonymousSiteAccessAnnotationButMethodHasUnlicensedSiteAccessAnnotation()
            throws NoSuchMethodException {
        assertEquals(
                AccessType.UNLICENSED_SITE_ACCESS,
                AccessType.getAccessType(
                        TestAnonymousSiteAccessMockedClass.class,
                        this.getClass().getMethod("mockedUnlicensedSiteAccessMethod")));
    }

    /**
     * Scenario: Resource has UnlicensedSiteAccess annotation
     * Precondition: Resource method has SystemAdminOnl annotation
     * Expectation: SystemAdminOnly annotation will be used
     */
    @Test
    public void filterWhenResourceHasUnlicensedSiteAccessAnnotationButMethodHasSystemAdminOnlyAnnotation()
            throws NoSuchMethodException {
        assertEquals(
                AccessType.SYSTEM_ADMIN_ONLY,
                AccessType.getAccessType(
                        TestUnlicensedSiteAccessMockedClass.class,
                        this.getClass().getMethod("mockedSystemAdminOnlyMethod")));
    }

    /**
     * Scenario: Resource has UnlicensedSiteAccess annotation
     * Precondition: Resource method has AdminOnly annotation
     * Expectation: AdminOnly annotation will be used
     */
    @Test
    public void filterWhenResourceHasUnlicensedSiteAccessAnnotationButMethodHasAdminOnlyAnnotation()
            throws NoSuchMethodException {
        assertEquals(
                AccessType.ADMIN_ONLY,
                AccessType.getAccessType(
                        TestUnlicensedSiteAccessMockedClass.class,
                        this.getClass().getMethod("mockedAdminOnlyMethod")));
    }

    /**
     * Scenario: Resource has UnlicensedSiteAccess annotation
     * Precondition: Resource method has AnonymousSiteAccess annotation
     * Expectation: AnonymousSiteAccess annotation will be used
     */
    @Test
    public void filterWhenResourceHasUnlicensedSiteAccessAnnotationButMethodHasAnonymousSiteAccessAnnotation()
            throws NoSuchMethodException {
        assertEquals(
                AccessType.ANONYMOUS_SITE_ACCESS,
                AccessType.getAccessType(
                        TestUnlicensedSiteAccessMockedClass.class,
                        this.getClass().getMethod("mockedAnonymousSiteAccessMethod")));
    }

    /**
     * Scenario: Resource has SystemAdminOnly annotation
     * Precondition: Resource method has AnonymousSiteAccess annotation
     * Expectation: AnonymousSiteAccess annotation will be used
     */
    @Test
    public void filterWhenResourceHasSystemAdminOnlyAnnotationButMethodHasAnonymousSiteAccessAnnotation()
            throws NoSuchMethodException {
        assertEquals(
                AccessType.ANONYMOUS_SITE_ACCESS,
                AccessType.getAccessType(
                        TestSystemAdminOnlyMockedClass.class,
                        this.getClass().getMethod("mockedAnonymousSiteAccessMethod")));
    }

    /**
     * Scenario: Resource has SystemAdminOnly annotation
     * Precondition: Resource method has UnlicensedSiteAccess annotation
     * Expectation: UnlicensedSiteAccess annotation will be used
     */
    @Test
    public void filterWhenResourceHasSystemAdminOnlyAnnotationButMethodHasUnlicensedSiteAccessAnnotation()
            throws NoSuchMethodException {
        assertEquals(
                AccessType.UNLICENSED_SITE_ACCESS,
                AccessType.getAccessType(
                        TestSystemAdminOnlyMockedClass.class,
                        this.getClass().getMethod("mockedUnlicensedSiteAccessMethod")));
    }

    /**
     * Scenario: Resource has SystemAdminOnly annotation
     * Precondition: Resource method has AdminOnly annotation
     * Expectation: AdminOnly annotation will be used
     */
    @Test
    public void filterWhenResourceHasSystemAdminOnlyAnnotationButMethodHasAdminOnlyAnnotation()
            throws NoSuchMethodException {
        assertEquals(
                AccessType.ADMIN_ONLY,
                AccessType.getAccessType(
                        TestSystemAdminOnlyMockedClass.class, this.getClass().getMethod("mockedAdminOnlyMethod")));
    }

    /**
     * Scenario: Resource has SystemAdminOnly annotation
     * Precondition: Resource method has UnrestrictedAccess annotation
     * Expectation: UnrestrictedAccess annotation will be used
     */
    @Test
    public void filterWhenResourceHasSystemAdminOnlyAnnotationButMethodHasUnrestrictedAccessAnnotation()
            throws NoSuchMethodException {
        assertEquals(
                AccessType.UNRESTRICTED_ACCESS,
                AccessType.getAccessType(
                        TestSystemAdminOnlyMockedClass.class,
                        this.getClass().getMethod("mockedUnrestrictedAccessMethod")));
    }

    /**
     * Scenario: Resource has AdminOnly annotation
     * Precondition: Resource method has AnonymousSiteAccess annotation
     * Expectation: AnonymousSiteAccess annotation will be used
     */
    @Test
    public void filterWhenResourceHasAdminOnlyAnnotationButMethodHasAnonymousSiteAccessAnnotation()
            throws NoSuchMethodException {
        assertEquals(
                AccessType.ANONYMOUS_SITE_ACCESS,
                AccessType.getAccessType(
                        TestAdminOnlyMockedClass.class, this.getClass().getMethod("mockedAnonymousSiteAccessMethod")));
    }

    /**
     * Scenario: Resource has AdminOnly annotation
     * Precondition: Resource method has UnlicensedSiteAccess annotation
     * Expectation: UnlicensedSiteAccess annotation will be used
     */
    @Test
    public void filterWhenResourceHasAdminOnlyAnnotationButMethodHasUnlicensedSiteAccessAnnotation()
            throws NoSuchMethodException {
        assertEquals(
                AccessType.UNLICENSED_SITE_ACCESS,
                AccessType.getAccessType(
                        TestAdminOnlyMockedClass.class, this.getClass().getMethod("mockedUnlicensedSiteAccessMethod")));
    }

    /**
     * Scenario: Resource has AdminOnly annotation
     * Precondition: Resource method has UnrestrictedAccess annotation
     * Expectation: UnrestrictedAccess annotation will be used
     */
    @Test
    public void filterWhenResourceHasAdminOnlyAnnotationButMethodHasUnrestrictedAccessAnnotation()
            throws NoSuchMethodException {
        assertEquals(
                AccessType.UNRESTRICTED_ACCESS,
                AccessType.getAccessType(
                        TestAdminOnlyMockedClass.class, this.getClass().getMethod("mockedUnrestrictedAccessMethod")));
    }

    @Test
    public void shouldIgnoreClassLoadersWhenComparingAnnotations() throws ClassNotFoundException {
        // given
        SystemAdminOnlyClassLoader loader = new SystemAdminOnlyClassLoader();
        Class<?> securedClass = loader.loadClass(TestSystemAdminOnlyMockedClass.class.getName());

        // when
        AccessType accessType = AccessType.getAccessType(securedClass, null);

        // then
        assertEquals(AccessType.SYSTEM_ADMIN_ONLY, accessType);
    }

    @SystemAdminOnly
    public void mockedSystemAdminOnlyMethod() {
        // this is a mocked method
    }

    @AdminOnly
    public void mockedAdminOnlyMethod() {
        // this is a mocked method
    }

    @AnonymousSiteAccess
    public void mockedAnonymousSiteAccessMethod() {
        // this is a mocked method
    }

    @UnlicensedSiteAccess
    public void mockedUnlicensedSiteAccessMethod() {
        // this is a mocked method
    }

    @UnrestrictedAccess
    public void mockedUnrestrictedAccessMethod() {
        // this is a mocked method
    }

    public void mockedMethod() {
        // this is a mocked method
    }

    static class TestMockedClass {
        // this is a mocked class
    }

    @SystemAdminOnly
    static class TestSystemAdminOnlyMockedClass {
        // this is a mocked class
    }

    @AdminOnly
    static class TestAdminOnlyMockedClass {
        // this is a mocked class
    }

    @AnonymousSiteAccess
    static class TestAnonymousSiteAccessMockedClass {
        // this is a mocked class
    }

    @UnlicensedSiteAccess
    static class TestUnlicensedSiteAccessMockedClass {
        // this is a mocked class
    }

    private static final class SystemAdminOnlyClassLoader extends ClassLoader {

        private final Set<String> supportedNames = Stream.of(
                        SystemAdminOnly.class.getName(), TestSystemAdminOnlyMockedClass.class.getName())
                .collect(Collectors.toSet());

        @Override
        public Class<?> loadClass(String name) throws ClassNotFoundException {
            if (!supportedNames.contains(name)) {
                return super.loadClass(name);
            }
            try {
                InputStream in = ClassLoader.getSystemResourceAsStream(name.replace(".", "/") + ".class");
                byte[] a = new byte[10000];
                int len = in.read(a);
                in.close();
                return defineClass(name, a, 0, len);
            } catch (IOException e) {
                throw new ClassNotFoundException();
            }
        }
    }
}
