package com.atlassian.plugins.rest.v2.expand;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.plugins.rest.api.expand.EntityCrawler;
import com.atlassian.plugins.rest.api.expand.annotation.Expandable;
import com.atlassian.plugins.rest.api.expand.expander.EntityExpander;
import com.atlassian.plugins.rest.api.expand.resolver.EntityExpanderResolver;
import com.atlassian.plugins.rest.api.internal.expand.parameter.DefaultExpandParameter;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Testing {@link EntityCrawler}
 */
public class EntityCrawlerTest {
    private DefaultEntityCrawler entityCrawler;

    @Before
    public void setUp() {
        entityCrawler = new DefaultEntityCrawler();
    }

    @Test
    public void testGetExpandableWithNullField() {
        assertNull(entityCrawler.getExpandable(null));
    }

    @Test
    public void testGetExpandableWithNoExpandableAnnotation() throws Exception {
        final Field field = SomeClass.class.getField("field1");
        assertNull(entityCrawler.getExpandable(field));
    }

    @Test
    public void testGetExpandableWithValuedExpandable() throws Exception {
        final Field field = SomeClass.class.getField("field2");
        assertEquals("field2Value", entityCrawler.getExpandable(field).value());
    }

    @Test
    public void testGetExpandableWithNamedXmlElement() throws Exception {
        final Field field = SomeClass.class.getField("field3");
        assertEquals("field3Value", entityCrawler.getExpandable(field).value());
    }

    @Test
    public void testGetExpandableWithUnNamedXmlElement() throws Exception {
        final Field field = SomeClass.class.getField("field4");
        assertEquals("field4", entityCrawler.getExpandable(field).value());
    }

    @Test
    public void testGetExpandableWithNoXmlElement() throws Exception {
        final Field field = SomeClass.class.getField("field5");
        assertEquals("field5", entityCrawler.getExpandable(field).value());
    }

    @Test
    public void testGetPossibleExpands() {
        SomeDerivedClass object = new SomeDerivedClass();
        entityCrawler.crawl(object, new DefaultExpandParameter(Collections.emptyList()), null);
        List<String> newExpands = Arrays.asList(object.expand.split(","));
        assertThat(newExpands, contains("field2Value", "field3Value", "field4", "field5"));
    }

    @Test
    public void testCrawlGetsFieldsFromSuperClass() {
        final Set<String> expectedFields =
                Stream.of("field2Value", "field3Value", "field4", "field5").collect(Collectors.toSet());

        entityCrawler.crawl(
                new SomeDerivedClass(),
                new DefaultExpandParameter(Collections.singleton("*")),
                new EntityExpanderResolver() {
                    public boolean hasExpander(Class<?> type) {
                        return true;
                    }

                    public <T> EntityExpander<T> getExpander(Class<? extends T> type) {
                        return (tExpandContext, expanderResolver, entityCrawler) -> {
                            assertTrue(expectedFields.remove(
                                    tExpandContext.getExpandable().value()));
                            return null;
                        };
                    }
                });
        assertTrue(expectedFields.isEmpty());
    }

    @Test
    public void testGetPossibleExpandsFromMultipleThreads() {
        ExecutorService service = Executors.newFixedThreadPool(10);
        List<SomeDerivedClass> entities = Collections.synchronizedList(new ArrayList<>());

        int iterations = 2000;
        for (int i = 0; i < iterations; i++) {
            service.submit(() -> {
                SomeDerivedClass entity = new SomeDerivedClass();
                entities.add(entity);
                entityCrawler.crawl(entity, new DefaultExpandParameter(Collections.emptyList()), null);
            });
        }

        shutdownAndAwaitTermination(service);
        assertThat(entities.size(), equalTo(iterations));

        for (SomeDerivedClass entity : entities) {
            assertNotNull(entity.expand);
            List<String> newExpands = Arrays.asList(entity.expand.split(","));
            assertThat(newExpands, contains("field2Value", "field3Value", "field4", "field5"));
        }
    }

    private static class SomeClass {
        public Object field1 = new Object();

        @Expandable("field2Value")
        public Object field2 = new Object();

        @Expandable
        @XmlElement(name = "field3Value")
        public Object field3 = new Object();

        @Expandable
        @XmlElement
        public Object field4 = new Object();

        @Expandable
        public Object field5 = new Object();

        @Expandable
        public Object ignoredField;
    }

    private static class SomeDerivedClass extends SomeClass {
        @XmlAttribute
        private String expand;
    }

    // recommended way from the ExecutorService java docs
    void shutdownAndAwaitTermination(ExecutorService pool) {
        pool.shutdown(); // Disable new tasks from being submitted
        try {
            // Wait a while for existing tasks to terminate
            if (!pool.awaitTermination(3, TimeUnit.SECONDS)) {
                pool.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being cancelled
                if (!pool.awaitTermination(3, TimeUnit.SECONDS)) System.err.println("Pool did not terminate");
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            pool.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }
}
