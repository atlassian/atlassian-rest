package com.atlassian.plugins.rest.v2.security.xsrf;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.HttpMethod;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.UriInfo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugins.rest.api.internal.security.cors.CorsDefaults;
import com.atlassian.plugins.rest.api.security.exception.XsrfCheckFailedException;
import com.atlassian.sal.api.web.context.HttpContext;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenAccessor;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenValidator;
import com.atlassian.sal.core.xsrf.XsrfRequestValidatorImpl;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class XsrfResourceFilterTest {
    private static final String URI_ONE = "http://foo.com/hello";
    private static final String URI_TWO = "http://bar.com/hello";
    private static final String URI_ONE_HTTPS = "https://foo.com/hello";
    private static final String URI_ONE_CUSTOM_PORT = "http://foo.com:1234/hello";
    private static final String URI_ONE_NO_PATH = "http://foo.com";
    private static final String URI_ONE_INVALID_QUERY_PARAM = URI_ONE + "?|";
    private static final String INVALID_URI = "!!! oh noes, invalid URI :O";
    private static final String URI_CHROME_EXTENSION = "chrome-extension://foobar";
    private static final String URI_SAFARI_EXTENSION = "safari-extension://foobar";

    private XsrfResourceFilter xsrfResourceFilter;

    @Mock
    private UriInfo uriInfo;

    @Mock
    private ContainerRequestContext requestContext;

    @Mock
    private HttpContext httpContext;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Before
    public void setUp() throws Exception {
        xsrfResourceFilter = new XsrfResourceFilter();
        xsrfResourceFilter.setXsrfRequestValidator(
                new XsrfRequestValidatorImpl(new IndependentXsrfTokenValidator(new IndependentXsrfTokenAccessor())));
        when(httpContext.getRequest()).thenReturn(httpServletRequest);
        xsrfResourceFilter.setHttpContext(httpContext);
        when(requestContext.getUriInfo()).thenReturn(uriInfo);
        when(uriInfo.getRequestUri()).thenReturn(new URI("http://default.com"));
        when(uriInfo.getPath()).thenReturn("/example");
    }

    @Test(expected = Test.None.class /* no exception expected */)
    public void testGetIsNotProtectedByXsrfChecks() throws IOException {
        when(requestContext.getMethod()).thenReturn("GET");
        xsrfResourceFilter.filter(requestContext);
    }

    @Test
    public void testPostWithApplicationFormUrlencodedTypeMediaTypeIsProtectedByXsrfChecks() {
        when(requestContext.getMethod()).thenReturn("POST");
        when(requestContext.getMediaType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        assertThrows(XsrfCheckFailedException.class, () -> xsrfResourceFilter.filter(requestContext));
    }

    @Test
    public void testPostWithMultipartFormDataMediaTypeIsProtectedByXsrfChecks() {
        when(requestContext.getMethod()).thenReturn("POST");
        when(requestContext.getMediaType()).thenReturn(MediaType.MULTIPART_FORM_DATA_TYPE);
        assertThrows(XsrfCheckFailedException.class, () -> xsrfResourceFilter.filter(requestContext));
    }

    @Test
    public void testPostWithTextPlainMediaTypeIsProtectedByXsrfChecks() {
        when(requestContext.getMethod()).thenReturn("POST");
        when(requestContext.getMediaType()).thenReturn(MediaType.TEXT_PLAIN_TYPE);
        assertThrows(XsrfCheckFailedException.class, () -> xsrfResourceFilter.filter(requestContext));
    }

    @Test(expected = Test.None.class /* no exception expected */)
    public void testPostWithNullMediaTypeIsNotProtectedByXsrfChecks() throws IOException {
        when(requestContext.getMethod()).thenReturn("POST");
        when(requestContext.getMediaType()).thenReturn(null);
        xsrfResourceFilter.filter(requestContext);
    }

    @Test(expected = Test.None.class /* no exception expected */)
    public void testPostSuccessWithCorrectHeaderValue() throws IOException {
        when(requestContext.getMediaType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        when(requestContext.getMethod()).thenReturn("POST");
        addTokenHeaderToRequest(httpServletRequest, XsrfResourceFilter.NO_CHECK);
        xsrfResourceFilter.filter(requestContext);
    }

    @Test(expected = Test.None.class /* no exception expected */)
    public void testPostSuccessWithOldHeaderValue() throws IOException {
        final String deprecatedHeaderValue = "nocheck";
        when(requestContext.getMediaType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        when(requestContext.getMethod()).thenReturn("POST");
        when(requestContext.getHeaderString(XsrfResourceFilter.TOKEN_HEADER)).thenReturn(deprecatedHeaderValue);
        xsrfResourceFilter.filter(requestContext);
    }

    @Test
    public void testPostFailureWithInvalidHeaderValue() {
        when(requestContext.getMediaType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        when(requestContext.getMethod()).thenReturn("POST");
        addTokenHeaderToRequest(httpServletRequest, "invalid");
        assertThrows(XsrfCheckFailedException.class, () -> xsrfResourceFilter.filter(requestContext));
    }

    @Test(expected = Test.None.class /* no exception expected */)
    public void testPostJsonSuccess() throws IOException {
        when(requestContext.getMethod()).thenReturn("POST");
        when(requestContext.getMediaType()).thenReturn(MediaType.APPLICATION_JSON_TYPE);
        xsrfResourceFilter.filter(requestContext);
    }

    @Test(expected = Test.None.class /* no exception expected */)
    public void testPutFormNotBlocked() throws IOException {
        when(requestContext.getMethod()).thenReturn("PUT");
        when(requestContext.getMediaType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        xsrfResourceFilter.filter(requestContext);
    }

    @Test(expected = Test.None.class /* no exception expected */)
    public void testPostWithValidXsrfTokenSuccess() throws IOException {
        when(requestContext.getMethod()).thenReturn("POST");
        when(requestContext.getMediaType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        for (String validToken : Arrays.asList("abc123", "ffff", "FFFx3~324", "☃☃☃")) {
            httpServletRequest = setupRequestWithXsrfToken(httpServletRequest, validToken, validToken);
            xsrfResourceFilter.filter(requestContext);
        }
    }

    @Test
    public void testPostWithInvalidXsrfTokenBlocked() {
        when(requestContext.getMethod()).thenReturn("POST");
        when(requestContext.getMediaType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        httpServletRequest = setupRequestWithXsrfToken(httpServletRequest, "abc123", "notabc123");
        assertThrows(XsrfCheckFailedException.class, () -> xsrfResourceFilter.filter(requestContext));
    }

    @Test
    public void testPostWithNullHttpServletRequestXsrfTokenBlocked() {
        when(requestContext.getMethod()).thenReturn("POST");
        when(requestContext.getMediaType()).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        when(httpContext.getRequest()).thenReturn(null);
        xsrfResourceFilter.setHttpContext(httpContext);
        assertThrows(XsrfCheckFailedException.class, () -> xsrfResourceFilter.filter(requestContext));
    }

    @Test(expected = Test.None.class /* no exception expected */)
    public void httpServletRequestMethodPreferredOverContainerRequest() throws IOException {
        when(requestContext.getMethod()).thenReturn(HttpMethod.PUT);
        when(httpServletRequest.getMethod()).thenReturn(HttpMethod.OPTIONS);
        xsrfResourceFilter.filter(requestContext);
    }

    @Test
    public void additionalBrowserChecksWithNullOriginAndReferrerFail() throws Exception {
        assertFalse(runAdditionalBrowserChecks(URI_ONE, null, null));
    }

    @Test
    public void additionalBrowserChecksWithEmptyOriginAndReferrerFail() throws Exception {
        assertFalse(runAdditionalBrowserChecks(URI_ONE, "", ""));
    }

    @Test
    public void additionalBrowserChecksWithInvalidOriginAndReferrerFail() throws Exception {
        assertFalse(runAdditionalBrowserChecks(URI_ONE, INVALID_URI, INVALID_URI));
    }

    @Test
    public void additionalBrowserChecksWithSameOriginPass() throws Exception {
        assertTrue(runAdditionalBrowserChecks(URI_ONE, URI_ONE, URI_TWO));
    }

    @Test
    public void additionalBrowserChecksWithSameOriginAndDifferentPathPass() throws Exception {
        assertTrue(runAdditionalBrowserChecks(URI_ONE, URI_ONE_NO_PATH, URI_TWO));
    }

    @Test
    public void additionalBrowserChecksWithSameOriginButDifferentSecurityLevelFail() throws Exception {
        assertFalse(runAdditionalBrowserChecks(URI_ONE_HTTPS, URI_ONE, URI_TWO));
        assertFalse(runAdditionalBrowserChecks(URI_ONE, URI_ONE_HTTPS, URI_TWO));
    }

    @Test
    public void additionalBrowserChecksWithSameOriginButDifferentPortsFail() throws Exception {
        assertFalse(runAdditionalBrowserChecks(URI_ONE, URI_ONE_CUSTOM_PORT, URI_TWO));
        assertFalse(runAdditionalBrowserChecks(URI_ONE_CUSTOM_PORT, URI_ONE, URI_TWO));
    }

    @Test
    public void additionalBrowserChecksWithSameReferrerPass() throws Exception {
        assertTrue(runAdditionalBrowserChecks(URI_ONE, URI_TWO, URI_ONE));
    }

    @Test
    public void additionalBrowserChecksWithSameOriginReferrerWithInvalidQueryParamPass() throws Exception {
        assertTrue(runAdditionalBrowserChecks(URI_ONE, "", URI_ONE_INVALID_QUERY_PARAM));
    }

    @Test
    public void additionalBrowserChecksWithSameReferrerButDifferentSecurityLevelFail() throws Exception {
        assertFalse(runAdditionalBrowserChecks(URI_ONE_HTTPS, URI_TWO, URI_ONE));
        assertFalse(runAdditionalBrowserChecks(URI_ONE, URI_TWO, URI_ONE_HTTPS));
    }

    @Test
    public void additionalBrowserChecksWithSameReferrerButDifferentPortsFail() throws Exception {
        assertFalse(runAdditionalBrowserChecks(URI_ONE, URI_TWO, URI_ONE_CUSTOM_PORT));
        assertFalse(runAdditionalBrowserChecks(URI_ONE_CUSTOM_PORT, URI_TWO, URI_ONE));
    }

    @Test
    public void additionalBrowserChecksWithDifferentOriginAndReferrerFail() throws Exception {
        assertFalse(runAdditionalBrowserChecks(URI_ONE, URI_TWO, URI_TWO));
    }

    @Test
    public void additionalBrowserChecksWithBrowserExtensionOriginPass() throws Exception {
        assertThat(runAdditionalBrowserChecks(URI_ONE, URI_CHROME_EXTENSION, ""), is(true));
        assertThat(runAdditionalBrowserChecks(URI_ONE_HTTPS, URI_SAFARI_EXTENSION, ""), is(true));
    }

    @Test
    public void additionalBrowserChecksPassIfCorsCredentialsNotAllowedAndNotSupplied() {
        xsrfResourceFilter.setCorsDefaults(setCorsDefaults(URI_ONE, true, false));
        when(requestContext.getHeaderString("Origin")).thenReturn(URI_ONE);
        assertTrue(xsrfResourceFilter.passesAdditionalBrowserChecks(requestContext));
    }

    @Test
    public void additionalBrowserChecksPassIfCorsCredentialsAllowedAndNotSupplied() {
        xsrfResourceFilter.setCorsDefaults(setCorsDefaults(URI_ONE, true, true));
        when(requestContext.getHeaderString("Origin")).thenReturn(URI_ONE);
        assertTrue(xsrfResourceFilter.passesAdditionalBrowserChecks(requestContext));
    }

    @Test
    public void additionalBrowserChecksPassIfCorsCredentialsAllowedAndCookiesSupplied() {
        xsrfResourceFilter.setCorsDefaults(setCorsDefaults(URI_ONE, true, true));
        when(requestContext.getHeaderString("Origin")).thenReturn(URI_ONE);
        setArbitraryCookie(requestContext);
        assertTrue(xsrfResourceFilter.passesAdditionalBrowserChecks(requestContext));
    }

    @Test
    public void additionalBrowserChecksPassIfCorsCredentialsAllowedAndHttpAuthHeaderSupplied() {
        xsrfResourceFilter.setCorsDefaults(setCorsDefaults(URI_ONE, true, true));
        when(requestContext.getHeaderString("Origin")).thenReturn(URI_ONE);
        when(requestContext.getHeaderString("Authorization")).thenReturn("Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==");
        assertTrue(xsrfResourceFilter.passesAdditionalBrowserChecks(requestContext));
    }

    @Test
    public void additionalBrowserChecksFailIfCorsCredentialsDisallowedButCookiesSupplied() {
        xsrfResourceFilter.setCorsDefaults(setCorsDefaults(URI_ONE, true, false));
        when(requestContext.getHeaderString("Origin")).thenReturn(URI_ONE);
        setArbitraryCookie(requestContext);
        assertFalse(xsrfResourceFilter.passesAdditionalBrowserChecks(requestContext));
    }

    @Test
    public void additionalBrowserChecksFailIfCorsCredentialsDisallowedButHttpAuthHeaderSupplied() {
        xsrfResourceFilter.setCorsDefaults(setCorsDefaults(URI_ONE, true, false));
        when(requestContext.getHeaderString("Origin")).thenReturn(URI_ONE);
        when(requestContext.getHeaderString("Authorization")).thenReturn("Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==");
        assertFalse(xsrfResourceFilter.passesAdditionalBrowserChecks(requestContext));
    }

    @Test
    public void additionalBrowserChecksTakePortNumbersIntoAccountWhenCheckingAllowedViaCors() {
        xsrfResourceFilter.setCorsDefaults(setCorsDefaults(URI_ONE, true, false));
        when(requestContext.getHeaderString("Origin")).thenReturn(URI_ONE_CUSTOM_PORT);
        assertFalse(xsrfResourceFilter.passesAdditionalBrowserChecks(requestContext));
    }

    @Test
    public void additionalBrowserChecksTakeHttpHttpsIntoAccountWhenCheckingAllowedViaCors() {
        xsrfResourceFilter.setCorsDefaults(setCorsDefaults(URI_ONE, true, false));
        when(requestContext.getHeaderString("Origin")).thenReturn(URI_ONE_HTTPS);
        assertFalse(xsrfResourceFilter.passesAdditionalBrowserChecks(requestContext));
    }

    static void addTokenHeaderToRequest(HttpServletRequest httpServletRequest, String headerValue) {
        when(httpServletRequest.getHeader(XsrfResourceFilter.TOKEN_HEADER)).thenReturn(headerValue);
    }

    private static HttpServletRequest setupRequestWithXsrfToken(
            HttpServletRequest request, String expectedToken, String formToken) {
        Cookie[] cookies = {new Cookie(IndependentXsrfTokenAccessor.XSRF_COOKIE_KEY, expectedToken)};
        when(request.getCookies()).thenReturn(cookies);
        when(request.getParameter(IndependentXsrfTokenValidator.XSRF_PARAM_NAME))
                .thenReturn(formToken);
        return request;
    }

    private boolean runAdditionalBrowserChecks(final String uri, final String origin, final String referrer)
            throws Exception {
        xsrfResourceFilter.setCorsDefaults(Collections.emptyList());

        when(requestContext.getHeaderString("Origin")).thenReturn(origin);
        when(requestContext.getHeaderString("Referer")).thenReturn(referrer);
        when(uriInfo.getRequestUri()).thenReturn(new URI(uri));

        return xsrfResourceFilter.passesAdditionalBrowserChecks(requestContext);
    }

    private List<CorsDefaults> setCorsDefaults(
            final String originUri, final boolean originAllowed, final boolean credentialsAllowed) {
        final CorsDefaults corsDefaults = mock(CorsDefaults.class);
        when(corsDefaults.allowsOrigin(originUri)).thenReturn(originAllowed);
        when(corsDefaults.allowsCredentials(originUri)).thenReturn(credentialsAllowed);
        return new ArrayList<>(Collections.singletonList(corsDefaults));
    }

    private void setArbitraryCookie(final ContainerRequestContext requestContext) {
        when(requestContext.getCookies())
                .thenReturn(Collections.singletonMap("foo", new jakarta.ws.rs.core.Cookie("bar", "baz")));
    }
}
