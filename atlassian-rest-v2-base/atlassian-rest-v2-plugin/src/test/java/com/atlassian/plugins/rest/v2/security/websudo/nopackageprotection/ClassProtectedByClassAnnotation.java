package com.atlassian.plugins.rest.v2.security.websudo.nopackageprotection;

import com.atlassian.sal.api.websudo.WebSudoRequired;

@WebSudoRequired
public class ClassProtectedByClassAnnotation {
    public void aMethod() {}
}
