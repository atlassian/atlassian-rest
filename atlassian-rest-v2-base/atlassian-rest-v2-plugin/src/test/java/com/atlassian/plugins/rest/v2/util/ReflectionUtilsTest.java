package com.atlassian.plugins.rest.v2.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SuppressWarnings("unchecked")
public class ReflectionUtilsTest {
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private AnnotatedElement element;

    @Test
    public void testGetAnnotationWithAnnotationPresent() {
        setAnnotationsOnElement(Rule.class, TestAnnotation.class, Mock.class);
        assertNotNull(ReflectionUtils.getAnnotation(TestAnnotation.class, element));
    }

    @Test
    public void testGetAnnotationWithAnnotationNotPresent() {
        setAnnotationsOnElement(Rule.class, Mock.class);
        assertNull(ReflectionUtils.getAnnotation(TestAnnotation.class, element));
    }

    @Test
    public void testGetAnnotationWithNoAnnotationsPresent() {
        setAnnotationsOnElement();
        assertNull(ReflectionUtils.getAnnotation(TestAnnotation.class, element));
    }

    private void setAnnotationsOnElement(Class<? extends Annotation>... annotationTypes) {
        final Annotation[] annotations = new Annotation[annotationTypes.length];
        for (int i = 0; i < annotationTypes.length; i++) {
            Annotation annotation = mock(annotationTypes[i]);
            doReturn(annotationTypes[i]).when(annotation).annotationType();
            annotations[i] = annotation;
        }
        when(element.getAnnotations()).thenReturn(annotations);
    }

    private @interface TestAnnotation {}
}
