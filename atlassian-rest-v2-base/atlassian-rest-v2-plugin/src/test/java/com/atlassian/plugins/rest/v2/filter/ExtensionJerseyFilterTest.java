package com.atlassian.plugins.rest.v2.filter;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.UriInfo;

import org.glassfish.jersey.internal.util.collection.MultivaluedStringMap;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ExtensionJerseyFilterTest {
    private static final String ACCEPT_HEADER = "Accept";

    @Mock
    private ContainerRequestContext containerRequest;

    @Mock
    private UriInfo uriInfo;

    private ExtensionJerseyFilter jerseyFilter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        jerseyFilter = new ExtensionJerseyFilter(Collections.singletonList("/excluded.*"));
    }

    @Test
    public void testDefaultConstructor() throws Exception {
        jerseyFilter = new ExtensionJerseyFilter();
        final String baseUri = "http://localhost:8080/rest";
        final MultivaluedMap<String, String> parameters = new MultivaluedStringMap();
        parameters.add("param1", "value1");
        parameters.add("param2", "value2.1");
        parameters.add("param2", "value2.2");
        testFilter(baseUri, parameters);
    }

    @Test
    public void testFilterWithoutQueryParameters() throws Exception {
        final String baseUri = "http://localhost:8080/rest";
        testFilter(baseUri, new MultivaluedStringMap());
    }

    @Test
    public void testFilterWithQueryParameters() throws Exception {
        final String baseUri = "http://localhost:8080/rest";
        final MultivaluedMap<String, String> parameters = new MultivaluedStringMap();
        parameters.add("param1", "value1");
        parameters.add("param2", "value2.1");
        parameters.add("param2", "value2.2");
        testFilter(baseUri, parameters);
    }

    @Test
    public void testFilterWithIPv6Address() throws Exception {
        final String baseUri = "http://[2600:1f18:674c:af01:46b3:e49e:6ac3:ff7b]:8080/rest";
        testFilter(baseUri, new MultivaluedStringMap());
    }

    @Test
    public void testDoNotFilterWhenExtensionNotMatched() throws Exception {
        testDoNotFilter("resource.myextension");
    }

    @Test
    public void testDoNotFilterWhenNoExtension() throws Exception {
        testDoNotFilter("resource");
    }

    @Test
    public void testDoNotFilterWhenExcluded() throws Exception {
        testDoNotFilter("excluded/resource.json");
    }

    private void testFilter(String baseUri, MultivaluedMap<String, String> queryParameters) throws Exception {
        final MultivaluedStringMap headers = new MultivaluedStringMap();
        when(uriInfo.getAbsolutePath()).thenReturn(new URI(baseUri + "application.json"));
        when(uriInfo.getBaseUri()).thenReturn(new URI(baseUri));
        when(uriInfo.getQueryParameters()).thenReturn(queryParameters);
        when(containerRequest.getUriInfo()).thenReturn(uriInfo);
        when(containerRequest.getHeaders()).thenReturn(headers);

        jerseyFilter.filter(containerRequest);
        final List<String> acceptHeader = headers.get(ACCEPT_HEADER);
        assertEquals(1, acceptHeader.size());
        assertEquals(MediaType.APPLICATION_JSON, acceptHeader.get(0));
        verify(containerRequest)
                .setRequestUri(argThat(new UriMatcher(new URI(baseUri + "application"), queryParameters)));
    }

    private void testDoNotFilter(String resourceName) throws Exception {
        final String baseUri = "http://localhost:8080/rest/";
        when(uriInfo.getAbsolutePath()).thenReturn(new URI(baseUri + resourceName));
        when(uriInfo.getBaseUri()).thenReturn(new URI(baseUri));
        when(containerRequest.getUriInfo()).thenReturn(uriInfo);
        jerseyFilter.filter(containerRequest);
        verify(containerRequest, never()).getHeaders();
        verify(containerRequest, never()).setRequestUri(any(), any());
    }

    private static class UriMatcher implements ArgumentMatcher<URI> {
        private final URI requestUri;
        private final MultivaluedMap<String, String> parameters;

        UriMatcher(URI requestUri, MultivaluedMap<String, String> parameters) {
            this.requestUri = requestUri;
            this.parameters = parameters;
        }

        public boolean matches(URI actual) {
            if (!actual.toString().startsWith(requestUri.toString())) {
                return false;
            }
            // check query parameters
            final String queryParametersString = '&' + actual.getQuery() + '&';
            for (Map.Entry<String, List<String>> queryParameter : parameters.entrySet()) {
                for (String value : queryParameter.getValue()) {
                    if (!queryParametersString.contains('&' + queryParameter.getKey() + '=' + value + '&')) {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
