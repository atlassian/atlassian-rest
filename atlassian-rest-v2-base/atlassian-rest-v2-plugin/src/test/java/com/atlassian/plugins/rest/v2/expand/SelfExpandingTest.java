package com.atlassian.plugins.rest.v2.expand;

import java.util.Collections;
import jakarta.xml.bind.annotation.XmlAttribute;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.plugins.rest.api.expand.EntityCrawler;
import com.atlassian.plugins.rest.api.expand.annotation.Expandable;
import com.atlassian.plugins.rest.api.expand.expander.SelfExpanding;
import com.atlassian.plugins.rest.api.internal.expand.parameter.DefaultExpandParameter;
import com.atlassian.plugins.rest.v2.expand.resolver.SelfExpandingEntityExpanderResolverImpl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SelfExpandingTest {
    private EntityCrawler entityCrawler;

    @Before
    public void setUp() {
        entityCrawler = new DefaultEntityCrawler();
    }

    @Test
    public void testSelfExpanding() {
        final SomeClass someClass = new SomeClass();
        assertFalse(someClass.expandable.isExpanded());
        entityCrawler.crawl(
                someClass,
                new DefaultExpandParameter(Collections.singletonList("*")),
                new SelfExpandingEntityExpanderResolverImpl());
        assertTrue(someClass.expandable.isExpanded());
    }

    public static class SomeClass {
        @Expandable
        public final SomeExpandableEntity expandable = new SomeExpandableEntity();
    }

    public static class SomeExpandableEntity implements SelfExpanding {
        @XmlAttribute
        private String expand;

        private boolean expanded = false;

        public void expand() {
            expanded = true;
        }

        public boolean isExpanded() {
            return expanded;
        }
    }
}
