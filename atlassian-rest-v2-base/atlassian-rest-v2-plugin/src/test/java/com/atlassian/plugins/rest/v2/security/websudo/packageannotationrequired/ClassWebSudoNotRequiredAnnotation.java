package com.atlassian.plugins.rest.v2.security.websudo.packageannotationrequired;

import com.atlassian.sal.api.websudo.WebSudoNotRequired;
import com.atlassian.sal.api.websudo.WebSudoRequired;

@WebSudoNotRequired
public class ClassWebSudoNotRequiredAnnotation {
    public void aMethod() {}

    @WebSudoRequired
    public void bMethod() {}
}
