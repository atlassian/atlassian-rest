package com.atlassian.plugins.rest.v2.exception.entity;

import java.net.URI;
import java.net.URL;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Request;

import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.internal.MapPropertiesDelegate;
import org.glassfish.jersey.server.ContainerRequest;
import org.junit.Before;
import org.junit.Test;

import static jakarta.ws.rs.core.HttpHeaders.ACCEPT;
import static jakarta.ws.rs.core.MediaType.TEXT_PLAIN_TYPE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import static com.atlassian.plugins.rest.v2.exception.constant.UncaughtExceptionEntityConstants.ATLASSIAN_REST_RESPONSE_STACKTRACES;
import static com.atlassian.plugins.rest.v2.exception.constant.UncaughtExceptionEntityConstants.ATLASSIAN_REST_RESPONSE_STACKTRACES_ABSENT;
import static com.atlassian.plugins.rest.v2.exception.constant.UncaughtExceptionEntityConstants.ATLASSIAN_REST_RESPONSE_STACKTRACES_PRESENT;
import static com.atlassian.plugins.rest.v2.exception.constant.UncaughtExceptionEntityConstants.NO_STACKTRACE_MESSAGE;

public class UncaughtExceptionEntityTest {
    private final String ERROR_ID = "error-id";
    private URI baseUri;
    private URI requestUri;

    @Before
    public void setUp() throws Exception {
        baseUri = new URL("http://localhost:5990/refapp").toURI();
        requestUri = new URL("http://localhost:5990/refapp/rest").toURI();
    }

    @Test
    public void stackTraceIsNotBlankWhenPropSet() {
        System.setProperty(ATLASSIAN_REST_RESPONSE_STACKTRACES, ATLASSIAN_REST_RESPONSE_STACKTRACES_PRESENT);
        assertTrue(StringUtils.isNotBlank(new UncaughtExceptionEntity(new Throwable(), ERROR_ID).getStackTrace()));
        System.setProperty(ATLASSIAN_REST_RESPONSE_STACKTRACES, ATLASSIAN_REST_RESPONSE_STACKTRACES_ABSENT);
    }

    @Test
    public void stackTraceContainsInfoWhenPropNotSet() {
        assertTrue(new UncaughtExceptionEntity(new Throwable(), ERROR_ID)
                .getStackTrace()
                .contains(NO_STACKTRACE_MESSAGE));
    }

    @Test
    public void messagesAreTakenFromThrowable() {
        assertNull(new UncaughtExceptionEntity(new Throwable(), ERROR_ID).getMessage());
        assertEquals("Message", new UncaughtExceptionEntity(new Throwable("Message"), ERROR_ID).getMessage());
    }

    @Test
    public void defaultConstructorSucceeds() {
        UncaughtExceptionEntity uncaughtExceptionEntity = new UncaughtExceptionEntity();
        assertNull(uncaughtExceptionEntity.getMessage());
    }

    @Test
    public void defaultIsXmlWithRealContainerRequestImplementation() {
        Request request = new ContainerRequest(baseUri, requestUri, "GET", null, new MapPropertiesDelegate(), null);

        MediaType mediaType = UncaughtExceptionEntity.variantFor(request);

        assertEquals(MediaType.APPLICATION_XML_TYPE, new MediaType(mediaType.getType(), mediaType.getSubtype()));
    }

    @Test
    public void textPlainIncludeCharsetParameterWithRealContainerRequestImplementation() {
        ContainerRequest request =
                new ContainerRequest(baseUri, requestUri, "GET", null, new MapPropertiesDelegate(), null);
        request.getHeaders().add(ACCEPT, TEXT_PLAIN_TYPE.getType());

        MediaType mediaType = UncaughtExceptionEntity.variantFor(request);

        assertEquals(MediaType.valueOf("text/plain; charset=utf-8"), mediaType);
    }
}
