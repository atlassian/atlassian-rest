package com.atlassian.plugins.rest.v2.sal;

import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.Plugin;
import com.atlassian.sal.api.net.Request;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@RunWith(value = MockitoJUnitRunner.class)
public class MarshallingRequestTest {

    @Mock
    private Request delegateRequest;

    @Mock
    private Plugin plugin;

    @Mock
    private MarshallingEntityHandler marshallingEntityHandler;

    /**
     * @throws Exception see REST-262
     */
    @Test
    public void testContentEncodingIsEmpty() throws Exception {
        MarshallingRequest marshallingRequest =
                new MarshallingRequest(delegateRequest, marshallingEntityHandler, plugin);
        marshallingRequest.setEntity(new Object());
        marshallingRequest.execute();
        verify(delegateRequest, never()).setHeader(eq("Content-Encoding"), anyString());
    }

    @Test
    public void testDefaultMediaTypeIsXml() throws Exception {
        MarshallingRequest marshallingRequest =
                new MarshallingRequest(delegateRequest, marshallingEntityHandler, plugin);
        marshallingRequest.setEntity(new Object());
        marshallingRequest.execute();
        verify(delegateRequest).setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML);
    }
}
