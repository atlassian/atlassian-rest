package com.atlassian.plugins.rest.v2.darkfeature;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.FeatureContext;
import jakarta.ws.rs.core.UriInfo;

import org.junit.Before;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.atlassian.plugins.rest.api.darkfeature.RequiresDarkFeature;
import com.atlassian.sal.api.features.DarkFeatureManager;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@SuppressWarnings("unused")
public class AbstractDarkFeatureTestResource {
    public static final String FEATURE_KEY = "feature.key";
    public static final String FEATURE_KEY_2 = "feature.key.2";
    public static final String NOT_FEATURE_KEY = "not.feature.key";

    @Mock
    protected ResourceInfo resourceInfo;

    @Mock
    protected DarkFeatureManager darkFeatureManager;

    @Mock
    protected ContainerRequestContext containerRequestContext;

    @Mock
    protected UriInfo uriInfo;

    @Mock
    protected FeatureContext featureContext;

    @InjectMocks
    protected DarkFeatureResourceFilter darkFeatureResourceFilter;

    @Before
    public void setup() throws MalformedURLException, URISyntaxException {
        initMocks(this);
        when(containerRequestContext.getUriInfo()).thenReturn(uriInfo);
        when(uriInfo.getRequestUri()).thenReturn(new URI("http://localhost:5990/refapp"));
    }

    protected void setFeatureEnabled(String key, boolean enabled) {
        when(darkFeatureManager.isEnabledForCurrentUser(key)).thenReturn(Optional.of(enabled));
    }

    protected void setMethodAnnotated(boolean isAnnotated) throws NoSuchMethodException {
        when(resourceInfo.getResourceMethod())
                .thenReturn(this.getClass().getMethod(isAnnotated ? "mockedAnnotatedMethod" : "mockedMethod"));
    }

    protected void setResourceAnnotated(boolean isAnnotated) {
        Class<?> testMockedClass = isAnnotated ? TestMockedAnnotatedClass.class : TestMockedClass.class;
        doReturn(testMockedClass).when(resourceInfo).getResourceClass();
    }

    public void mockedMethod() {
        // this is a mocked method
    }

    @RequiresDarkFeature(FEATURE_KEY)
    public void mockedAnnotatedMethod() {
        // this is a mocked method
    }

    protected static class TestMockedClass {
        // this is a mocked class
    }

    @RequiresDarkFeature(FEATURE_KEY)
    protected static class TestMockedAnnotatedClass {
        // this is a mocked class
    }

    @RequiresDarkFeature({FEATURE_KEY, FEATURE_KEY_2})
    protected static class TestMockedMultipleAnnotatedClass {
        // this is a mocked class
    }
}
