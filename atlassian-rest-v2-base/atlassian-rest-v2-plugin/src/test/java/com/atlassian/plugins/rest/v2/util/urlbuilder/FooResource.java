package com.atlassian.plugins.rest.v2.util.urlbuilder;

import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;

@Path("/dummy")
public class FooResource {
    public FooResource() {}

    @Path("/sub")
    public Response subResource() {
        return null;
    }

    @Path("/with-path-param/{param}")
    public Response withPathParam(@PathParam("param") String param) {
        return null;
    }

    @Path("/with-arg")
    public Response withArg(@QueryParam("param") ReturnType param) {
        return null;
    }

    @Path("/string-return")
    public String stringReturn() {
        return "";
    }

    @Path("/boolean")
    public boolean returnBoolean() {
        return false;
    }

    @Path("/char")
    public char returnChar() {
        return 1;
    }

    @Path("/byte")
    public byte returnByte() {
        return 1;
    }

    @Path("/short")
    public short returnShort() {
        return 1;
    }

    @Path("/int")
    public int returnInt() {
        return 1;
    }

    @Path("/long")
    public long returnLong() {
        return 1;
    }

    @Path("/float")
    public float returnFloat() {
        return 1;
    }

    @Path("/double")
    public double returnDouble() {
        return 1;
    }

    public static class ReturnType {
        public final int a;
        public final String b;

        public ReturnType(String param) {
            this.a = param.length();
            this.b = param;
        }
    }
}
