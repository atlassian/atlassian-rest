package com.atlassian.plugins.rest.v2.security.websudo;

import java.util.Base64;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.atlassian.plugins.rest.v2.util.ServletUtils;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public final class SalWebSudoResourceContextTest {

    @Mock
    private WebSudoManager webSudoManager;

    @Mock
    private UserManager userManager;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpSession httpSession;

    private SalWebSudoResourceContext salWebSudoResourceContext;

    @Before
    public void setUp() {
        initMocks(this);
        salWebSudoResourceContext = new SalWebSudoResourceContext(webSudoManager, userManager);
        ServletUtils.setHttpServletRequest(request);
    }

    @After
    public void teardown() {
        webSudoManager = null;
        request = null;
        salWebSudoResourceContext = null;
    }

    @Test
    public void enforceWebSudoProtection() {
        when(webSudoManager.canExecuteRequest(request)).thenReturn(false);
        assertTrue(salWebSudoResourceContext.shouldEnforceWebSudoProtection());
    }

    @Test
    public void dontEnforceWebSudoProtection() {
        when(webSudoManager.canExecuteRequest(request)).thenReturn(true);
        assertFalse(salWebSudoResourceContext.shouldEnforceWebSudoProtection());
    }

    @Test
    public void basicAuthRequiresWebSudoWithoutValidCredentials() {
        // From http://www.ietf.org/rfc/rfc2617.txt
        when(request.getHeader("Authorization")).thenReturn("Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==");

        // Expect WebSudo because credentials are not validated
        assertTrue(salWebSudoResourceContext.shouldEnforceWebSudoProtection());
        verify(webSudoManager).canExecuteRequest(any());
    }

    @Test
    public void basicAuthHeaderIsMissing() {
        when(request.getHeader("Authorization")).thenReturn(null);
        assertTrue(salWebSudoResourceContext.shouldEnforceWebSudoProtection());
        verify(webSudoManager).canExecuteRequest(any());
    }

    @Test
    public void basicAuthRequiresWebSudoWithInvalidCredentials() {
        String username = "Aladdin";
        String password = "wrong password";
        String basicAuth = AuthUtils.createBasicAuthHeader(username, password);
        // Basic Auth header is present but invalid
        when(request.getHeader("Authorization")).thenReturn(basicAuth);
        when(userManager.authenticate(username, password)).thenReturn(false);
        assertTrue(salWebSudoResourceContext.shouldEnforceWebSudoProtection());
        verify(webSudoManager).canExecuteRequest(any());
    }

    @Test
    public void basicAuthDoesNotRequireWebSudoWithValidCredentials() {
        String username = "Aladdin";
        String password = "open sesame";
        String basicAuth = AuthUtils.createBasicAuthHeader(username, password);
        when(request.getHeader("Authorization")).thenReturn(basicAuth);
        when(userManager.authenticate(username, password)).thenReturn(true);
        assertFalse(salWebSudoResourceContext.shouldEnforceWebSudoProtection());
        verify(webSudoManager, never()).canExecuteRequest(any());
    }

    @Test
    public void bearerTokenAuthDoesNotRequireWebSudo() {
        when(httpSession.getAttribute("is.pats.enabled")).thenReturn("true");
        when(request.getHeader("Authorization")).thenReturn("Bearer QWxhZGRpbjpvcGVuIHNlc2FtZQ==");
        when(request.getSession(false)).thenReturn(httpSession);

        assertFalse(salWebSudoResourceContext.shouldEnforceWebSudoProtection());
        verify(webSudoManager, never()).canExecuteRequest(any());
    }

    @Test
    public void bearerTokenShouldRequireWebsudoIfPersonalAccessTokensNotEnabled() {
        when(request.getHeader("Authorization")).thenReturn("Bearer QWxhZGRpbjpvcGVuIHNlc2FtZQ==");
        when(request.getSession()).thenReturn(httpSession);

        assertTrue(salWebSudoResourceContext.shouldEnforceWebSudoProtection());
        verify(webSudoManager).canExecuteRequest(any());
    }

    @Test
    public void bearerTokenShouldRequireWebsudoWhenSessionDoesNotExist() {
        when(request.getHeader("Authorization")).thenReturn("Bearer QWxhZGRpbjpvcGVuIHNlc2FtZQ==");
        when(request.getSession(false)).thenReturn(null);

        assertTrue(salWebSudoResourceContext.shouldEnforceWebSudoProtection());
        verify(webSudoManager).canExecuteRequest(any());
    }
}

class AuthUtils {
    public static String createBasicAuthHeader(String username, String password) {
        String credentials = username + ":" + password;
        String encodedCredentials = Base64.getEncoder().encodeToString(credentials.getBytes());
        return "Basic " + encodedCredentials;
    }
}
