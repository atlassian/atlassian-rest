package com.atlassian.plugins.rest.v2.security.cors;

import java.lang.reflect.Field;
import jakarta.ws.rs.HttpMethod;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.FeatureContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugins.rest.api.security.annotation.CorsAllowed;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CorsResourceDynamicFeatureTest {
    private static final String ALLOW_METHOD_FIELD = "allowMethod";

    @Mock
    private CorsDefaultService corsDefaultService;

    @Mock
    private ResourceInfo resourceInfo;

    @Mock
    private FeatureContext context;

    private CorsResourceDynamicFeature corsResourceDynamicFeature;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        corsResourceDynamicFeature = new CorsResourceDynamicFeature(corsDefaultService);
    }

    @Test
    public void testThatCorsAllowedIsDetectedOnMethod()
            throws NoSuchMethodException, NoSuchFieldException, IllegalAccessException {
        when(resourceInfo.getResourceMethod()).thenReturn(this.getClass().getMethod("mockedAnnotatedClass"));

        corsResourceDynamicFeature.configure(resourceInfo, context);

        CorsResourceFilter filter = captureCorsResourceFilter();
        assertAllowMethod(HttpMethod.GET, filter);
    }

    @Test
    public void testThatCorsAllowedIsDetectedOnResource()
            throws NoSuchMethodException, IllegalAccessException, NoSuchFieldException {
        when(resourceInfo.getResourceMethod()).thenReturn(this.getClass().getMethod("mockedHttpAnnotatedClass"));
        Class<?> testMockedClass = TestAnnotatedMockedClass.class;
        doReturn(testMockedClass).when(resourceInfo).getResourceClass();

        corsResourceDynamicFeature.configure(resourceInfo, context);

        CorsResourceFilter filter = captureCorsResourceFilter();
        assertAllowMethod(HttpMethod.POST, filter);
    }

    @Test
    public void testThatCorsAllowedIsDetectedOnResourceWithHttpAnnotationOnMethod()
            throws NoSuchMethodException, NoSuchFieldException, IllegalAccessException {
        when(resourceInfo.getResourceMethod()).thenReturn(this.getClass().getMethod("mockedClass"));
        Class<?> testMockedClass = TestAnnotatedMockedClass.class;
        doReturn(testMockedClass).when(resourceInfo).getResourceClass();

        corsResourceDynamicFeature.configure(resourceInfo, context);

        CorsResourceFilter filter = captureCorsResourceFilter();
        assertAllowMethod(HttpMethod.GET, filter);
    }

    @Test
    public void testThatNoCorsAllowedAnnotationsResultsInEmptyFilters() throws NoSuchMethodException {
        when(resourceInfo.getResourceMethod()).thenReturn(this.getClass().getMethod("mockedClass"));
        Class<?> testMockedClass = TestMockedClass.class;
        doReturn(testMockedClass).when(resourceInfo).getResourceClass();

        corsResourceDynamicFeature.configure(resourceInfo, context);
        verify(context, never()).register(CorsResourceFilter.class);
    }

    private CorsResourceFilter captureCorsResourceFilter() {
        ArgumentCaptor<CorsResourceFilter> argumentCaptor = ArgumentCaptor.forClass(CorsResourceFilter.class);
        verify(context).register(argumentCaptor.capture());
        return argumentCaptor.getValue();
    }

    private static void assertAllowMethod(String expectedMethod, CorsResourceFilter filter)
            throws NoSuchFieldException, IllegalAccessException {
        Field allowMethodField = CorsResourceFilter.class.getDeclaredField(ALLOW_METHOD_FIELD);
        allowMethodField.setAccessible(true);
        assertEquals(expectedMethod, allowMethodField.get(filter));
    }

    public void mockedClass() {
        // this is a mocked class
    }

    @CorsAllowed
    public void mockedAnnotatedClass() {
        // this is a mocked class
    }

    @POST
    public void mockedHttpAnnotatedClass() {
        // this is a mocked class
    }

    static class TestMockedClass {
        // this is a mocked class
    }

    @CorsAllowed
    static class TestAnnotatedMockedClass {
        // this is a mocked class
    }
}
