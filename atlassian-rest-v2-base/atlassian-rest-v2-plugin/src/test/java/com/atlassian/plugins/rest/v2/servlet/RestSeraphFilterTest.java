package com.atlassian.plugins.rest.v2.servlet;

import java.io.IOException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import static com.atlassian.plugins.rest.v2.servlet.RestSeraphFilter.DEFAULT_ATTRIBUTE;

public class RestSeraphFilterTest {

    @Test
    public void shouldSetAnyAuthAttribute() throws ServletException, IOException {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final FilterChain chain = mock(FilterChain.class);

        when(request.getServletPath()).thenReturn("/rest");

        RestSeraphFilter restSeraphFilter = new RestSeraphFilter();
        restSeraphFilter.doFilter(request, response, chain);

        verify(request).getServletPath();
        verify(request).getAttribute(DEFAULT_ATTRIBUTE);
        verify(request).setAttribute(DEFAULT_ATTRIBUTE, "any");
        verifyNoMoreInteractions(request);
    }
}
