package com.atlassian.plugins.rest.v2.exception;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;
import jakarta.xml.bind.JAXB;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.atlassian.plugins.rest.api.model.Link;
import com.atlassian.plugins.rest.api.model.Status;
import com.atlassian.plugins.rest.v2.json.DefaultJaxbJsonMarshaller;
import com.atlassian.plugins.rest.v2.json.ObjectMapperFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class NotFoundExceptionMapperTest {
    private static final DefaultJaxbJsonMarshaller JAXB_JSON_MARSHALLER = new DefaultJaxbJsonMarshaller();
    private static final ObjectMapper OBJECT_MAPPER = ObjectMapperFactory.createObjectMapper();
    private static final Unmarshaller STATUS_UNMARSHALLER;

    static {
        try {
            STATUS_UNMARSHALLER = JAXBContext.newInstance(Status.class).createUnmarshaller();
        } catch (JAXBException e) {
            throw new RuntimeException();
        }
    }

    private NotFoundExceptionMapper notFoundExceptionMapper;

    @Mock
    private Request request;

    @Before
    public void setupMocks() {
        notFoundExceptionMapper = new NotFoundExceptionMapper();
        notFoundExceptionMapper.request = request;
    }

    @Test
    public void exceptionAlwaysMapsToUnauthorized() {
        NotFoundException notFoundException = new NotFoundException();
        Response response = notFoundExceptionMapper.toResponse(notFoundException);
        assertEquals("Response status should be not found", 404, response.getStatus());
    }

    @Test
    public void shouldMarshalStatus() throws Exception {
        NotFoundException notFoundException = new NotFoundException("Not found");
        Response response = notFoundExceptionMapper.toResponse(notFoundException);
        String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" + "<status>\n"
                + "    <status-code>404</status-code>\n"
                + "    <message>Not found</message>\n"
                + "</status>\n";

        assertXML(response, expected);
    }

    @Test
    public void shouldMarshalStatusWithSingleResourceCreatedAndUpdated() throws Exception {
        Response response = createResponseWithSingleResourcesCreatedAndUpdated();
        String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" + "<status>\n"
                + "    <status-code>404</status-code>\n"
                + "    <message>Not found</message>\n"
                + "    <resources-created>\n"
                + "        <link href=\"foo1\" rel=\"add\"/>\n"
                + "    </resources-created>\n"
                + "    <resources-updated>\n"
                + "        <link href=\"bar1\" rel=\"add\"/>\n"
                + "    </resources-updated>\n"
                + "</status>\n";

        assertXML(response, expected);
    }

    @Test
    public void shouldMarshalStatusWithMultipleResourcesCreatedAndUpdated() throws Exception {
        Response response = createResponseWithMultipleResourcesCreatedAndUpdated();
        String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" + "<status>\n"
                + "    <status-code>404</status-code>\n"
                + "    <message>Not found</message>\n"
                + "    <resources-created>\n"
                + "        <link href=\"foo1\" rel=\"add\"/>\n"
                + "        <link href=\"foo2\" rel=\"add\"/>\n"
                + "    </resources-created>\n"
                + "    <resources-updated>\n"
                + "        <link href=\"bar1\" rel=\"add\"/>\n"
                + "        <link href=\"bar2\" rel=\"add\"/>\n"
                + "    </resources-updated>\n"
                + "</status>\n";

        assertXML(response, expected);
    }

    @Test
    public void shouldSerializeStatusToJson() throws Exception {
        NotFoundException notFoundException = new NotFoundException("Not found");
        Response response = notFoundExceptionMapper.toResponse(notFoundException);
        String expectedJson = ("{\"message\":\"Not found\",\"status-code\":404}");

        assertJSON(response, expectedJson);
    }

    @Test
    public void shouldSerializeStatusWithoutCustomMessageToJson() throws Exception {
        NotFoundException notFoundException = new NotFoundException();
        Response response = notFoundExceptionMapper.toResponse(notFoundException);
        String expectedJson = ("{\"message\":\"HTTP 404 Not Found\",\"status-code\":404}");

        assertJSON(response, expectedJson);
    }

    @Test
    public void shouldSerializeStatusToJsonWithSingleResourceCreatedAndUpdated() throws Exception {
        Response response = createResponseWithSingleResourcesCreatedAndUpdated();
        String expectedJson = "{\"message\":\"Not found\",\"status-code\":404,"
                + "\"resources-created\":[{\"href\":\"foo1\",\"rel\":\"add\"}],"
                + "\"resources-updated\":[{\"href\":\"bar1\",\"rel\":\"add\"}]}";

        assertJSON(response, expectedJson);
    }

    @Test
    public void shouldSerializeStatusToJsonWithMultipleResourcesCreatedAndUpdated() throws Exception {
        Response response = createResponseWithMultipleResourcesCreatedAndUpdated();
        String expectedJson = "{\"message\":\"Not found\",\"status-code\":404,"
                + "\"resources-created\":[{\"href\":\"foo1\",\"rel\":\"add\"},{\"href\":\"foo2\",\"rel\":\"add\"}],"
                + "\"resources-updated\":[{\"href\":\"bar1\",\"rel\":\"add\"},{\"href\":\"bar2\",\"rel\":\"add\"}]}";

        assertJSON(response, expectedJson);
    }

    private static void assertJSON(Response response, String expectedJson) throws IOException {
        Object entity = response.getEntity();
        String actualJson = JAXB_JSON_MARSHALLER.marshal(entity);

        Status actualStatus = OBJECT_MAPPER.readValue(actualJson, Status.class);
        Status expectedStatus = OBJECT_MAPPER.readValue(expectedJson, Status.class);

        assertNotNull(actualJson);
        assertEquals(expectedStatus, actualStatus);
        assertEquals("Response status should be not found", 404, response.getStatus());
    }

    private void assertXML(Response response, String expected) throws JAXBException {
        String xmlString = getEntityAsXML(response);

        Status actualStatus = unmarshallStatus(xmlString);
        Status expectedStatus = unmarshallStatus(expected);

        assertNotNull(xmlString);
        assertEquals(actualStatus, expectedStatus);
        assertEquals("Response status should be not found", 404, response.getStatus());
    }

    private static String getEntityAsXML(Response response) {
        Object entity = response.getEntity();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JAXB.marshal(entity, baos);
        return baos.toString();
    }

    private Status unmarshallStatus(String xmlString) throws JAXBException {
        return (Status) STATUS_UNMARSHALLER.unmarshal(new StringReader(xmlString));
    }

    private Response createResponseWithSingleResourcesCreatedAndUpdated() throws URISyntaxException {
        return Status.notFound()
                .created(Link.add(new URI("foo1")))
                .updated(Link.add(new URI("bar1")))
                .message(new NotFoundException("Not found").getMessage())
                .responseBuilder()
                .type(Status.variantFor(request))
                .build();
    }

    private Response createResponseWithMultipleResourcesCreatedAndUpdated() throws URISyntaxException {
        return Status.notFound()
                .created(Link.add(new URI("foo1")))
                .created(Link.add(new URI("foo2")))
                .updated(Link.add(new URI("bar1")))
                .updated(Link.add(new URI("bar2")))
                .message(new NotFoundException("Not found").getMessage())
                .responseBuilder()
                .type(Status.variantFor(request))
                .build();
    }
}
