package com.atlassian.plugins.rest.v2.security.websudo;

import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ResourceInfo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugins.rest.api.security.exception.WebSudoRequiredException;
import com.atlassian.plugins.rest.v2.security.websudo.nopackageprotection.ClassNoAnnotation;
import com.atlassian.plugins.rest.v2.security.websudo.nopackageprotection.ClassProtectedByClassAnnotation;
import com.atlassian.plugins.rest.v2.security.websudo.nopackageprotection.MethodProtectedByMethodAnnotation;
import com.atlassian.plugins.rest.v2.security.websudo.packageannotationnotrequired.MethodOverridesPackage;
import com.atlassian.plugins.rest.v2.security.websudo.packageannotationrequired.ClassProtectedByPackageAnnotation;
import com.atlassian.plugins.rest.v2.security.websudo.packageannotationrequired.ClassWebSudoNotRequiredAnnotation;
import com.atlassian.plugins.rest.v2.security.websudo.packageannotationrequired.MethodWebSudoNotRequiredAnnotation;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;

import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Test the WebSudoResourceFilter
 * <p>
 * <p>
 * Annotations on more specific types override annotations on less specific element types.
 * <p>
 * Package > Class > Method
 * <p>
 * E.g. A method with a @WebSudoNotRequired annotation vetoes the @WebSudoRequired annotation on a class
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class WebSudoResourceFilterTest {
    @Mock
    private ContainerRequestContext containerRequest;

    @Mock
    private ResourceInfo resourceInfo;

    @Mock
    private WebSudoManager webSudoManager;

    @Mock
    private UserManager userManager;

    private SalWebSudoResourceContext webSudoResourceContext;

    @InjectMocks
    private WebSudoResourceFilter webSudoResourceFilter;

    @Before
    public void setUp() {
        webSudoResourceContext = Mockito.spy(new SalWebSudoResourceContext(webSudoManager, userManager));
        webSudoResourceFilter = new WebSudoResourceFilter(webSudoResourceContext);
        initMocks(this);
    }

    @Test
    public void filterPassesWithWebSudoProtectionOn() throws NoSuchMethodException {
        doReturn(false).when(webSudoResourceContext).shouldEnforceWebSudoProtection();
        Class<?> testMockedClass = ClassProtectedByPackageAnnotation.class;
        doReturn(testMockedClass).when(resourceInfo).getResourceClass();
        doReturn(testMockedClass.getMethod("aMethod")).when(resourceInfo).getResourceMethod();
        webSudoResourceFilter.filter(containerRequest);
    }

    @Test
    public void filterPassesWithWebSudoProtectionOnNoAnnotations() throws NoSuchMethodException {
        doReturn(false).when(webSudoResourceContext).shouldEnforceWebSudoProtection();
        Class<?> testMockedClass = ClassNoAnnotation.class;
        doReturn(testMockedClass).when(resourceInfo).getResourceClass();
        doReturn(testMockedClass.getMethod("aMethod")).when(resourceInfo).getResourceMethod();
        webSudoResourceFilter.filter(containerRequest);
    }

    @Test
    public void filterPasses() throws NoSuchMethodException {
        doReturn(true).when(webSudoResourceContext).shouldEnforceWebSudoProtection();
        Class<?> testMockedClass = MethodProtectedByMethodAnnotation.class;
        doReturn(testMockedClass).when(resourceInfo).getResourceClass();
        doReturn(testMockedClass.getMethod("aMethod")).when(resourceInfo).getResourceMethod();
        webSudoResourceFilter.filter(containerRequest);
    }

    @Test
    public void filterPassesWithWebSudoNotRequiredClassAnnotation() throws NoSuchMethodException {
        doReturn(true).when(webSudoResourceContext).shouldEnforceWebSudoProtection();
        Class<?> testMockedClass = ClassWebSudoNotRequiredAnnotation.class;
        doReturn(testMockedClass).when(resourceInfo).getResourceClass();
        doReturn(testMockedClass.getMethod("aMethod")).when(resourceInfo).getResourceMethod();
        webSudoResourceFilter.filter(containerRequest);
    }

    @Test
    public void filterPassesWithWebSudoNotRequiredMethodAnnotation() throws NoSuchMethodException {
        doReturn(true).when(webSudoResourceContext).shouldEnforceWebSudoProtection();
        Class<?> testMockedClass = MethodWebSudoNotRequiredAnnotation.class;
        doReturn(testMockedClass).when(resourceInfo).getResourceClass();
        doReturn(testMockedClass.getMethod("aMethod")).when(resourceInfo).getResourceMethod();
        webSudoResourceFilter.filter(containerRequest);
    }

    @Test
    public void filterRejectedWithPackageAnnotation() throws NoSuchMethodException {
        doReturn(true).when(webSudoResourceContext).shouldEnforceWebSudoProtection();
        Class<?> testMockedClass = ClassProtectedByPackageAnnotation.class;
        doReturn(testMockedClass).when(resourceInfo).getResourceClass();
        doReturn(testMockedClass.getMethod("aMethod")).when(resourceInfo).getResourceMethod();
        assertThrows(WebSudoRequiredException.class, () -> webSudoResourceFilter.filter(containerRequest));
    }

    @Test
    public void filterRejectedWithClassAnnotation() throws NoSuchMethodException {
        doReturn(true).when(webSudoResourceContext).shouldEnforceWebSudoProtection();
        Class<?> testMockedClass = ClassProtectedByClassAnnotation.class;
        doReturn(testMockedClass).when(resourceInfo).getResourceClass();
        doReturn(testMockedClass.getMethod("aMethod")).when(resourceInfo).getResourceMethod();
        assertThrows(WebSudoRequiredException.class, () -> webSudoResourceFilter.filter(containerRequest));
    }

    @Test
    public void filterRejectedWithMethodAnnotation() throws NoSuchMethodException {
        doReturn(true).when(webSudoResourceContext).shouldEnforceWebSudoProtection();
        Class<?> testMockedClass = MethodProtectedByMethodAnnotation.class;
        doReturn(testMockedClass).when(resourceInfo).getResourceClass();
        doReturn(testMockedClass.getMethod("bMethod")).when(resourceInfo).getResourceMethod();
        assertThrows(WebSudoRequiredException.class, () -> webSudoResourceFilter.filter(containerRequest));
    }

    @Test
    public void filterRejectedMethodAnnotationOverridesClassAnnotation() throws NoSuchMethodException {
        doReturn(true).when(webSudoResourceContext).shouldEnforceWebSudoProtection();
        Class<?> testMockedClass = ClassWebSudoNotRequiredAnnotation.class;
        doReturn(testMockedClass).when(resourceInfo).getResourceClass();
        doReturn(testMockedClass.getMethod("bMethod")).when(resourceInfo).getResourceMethod();
        assertThrows(WebSudoRequiredException.class, () -> webSudoResourceFilter.filter(containerRequest));
    }

    @Test
    public void filterRejectedMethodAnnotationOverridesPackageAnnotation() throws NoSuchMethodException {
        doReturn(true).when(webSudoResourceContext).shouldEnforceWebSudoProtection();
        Class<?> testMockedClass = MethodOverridesPackage.class;
        doReturn(testMockedClass).when(resourceInfo).getResourceClass();
        doReturn(testMockedClass.getMethod("aMethod")).when(resourceInfo).getResourceMethod();
        assertThrows(WebSudoRequiredException.class, () -> webSudoResourceFilter.filter(containerRequest));
    }
}
