package com.atlassian.plugins.rest.v2.util.urlbuilder;

import java.net.URI;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @since 2.2
 */
public class RestUrlBuilderImplTest {
    private RestUrlBuilderImpl urlBuilder;

    @Before
    public void setup() {
        urlBuilder = new RestUrlBuilderImpl();
    }

    @Test
    public void shouldReturnValidURI() throws Exception {
        URI uri = urlBuilder.getUrlFor(new URI("http://base2"), FooResource.class, FooResource::subResource);

        assertEquals(new URI("http://base2/dummy/sub"), uri);
    }

    @Test
    public void shouldIncludePathParam() throws Exception {
        URI uri = urlBuilder.getUrlFor(new URI("http://base2"), FooResource.class, res -> res.withPathParam("foo"));

        assertEquals(new URI("http://base2/dummy/with-path-param/foo"), uri);
    }

    @Test
    public void shouldIgnoreQueryParam() throws Exception {
        URI uri = urlBuilder.getUrlFor(
                new URI("http://base2"), FooResource.class, res -> res.withArg(new FooResource.ReturnType("ignored")));

        assertEquals(new URI("http://base2/dummy/with-arg"), uri);
    }

    @Test
    public void shouldProperlyHandleNonResponseReturn() throws Exception {
        URI uri = urlBuilder.getUrlFor(new URI("http://base2"), FooResource.class, FooResource::stringReturn);

        assertEquals(new URI("http://base2/dummy/string-return"), uri);
    }

    @Test
    public void shouldProperlyHandleBooleanReturn() throws Exception {
        URI uri = urlBuilder.getUrlFor(new URI("http://base2"), FooResource.class, FooResource::returnBoolean);

        assertEquals(new URI("http://base2/dummy/boolean"), uri);
    }

    @Test
    public void shouldProperlyHandleCharReturn() throws Exception {
        URI uri = urlBuilder.getUrlFor(new URI("http://base2"), FooResource.class, FooResource::returnChar);

        assertEquals(new URI("http://base2/dummy/char"), uri);
    }

    @Test
    public void shouldProperlyHandleByteReturn() throws Exception {
        URI uri = urlBuilder.getUrlFor(new URI("http://base2"), FooResource.class, FooResource::returnByte);

        assertEquals(new URI("http://base2/dummy/byte"), uri);
    }

    @Test
    public void shouldProperlyHandleShortReturn() throws Exception {
        URI uri = urlBuilder.getUrlFor(new URI("http://base2"), FooResource.class, FooResource::returnShort);

        assertEquals(new URI("http://base2/dummy/short"), uri);
    }

    @Test
    public void shouldProperlyHandleIntReturn() throws Exception {
        URI uri = urlBuilder.getUrlFor(new URI("http://base2"), FooResource.class, FooResource::returnInt);

        assertEquals(new URI("http://base2/dummy/int"), uri);
    }

    @Test
    public void shouldProperlyHandleLongReturn() throws Exception {
        URI uri = urlBuilder.getUrlFor(new URI("http://base2"), FooResource.class, FooResource::returnLong);

        assertEquals(new URI("http://base2/dummy/long"), uri);
    }

    @Test
    public void shouldProperlyHandleFloatReturn() throws Exception {
        URI uri = urlBuilder.getUrlFor(new URI("http://base2"), FooResource.class, FooResource::returnFloat);

        assertEquals(new URI("http://base2/dummy/float"), uri);
    }

    @Test
    public void shouldProperlyHandleDoubleReturn() throws Exception {
        URI uri = urlBuilder.getUrlFor(new URI("http://base2"), FooResource.class, FooResource::returnDouble);

        assertEquals(new URI("http://base2/dummy/double"), uri);
    }
}
