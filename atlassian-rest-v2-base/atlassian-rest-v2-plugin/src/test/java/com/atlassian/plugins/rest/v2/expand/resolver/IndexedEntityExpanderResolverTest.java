package com.atlassian.plugins.rest.v2.expand.resolver;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.plugins.rest.api.expand.annotation.IndexedExpand;
import com.atlassian.plugins.rest.api.expand.parameter.Indexes;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Testing {@link IndexedEntityExpanderResolver}
 */
public class IndexedEntityExpanderResolverTest {
    private IndexedEntityExpanderResolver resolver;

    @Before
    public void setUp() {
        resolver = new IndexedEntityExpanderResolver();
    }

    @Test
    public void testHasExpanderWithNullClass() {
        try {
            final Class clazz = null;
            resolver.hasExpander(clazz);
            fail();
        } catch (NullPointerException e) {
            // expected
        }
    }

    @Test
    public void testGetExpanderWithNullClass() {
        try {
            final Class clazz = null;
            resolver.getExpander(clazz);
            fail();
        } catch (NullPointerException e) {
            // expected
        }
    }

    @Test
    public void testHasExpanderWithClassForObjectWithNoIndexedMethod() {
        assertFalse(resolver.hasExpander(Object.class));
    }

    @Test
    public void testGetExpanderWithClassForObjectWithNoIndexedMethod() {
        assertNull(resolver.getExpander(Object.class));
    }

    @Test
    public void testHasExpanderWithClassForClassWithIndexedMethod() {
        assertTrue(resolver.hasExpander(ClassWithIndexedMethod.class));
    }

    @Test
    public void testGetExpanderWithClassForClassWithIndexedMethod() {
        assertNotNull(resolver.getExpander(ClassWithIndexedMethod.class));
    }

    public static class ClassWithIndexedMethod {
        @IndexedExpand
        public void expand(Indexes indexes) {}
    }
}
