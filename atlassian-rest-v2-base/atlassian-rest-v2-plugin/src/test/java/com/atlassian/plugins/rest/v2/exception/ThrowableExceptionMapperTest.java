package com.atlassian.plugins.rest.v2.exception;

import java.util.Collections;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Variant;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugins.rest.v2.exception.entity.UncaughtExceptionEntity;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(MockitoJUnitRunner.class)
public class ThrowableExceptionMapperTest {
    @Mock
    private Request request;

    @InjectMocks
    private ThrowableExceptionMapper mapper;

    @Before
    public void init() {
        initMocks(this);
    }

    @Test
    public void testSomeThrowable() {
        mapper.request = mock(Request.class);
        when(mapper.request.selectVariant(Mockito.any())).thenReturn(new Variant(MediaType.TEXT_HTML_TYPE, "", ""));

        RuntimeException runtimeException = new RuntimeException("foo");
        Response response = mapper.toResponse(runtimeException);
        assertEquals(response.getStatus(), Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
        assertEquals("foo", ((UncaughtExceptionEntity) response.getEntity()).getMessage());
    }

    @Test
    public void testWebApplicationException() {
        when(request.selectVariant(any())).thenReturn(null);
        WebApplicationException webApplicationException = new WebApplicationException(431);
        Response response = mapper.toResponse(webApplicationException);
        assertEquals(431, response.getStatus());
    }

    @Test
    public void testWebApplicationExceptionWithNotSupportedStatus() {
        when(request.selectVariant(any())).thenReturn(null);
        WebApplicationException webApplicationException = new WebApplicationException(444);
        Response response = mapper.toResponse(webApplicationException);
        assertEquals(444, response.getStatus());
    }

    @Test
    public void getResponseAsHtml() {
        Exception exception = new Exception();
        mapper.request = mock(Request.class);
        when(mapper.request.selectVariant(Mockito.any())).thenReturn(new Variant(MediaType.TEXT_HTML_TYPE, "", ""));

        Response response = mapper.toResponse(exception);
        assertEquals(
                Collections.singletonList(MediaType.TEXT_HTML_TYPE),
                response.getMetadata().get("Content-Type"));
        assertEquals(500, response.getStatus());
    }
}
