package com.atlassian.plugins.rest.v2.json;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;

import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.jackson.internal.jackson.jaxrs.json.JacksonJsonProvider;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

import com.atlassian.plugins.rest.api.json.JaxbJsonMarshaller;
import com.atlassian.plugins.rest.api.json.JsonMarshallingException;

import static com.google.common.collect.ImmutableList.copyOf;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;

@RunWith(MockitoJUnitRunner.class)
public class DefaultJaxbJsonMarshallerTest {
    private JaxbJsonMarshaller marshaller;
    private final ObjectMapper mapper = new ObjectMapper();

    @Mock
    private JacksonJsonProvider jacksonJsonProvider;

    private JsonNode asJsonNode(String json) throws IOException {
        return mapper.readTree(json);
    }

    @Before
    public void setUp() {
        marshaller = new DefaultJaxbJsonMarshaller();
    }

    @Test
    public void testMarshalBoolean() {
        assertEquals("true", marshaller.marshal(Boolean.TRUE));
    }

    @Test
    public void testMarshalUrlWithSpaces() throws Exception {
        assertEquals(
                "\"https://user:paswd@a.b.c:8443/path/to%20file/on%20disk/aaa%20bbb%20ccc.p%20d%20f?q%20u%20e%20r%20y#frag%20ment\"",
                marshaller.marshal(new URI(
                        "https",
                        "user:paswd",
                        "a.b.c",
                        8443,
                        "/path/to file/on disk/aaa bbb ccc.p d f",
                        "q u e r y",
                        "frag ment")));
    }

    @Test
    public void testMarshalInteger() {
        assertEquals("2", marshaller.marshal(2));
    }

    @Test
    @Ignore("Guava is not supported")
    public void testMarshalGuavaIterable() {
        assertEquals(
                "[\"ad\",\"bd\",\"cd\"]",
                marshaller.marshal(
                        Stream.of("a", "b", "c").map(input -> input + "d").collect(Collectors.toList())));
    }

    @Test
    @Ignore("Guava is not supported")
    public void testMarshalEmptyGuavaIterable() {
        assertEquals(
                "[]",
                marshaller.marshal(Lists.<String>newLinkedList().stream()
                        .map(input -> input + "d")
                        .collect(Collectors.toList())));
    }

    /**
     * Following Test demonstrates the retention of type information is Inherited Jackson types. This is important with respect to the
     * test testMarshalJaxbInheritanceTypes
     **/
    @Test
    public void testMarshalJacksonInheritanceTypes() throws IOException {
        final String marshalledjacksonDerived1 = marshaller.marshal(new JacksonDerived1("base", "derived1"));
        final String marshalledjacksonDerived2 = marshaller.marshal(new JacksonDerived2("base", "derived2"));
        assertEquals(
                "{\"type\":\"jacksonDerived1\",\"base\":\"base\",\"derived1\":\"derived1\"}",
                marshalledjacksonDerived1);
        assertEquals(
                "{\"type\":\"jacksonDerived2\",\"base\":\"base\",\"derived2\":\"derived2\"}",
                marshalledjacksonDerived2);
        final JsonNode jsonNodeDerived1 = asJsonNode(marshalledjacksonDerived1);
        final JsonNode jsonNodeDerived2 = asJsonNode(marshalledjacksonDerived2);
        // asserts the fact that type INFO is RETAINED :)
        assertThat(jsonNodeDerived1, hasTextNode("type", "jacksonDerived1"));
        assertThat(jsonNodeDerived2, hasTextNode("type", "jacksonDerived2"));
    }

    @Test
    public void testMarshalJaxbInheritanceTypes() throws IOException {
        final String marshalledjaxbDerived1 = marshaller.marshal(new JaxbDerived1("base", "derived1"));
        final String marshalledjaxbDerived2 = marshaller.marshal(new JaxbDerived2("base", "derived2"));
        assertEquals("{\"base\":\"base\",\"derived1\":\"derived1\"}", marshalledjaxbDerived1);
        assertEquals("{\"base\":\"base\",\"derived2\":\"derived2\"}", marshalledjaxbDerived2);
        final JsonNode jsonNodeDerived1 = asJsonNode(marshalledjaxbDerived1);
        final JsonNode jsonNodeDerived2 = asJsonNode(marshalledjaxbDerived2);
        // asserts the fact that type info is LOST :(. This has significant consequences to DC products that use
        // inheritance types in Public API.
        assertThat(jsonNodeDerived1, not(hasTextNode("type", "derived1")));
        assertThat(jsonNodeDerived2, not(hasTextNode("type", "derived2")));
    }

    @Test
    public void testMarshallingThrowsException() throws IOException {
        doThrow(IOException.class).when(jacksonJsonProvider).writeTo(any(), any(), any(), any(), any(), any(), any());
        JaxbJsonMarshaller jaxbJsonMarshaller = new DefaultJaxbJsonMarshaller(jacksonJsonProvider);
        assertThrows(JsonMarshallingException.class, () -> jaxbJsonMarshaller.marshal(new Object()));
    }

    static class JsonBeanWithIterable {
        private final Iterable<String> strings;

        public JsonBeanWithIterable(Iterable<String> strings) {
            this.strings = strings;
        }

        @JsonProperty
        public Iterable<String> getStrings() {
            return strings;
        }
    }

    @Test
    @Ignore("Guava is not supported")
    public void testMarshalEmptyGuavaIterableInObject() {
        assertEquals(
                "{\"strings\":[]}",
                marshaller.marshal(new JsonBeanWithIterable(Lists.<String>newLinkedList().stream()
                        .map(input -> input + "d")
                        .collect(Collectors.toList()))));
    }

    @Test
    @Ignore("Guava is not supported")
    public void testMarshalNotEmptyGuavaIterableInObject() {
        assertEquals(
                "{\"strings\":[\"testd\"]}",
                marshaller.marshal(new JsonBeanWithIterable(Arrays.stream(new String[] {"test"})
                        .map(input -> input + "d")
                        .collect(Collectors.toList()))));
    }

    static class IterableBean implements Iterable<String> {
        @JsonProperty
        private long id;

        @JsonProperty
        private List<String> tabs;

        public IterableBean(final long id, final Iterable<String> tabs) {
            this.id = id;
            this.tabs = copyOf(tabs);
        }

        public long getId() {
            return id;
        }

        public List<String> getTabs() {
            return tabs;
        }

        @Override
        public Iterator<String> iterator() {
            return getTabs().iterator();
        }
    }

    @Test
    public void testMarshalIterableBean() {
        assertEquals(
                "{\"id\":1,\"tabs\":[\"tab1\",\"tab2\"]}",
                marshaller.marshal(new IterableBean(1, Lists.newArrayList("tab1", "tab2"))));
    }

    @Test
    public void testMarshalString() {
        assertEquals("\"foobar\"", marshaller.marshal("foobar"));
    }

    @Test
    public void testMarshalMap() {
        assertEquals("{\"foo\":\"bar\"}", marshaller.marshal(Collections.singletonMap("foo", "bar")));
    }

    @Test
    public void testMarshalList() {
        assertEquals("[\"foo\",\"bar\"]", marshaller.marshal(Arrays.asList("foo", "bar")));
    }

    @Test
    public void testMarshalObjectWithMember() {
        assertEquals("{\"string\":\"foo\"}", marshaller.marshal(new ObjectWithMember("foo")));
    }

    @Test
    public void testMarshalObjectWithNullMember() {
        assertEquals("{}", marshaller.marshal(new ObjectWithMember(null)));
    }

    @Test
    public void testMarshalObjectWithMemberMissingAnnotation() {
        assertEquals("{}", marshaller.marshal(new ObjectWithMemberMissingAnnotation("foo")));
    }

    @Test
    public void testMarshalObjectWithMemberWithRenaming() {
        assertEquals("{\"str\":\"foo\"}", marshaller.marshal(new ObjectWithMemberWithRenaming("foo")));
    }

    @Test
    public void testMarshalObjectWithJsonAnnotatedPropertyNull() {
        assertEquals(
                "Default behaviour with @JsonProperty annotations should exclude null members",
                "{}",
                marshaller.marshal(new JsonBeanInclusionDefault(null)));
    }

    @Test
    public void testMarshalObjectWithJsonAnnotatedPropertyNullAndAlwaysInclusion() {
        assertEquals(
                "If we ask for null members to always be included they should be in the result",
                "{\"name\":null}",
                marshaller.marshal(new JsonBeanInclusionAlways(null)));
    }

    @Test
    public void testMarshalObjectWithJsonAnnotatedPropertyNullAndNonNullInclusion() {
        assertEquals(
                "Non-NULL inclusion means null members should not be in the output",
                "{}",
                marshaller.marshal(new JsonBeanInclusionNonNull(null)));
    }

    @XmlRootElement
    private static class ObjectWithMember {
        @SuppressWarnings("unused")
        @XmlElement
        private final String string;

        public ObjectWithMember(String string) {
            this.string = string;
        }
    }

    @XmlRootElement
    private static class ObjectWithMemberMissingAnnotation {
        @SuppressWarnings("unused")
        private final String string;

        public ObjectWithMemberMissingAnnotation(String string) {
            this.string = string;
        }
    }

    @XmlRootElement
    private static class ObjectWithMemberWithRenaming {
        @SuppressWarnings("unused")
        @XmlElement(name = "str")
        private final String string;

        public ObjectWithMemberWithRenaming(String string) {
            this.string = string;
        }
    }

    static class JsonBeanInclusionDefault {
        @JsonProperty
        public final String name;

        JsonBeanInclusionDefault(String name) {
            this.name = name;
        }
    }

    @JsonInclude
    static class JsonBeanInclusionAlways {
        @JsonProperty
        public final String name;

        JsonBeanInclusionAlways(String name) {
            this.name = name;
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    static class JsonBeanInclusionNonNull {
        @JsonProperty
        public final String name;

        JsonBeanInclusionNonNull(String name) {
            this.name = name;
        }
    }

    @XmlRootElement(name = "jaxbbase")
    @XmlSeeAlso({JaxbDerived1.class, JaxbDerived2.class})
    abstract static class JaxbBase {
        @XmlElement(name = "base")
        private final String base;

        JaxbBase(String base) {
            this.base = base;
        }
    }

    @XmlType(name = "jaxbDerived1")
    static class JaxbDerived1 extends JaxbBase {
        @XmlElement(name = "derived1")
        private final String derived1;

        JaxbDerived1(String base, String derived1) {
            super(base);
            this.derived1 = derived1;
        }
    }

    @XmlType(name = "jaxbDerived2")
    static class JaxbDerived2 extends JaxbBase {
        @XmlElement(name = "derived2")
        private final String derived2;

        JaxbDerived2(String base, String derived2) {
            super(base);
            this.derived2 = derived2;
        }
    }

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
    @JsonSubTypes({
        @JsonSubTypes.Type(value = JacksonDerived1.class, name = "jacksonDerived1"),
        @JsonSubTypes.Type(value = JacksonDerived2.class, name = "jacksonDerived2")
    })
    @JsonIgnoreProperties(ignoreUnknown = true)
    abstract static class JacksonBase {
        @JsonProperty
        private final String base;

        JacksonBase(String base) {
            this.base = base;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    static class JacksonDerived1 extends JacksonBase {
        @JsonProperty
        private final String derived1;

        JacksonDerived1(String base, String derived1) {
            super(base);
            this.derived1 = derived1;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    static class JacksonDerived2 extends JacksonBase {
        @JsonProperty
        private final String derived2;

        JacksonDerived2(String base, String derived2) {
            super(base);
            this.derived2 = derived2;
        }
    }

    // Utlity hamcrest matchers. Good to put under test utils if it exists
    private static Matcher<JsonNode> hasTextNode(final String name, final String value) {
        return new TypeSafeMatcher<JsonNode>() {
            TypeSafeMatcher<JsonNode> valueMatcher = isText(value);

            @Override
            protected boolean matchesSafely(JsonNode item) {
                return valueMatcher.matches(item.get(name));
            }

            @Override
            public void describeTo(Description description) {
                description.appendValue(name).appendText("=");
                valueMatcher.describeTo(description);
            }
        };
    }

    private static TypeSafeMatcher<JsonNode> isText(final String expected) {
        return new TypeSafeMatcher<JsonNode>() {
            @Override
            protected boolean matchesSafely(JsonNode item) {
                return StringUtils.equals(item.asText(), expected);
            }

            @Override
            public void describeTo(Description description) {
                description.appendValue(expected);
            }
        };
    }
}
