package com.atlassian.plugins.rest.v2.expand.resolver;

import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Testing {@link ListEntityExpanderResolver}
 */
public class ListEntityExpanderResolverTest {
    private ListEntityExpanderResolver resolver;

    @Before
    public void setUp() {
        resolver = new ListEntityExpanderResolver();
    }

    @Test
    public void testHasExpanderWithNullClass() {
        try {
            final Class clazz = null;
            resolver.hasExpander(clazz);
            fail();
        } catch (NullPointerException e) {
            // expected
        }
    }

    @Test
    public void testHasExpanderWithClassForList() {
        assertTrue(resolver.hasExpander(List.class));
    }

    @Test
    public void testHasExpanderWithClassForObject() {
        assertFalse(resolver.hasExpander(Object.class));
    }

    @Test
    public void testHasExpanderWithClassForCollection() {
        assertFalse(resolver.hasExpander(Collection.class));
    }

    @Test
    public void testGetExpanderWithClassForList() {
        assertNotNull(resolver.getExpander(List.class));
    }

    @Test
    public void testGetExpanderWithClassForObject() {
        assertNull(resolver.getExpander(Object.class));
    }

    @Test
    public void testGetExpanderWithClassForCollection() {
        assertNull(resolver.getExpander(Collection.class));
    }
}
