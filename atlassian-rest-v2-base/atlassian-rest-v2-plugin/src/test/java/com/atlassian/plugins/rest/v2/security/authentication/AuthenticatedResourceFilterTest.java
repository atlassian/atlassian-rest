package com.atlassian.plugins.rest.v2.security.authentication;

import java.util.Optional;
import java.util.Set;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ResourceInfo;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.reflect.Whitebox;

import com.atlassian.annotations.security.ScopesAllowed;
import com.atlassian.oauth2.scopes.api.Scope;
import com.atlassian.oauth2.scopes.api.ScopesRequestCache;
import com.atlassian.oauth2.scopes.api.ScopesRequestCache.RequestCache;
import com.atlassian.plugins.rest.api.security.annotation.AdminOnly;
import com.atlassian.plugins.rest.api.security.annotation.AnonymousSiteAccess;
import com.atlassian.plugins.rest.api.security.annotation.SystemAdminOnly;
import com.atlassian.plugins.rest.api.security.annotation.UnlicensedSiteAccess;
import com.atlassian.plugins.rest.api.security.exception.AuthenticationRequiredException;
import com.atlassian.plugins.rest.api.security.exception.AuthorizationException;
import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;

import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.toSet;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import static com.atlassian.plugins.rest.v2.security.authentication.AuthenticatedResourceFilter.DEFAULT_TO_LICENSED_ACCESS_FEATURE_KEY;

/**
 * Unit test class for {@link AuthenticatedResourceFilter}
 * <p>
 *
 * @since 6.3.0
 */
public class AuthenticatedResourceFilterTest {

    private AuthenticatedResourceFilter authenticatedResourceFilter;

    @Mock
    private ResourceInfo resourceInfo;

    @Mock
    private UserManager mockUserManager;

    @Mock
    private ContainerRequestContext mockContainerRequestContext;

    @Mock
    private DarkFeatureManager mockDarkFeatureManager;

    @Mock
    private ScopesRequestCache mockScopesRequestCache;

    private UserKey userKey;

    @Before
    public void setUp() {
        initMocks(this);
        userKey = new UserKey("test_user");
        authenticatedResourceFilter =
                new AuthenticatedResourceFilter(mockUserManager, mockDarkFeatureManager, mockScopesRequestCache);
        Whitebox.setInternalState(authenticatedResourceFilter, "resourceInfo", resourceInfo);
    }

    /**
     * Scenario: A Sysadmin user is trying to access a resource which has SystemAdminOnly annotation
     * Precondition: User is System Admin
     * Expectation: User will be able to access the resource
     */
    @Test(expected = Test.None.class /* no exception expected */)
    public void filterWhenResourceHasSystemAdminOnlyAnnotation() throws NoSuchMethodException {
        when(mockUserManager.getRemoteUserKey()).thenReturn(userKey);
        when(mockUserManager.isSystemAdmin(userKey)).thenReturn(true);
        when(resourceInfo.getResourceMethod()).thenReturn(this.getClass().getMethod("mockedSystemAdminOnlyMethod"));
        authenticatedResourceFilter.filter(mockContainerRequestContext);
    }

    /**
     * Scenario: An authenticated licensed user is trying to access a resource which has SystemAdminOnly annotation
     * Precondition: 1. User is not SystemAdmin
     * 2. Site has global AnonymousAccess permission
     * 3. Site has LimitedUnlicensedAccess permission
     * Expectation: User will not be able to access the resource as requested resource is only accessibly by System Admin user
     */
    @Test
    public void
            filterWhenResourceHasSystemAdminOnlyAnnotationButUserIsNotSysadminWithGlobalAnonymousAccessEnabledAndLimitedUnlicensedAccessEnabled()
                    throws NoSuchMethodException {
        when(mockUserManager.getRemoteUserKey()).thenReturn(userKey);
        when(mockUserManager.isAnonymousAccessEnabled()).thenReturn(true);
        when(mockUserManager.isLimitedUnlicensedAccessEnabled()).thenReturn(true);
        when(mockUserManager.isSystemAdmin(userKey)).thenReturn(false);
        when(mockUserManager.isLicensed(userKey)).thenReturn(true);
        when(resourceInfo.getResourceMethod()).thenReturn(this.getClass().getMethod("mockedSystemAdminOnlyMethod"));
        assertThrows(
                AuthorizationException.class, () -> authenticatedResourceFilter.filter(mockContainerRequestContext));
    }

    /**
     * Scenario: An Admin user is trying to access a resource which has AdminOnly annotation
     * Precondition: User is Admin
     * Expectation: User will be able to access the resource
     */
    @Test(expected = Test.None.class /* no exception expected */)
    public void filterWhenResourceHasAdminOnlyAnnotation() throws NoSuchMethodException {
        when(mockUserManager.getRemoteUserKey()).thenReturn(userKey);
        when(mockUserManager.isSystemAdmin(userKey)).thenReturn(false);
        when(mockUserManager.isAdmin(userKey)).thenReturn(true);
        when(resourceInfo.getResourceMethod()).thenReturn(this.getClass().getMethod("mockedAdminOnlyMethod"));
        authenticatedResourceFilter.filter(mockContainerRequestContext);
    }

    /**
     * Scenario: An authenticated user is trying to access a resource which has AdminOnly annotation
     * Precondition: 1. User is not Admin
     * 2. Site has global AnonymousAccess permission
     * 3. Site has LimitedUnlicensedAccess permission
     * Expectation: User will not be able to access the resource as requested resource is only accessibly by Admin user
     */
    @Test
    public void
            filterWhenResourceHasAdminOnlyAnnotationButUserIsNotAdminWithGlobalAnonymousAccessEnabledAndLimitedUnlicensedAccessEnabled()
                    throws NoSuchMethodException {
        when(mockUserManager.getRemoteUserKey()).thenReturn(userKey);
        when(mockUserManager.isSystemAdmin(userKey)).thenReturn(false);
        when(mockUserManager.isAdmin(userKey)).thenReturn(false);
        when(mockUserManager.isAnonymousAccessEnabled()).thenReturn(true);
        when(mockUserManager.isLimitedUnlicensedAccessEnabled()).thenReturn(true);
        when(resourceInfo.getResourceMethod()).thenReturn(this.getClass().getMethod("mockedAdminOnlyMethod"));
        assertThrows(
                AuthorizationException.class, () -> authenticatedResourceFilter.filter(mockContainerRequestContext));
    }

    /**
     * Scenario: An anonymous user is trying to access a resource which has AnonymousSiteAccess annotation and site has global
     * AnonymousAccess permission
     * Precondition: 1. User is not authenticated
     * 2. Site has global AnonymousAccess permission
     * 3. Site has no LimitedUnlicensedAccess permission
     * Expectation: User will be able to access the resource as resource has AnonymousSiteAccess annotation and site has global
     * AnonymousAccess permission
     */
    @Test(expected = Test.None.class /* no exception expected */)
    public void filterWhenResourceHasAnonymousSiteAccessAnnotationWithGlobalAnonymousAccessEnabled()
            throws NoSuchMethodException {
        when(mockUserManager.getRemoteUserKey()).thenReturn(null);
        when(mockUserManager.isAnonymousAccessEnabled()).thenReturn(true);
        when(resourceInfo.getResourceMethod()).thenReturn(this.getClass().getMethod("mockedAnonymousSiteAccessMethod"));
        authenticatedResourceFilter.filter(mockContainerRequestContext);
    }

    /**
     * Scenario: An anonymous user is trying to access a resource which has AnonymousSiteAccess annotation but site doesn't
     * have global AnonymousAccess permission
     * Precondition: 1. User is not authenticated
     * 2. Site has no global AnonymousAccess permission
     * 3. Site has no LimitedUnlicensedAccess permission
     * Expectation: User will not be able to access the resource as resource has AnonymousSiteAccess annotation but site has no
     * global AnonymousAccess permission
     */
    @Test
    public void
            filterWhenResourceHasAnonymousSiteAccessAnnotationWithGlobalAnonymousAccessDisabledAndLimitedUnlicensedAccessDisabled()
                    throws NoSuchMethodException {
        when(mockUserManager.getRemoteUserKey()).thenReturn(null);
        when(mockUserManager.isAnonymousAccessEnabled()).thenReturn(false);
        when(mockUserManager.isLimitedUnlicensedAccessEnabled()).thenReturn(false);
        when(resourceInfo.getResourceMethod()).thenReturn(this.getClass().getMethod("mockedAnonymousSiteAccessMethod"));
        assertThrows(
                AuthenticationRequiredException.class,
                () -> authenticatedResourceFilter.filter(mockContainerRequestContext));
    }

    /**
     * Scenario: An anonymous user is trying to access a resource which has AnonymousSiteAccess annotation and site doesn't
     * have global AnonymousAccess permission but has LimitedUnlicensedAccess permission
     * Precondition: 1. User is not authenticated
     * 2. Site has no global AnonymousAccess permission
     * 3. Site has LimitedUnlicensedAccess permission
     * Expectation: User will not be able to access the resource as resource has AnonymousSiteAccess annotation and site
     * doesn't have global AnonymousAccess permission.
     * Because user is not authenticated, if user is authenticated, though global anonymous access is not enabled,
     * AnonymousSiteAccess will allow unlicensed user access.
     */
    @Test
    public void
            filterWhenResourceHasAnonymousSiteAccessAnnotationWithGlobalAnonymousAccessDisabledButLimitedUnlicensedAccessEnabled()
                    throws NoSuchMethodException {
        when(mockUserManager.getRemoteUserKey()).thenReturn(null);
        when(mockUserManager.isAnonymousAccessEnabled()).thenReturn(false);
        when(mockUserManager.isLimitedUnlicensedAccessEnabled()).thenReturn(true);
        when(resourceInfo.getResourceMethod()).thenReturn(this.getClass().getMethod("mockedAnonymousSiteAccessMethod"));
        assertThrows(
                AuthenticationRequiredException.class,
                () -> authenticatedResourceFilter.filter(mockContainerRequestContext));
    }

    /**
     * Scenario: A user is trying to access a resource which has UnlicensedSiteAccess annotation and site doesn't have global
     * AnonymousAccess permission but has LimitedUnlicensedAccess permission
     * Precondition: 1. User is not authenticated
     * 2. Site has no global AnonymousAccess permission
     * 3. Site has LimitedUnlicensedAccess permission
     * Expectation: User will be able to access the resource as resource has UnlicensedSiteAccess annotation and site has
     * LimitedUnlicensedAccess permission.
     */
    @Test(expected = Test.None.class /* no exception expected */)
    public void
            filterWhenResourceHasUnlicensedSiteAccessAnnotationWithLimitedUnlicensedAccessEnabledAndGlobalAnonymousAccessDisabled()
                    throws NoSuchMethodException {
        when(mockUserManager.getRemoteUserKey()).thenReturn(userKey);
        when(mockUserManager.isAnonymousAccessEnabled()).thenReturn(false);
        when(mockUserManager.isLimitedUnlicensedUser(userKey)).thenReturn(true);
        when(resourceInfo.getResourceMethod())
                .thenReturn(this.getClass().getMethod("mockedUnlicensedSiteAccessMethod"));
        authenticatedResourceFilter.filter(mockContainerRequestContext);
    }

    /**
     * Scenario: An anonymous user is trying to access a resource which has UnlicensedSiteAccess annotation and site has global
     * AnonymousAccess permission but doesn't have LimitedUnlicensedAccess permission
     * Precondition: 1. User is not authenticated
     * 2. Site has global AnonymousAccess permission
     * 3. Site has no LimitedUnlicensedAccess permission
     * Expectation: User will not be able to access the resource as resource has UnlicensedSiteAccess annotation and site
     * doesn't have LimitedUnlicensedAccess permission though site has global AnonymousAccess permission.
     * Because UnlicensedSiteAccess annotation will only allow unlicensed user when site will enable LimitedUnlicensedAccess
     * permission
     */
    @Test
    public void
            filterWhenResourceHasUnlicensedSiteAccessAnnotationWithLimitedUnlicensedAccessDisabledAndGlobalAnonymousAccessEnabled()
                    throws NoSuchMethodException {
        when(mockUserManager.getRemoteUserKey()).thenReturn(null);
        when(mockUserManager.isAnonymousAccessEnabled()).thenReturn(true);
        when(mockUserManager.isLimitedUnlicensedAccessEnabled()).thenReturn(false);
        when(resourceInfo.getResourceMethod())
                .thenReturn(this.getClass().getMethod("mockedUnlicensedSiteAccessMethod"));
        assertThrows(
                AuthenticationRequiredException.class,
                () -> authenticatedResourceFilter.filter(mockContainerRequestContext));
    }

    /**
     * Scenario: An anonymous user is trying to access a resource which has UnlicensedSiteAccess annotation and site has global
     * AnonymousAccess permission but doesn't have LimitedUnlicensedAccess permission
     * Precondition: 1. User is not authenticated
     * 2. Site has global AnonymousAccess permission
     * 3. Site has no LimitedUnlicensedAccess permission
     * Expectation: User will not be able to access the resource as resource has no annotation though site has global
     * AnonymousAccess permission.
     */
    @Test
    public void filterWhenResourceHasNoAnnotationWithLimitedUnlicensedAccessDisabledAndGlobalAnonymousAccessDisabled() {
        when(mockUserManager.getRemoteUserKey()).thenReturn(null);
        when(mockUserManager.isAnonymousAccessEnabled()).thenReturn(false);
        when(mockUserManager.isLimitedUnlicensedAccessEnabled()).thenReturn(false);
        assertThrows(
                AuthenticationRequiredException.class,
                () -> authenticatedResourceFilter.filter(mockContainerRequestContext));
    }

    /**
     * Scenario: An unlicensed user is trying to access a resource which has no annotation.
     * Precondition:
     * 1. User is authenticated
     * 2. User is not licensed
     * Expectation: User will not be able to access the resource as only licensed user will be allowed.
     */
    @Test
    public void filterWhenResourceHasNoAnnotationWithDarkFeatureEnabledAndUserIsNotLicensed() {
        when(mockUserManager.getRemoteUserKey()).thenReturn(userKey);
        when(mockUserManager.isLicensed(userKey)).thenReturn(false);
        assertThrows(
                AuthorizationException.class, () -> authenticatedResourceFilter.filter(mockContainerRequestContext));
    }

    /**
     * Scenario: A licensed user is trying to access a resource which has no annotation.
     * Precondition:
     * 1. User is authenticated
     * 2. User is Licensed
     * Expectation: User will be able to access the resource.
     */
    @Test(expected = Test.None.class /* no exception expected */)
    public void filterWhenResourceHasNoAnnotationWithDarkFeatureEnabledAndUserIsLicensed() {
        when(mockUserManager.getRemoteUserKey()).thenReturn(userKey);
        when(mockUserManager.isLicensed(userKey)).thenReturn(true);
        authenticatedResourceFilter.filter(mockContainerRequestContext);
    }

    /**
     * Scenario: An unauthenticated user is trying to access a resource which has no annotation.
     * Precondition:
     * 1. User is not authenticated
     * Expectation: Anonymous won't be able to access the resource
     */
    @Test(expected = AuthenticationRequiredException.class)
    public void filterWhenResourceHasNoAnnotationWithoutFlagSetAndUserIsAnonymous() {
        when(mockUserManager.getRemoteUserKey()).thenReturn(null);
        when(mockUserManager.isLicensed(userKey)).thenReturn(false);
        authenticatedResourceFilter.filter(mockContainerRequestContext);
    }

    /**
     * Scenario: An unlicensed user is trying to access a resource which has no annotation
     * and the atlassian.rest.default.to.licensed.access.disabled dark feature is enabled.
     * Precondition:
     * 1. User is authenticated
     * 2. User is unlicensed
     * 3. atlassian.rest.default.to.licensed.access.disabled dark feature is enabled
     * Expectation: User will be able to access the resource.
     */
    @Test(expected = Test.None.class /* no exception expected */)
    public void gainAccessWhenUnlicensedUserAndDefaultLicensedIsDisabled() {
        when(mockUserManager.getRemoteUserKey()).thenReturn(userKey);
        when(mockUserManager.isLicensed(userKey)).thenReturn(false);
        when(mockDarkFeatureManager.isEnabledForAllUsers(DEFAULT_TO_LICENSED_ACCESS_FEATURE_KEY))
                .thenReturn(Optional.of(true));
        authenticatedResourceFilter.filter(mockContainerRequestContext);
    }

    /**
     * Scenario: An unlicensed user is trying to access a resource which has no annotation
     * and the atlassian.rest.default.to.licensed.access.disabled dark feature is disabled.
     * Precondition:
     * 1. User is authenticated
     * 2. User is unlicensed
     * 3. atlassian.rest.default.to.licensed.access.disabled dark feature is disabled
     * Expectation: User will not be able to access the resource.
     */
    @Test(expected = Test.None.class /* no exception expected */)
    public void gainAccessWhenUnlicensedUserAndDefaultLicensedIsEnabled() {
        when(mockUserManager.getRemoteUserKey()).thenReturn(userKey);
        when(mockUserManager.isLicensed(userKey)).thenReturn(false);
        when(mockDarkFeatureManager.isEnabledForAllUsers(DEFAULT_TO_LICENSED_ACCESS_FEATURE_KEY))
                .thenReturn(Optional.of(false));
        assertThrows(
                AuthorizationException.class, () -> authenticatedResourceFilter.filter(mockContainerRequestContext));
    }

    @Test(expected = AuthorizationException.class)
    public void unlicensedUser_AnonymousSiteAccess_AnonymousAccessEnabled_LimitedUnlicensedSiteAccessDisabled()
            throws Exception {
        when(mockUserManager.getRemoteUserKey()).thenReturn(userKey);
        when(mockUserManager.isLimitedUnlicensedUser(userKey)).thenReturn(false);
        when(mockUserManager.isLicensed(userKey)).thenReturn(false);
        when(mockUserManager.isAnonymousAccessEnabled()).thenReturn(true);
        when(resourceInfo.getResourceMethod()).thenReturn(this.getClass().getMethod("mockedAnonymousSiteAccessMethod"));
        authenticatedResourceFilter.filter(mockContainerRequestContext);
    }

    @Test(expected = AuthorizationException.class)
    public void nonSysAdminUser_UnannotatedResource_SysAdminFFEnabled() throws Exception {
        when(mockDarkFeatureManager.isEnabledForAllUsers(
                        AuthenticatedResourceFilter.DEFAULT_TO_SYSADMIN_ACCESS_FEATURE_KEY))
                .thenReturn(Optional.of(true));
        when(mockUserManager.getRemoteUserKey()).thenReturn(userKey);
        when(mockUserManager.isSystemAdmin(userKey)).thenReturn(false);
        when(resourceInfo.getResourceMethod()).thenReturn(this.getClass().getMethod("mockedUnannotatedMethod"));
        authenticatedResourceFilter.filter(mockContainerRequestContext);
    }

    @Test(expected = Test.None.class /* no exception expected */)
    public void sysAdminUser_UnannotatedResource_SysAdminFFEnabled() throws Exception {
        when(mockDarkFeatureManager.isEnabledForAllUsers(
                        AuthenticatedResourceFilter.DEFAULT_TO_SYSADMIN_ACCESS_FEATURE_KEY))
                .thenReturn(Optional.of(true));
        when(mockUserManager.getRemoteUserKey()).thenReturn(userKey);
        when(mockUserManager.isSystemAdmin(userKey)).thenReturn(true);
        when(resourceInfo.getResourceMethod()).thenReturn(this.getClass().getMethod("mockedUnannotatedMethod"));
        authenticatedResourceFilter.filter(mockContainerRequestContext);
    }

    @Test
    public void filterWhenServiceAccountHasTheRequiredScope() throws NoSuchMethodException {
        mockScopesRequestCacheReturns(Set.of("admin", "read"));
        when(resourceInfo.getResourceMethod()).thenReturn(this.getClass().getMethod("mockedScopesAllowedMethod"));

        authenticatedResourceFilter.filter(mockContainerRequestContext);
    }

    @Test
    public void filterWhenServiceAccountDoesNotHaveTheRequiredScope() throws NoSuchMethodException {
        mockScopesRequestCacheReturns(Set.of("admin"));
        when(mockUserManager.getRemoteUserKey()).thenReturn(userKey);
        when(resourceInfo.getResourceMethod()).thenReturn(this.getClass().getMethod("mockedScopesAllowedMethod"));

        assertThrows(
                AuthorizationException.class, () -> authenticatedResourceFilter.filter(mockContainerRequestContext));
    }

    @Test
    public void filterWhenServiceAccountDoesNotHaveScopes() throws NoSuchMethodException {
        mockScopesRequestCacheReturns(emptySet());
        when(mockUserManager.getRemoteUserKey()).thenReturn(userKey);
        when(resourceInfo.getResourceMethod()).thenReturn(this.getClass().getMethod("mockedScopesAllowedMethod"));

        assertThrows(
                AuthorizationException.class, () -> authenticatedResourceFilter.filter(mockContainerRequestContext));
    }

    @Test
    public void filterWhenScopesAllowedIsCombinedWithAnotherAnnotations_systemAdmin() throws NoSuchMethodException {
        when(mockUserManager.getRemoteUserKey()).thenReturn(userKey);
        when(mockUserManager.isSystemAdmin(userKey)).thenReturn(true);
        when(resourceInfo.getResourceMethod())
                .thenReturn(this.getClass().getMethod("mockedAdminOnlyAndScopesAllowedMethod"));

        authenticatedResourceFilter.filter(mockContainerRequestContext);
    }

    @Test
    public void filterWhenScopesAllowedIsCombinedWithAnotherAnnotations_serviceAccount() throws NoSuchMethodException {
        mockScopesRequestCacheReturns(Set.of("read"));
        when(mockUserManager.getRemoteUserKey()).thenReturn(userKey);
        when(mockUserManager.isLicensed(userKey)).thenReturn(false);
        when(resourceInfo.getResourceMethod())
                .thenReturn(this.getClass().getMethod("mockedAdminOnlyAndScopesAllowedMethod"));

        authenticatedResourceFilter.filter(mockContainerRequestContext);
    }

    private void mockScopesRequestCacheReturns(Set<String> scopes) {
        Set<Scope> mockScopes = scopes.stream()
                .map(scope -> {
                    Scope readScope = mock(Scope.class);
                    when(readScope.getName()).thenReturn(scope);
                    return readScope;
                })
                .collect(toSet());

        RequestCache requestCache = Mockito.mock(RequestCache.class);
        when(requestCache.getScopes()).thenReturn(mockScopes);
        when(mockScopesRequestCache.getRequestCache()).thenReturn(requestCache);
        when(mockScopesRequestCache.isScopePermitted(Mockito.anyString())).thenAnswer(invocation -> {
            String scope = invocation.getArgument(0);
            return scopes.contains(scope);
        });
    }

    @SystemAdminOnly
    public void mockedSystemAdminOnlyMethod() {
        // this is a mocked method
    }

    @AdminOnly
    public void mockedAdminOnlyMethod() {
        // this is a mocked method
    }

    @AnonymousSiteAccess
    public void mockedAnonymousSiteAccessMethod() {
        // this is a mocked method
    }

    @UnlicensedSiteAccess
    public void mockedUnlicensedSiteAccessMethod() {
        // this is a mocked method
    }

    @ScopesAllowed(requiredScope = {"read", "write"})
    public void mockedScopesAllowedMethod() {
        // this is a mocked method
    }

    @AdminOnly
    @ScopesAllowed(requiredScope = {"read"})
    public void mockedAdminOnlyAndScopesAllowedMethod() {
        // this is a mocked method
    }

    public void mockedUnannotatedMethod() {
        // this is a mocked method
    }
}
