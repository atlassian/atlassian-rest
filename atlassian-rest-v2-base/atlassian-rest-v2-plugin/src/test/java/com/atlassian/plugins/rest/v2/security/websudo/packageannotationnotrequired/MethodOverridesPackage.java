package com.atlassian.plugins.rest.v2.security.websudo.packageannotationnotrequired;

import com.atlassian.sal.api.websudo.WebSudoRequired;

public class MethodOverridesPackage {
    @WebSudoRequired
    public void aMethod() {}
}
