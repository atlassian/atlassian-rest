package com.atlassian.plugins.rest.v2.security.xsrf;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.FeatureContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.plugins.rest.v2.security.cors.CorsDefaultService;
import com.atlassian.sal.api.web.context.HttpContext;
import com.atlassian.sal.api.xsrf.XsrfRequestValidator;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenAccessor;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenValidator;
import com.atlassian.sal.core.xsrf.XsrfRequestValidatorImpl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class XsrfResourceDynamicFeatureTest {
    @Mock
    private HttpContext httpContext;

    @Mock
    private CorsDefaultService corsDefaultService;

    @Mock
    private ResourceInfo resourceInfo;

    @Mock
    private FeatureContext context;

    private XsrfResourceDynamicFeature xsrfResourceDynamicFeature;

    @Before
    public void setUp() {
        XsrfRequestValidator xsrfRequestValidator =
                new XsrfRequestValidatorImpl(new IndependentXsrfTokenValidator(new IndependentXsrfTokenAccessor()));
        xsrfResourceDynamicFeature =
                new XsrfResourceDynamicFeature(httpContext, xsrfRequestValidator, corsDefaultService);
    }

    @Test
    public void testCreateWithXsrfProtectionExcludedAnnotation() throws NoSuchMethodException {
        when(resourceInfo.getResourceMethod())
                .thenReturn(this.getClass().getMethod("mockedXsrfProtectionExcludedMethod"));
        xsrfResourceDynamicFeature.configure(resourceInfo, context);
        verify(context, never()).register(any(XsrfResourceFilter.class));
    }

    @Test
    public void testCreateWithoutXsrfProtectionExcludedAnnotation() throws NoSuchMethodException {
        when(resourceInfo.getResourceMethod()).thenReturn(this.getClass().getMethod("mockedMethod"));
        xsrfResourceDynamicFeature.configure(resourceInfo, context);
        verify(context).register(any(XsrfResourceFilter.class));
    }

    @Test
    public void testCreateGetResourceWithoutRequiresXsrfCheckAnnotation() throws NoSuchMethodException {
        when(resourceInfo.getResourceMethod()).thenReturn(this.getClass().getMethod("mockedGetMethod"));
        xsrfResourceDynamicFeature.configure(resourceInfo, context);
        verify(context, never()).register(any(XsrfResourceFilter.class));
    }

    @XsrfProtectionExcluded
    public void mockedXsrfProtectionExcludedMethod() {
        // this is a mocked method
    }

    public void mockedMethod() {
        // this is a mocked method
    }

    @GET
    public String mockedGetMethod() {
        // this is a mocked method
        return "";
    }
}
