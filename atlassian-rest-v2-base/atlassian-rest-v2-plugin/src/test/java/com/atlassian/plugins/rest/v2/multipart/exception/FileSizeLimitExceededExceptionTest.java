package com.atlassian.plugins.rest.v2.multipart.exception;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FileSizeLimitExceededExceptionTest {

    @Test
    public void isExpectedStatusCode() {
        FileSizeLimitExceededException ex = new FileSizeLimitExceededException("Simple message");
        assertEquals(413, ex.getResponse().getStatus());
    }
}
