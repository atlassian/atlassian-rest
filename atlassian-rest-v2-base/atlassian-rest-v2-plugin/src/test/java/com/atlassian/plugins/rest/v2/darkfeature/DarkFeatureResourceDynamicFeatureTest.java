package com.atlassian.plugins.rest.v2.darkfeature;

import org.junit.Test;
import org.mockito.InjectMocks;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class DarkFeatureResourceDynamicFeatureTest extends AbstractDarkFeatureTestResource {
    @InjectMocks
    private DarkFeatureResourceDynamicFeature darkFeatureResourceDynamicFeature;

    @Test
    public void testCreateWithNoAnnotations() throws NoSuchMethodException {
        setResourceAnnotated(false);
        setMethodAnnotated(false);
        darkFeatureResourceDynamicFeature.configure(resourceInfo, featureContext);
        verify(featureContext, times(0)).register(any(DarkFeatureResourceFilter.class));
    }

    @Test
    public void testCreateWithResourceAnnotations() throws NoSuchMethodException {
        setResourceAnnotated(true);
        setMethodAnnotated(false);
        darkFeatureResourceDynamicFeature.configure(resourceInfo, featureContext);
        verify(featureContext).register(any(DarkFeatureResourceFilter.class));
    }

    @Test
    public void testCreateWithMethodAnnotations() throws NoSuchMethodException {
        setResourceAnnotated(false);
        setMethodAnnotated(true);
        darkFeatureResourceDynamicFeature.configure(resourceInfo, featureContext);
        verify(featureContext).register(any(DarkFeatureResourceFilter.class));
    }

    @Test
    public void testCreateWithResourceAndMethodAnnotations() throws NoSuchMethodException {
        setResourceAnnotated(true);
        setMethodAnnotated(true);
        darkFeatureResourceDynamicFeature.configure(resourceInfo, featureContext);
        verify(featureContext).register(any(DarkFeatureResourceFilter.class));
    }
}
