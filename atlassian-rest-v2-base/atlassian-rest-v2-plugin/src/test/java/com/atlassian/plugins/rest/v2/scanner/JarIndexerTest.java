package com.atlassian.plugins.rest.v2.scanner;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.jar.JarFile;

import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.Bundle;

import com.atlassian.rest.test.jar.AnAnnotation;
import com.atlassian.rest.test.jar.AnnotatedClass;
import com.atlassian.rest.test.jar.ClassWithAnnotatedMethod;
import com.atlassian.rest.test.jar2.AnotherAnnotatedClass;
import com.atlassian.rest.test.jar2.jar3.YetAnotherAnnotatedClass;
import com.atlassian.rest.test.jar4.BundledAnnotatedClass;
import com.atlassian.rest.test.jar4.BundledClassWithAnnotatedMethod;

import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static com.atlassian.plugins.rest.v2.scanner.AnnotatedClassScannerTest.assertCollectionEquals;

public class JarIndexerTest {

    private AnnotatedClassScanner scanner;
    private Bundle bundle;
    private JarIndexer jarIndexer;
    private JarFile jarFile;

    private Set<String> packages;

    @Before
    public void setUp() throws ClassNotFoundException, IOException {
        bundle = mock(Bundle.class);
        when(bundle.getLocation()).thenReturn(getTestJar().getAbsolutePath());
        when(bundle.loadClass(AnnotatedClass.class.getName())).thenReturn((Class) AnnotatedClass.class);
        when(bundle.loadClass(AnotherAnnotatedClass.class.getName())).thenReturn((Class) AnotherAnnotatedClass.class);
        when(bundle.loadClass(YetAnotherAnnotatedClass.class.getName()))
                .thenReturn((Class) YetAnotherAnnotatedClass.class);
        when(bundle.loadClass(ClassWithAnnotatedMethod.class.getName()))
                .thenReturn((Class) ClassWithAnnotatedMethod.class);
        when(bundle.loadClass(BundledAnnotatedClass.class.getName())).thenReturn((Class) BundledAnnotatedClass.class);
        when(bundle.loadClass(BundledClassWithAnnotatedMethod.class.getName()))
                .thenReturn((Class) BundledClassWithAnnotatedMethod.class);

        scanner = new AnnotatedClassScanner(bundle, true, AnAnnotation.class);
        File bundleFile = scanner.getBundleFile(bundle);
        jarFile = new JarFile(bundleFile);
        packages = new HashSet<>();
        jarIndexer = new JarIndexer(jarFile, packages, true, scanner.getAnnotationSet(AnAnnotation.class), bundle);
    }

    private File getTestJar() {
        return new File(
                Objects.requireNonNull(this.getClass().getClassLoader().getResource("atlassian-rest-test-jar.jar"))
                        .getFile());
    }

    private File getBrokenTestJar() {
        return new File(Objects.requireNonNull(
                        this.getClass().getClassLoader().getResource("atlassian-rest-broken-test-jar.jar"))
                .getFile());
    }

    @Test
    public void testScanJarWithBrokenJar() throws IOException {
        when(bundle.getLocation()).thenReturn(getBrokenTestJar().getAbsolutePath());
        File bundleFile = scanner.getBundleFile(bundle);
        jarFile = new JarFile(bundleFile);
        packages = new HashSet<>();
        jarIndexer = new JarIndexer(jarFile, packages, true, scanner.getAnnotationSet(AnAnnotation.class), bundle);
        assertThrows(JarIndexerException.class, () -> jarIndexer.scanJar());
    }

    @Test
    public void testScanJarWithFlagTrueWithoutBasePackage() {
        testScanJar(Arrays.asList(
                AnnotatedClass.class,
                AnotherAnnotatedClass.class,
                YetAnotherAnnotatedClass.class,
                ClassWithAnnotatedMethod.class,
                BundledAnnotatedClass.class,
                BundledClassWithAnnotatedMethod.class));
    }

    @Test
    public void testScanJarWithFlagTrueWithABasePackage() {
        packages.add("com/atlassian/rest/test/jar4");
        testScanJar(Arrays.asList(BundledAnnotatedClass.class, BundledClassWithAnnotatedMethod.class));
    }

    @Test
    public void testScanJarWithFlagTrueWithMultiplePackages() {
        packages.add("com/atlassian/rest/test/jar4");
        packages.add("com/atlassian/rest/test/jar2");
        testScanJar(Arrays.asList(
                BundledAnnotatedClass.class,
                BundledClassWithAnnotatedMethod.class,
                AnotherAnnotatedClass.class,
                YetAnotherAnnotatedClass.class));
    }

    @Test
    public void testScanJarWithFlagFalseWithoutBasePackage() {
        jarIndexer = new JarIndexer(jarFile, packages, false, scanner.getAnnotationSet(AnAnnotation.class), bundle);
        testScanJar(Arrays.asList(
                AnnotatedClass.class,
                AnotherAnnotatedClass.class,
                YetAnotherAnnotatedClass.class,
                ClassWithAnnotatedMethod.class));
    }

    @Test
    public void testScanJarWithFlagFalseWithABasePackage() {
        jarIndexer = new JarIndexer(jarFile, packages, false, scanner.getAnnotationSet(AnAnnotation.class), bundle);
        packages.add("com/atlassian/rest/test/jar4");
        testScanJar(Collections.emptyList());
    }

    @Test
    public void testScanJarWithFlagFalseWithMultiplePackages() {
        jarIndexer = new JarIndexer(jarFile, packages, false, scanner.getAnnotationSet(AnAnnotation.class), bundle);
        packages.add("com/atlassian/rest/test/jar4");
        packages.add("com/atlassian/rest/test/jar2");
        testScanJar(Arrays.asList(AnotherAnnotatedClass.class, YetAnotherAnnotatedClass.class));
    }

    public void testScanJar(List<Class<?>> expected) {
        Set<Class<?>> resultSet = jarIndexer.scanJar();
        assertCollectionEquals(expected, new ArrayList<>(resultSet));
    }
}
