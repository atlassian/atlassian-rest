package com.atlassian.plugins.rest.v2.security.cors;

import java.lang.annotation.Annotation;
import jakarta.ws.rs.HttpMethod;
import jakarta.ws.rs.container.DynamicFeature;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.FeatureContext;
import jakarta.ws.rs.ext.Provider;

import com.atlassian.plugins.rest.api.security.annotation.CorsAllowed;

/**
 * DynamicFeature for the Cross-Origin Resource Sharing resource filter, triggering off
 * {@link CorsAllowed}.
 *
 * @since 2.6
 */
@Provider
public class CorsResourceDynamicFeature implements DynamicFeature {

    private final CorsDefaultService corsDefaultService;

    public CorsResourceDynamicFeature(CorsDefaultService corsDefaultService) {
        this.corsDefaultService = corsDefaultService;
    }

    @Override
    public void configure(ResourceInfo resourceInfo, FeatureContext context) {
        if (annotationIsPresent(resourceInfo)) {
            String targetMethod = HttpMethod.GET;
            for (Annotation annotation : resourceInfo.getResourceMethod().getAnnotations()) {
                HttpMethod httpMethod = annotation.annotationType().getAnnotation(HttpMethod.class);
                if (httpMethod != null) {
                    targetMethod = httpMethod.value();
                    break;
                }
            }
            context.register(new CorsResourceFilter(targetMethod, corsDefaultService));
        }
    }

    private boolean annotationIsPresent(ResourceInfo resourceInfo) {
        return resourceInfo.getResourceMethod().isAnnotationPresent(CorsAllowed.class)
                || resourceInfo.getResourceClass().isAnnotationPresent(CorsAllowed.class)
                || packageHasAnnotation(resourceInfo.getResourceClass().getPackage());
    }

    private boolean packageHasAnnotation(Package resourcePackage) {
        return resourcePackage != null && resourcePackage.isAnnotationPresent(CorsAllowed.class);
    }
}
