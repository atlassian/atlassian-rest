package com.atlassian.plugins.rest.v2.expand.resolver;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.atlassian.plugins.rest.api.expand.EntityCrawler;
import com.atlassian.plugins.rest.api.expand.ExpandContext;
import com.atlassian.plugins.rest.api.expand.annotation.IndexedExpand;
import com.atlassian.plugins.rest.api.expand.expander.EntityExpander;
import com.atlassian.plugins.rest.api.expand.parameter.Indexes;
import com.atlassian.plugins.rest.api.expand.resolver.EntityExpanderResolver;
import com.atlassian.plugins.rest.api.internal.expand.exception.ExpandIndexException;

import static java.util.Objects.requireNonNull;

/**
 * {@link EntityExpanderResolver Entity expander resolver} that will get resolver for classes (objects) with method annotated with {@link IndexedExpand @IndexedExpand}.
 * Method signature should be the same as below:
 * <code>
 * {@literal @}IndexedExpand public void expandInstance(Indexes indexes)
 * {
 * // your code to expand the object here
 * }
 * </code>
 * Can be used as a simpler alternative to {@link ListWrapperEntityExpanderResolverImpl}.
 */
public class IndexedEntityExpanderResolver implements EntityExpanderResolver {
    public boolean hasExpander(Class<?> type) {
        return getConstrainMethod(requireNonNull(type)) != null;
    }

    public <T> EntityExpander<T> getExpander(Class<? extends T> type) {
        final Method method = getConstrainMethod(requireNonNull(type));
        return method != null ? (EntityExpander<T>) new IndexedExpandEntityExpander(method) : null;
    }

    private <T> Method getConstrainMethod(Class<? extends T> type) {
        for (Method method : type.getDeclaredMethods()) {
            if (method.getAnnotation(IndexedExpand.class) != null
                    && method.getParameterTypes().length == 1
                    && method.getParameterTypes()[0].equals(Indexes.class)) {
                return method;
            }
        }
        return null;
    }

    private static class IndexedExpandEntityExpander implements EntityExpander<Object> {
        private final Method method;

        public IndexedExpandEntityExpander(Method method) {
            this.method = requireNonNull(method);
        }

        public Object expand(
                ExpandContext<Object> context, EntityExpanderResolver expanderResolver, EntityCrawler entityCrawler) {
            final Object entity = context.getEntity();
            try {
                method.invoke(entity, context.getEntityExpandParameter().getIndexes(context.getExpandable()));
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new ExpandIndexException(e);
            }

            entityCrawler.crawl(
                    entity,
                    context.getEntityExpandParameter().getExpandParameter(context.getExpandable()),
                    expanderResolver);

            return entity;
        }
    }
}
