package com.atlassian.plugins.rest.v2.descriptor;

import jakarta.servlet.Filter;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugin.servlet.ServletModuleManager;
import com.atlassian.plugin.servlet.descriptors.ServletFilterModuleDescriptor;
import com.atlassian.plugins.rest.v2.RestApiContext;
import com.atlassian.plugins.rest.v2.jersey.ResourceConfigFactory;
import com.atlassian.plugins.rest.v2.servlet.RestDelegatingServletFilter;
import com.atlassian.plugins.rest.v2.version.ApiVersion;

import static java.util.Objects.requireNonNull;

/**
 * Copy of {@link com.atlassian.plugins.rest.module.RestServletFilterModuleDescriptor}
 * Differences:
 * <ul>
 *     <li>different `getName` literal</li>
 *     <li>simplified `init` method</li>
 * </ul>
 * This file is almost 100% the same as original one. It could be shared in both modules.
 */
public class RestServletFilterModuleDescriptor extends ServletFilterModuleDescriptor {
    private final RestDelegatingServletFilter restDelegatingServletFilter;
    private final RestApiContext restApiContext;
    private static final String DISABLE_WADL_PROPERTY = "com.sun.jersey.config.feature.DisableWADL";

    RestServletFilterModuleDescriptor(
            OsgiPlugin plugin,
            ModuleFactory moduleFactory,
            ServletModuleManager servletModuleManager,
            RestApiContext restApiContext,
            ResourceConfigFactory resourceConfigFactory) {
        super(
                requireNonNull(moduleFactory, "moduleFactory can't be null"),
                requireNonNull(servletModuleManager, "servletModuleManager can't be nul"));
        this.restApiContext = requireNonNull(restApiContext, "restApiContext can't be null");
        this.restDelegatingServletFilter =
                new RestDelegatingServletFilter(plugin, restApiContext, resourceConfigFactory);
    }

    @Override
    public void init(Plugin plugin, Element element) {
        super.init(plugin, element);

        // JRADEV-23134 : allow OD to switch off WADL generation, via system property
        getInitParams().put(DISABLE_WADL_PROPERTY, System.getProperty(DISABLE_WADL_PROPERTY, "false"));
        restDelegatingServletFilter.registerSingletonsFromInitParams(getInitParams());
    }

    @Override
    protected void validate(Element element) {
        // Do nothing
    }

    @Override
    public String getName() {
        return "Rest V2 Servlet Filter";
    }

    @Override
    public Filter getModule() {
        return restDelegatingServletFilter;
    }

    public String getBasePath() {
        return restApiContext.getApiPath();
    }

    public ApiVersion getVersion() {
        return restApiContext.getVersion();
    }

    private static boolean resourcesAvailable(Plugin plugin, String... resources) {
        for (final String resource : resources) {
            if (plugin.getResource(resource) == null) {
                return false;
            }
        }
        return true;
    }
}
