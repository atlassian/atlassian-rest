package com.atlassian.plugins.rest.v2.security.xsrf;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import jakarta.annotation.Priority;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.DynamicFeature;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.FeatureContext;
import jakarta.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.plugins.rest.v2.security.cors.CorsDefaultService;
import com.atlassian.sal.api.web.context.HttpContext;
import com.atlassian.sal.api.xsrf.XsrfRequestValidator;

import static java.util.Objects.requireNonNull;

/**
 * Dynamic feature for the XSRF resource filter
 *
 * @since 2.4
 */
@Priority(Priorities.AUTHORIZATION)
@Provider
public class XsrfResourceDynamicFeature implements DynamicFeature {
    private static final Logger log = LoggerFactory.getLogger(XsrfResourceDynamicFeature.class);

    private final HttpContext httpContext;
    private final XsrfRequestValidator xsrfRequestValidator;
    private final CorsDefaultService corsDefaultService;

    public XsrfResourceDynamicFeature(
            HttpContext httpContext, XsrfRequestValidator xsrfRequestValidator, CorsDefaultService corsDefaultService) {
        this.httpContext = requireNonNull(httpContext, "httpContext can't be null");
        this.xsrfRequestValidator = requireNonNull(xsrfRequestValidator, "xsrfRequestValidator can't be null");
        this.corsDefaultService = corsDefaultService;
    }

    @Override
    public void configure(ResourceInfo resourceInfo, FeatureContext context) {
        final Method method = resourceInfo.getResourceMethod();
        final boolean hasExcludeAnnotation = isXsrfProtectionExcludedAnnotationPresent(method.getAnnotations());
        XsrfResourceFilter xsrfResourceFilter = null;

        if (!method.isAnnotationPresent(GET.class) && !hasExcludeAnnotation) {
            xsrfResourceFilter = new XsrfResourceFilter();
        }
        if (xsrfResourceFilter != null) {
            xsrfResourceFilter.setHttpContext(httpContext);
            xsrfResourceFilter.setXsrfRequestValidator(xsrfRequestValidator);
            xsrfResourceFilter.setCorsDefaults(corsDefaultService.getCorsDefaults());
            context.register(xsrfResourceFilter);
        }
    }

    /**
     * Returns true if any of the given annotations is the
     * XsrfProtectionExcluded annotation.
     *
     * @param annotations the annotations to check against.
     * @return true if any of the given annotations is the
     * XsrfProtectionExcluded annotation. Otherwise, returns false.
     */
    private static boolean isXsrfProtectionExcludedAnnotationPresent(Annotation[] annotations) {
        for (Annotation annotation : annotations) {
            if (annotation
                    .annotationType()
                    .getCanonicalName()
                    .equals(XsrfProtectionExcluded.class.getCanonicalName())) {
                if (!annotation.annotationType().equals(XsrfProtectionExcluded.class)) {
                    log.warn(
                            "Detected usage of the com.atlassian.annotations.security. "
                                    + "XsrfProtectionExcluded annotation loaded from elsewhere. {} != {}",
                            XsrfProtectionExcluded.class.getClassLoader(),
                            annotation.annotationType().getClassLoader());
                }
                return true;
            }
        }
        return false;
    }
}
