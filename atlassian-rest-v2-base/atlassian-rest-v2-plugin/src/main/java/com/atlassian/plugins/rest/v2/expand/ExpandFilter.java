package com.atlassian.plugins.rest.v2.expand;

import java.io.IOException;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;

import com.atlassian.plugins.rest.api.expand.parameter.ExpandParameter;
import com.atlassian.plugins.rest.api.expand.resolver.EntityExpanderResolver;
import com.atlassian.plugins.rest.api.internal.expand.parameter.DefaultExpandParameter;

import static java.util.Objects.requireNonNull;

/**
 * The {@link ContainerResponseFilter} responsible for expanding the entity returned from the resource method. The filter
 * is global to all plugins and products that implement REST.
 *
 * @since 2.0
 */
public class ExpandFilter implements ContainerResponseFilter {
    private final EntityExpanderResolver expanderResolver;
    private final String expandParameterName;

    public ExpandFilter(EntityExpanderResolver expanderResolver) {
        this("expand", expanderResolver);
    }

    public ExpandFilter(String expandParameterName, EntityExpanderResolver expanderResolver) {
        this.expanderResolver = expanderResolver;
        this.expandParameterName = requireNonNull(expandParameterName);
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
            throws IOException {
        final ExpandParameter expandParameter = new DefaultExpandParameter(
                requestContext.getUriInfo().getQueryParameters().get(expandParameterName));
        new DefaultEntityCrawler().crawl(responseContext.getEntity(), expandParameter, expanderResolver);
    }
}
