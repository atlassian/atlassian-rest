package com.atlassian.plugins.rest.v2.exception;

import java.util.UUID;
import jakarta.annotation.Priority;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugins.rest.v2.exception.entity.UncaughtExceptionEntity;

/**
 * A generic exception mapper that will map any {@link Throwable throwable}.  Handles the special case of
 * {@link WebApplicationException}, which provides its own response.
 *
 * @since 1.0
 */
@Priority(Priorities.USER)
@Provider
public class ThrowableExceptionMapper implements ExceptionMapper<Throwable> {
    private static final Logger log = LoggerFactory.getLogger(ThrowableExceptionMapper.class);

    @Context
    Request request;

    public Response toResponse(Throwable throwable) {
        if (throwable instanceof WebApplicationException) {
            // Internal Server Error: a bug or other error -> log it with a stack trace just in case it wasn't logged
            // already
            // Note that other 5xx codes are specific well-known codes that do not indicate a potential bug.
            // For more details see: https://bitbucket.org/atlassian/atlassian-rest/pull-request/12#comment-1088837

            final WebApplicationException webEx = (WebApplicationException) throwable;
            if (webEx.getResponse().getStatus() == Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()) {
                final String errorId = UUID.randomUUID().toString();
                log.error(
                        "Internal Server Error {} in REST: {}: {}",
                        errorId,
                        webEx.getResponse().getStatus(),
                        webEx.getResponse(),
                        webEx);

                return Response.status(webEx.getResponse().getStatus())
                        .entity(new UncaughtExceptionEntity(webEx, errorId))
                        .type(UncaughtExceptionEntity.variantFor(request))
                        .build();
            } else {
                log.debug("REST response: {}: {}", webEx.getResponse().getStatus(), webEx.getResponse());
                return Response.status(webEx.getResponse().getStatus())
                        .entity(new UncaughtExceptionEntity(webEx.getResponse().getStatus(), webEx))
                        .type(UncaughtExceptionEntity.variantFor(request))
                        .build();
            }
        } else {
            final String errorId = UUID.randomUUID().toString();
            log.error("Uncaught exception {} thrown by REST service: {}", errorId, throwable.getMessage(), throwable);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new UncaughtExceptionEntity(throwable, errorId))
                    .type(UncaughtExceptionEntity.variantFor(request))
                    .build();
        }
    }
}
