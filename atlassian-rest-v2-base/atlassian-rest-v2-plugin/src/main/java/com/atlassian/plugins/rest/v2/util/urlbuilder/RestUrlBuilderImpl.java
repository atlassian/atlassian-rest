package com.atlassian.plugins.rest.v2.util.urlbuilder;

import java.net.URI;
import java.util.function.Consumer;

import com.atlassian.plugins.rest.api.util.RestUrlBuilder;

/**
 * @since 2.2
 */
public class RestUrlBuilderImpl implements RestUrlBuilder {
    @Override
    public <T> URI getUrlFor(URI baseUri, Class<T> resourceClass, Consumer<T> consumer) {
        ResourcePathHandler callback = new ResourcePathHandler(resourceClass, baseUri);
        T proxy = ProxyUtils.create(resourceClass, callback);
        consumer.accept(proxy);

        return callback.getUri();
    }
}
