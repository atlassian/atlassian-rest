package com.atlassian.plugins.rest.v2.exception;

import jakarta.annotation.Priority;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugins.rest.api.model.Status;
import com.atlassian.plugins.rest.api.security.exception.AccessDeniedException;

/**
 * Exception mapper that takes care of {@link SecurityException security exceptions}
 *
 * @since 1.0
 */
@Priority(Priorities.USER)
@Provider
public class SecurityExceptionMapper implements ExceptionMapper<SecurityException> {
    private static final Logger log = LoggerFactory.getLogger(SecurityExceptionMapper.class);

    @Context
    Request request;

    @Override
    public Response toResponse(SecurityException exception) {
        log.debug("Security Exception in REST: ", exception);

        if (exception instanceof AccessDeniedException) {
            return Status.fromStatusCode(((AccessDeniedException) exception).getHttpResponseStatusCode())
                    .message(exception.getMessage())
                    .responseBuilder()
                    .type(Status.variantFor(request))
                    .build();
        }

        return Status.unauthorized()
                .message(exception.getMessage())
                .responseBuilder()
                .type(Status.variantFor(request))
                .build();
    }
}
