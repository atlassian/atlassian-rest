package com.atlassian.plugins.rest.v2.servlet;

import java.io.IOException;
import java.util.Map;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugins.rest.v2.ChainingClassLoader;
import com.atlassian.plugins.rest.v2.RestApiContext;
import com.atlassian.plugins.rest.v2.Slf4jBridge;
import com.atlassian.plugins.rest.v2.descriptor.RestModuleDescriptor;
import com.atlassian.plugins.rest.v2.filter.ExtensionJerseyFilter;
import com.atlassian.plugins.rest.v2.jersey.JerseyOsgiServletContainer;
import com.atlassian.plugins.rest.v2.jersey.ResourceConfigFactory;

import static java.util.Arrays.asList;

/**
 * Copy of {@link com.atlassian.plugins.rest.module.RestDelegatingServletFilter}
 * Differences:
 * <ul>
 *     <li>removed `SLF4J Bridge` (not sure if it is still needed for proper logging in Jersey context)</li>
 *     <li>`servletContainer` is a `Filter` to remove bound to Jersey (original ServletContainer is a subtype of Filter)</li>
 *     <li>extracted internal class `JerseyOsgiServletContainer` to separate file</li>
 * </ul>
 * Changes made in this file removed direct dependency to Jersey code and removed elements that seem to be obsolete (at least in this simple implementation).
 * Despite many changes I think it is still possible to create common class used by both modules.
 */
public class RestDelegatingServletFilter implements Filter {
    private static final Logger log = LoggerFactory.getLogger(RestDelegatingServletFilter.class);

    /**
     * Helper object for installing and un-installing the SLF4J bridge.
     */
    private static final Slf4jBridge.Helper SLF4J_BRIDGE = Slf4jBridge.createHelper();

    private static final String PARAM_EXTENSION_FILTER_EXCLUDES = "extension.filter.excludes";

    /**
     * Overriden servlet container
     */
    private final Filter servletContainer;

    private final ResourceConfig resourceConfig;

    /**
     * This class loader is set as the thread context class loader whenever we are calling into Jersey. We need to reuse
     * the same class loader every time because when this filter is initialised it sets up the JUL->SLF4J bridge (the
     * same one that allows ).
     * <p>
     * The thread context class loader (TCCL) is set to this class loader when calling into Jersey. We keep a reference
     * to it as opposed to creating a new one in every invocation because Jersey uses java.util.logging, which on
     * Tomcat is implemented by JULI.
     */
    private ClassLoader chainingClassLoader;

    public RestDelegatingServletFilter(
            OsgiPlugin plugin, RestApiContext restContextPath, ResourceConfigFactory resourceConfigFactory) {
        this.resourceConfig = resourceConfigFactory.createConfig(
                plugin, restContextPath.getPackages(), restContextPath.getIndexBundledJars());
        this.servletContainer = new JerseyOsgiServletContainer(plugin, restContextPath, resourceConfig);
    }

    public void init(FilterConfig config) throws ServletException {
        initChainingClassLoader();
        initServletContainer(config);
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        ClassLoader currentThreadClassLoader = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(chainingClassLoader);
        try {
            servletContainer.doFilter(request, response, chain);
        } finally {
            Thread.currentThread().setContextClassLoader(currentThreadClassLoader);
        }
    }

    public void destroy() {
        destroyServletContainer();
    }

    public void registerSingletonsFromInitParams(Map<String, String> initParams) {
        // we probably shouldn't register anything inside this class, it should be done by some ResourceConfig manager
        if (initParams.containsKey(PARAM_EXTENSION_FILTER_EXCLUDES)) {
            final String[] excludes = StringUtils.split(initParams.get(PARAM_EXTENSION_FILTER_EXCLUDES));
            resourceConfig.register(new ExtensionJerseyFilter(asList(excludes)));
        } else {
            resourceConfig.register(new ExtensionJerseyFilter());
        }
    }

    private void initServletContainer(FilterConfig config) throws ServletException {
        ClassLoader currentThreadClassLoader = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(chainingClassLoader);
        try {
            SLF4J_BRIDGE.install();
            servletContainer.init(config);
        } finally {
            Thread.currentThread().setContextClassLoader(currentThreadClassLoader);
        }
    }

    private void destroyServletContainer() {
        ClassLoader currentThreadClassLoader = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(chainingClassLoader);
        try {
            servletContainer.destroy();
            SLF4J_BRIDGE.uninstall();
        } finally {
            Thread.currentThread().setContextClassLoader(currentThreadClassLoader);
        }
    }

    private void initChainingClassLoader() {
        chainingClassLoader = new ChainingClassLoader(
                RestModuleDescriptor.class.getClassLoader(),
                Thread.currentThread().getContextClassLoader());
    }
}
