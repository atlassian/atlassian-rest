package com.atlassian.plugins.rest.v2.exception.entity;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Variant;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

import com.atlassian.plugins.rest.api.model.Status;

import static com.atlassian.plugins.rest.v2.exception.constant.UncaughtExceptionEntityConstants.ATLASSIAN_REST_RESPONSE_STACKTRACES;
import static com.atlassian.plugins.rest.v2.exception.constant.UncaughtExceptionEntityConstants.ATLASSIAN_REST_RESPONSE_STACKTRACES_ABSENT;
import static com.atlassian.plugins.rest.v2.exception.constant.UncaughtExceptionEntityConstants.ATLASSIAN_REST_RESPONSE_STACKTRACES_PRESENT;
import static com.atlassian.plugins.rest.v2.exception.constant.UncaughtExceptionEntityConstants.NO_STACKTRACE_MESSAGE;

/**
 * An analog of {@link Status} specifically for uncaught exceptions. The stack trace is
 * included.
 */
@XmlRootElement(name = "status")
public class UncaughtExceptionEntity {
    private static final Integer INTERNAL_SERVER_ERROR_CODE = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
    /**
     * This is a redundant duplicate of the HTTP error code, but it may be clearer in
     * a browser.
     */
    @XmlElement(name = "status-code")
    private final Integer code;

    /**
     * A humane readable message for the given status.
     */
    @XmlElement
    private final String message;

    @XmlElement(name = "stack-trace")
    private final String stackTrace;

    /**
     * JAXB requires a default constructor.
     */
    public UncaughtExceptionEntity() {
        this.code = null;
        this.message = null;
        this.stackTrace = null;
    }

    public UncaughtExceptionEntity(Throwable throwable, String errorId) {
        this.code = INTERNAL_SERVER_ERROR_CODE;
        this.message = throwable.getMessage();
        boolean shouldSeeStacktrace = System.getProperty(
                        ATLASSIAN_REST_RESPONSE_STACKTRACES, ATLASSIAN_REST_RESPONSE_STACKTRACES_ABSENT)
                .equals(ATLASSIAN_REST_RESPONSE_STACKTRACES_PRESENT);
        if (shouldSeeStacktrace) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            throwable.printStackTrace(printWriter);
            this.stackTrace = stringWriter.toString();
        } else {
            this.stackTrace = NO_STACKTRACE_MESSAGE + errorId;
        }
    }

    public UncaughtExceptionEntity(Integer code, Throwable throwable) {
        this.code = code;
        this.message = throwable.getMessage();
        this.stackTrace = null;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    private static final MediaType TEXT_PLAIN_UTF8_TYPE = MediaType.valueOf("text/plain; charset=utf-8");

    /**
     * These are the media types that an UncaughtExceptionEntity can be represented as.
     */
    private static final List<Variant> POSSIBLE_VARIANTS = Variant.mediaTypes(
                    MediaType.APPLICATION_XML_TYPE, MediaType.APPLICATION_JSON_TYPE, MediaType.TEXT_PLAIN_TYPE)
            .add()
            .build();

    public static MediaType variantFor(Request request) {
        Variant variant = request.selectVariant(POSSIBLE_VARIANTS);
        if (variant == null) {
            variant = POSSIBLE_VARIANTS.get(0);
        }

        /* If we include the charset in the variant then it gets prioritised as a default. Select
         * it as text/plain and then switch in the variant with the charset here.
         */
        MediaType mediaType = variant.getMediaType();

        if (mediaType.equals(MediaType.TEXT_PLAIN_TYPE)) {
            return TEXT_PLAIN_UTF8_TYPE;
        } else {
            return mediaType;
        }
    }

    @Override
    public String toString() {
        return "code=" + code + ", message='" + message + '\'' + ", stackTrace='" + stackTrace;
    }
}
