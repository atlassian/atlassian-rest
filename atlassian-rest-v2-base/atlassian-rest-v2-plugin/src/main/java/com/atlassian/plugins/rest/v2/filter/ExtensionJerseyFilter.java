package com.atlassian.plugins.rest.v2.filter;

import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.PreMatching;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.UriBuilder;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import com.google.common.collect.ImmutableMap;

import static jakarta.ws.rs.core.MediaType.APPLICATION_ATOM_XML;
import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;
import static jakarta.ws.rs.core.MediaType.APPLICATION_XML;
import static jakarta.ws.rs.core.MediaType.TEXT_HTML;
import static jakarta.ws.rs.core.MediaType.TEXT_PLAIN;

/**
 * A filter to handle URI with extensions. It will set the correct accept header for each extension and remove the
 * extension from the URI to allow for normal processing of the request.
 * <p>
 * Currently supported extension and their matching headers are defined in
 * {@link #EXTENSION_TO_ACCEPT_HEADER the extension to header mapping}.
 * <p>
 * One can exclude URIs from being processed by this filter. Simply create the filter with a collection of
 * {@link Pattern patterns} to be excluded.
 * <p>
 * <strong>Example:</strong> URI {@code http://localhost:8080/app/rest/my/resource.json} would be automatically
 * mapped to URI {@code http://localhost:8080/app/rest/my/resource} with its {@code accept} header set to
 * {@code application/json}
 */
@PreMatching
public class ExtensionJerseyFilter implements ContainerRequestFilter {
    private static final String DOT = ".";

    private final Collection<Pattern> pathExcludePatterns;

    public ExtensionJerseyFilter() {
        pathExcludePatterns = new LinkedList<>();
    }

    public ExtensionJerseyFilter(Collection<String> pathExcludePatterns) {
        Validate.notNull(pathExcludePatterns, "The object must not be null");
        this.pathExcludePatterns = compilePatterns(pathExcludePatterns);
    }

    private final Map<String, String> EXTENSION_TO_ACCEPT_HEADER = new ImmutableMap.Builder<String, String>()
            .put("txt", TEXT_PLAIN)
            .put("htm", TEXT_HTML)
            .put("html", TEXT_HTML)
            .put("json", APPLICATION_JSON)
            .put("xml", APPLICATION_XML)
            .put("atom", APPLICATION_ATOM_XML)
            .build();

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        final String absoluteUri = requestContext.getUriInfo().getAbsolutePath().toString();
        final String extension = StringUtils.substringAfterLast(absoluteUri, DOT);
        if (shouldFilter(
                "/"
                        + StringUtils.difference(
                                requestContext.getUriInfo().getBaseUri().toString(), absoluteUri),
                extension)) {
            requestContext.getHeaders().putSingle(HttpHeaders.ACCEPT, EXTENSION_TO_ACCEPT_HEADER.get(extension));
            final String absoluteUriWithoutExtension = StringUtils.substringBeforeLast(absoluteUri, DOT);
            requestContext.setRequestUri(getRequestUri(
                    absoluteUriWithoutExtension, requestContext.getUriInfo().getQueryParameters()));
        }
    }

    private boolean shouldFilter(String restPath, String extension) {
        for (Pattern pattern : pathExcludePatterns) {
            if (pattern.matcher(restPath).matches()) {
                return false;
            }
        }
        return EXTENSION_TO_ACCEPT_HEADER.containsKey(extension);
    }

    private URI getRequestUri(String absoluteUriWithoutExtension, Map<String, List<String>> queryParams) {
        final UriBuilder requestUriBuilder = UriBuilder.fromUri(absoluteUriWithoutExtension);
        for (Map.Entry<String, List<String>> queryParamEntry : queryParams.entrySet()) {
            for (String value : queryParamEntry.getValue()) {
                requestUriBuilder.queryParam(queryParamEntry.getKey(), value);
            }
        }
        return requestUriBuilder.build();
    }

    private Collection<Pattern> compilePatterns(Collection<String> pathExcludePatterns) {
        final Collection<Pattern> patterns = new LinkedList<>();
        for (String pattern : pathExcludePatterns) {
            patterns.add(Pattern.compile(pattern));
        }
        return patterns;
    }
}
