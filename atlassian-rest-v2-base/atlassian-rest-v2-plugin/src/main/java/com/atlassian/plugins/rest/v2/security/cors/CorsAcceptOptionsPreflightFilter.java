package com.atlassian.plugins.rest.v2.security.cors;

import java.io.IOException;
import jakarta.annotation.Priority;
import jakarta.ws.rs.HttpMethod;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.PreMatching;
import jakarta.ws.rs.ext.Provider;

import com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders;

import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.CORS_PREFLIGHT_REQUESTED;
import static com.atlassian.plugins.rest.v2.security.cors.CorsResourceFilter.extractOrigin;

/**
 * This is a filter to force Jersey to handle OPTIONS when part of a preflight cors check.
 *
 * @since 2.6
 */
@Priority(Priorities.HEADER_DECORATOR)
@PreMatching
@Provider
public class CorsAcceptOptionsPreflightFilter implements ContainerRequestFilter {
    @Override
    public void filter(ContainerRequestContext request) throws IOException {
        if (request.getMethod().equals(HttpMethod.OPTIONS)) {
            String origin = extractOrigin(request);
            String targetMethod = request.getHeaderString(CorsHeaders.ACCESS_CONTROL_REQUEST_METHOD.value());
            if (targetMethod != null && origin != null) {
                request.setMethod(targetMethod);
                request.setProperty(CORS_PREFLIGHT_REQUESTED, "true");
            }
        }
    }
}
