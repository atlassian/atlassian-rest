package com.atlassian.plugins.rest.v2.util.urlbuilder;

import java.lang.reflect.Method;
import java.net.URI;

class ResourcePathHandler extends ResourceInvokable {

    private URI uri;

    public ResourcePathHandler(Class<?> resourceClass, URI baseUri) {
        super(resourceClass, baseUri);
    }

    public Object invoke(Object o, Method method, Object[] args) {
        uri = getURI(method, args);

        return dummyReturn(method.getReturnType());
    }

    public URI getUri() {
        return uri;
    }

    // returning `null` will throw `NullPointerException` if returning type is primitive
    private Object dummyReturn(Class<?> clazz) {
        if (clazz.equals(boolean.class)) {
            return false;
        } else if (clazz.equals(char.class)) {
            return (char) 0;
        } else if (clazz.equals(byte.class)) {
            return (byte) 0;
        } else if (clazz.equals(short.class)) {
            return (short) 0;
        } else if (clazz.equals(int.class)) {
            return 0;
        } else if (clazz.equals(long.class)) {
            return (long) 0;
        } else if (clazz.equals(float.class)) {
            return (float) 0.0;
        } else if (clazz.equals(double.class)) {
            return 0.0;
        } else {
            return null;
        }
    }
}
