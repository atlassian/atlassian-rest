package com.atlassian.plugins.rest.v2.scanner;

public class AnnotatedScannerException extends RuntimeException {
    public AnnotatedScannerException(String message) {
        super(message);
    }

    public AnnotatedScannerException(String message, Throwable cause) {
        super(message, cause);
    }
}
