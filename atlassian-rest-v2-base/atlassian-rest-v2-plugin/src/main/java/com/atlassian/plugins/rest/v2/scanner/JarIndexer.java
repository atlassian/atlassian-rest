package com.atlassian.plugins.rest.v2.scanner;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.commons.io.FileUtils;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.Attribute;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.osgi.framework.Bundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Opens JarFile jar and searches through packages defined in packageNames for class or jar files.
 * <p>Once a class file is found, it calls method to analyze the file for annotations</p>
 * <p>Once a jar file is found and indexBundleJars flag is true, it performs deep scan which runs JarIndexer class
 * recursively
 * through the found jar</p>
 * <p>when indexBundleJars is false, deep scan is disabled</p>
 */
public class JarIndexer {

    public static final String THRESHOLD_ENTRIES_PROPERTY = "rest.annotation.scanner.jar.indexer.threshold.limit";
    private static final int THRESHOLD_ENTRIES =
            Integer.parseInt(System.getProperty(THRESHOLD_ENTRIES_PROPERTY, "50000"));
    private static final Logger LOGGER = LoggerFactory.getLogger(JarIndexer.class);

    private final JarFile jar;
    private final Set<String> packageNames;
    private final boolean indexBundledJars;
    private final Set<String> annotations;
    private final Bundle bundle;

    JarIndexer(
            JarFile jar, Set<String> packageNames, boolean indexBundledJars, Set<String> annotations, Bundle bundle) {
        this.jar = jar;
        this.packageNames = packageNames;
        this.indexBundledJars = indexBundledJars;
        this.annotations = annotations;
        this.bundle = bundle;
    }

    public Set<Class<?>> scanJar() {
        final Set<Class<?>> classes = new HashSet<>();

        int totalEntryArchive = 0;

        for (Enumeration<JarEntry> entries = jar.entries(); entries.hasMoreElements(); ) {
            totalEntryArchive++;
            if (!isJarEntryThresholdSafe(totalEntryArchive)) {
                LOGGER.warn(
                        "Archive {} exceeds a threshold of {} entries, which can lead to inodes exhaustion of the system",
                        jar.getName(),
                        THRESHOLD_ENTRIES);
                totalEntryArchive = Integer.MIN_VALUE;
            }
            final JarEntry jarEntry = entries.nextElement();
            if (isClassFile(jarEntry) && shouldBeScanned(jarEntry)) {
                classes.add(analyzeClassFile(jar, jarEntry));
            } else if (isJarFile(jarEntry) && indexBundledJars) {
                classes.addAll(scanDeeply(jarEntry));
            }
        }

        classes.remove(null);
        return classes;
    }

    private Class<?> analyzeClassFile(JarFile jarFile, JarEntry entry) {
        final AnnotatedClassVisitor visitor = new AnnotatedClassVisitor(annotations);
        getClassReader(jarFile, entry).accept(visitor, 0);
        return visitor.hasAnnotation() ? getClassForName(visitor.className) : null;
    }

    private Class<?> getClassForName(String className) {
        try {
            return bundle.loadClass(className.replace("/", "."));
        } catch (ClassNotFoundException ex) {
            throw new JarIndexerException(
                    "A class file of the class name, " + className + " is identified but the class could not be loaded",
                    ex);
        }
    }

    private ClassReader getClassReader(JarFile jarFile, JarEntry entry) {
        try (InputStream is = jarFile.getInputStream(entry)) {
            return new ClassReader(is);
        } catch (IOException | IllegalArgumentException ex) {
            throw new JarIndexerException("Error accessing input stream of the jar file " + jarFile.getName(), ex);
        }
    }

    private Set<Class<?>> scanDeeply(JarEntry jarEntry) {
        try (InputStream inputStream = jar.getInputStream(jarEntry)) {
            File jF = extractIsToTempFile(jarEntry.getName().replace(".jar", ""), inputStream);
            try (JarFile jarFile = new JarFile(jF)) {
                return new JarIndexer(jarFile, packageNames, false, annotations, bundle).scanJar();
            } finally {
                if (!jF.delete()) {
                    LOGGER.warn(
                            "An error occurred while deleting a temporary jar, project target might get overflown with temporary jar files");
                }
            }
        } catch (IOException e) {
            throw new JarIndexerException("An error has occurred while scanning a jar file", e);
        }
    }

    private File extractIsToTempFile(String fileName, InputStream fileInputStream) {

        try {
            File f = File.createTempFile(fileName, ".jar");
            FileUtils.copyInputStreamToFile(fileInputStream, f);
            return f;

        } catch (IOException e) {
            throw new JarIndexerException("An error has occurred while extracting from jar input stream.", e);
        }
    }

    private boolean isClassFile(JarEntry jarEntry) {
        return !jarEntry.isDirectory()
                && jarEntry.getName().endsWith(".class")
                && !jarEntry.getName().endsWith("module-info.class");
    }

    private boolean isJarFile(JarEntry jarEntry) {
        return !jarEntry.isDirectory() && jarEntry.getName().endsWith(".jar");
    }

    private boolean shouldBeScanned(JarEntry jarEntry) {

        return packageNames.isEmpty()
                || packageNames.stream()
                        .anyMatch(packageName -> jarEntry.getName().startsWith(packageName));
    }

    private boolean isJarEntryThresholdSafe(int totalEntryArchive) {
        return totalEntryArchive <= THRESHOLD_ENTRIES;
    }

    private static final class AnnotatedClassVisitor extends ClassVisitor {

        private final Set<String> annotations;

        /**
         * The name of the visited class.
         */
        private String className;

        /**
         * True if the class has the correct scope
         */
        private boolean isScoped;
        /**
         * True if the class has the correct declared annotations
         */
        private boolean isAnnotated;

        public AnnotatedClassVisitor(Set<String> annotations) {
            super(Opcodes.ASM9);
            this.annotations = annotations;
        }

        @Override
        public void visit(
                int version, int access, String name, String signature, String superName, String[] interfaces) {
            className = name;
            isScoped = (access & Opcodes.ACC_PUBLIC) != 0;
            isAnnotated = false;
        }

        @Override
        public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
            isAnnotated |= annotations.contains(desc);
            return null;
        }

        @Override
        public void visitInnerClass(String name, String outerName, String innerName, int access) {
            // If the name of the class that was visited is equal to the name of this visited inner class then
            // this access field needs to be used for checking the scope of the inner class
            if (className.equals(name)) {
                isScoped = (access & Opcodes.ACC_PUBLIC) != 0;
                // Inner classes need to be statically scoped
                isScoped &= (access & Opcodes.ACC_STATIC) == Opcodes.ACC_STATIC;
            }
        }

        boolean hasAnnotation() {
            return isScoped && isAnnotated;
        }

        @Override
        public void visitEnd() {
            // Do nothing
        }

        @Override
        public void visitOuterClass(String string, String string0, String string1) {
            // Do nothing
        }

        @Override
        public FieldVisitor visitField(int i, String string, String string0, String string1, Object object) {
            return null;
        }

        @Override
        public void visitSource(String string, String string0) {
            // Do nothing
        }

        @Override
        public void visitAttribute(Attribute attribute) {
            // Do nothing
        }

        @Override
        public MethodVisitor visitMethod(int i, String string, String string0, String string1, String[] string2) {
            if (isAnnotated) {
                // the class has already been found annotated, no need to visit methods
                return null;
            }

            return new MethodVisitor(Opcodes.ASM5) {
                @Override
                public AnnotationVisitor visitAnnotationDefault() {
                    return null;
                }

                @Override
                public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
                    isAnnotated |= annotations.contains(desc);
                    return null;
                }

                @Override
                public AnnotationVisitor visitParameterAnnotation(int parameter, String desc, boolean visible) {
                    return null;
                }

                @Override
                public void visitAttribute(Attribute attr) {
                    // Do nothing
                }

                @Override
                public void visitCode() {
                    // Do nothing
                }

                @Override
                public void visitFrame(int type, int nLocal, Object[] local, int nStack, Object[] stack) {
                    // Do nothing
                }

                @Override
                public void visitInsn(int opcode) {
                    // Do nothing
                }

                @Override
                public void visitIntInsn(int opcode, int operand) {
                    // Do nothing
                }

                @Override
                public void visitVarInsn(int opcode, int varIndex) {
                    // Do nothing
                }

                @Override
                public void visitTypeInsn(int opcode, String type) {
                    // Do nothing
                }

                @Override
                public void visitFieldInsn(int opcode, String owner, String name, String desc) {
                    // Do nothing
                }

                @Override
                public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean itf) {
                    // Do nothing
                }

                @Override
                public void visitJumpInsn(int opcode, Label label) {
                    // Do nothing
                }

                @Override
                public void visitLabel(Label label) {
                    // Do nothing
                }

                @Override
                public void visitLdcInsn(Object cst) {
                    // Do nothing
                }

                @Override
                public void visitIincInsn(int varIndex, int increment) {
                    // Do nothing
                }

                @Override
                public void visitTableSwitchInsn(int min, int max, Label dflt, Label[] labels) {
                    // Do nothing
                }

                @Override
                public void visitLookupSwitchInsn(Label dflt, int[] keys, Label[] labels) {
                    // Do nothing
                }

                @Override
                public void visitMultiANewArrayInsn(String desc, int dims) {
                    // Do nothing
                }

                @Override
                public void visitTryCatchBlock(Label start, Label end, Label handler, String type) {
                    // Do nothing
                }

                @Override
                public void visitLocalVariable(
                        String name, String desc, String signature, Label start, Label end, int index) {
                    // Do nothing
                }

                @Override
                public void visitLineNumber(int line, Label start) {
                    // Do nothing
                }

                @Override
                public void visitMaxs(int maxStack, int maxLocals) {
                    // Do nothing
                }

                @Override
                public void visitEnd() {
                    // Do nothing
                }
            };
        }
    }
}
