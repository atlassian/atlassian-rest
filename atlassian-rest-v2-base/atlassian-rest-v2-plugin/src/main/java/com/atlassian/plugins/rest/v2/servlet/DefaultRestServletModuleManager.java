package com.atlassian.plugins.rest.v2.servlet;

import java.util.Comparator;
import java.util.Optional;
import java.util.SortedSet;
import jakarta.servlet.DispatcherType;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.Multimaps;
import com.google.common.collect.SortedSetMultimap;
import com.google.common.collect.TreeMultimap;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.servlet.DefaultServletModuleManager;
import com.atlassian.plugin.servlet.ServletModuleManager;
import com.atlassian.plugin.servlet.descriptors.ServletFilterModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptor;
import com.atlassian.plugin.servlet.filter.FilterDispatcherCondition;
import com.atlassian.plugin.servlet.filter.FilterLocation;
import com.atlassian.plugin.servlet.util.DefaultPathMapper;
import com.atlassian.plugin.servlet.util.PathMapper;
import com.atlassian.plugins.rest.v2.RestApiContext;
import com.atlassian.plugins.rest.v2.descriptor.RestServletFilterModuleDescriptor;

import static com.google.common.collect.Ordering.natural;

/**
 * Default implementation of the {@link RestServletModuleManager} interface.
 * This class manages RestServlet modules and delegates other servlet management tasks
 * to an instance of {@link ServletModuleManager}.
 *
 * <p>This class maintains a multimap of filter module descriptors, where the key is
 * the "api path" of the REST module descriptor. Each descriptor is associated with
 * a sorted set of {@link RestServletFilterModuleDescriptor}s, sorted by version.
 *
 * @see RestServletModuleManager
 * @see ServletModuleManager
 */
public class DefaultRestServletModuleManager implements RestServletModuleManager {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultRestServletModuleManager.class);

    private static final RestServletFilterModuleDescriptorComparator VALUE_COMPARATOR =
            new RestServletFilterModuleDescriptorComparator();
    public static final String REST_PATH = "/rest";

    /**
     * Multimap of filter module descriptors, the key is the "api path" of the REST module descriptor.
     */
    private final SortedSetMultimap<String, RestServletFilterModuleDescriptor> filterModuleDescriptors =
            Multimaps.synchronizedSortedSetMultimap(TreeMultimap.create(natural(), VALUE_COMPARATOR));

    private final ServletModuleManager delegateModuleManager;
    private final PathMapper filterPathMapper;
    private final String path;

    public DefaultRestServletModuleManager(PluginEventManager pluginEventManager) {
        this.filterPathMapper = new DefaultPathMapper();
        this.delegateModuleManager =
                new DefaultServletModuleManager(pluginEventManager, new DefaultPathMapper(), filterPathMapper);
        this.path = REST_PATH;
    }

    DefaultRestServletModuleManager(ServletModuleManager delegate, PathMapper filterPathMapper) {
        this.filterPathMapper = filterPathMapper;
        this.delegateModuleManager = delegate;
        this.path = REST_PATH;
    }

    @Override
    public void addServlet(Plugin plugin, String servletName, String className) {
        delegateModuleManager.addServlet(plugin, servletName, className);
    }

    @Override
    public void addServlet(Plugin plugin, String servletName, HttpServlet servlet, ServletContext servletContext) {
        delegateModuleManager.addServlet(plugin, servletName, servlet, servletContext);
    }

    @Override
    public void addServletModule(ServletModuleDescriptor descriptor) {
        delegateModuleManager.addServletModule(descriptor);
    }

    @Override
    public HttpServlet getServlet(String path, ServletConfig servletConfig) throws ServletException {
        return delegateModuleManager.getServlet(path, servletConfig);
    }

    @Override
    public void removeServletModule(ServletModuleDescriptor descriptor) {
        delegateModuleManager.removeServletModule(descriptor);
    }

    /**
     * Adds a new ServletFilterModuleDescriptor to the delegateModuleManager.
     * Before adding, it checks if the paths in the descriptor are already in use.The paths in a descriptor is
     * already a path+version combination.
     * If a path+version combination is already in use by another filter module descriptor, the filter module will be
     * disabled.
     *
     * <p>If the descriptor is an instance of {@link RestServletFilterModuleDescriptor},
     * it retrieves the latest version of the filter module descriptor for the base path
     * using {@link #getRestServletFilterModuleDescriptorForLatest(String)}.
     * If the new descriptor's version is greater than the existing one,
     * the method updates the filterPathMapper and filterModuleDescriptors with the new descriptor.
     *
     * <p>The version comparison is done using
     * {@link RestServletFilterModuleDescriptorComparator#compare(RestServletFilterModuleDescriptor, RestServletFilterModuleDescriptor)}.
     *
     * <p>The path pattern for the new descriptor is created using {@link #getLatestPathPattern(String)}.
     *
     * <p>Finally, the method delegates the adding of the filter module to the delegateModuleManager,
     * which will handle the filter module regardless of its type.
     *
     * @param descriptor the filter module descriptor to be added.
     *                   If it is an instance of RestServletFilterModuleDescriptor,
     *                   it is added to filterModuleDescriptors and its path information
     *                   is used to update filterPathMapper.
     */
    @Override
    public void addFilterModule(ServletFilterModuleDescriptor descriptor) {
        if (descriptor instanceof RestServletFilterModuleDescriptor) {
            Optional<RestServletFilterModuleDescriptor> optionalDescriptorPath = findClashingDescriptorPath(descriptor);

            if (optionalDescriptorPath.isPresent()) {
                handleDescriptorPathClash(descriptor, optionalDescriptorPath.get());
                return;
            }

            updateRestFilterModuleDescriptor((RestServletFilterModuleDescriptor) descriptor);
        }
        delegateModuleManager.addFilterModule(descriptor);
    }

    private Optional<RestServletFilterModuleDescriptor> findClashingDescriptorPath(
            ServletFilterModuleDescriptor descriptor) {
        if (!(descriptor instanceof RestServletFilterModuleDescriptor)) {
            return Optional.empty();
        }

        RestServletFilterModuleDescriptor restDescriptor = (RestServletFilterModuleDescriptor) descriptor;
        return filterModuleDescriptors.get(restDescriptor.getBasePath()).stream()
                .filter(filterModuleDescriptor ->
                        filterModuleDescriptor.getPaths().stream().anyMatch(restDescriptor.getPaths()::contains))
                .filter(filterModuleDescriptor ->
                        !filterModuleDescriptor.getCompleteKey().equals(restDescriptor.getCompleteKey()))
                .findFirst();
    }

    private void handleDescriptorPathClash(
            ServletFilterModuleDescriptor descriptor,
            RestServletFilterModuleDescriptor existingRestServletFilterModuleDescriptor) {
        LOG.warn(
                "A clash exists for the path+version combination '{}'. "
                        + "The same combination is defined in another filter module descriptor: '{}'. "
                        + "Disabling the filter module '{}.",
                descriptor.getPaths().get(0),
                existingRestServletFilterModuleDescriptor.getCompleteKey(),
                descriptor.getCompleteKey());
        descriptor.disabled();
    }

    private void updateRestFilterModuleDescriptor(RestServletFilterModuleDescriptor descriptor) {
        final RestServletFilterModuleDescriptor latest =
                getRestServletFilterModuleDescriptorForLatest(descriptor.getBasePath());
        if (VALUE_COMPARATOR.compare(latest, descriptor) < 0) {
            updateLatestModuleDescriptor(latest);
            filterPathMapper.put(descriptor.getCompleteKey(), getLatestPathPattern(descriptor.getBasePath()));
        }
        filterModuleDescriptors.put(descriptor.getBasePath(), descriptor);
    }

    private void updateLatestModuleDescriptor(RestServletFilterModuleDescriptor latest) {
        if (latest != null) {
            filterPathMapper.put(latest.getCompleteKey(), null);
            latest.getPaths().forEach(p -> filterPathMapper.put(latest.getCompleteKey(), p));
        }
    }

    private RestServletFilterModuleDescriptor getRestServletFilterModuleDescriptorForLatest(String path) {
        if (path == null) {
            return null;
        }

        final SortedSet<RestServletFilterModuleDescriptor> moduleDescriptors = filterModuleDescriptors.get(path);
        return moduleDescriptors.isEmpty() ? null : moduleDescriptors.last();
    }

    /**
     * @deprecated since 3.5.0. Use {@link #getFilters(FilterLocation, String, FilterConfig, DispatcherType)} instead
     */
    @Deprecated
    @Override
    public Iterable<Filter> getFilters(
            FilterLocation location,
            String pathInfo,
            FilterConfig filterConfig,
            FilterDispatcherCondition filterDispatcherCondition)
            throws ServletException {
        return delegateModuleManager.getFilters(
                location, StringUtils.removeStart(pathInfo, path), filterConfig, filterDispatcherCondition);
    }

    @Override
    public Iterable<Filter> getFilters(
            FilterLocation location, String pathInfo, FilterConfig filterConfig, DispatcherType dispatcherType) {
        return delegateModuleManager.getFilters(
                location, StringUtils.removeStart(pathInfo, path), filterConfig, dispatcherType);
    }

    @Override
    public void removeFilterModule(ServletFilterModuleDescriptor descriptor) {
        if (descriptor instanceof RestServletFilterModuleDescriptor) {
            final RestServletFilterModuleDescriptor restServletFilterModuleDescriptor =
                    (RestServletFilterModuleDescriptor) descriptor;

            // check if it was the latest, before removing from the MultiMap
            RestServletFilterModuleDescriptor latest =
                    getRestServletFilterModuleDescriptorForLatest(restServletFilterModuleDescriptor.getBasePath());
            filterModuleDescriptors.get(restServletFilterModuleDescriptor.getBasePath()).stream()
                    .filter(filterModuleDescriptor -> filterModuleDescriptor
                            .getCompleteKey()
                            .equals(restServletFilterModuleDescriptor.getCompleteKey()))
                    .findFirst()
                    .ifPresent(servletFilterModuleDescriptor -> filterModuleDescriptors.remove(
                            servletFilterModuleDescriptor.getBasePath(), servletFilterModuleDescriptor));

            if (latest != null && latest.getCompleteKey().equals(descriptor.getCompleteKey())) {
                // latest has changed as we have removed an item from the multimap
                latest = getRestServletFilterModuleDescriptorForLatest(restServletFilterModuleDescriptor.getBasePath());
                if (latest != null) {
                    filterPathMapper.put(latest.getCompleteKey(), getLatestPathPattern(latest.getBasePath()));
                }
            }
        }

        // remaining mapping of the descriptor will be removed by this call.
        delegateModuleManager.removeFilterModule(descriptor);
    }

    String getLatestPathPattern(String basePath) {
        return basePath + RestApiContext.LATEST + RestApiContext.ANY_PATH_PATTERN;
    }

    private static final class RestServletFilterModuleDescriptorComparator
            implements Comparator<RestServletFilterModuleDescriptor> {
        public int compare(
                RestServletFilterModuleDescriptor descriptor1, RestServletFilterModuleDescriptor descriptor2) {
            if (descriptor1 == null) {
                return -1;
            }
            if (descriptor2 == null) {
                return +1;
            }
            return descriptor1.getVersion().compareTo(descriptor2.getVersion());
        }
    }
}
