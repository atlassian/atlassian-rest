package com.atlassian.plugins.rest.v2.filter;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugins.rest.api.deprecation.annotation.DeprecatedEndpoint;

@Provider
public class DeprecationFilter implements ContainerResponseFilter {

    /**
     * Logger for this class.
     */
    private static final Logger log = LoggerFactory.getLogger(DeprecationFilter.class);

    private static final Map<String, String> MIME_TYPE_MAP = new HashMap<>();

    /**
     * @DeprecatedEndpoint has option to add link for the providing information about deprecation, here useful mimetypes are added to support them
     */
    static {
        MIME_TYPE_MAP.put("html", "text/html");
        MIME_TYPE_MAP.put("htm", "text/html");
        MIME_TYPE_MAP.put("md", "text/html");
        MIME_TYPE_MAP.put("pdf", "application/pdf");
        MIME_TYPE_MAP.put("txt", "text/plain");
        MIME_TYPE_MAP.put("xml", "application/xml");
        MIME_TYPE_MAP.put("json", "application/json");
    }

    @Context
    private ResourceInfo resourceInfo;

    private static final String DEFAULT_DEPRECATION_DATE = "1970-01-01T00:00:00Z";

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
            throws IOException {
        DeprecatedEndpoint deprecationData = getDeprecationData();
        if (deprecationData != null) {
            String isoDate = deprecationData.since();
            try {
                Instant instant = Instant.parse(isoDate);
                long timestamp = instant.getEpochSecond();
                responseContext.getHeaders().add("Deprecation", "@" + timestamp);
            } catch (DateTimeParseException e) {
                log.error(
                        "Given timestamp {} is not in correct ISO-8601 format. Hence setting the default platform 7 release date {}",
                        isoDate,
                        DEFAULT_DEPRECATION_DATE);
                responseContext
                        .getHeaders()
                        .add(
                                "Deprecation",
                                "@" + Instant.parse(DEFAULT_DEPRECATION_DATE).getEpochSecond());
            }

            if (!deprecationData.link().isEmpty() && validateLink(deprecationData.link(), deprecationData.linkType())) {
                final String validatedLink = deprecationData.link();
                responseContext
                        .getHeaders()
                        .add(
                                "Link",
                                "<" + validatedLink + ">;" + "rel=\"deprecation\";type=\"" + deprecationData.linkType()
                                        + "\"");
            }
        }
    }

    private boolean validateLink(String link, String linkType) {
        try {
            final URL url = new URL(link);
            String path = url.getPath();
            String mimeType;

            if (path == null || !path.contains(".")) {
                mimeType = "text/html";
            } else {
                String extension = getExtension(path);
                mimeType = MIME_TYPE_MAP.getOrDefault(extension, "text/html");
            }
            return linkType.equals(mimeType);
        } catch (MalformedURLException e) {
            log.error("Malformed URL of link: {} for given linkType: {}", link, linkType);
        }
        return false;
    }

    private String getExtension(String path) {
        int lastDotIndex = path.lastIndexOf('.');
        return path.substring(lastDotIndex + 1).toLowerCase();
    }

    private DeprecatedEndpoint getDeprecationData() {
        final Method resourceMethod = resourceInfo.getResourceMethod();
        if (null != resourceMethod && resourceMethod.isAnnotationPresent(DeprecatedEndpoint.class)) {
            return resourceMethod.getAnnotation(DeprecatedEndpoint.class);
        }

        Class<?> resourceClass = resourceInfo.getResourceClass();
        if (null != resourceClass && resourceClass.isAnnotationPresent(DeprecatedEndpoint.class)) {
            return resourceClass.getAnnotation(DeprecatedEndpoint.class);
        }

        if (null != resourceClass) {
            final Package resourcePackage = resourceInfo.getResourceClass().getPackage();
            if (null != resourcePackage && resourcePackage.isAnnotationPresent(DeprecatedEndpoint.class)) {
                return resourcePackage.getAnnotation(DeprecatedEndpoint.class);
            }
        }
        return null;
    }
}
