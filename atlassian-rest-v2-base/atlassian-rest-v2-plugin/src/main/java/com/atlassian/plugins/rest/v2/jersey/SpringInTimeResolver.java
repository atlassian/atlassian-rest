package com.atlassian.plugins.rest.v2.jersey;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.glassfish.hk2.api.Injectee;
import org.glassfish.hk2.api.JustInTimeInjectionResolver;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;

import com.atlassian.plugin.osgi.factory.OsgiPlugin;

/**
 * Jersey DI mechanism will call {@link #justInTimeResolution(Injectee)} if it cannot find resources to inject in its scope.
 * Missing resource is searched in plugin's local bean registry and registered in Jersey if found.
 * <br>
 * <br>
 * This approach leaves Jersey DI all the heavy stuff (e.g. injecting fields marked with {@link jakarta.ws.rs.core.Context})
 * and gives possibility to inject beans defined in the plugin.
 */
public class SpringInTimeResolver implements JustInTimeInjectionResolver {

    private final ServiceLocator locator;
    private final OsgiPlugin osgiPlugin;

    @Inject
    public SpringInTimeResolver(ServiceLocator locator, OsgiPlugin osgiPlugin) {
        this.locator = locator;
        this.osgiPlugin = osgiPlugin;
    }

    @Override
    public boolean justInTimeResolution(Injectee failedInjectionPoint) {
        Class<?> aClass = getClass(failedInjectionPoint.getRequiredType());
        String name = getName(failedInjectionPoint);

        try {
            Object bean = name != null
                    ? osgiPlugin.getContainerAccessor().getBean(name)
                    : osgiPlugin.getContainerAccessor().getBean(aClass);
            ServiceLocatorUtilities.addOneConstant(locator, bean, name, failedInjectionPoint.getRequiredType());

            return true;
        } catch (NoSuchBeanDefinitionException ignored) {
            // if we cannot find bean, simply return false
            return false;
        }
    }

    private String getName(Injectee injectee) {
        return injectee.getRequiredQualifiers().stream()
                .filter(Named.class::isInstance)
                .map(an -> ((Named) an).value())
                .findFirst()
                .orElse(null);
    }

    private Class<?> getClass(Type type) {
        if (type instanceof Class) {
            return (Class<?>) type;
        }

        if (type instanceof ParameterizedType) {
            ParameterizedType pt = (ParameterizedType) type;

            return (Class<?>) pt.getRawType();
        }

        return null;
    }
}
