package com.atlassian.plugins.rest.v2.exception;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import jakarta.annotation.Priority;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyWriter;
import jakarta.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugins.rest.v2.exception.entity.UncaughtExceptionEntity;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Writes {@link UncaughtExceptionEntity}s out as plain text.
 */
@Priority(Priorities.USER)
@Produces(MediaType.TEXT_PLAIN)
@Provider
public class UncaughtExceptionEntityWriter implements MessageBodyWriter<UncaughtExceptionEntity> {
    private static final Logger log = LoggerFactory.getLogger(UncaughtExceptionEntityWriter.class);

    @Override
    public long getSize(
            UncaughtExceptionEntity t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return -1;
    }

    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return UncaughtExceptionEntity.class.isAssignableFrom(type);
    }

    public void writeTo(
            UncaughtExceptionEntity uncaughtExceptionEntity,
            Class<?> type,
            Type genericType,
            Annotation[] annotations,
            MediaType mediaType,
            MultivaluedMap<String, Object> httpHeaders,
            OutputStream entityStream)
            throws IOException {
        log.debug(
                "Uncaught Exception in REST: {}: {}",
                uncaughtExceptionEntity.getMessage(),
                uncaughtExceptionEntity.getStackTrace());

        String plainText = uncaughtExceptionEntity.toString();
        entityStream.write(plainText.getBytes(UTF_8));
    }
}
