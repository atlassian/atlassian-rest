package com.atlassian.plugins.rest.v2.multipart.exception;

import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;

/**
 * Exception indicating the file count limit was exceeded
 *
 * @since 7.1.5
 */
public class FileCountLimitExceededException extends WebApplicationException {
    private static final int PAYLOAD_TOO_LARGE = 413;

    private static int getStatusCode() {
        return PAYLOAD_TOO_LARGE;
    }

    public FileCountLimitExceededException(String message) {
        super(Response.status(getStatusCode()).entity(message).build());
    }
}
