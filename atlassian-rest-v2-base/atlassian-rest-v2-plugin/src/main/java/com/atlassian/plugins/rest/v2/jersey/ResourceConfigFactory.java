package com.atlassian.plugins.rest.v2.jersey;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.xml.stream.XMLInputFactory;
import jakarta.inject.Singleton;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.ext.Provider;

import org.glassfish.hk2.api.JustInTimeInjectionResolver;
import org.glassfish.hk2.api.PerLookup;
import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.internal.inject.CustomAnnotationLiteral;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.server.spi.internal.ValueParamProvider;
import org.glassfish.jersey.servlet.spi.FilterUrlMappingsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.oauth2.scopes.api.ScopesRequestCache;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugins.rest.api.expand.resolver.EntityExpanderResolver;
import com.atlassian.plugins.rest.api.internal.spi.config.ResourceConfigHostContext;
import com.atlassian.plugins.rest.api.internal.spi.config.impl.DefaultResourceConfigHostContext;
import com.atlassian.plugins.rest.api.multipart.MultipartHandler;
import com.atlassian.plugins.rest.v2.darkfeature.DarkFeatureResourceDynamicFeature;
import com.atlassian.plugins.rest.v2.exception.NotFoundExceptionMapper;
import com.atlassian.plugins.rest.v2.exception.SecurityExceptionMapper;
import com.atlassian.plugins.rest.v2.exception.ThrowableExceptionMapper;
import com.atlassian.plugins.rest.v2.exception.UncaughtExceptionEntityWriter;
import com.atlassian.plugins.rest.v2.expand.ExpandFilter;
import com.atlassian.plugins.rest.v2.expand.resolver.ChainingEntityExpanderResolver;
import com.atlassian.plugins.rest.v2.expand.resolver.IdentityEntityExpanderResolver;
import com.atlassian.plugins.rest.v2.expand.resolver.IndexedEntityExpanderResolver;
import com.atlassian.plugins.rest.v2.expand.resolver.ListEntityExpanderResolver;
import com.atlassian.plugins.rest.v2.expand.resolver.ListWrapperEntityExpanderResolverImpl;
import com.atlassian.plugins.rest.v2.expand.resolver.PluginEntityExpanderResolver;
import com.atlassian.plugins.rest.v2.expand.resolver.SelfExpandingEntityExpanderResolverImpl;
import com.atlassian.plugins.rest.v2.filter.DeprecationFilter;
import com.atlassian.plugins.rest.v2.multipart.jersey.MultipartFormMessageBodyReader;
import com.atlassian.plugins.rest.v2.multipart.jersey.MultipartFormParamValueParamProvider;
import com.atlassian.plugins.rest.v2.multipart.jersey.MultipartHandlerSupplier;
import com.atlassian.plugins.rest.v2.scanner.AnnotatedClassScanner;
import com.atlassian.plugins.rest.v2.security.antisniffing.AntiSniffingResponseFilter;
import com.atlassian.plugins.rest.v2.security.authentication.AuthenticatedResourceFilter;
import com.atlassian.plugins.rest.v2.security.cors.CorsAcceptOptionsPreflightFilter;
import com.atlassian.plugins.rest.v2.security.cors.CorsDefaultService;
import com.atlassian.plugins.rest.v2.security.cors.CorsResourceDynamicFeature;
import com.atlassian.plugins.rest.v2.security.websudo.SalWebSudoResourceContext;
import com.atlassian.plugins.rest.v2.security.websudo.WebSudoResourceFilter;
import com.atlassian.plugins.rest.v2.security.xsrf.XsrfResourceDynamicFeature;
import com.atlassian.plugins.rest.v2.xml.XmlInputFactoryInjectionProvider;
import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.web.context.HttpContext;
import com.atlassian.sal.api.xsrf.XsrfRequestValidator;

public class ResourceConfigFactory {

    private final Logger log = LoggerFactory.getLogger(ResourceConfigFactory.class);

    private final UserManager userManager;
    private final DarkFeatureManager darkFeatureManager;
    private final SalWebSudoResourceContext salWebSudoResourceContext;
    private final HttpContext httpContext;
    private final XsrfRequestValidator xsrfRequestValidator;
    private final CorsDefaultService corsDefaultService;
    private final Collection<ResourceConfigHostContext> resourceConfigHostContexts;
    private final ScopesRequestCache scopesRequestCache;

    public ResourceConfigFactory(
            UserManager userManager,
            DarkFeatureManager darkFeatureManager,
            SalWebSudoResourceContext salWebSudoResourceContext,
            HttpContext httpContext,
            XsrfRequestValidator xsrfRequestValidator,
            CorsDefaultService corsDefaultService,
            Collection<ResourceConfigHostContext> resourceConfigHostContexts,
            ScopesRequestCache scopesRequestCache) {
        this.userManager = userManager;
        this.darkFeatureManager = darkFeatureManager;
        this.salWebSudoResourceContext = salWebSudoResourceContext;
        this.httpContext = httpContext;
        this.xsrfRequestValidator = xsrfRequestValidator;
        this.corsDefaultService = corsDefaultService;
        this.resourceConfigHostContexts = resourceConfigHostContexts;
        this.scopesRequestCache = scopesRequestCache;
    }

    public ResourceConfig createConfig(OsgiPlugin osgiPlugin, Set<String> packages, boolean indexBundledJars) {
        final Set<Class<?>> scan = new AnnotatedClassScanner(
                        osgiPlugin.getBundle(), indexBundledJars, Provider.class, Path.class)
                .scan(packages);
        return new ResourceConfig()
                .property(ServerProperties.RESPONSE_SET_STATUS_OVER_SEND_ERROR, true)
                .registerClasses(scan)
                .registerClasses(RestJacksonJaxbJsonProvider.class)
                .register(new CustomBinder(osgiPlugin))
                .register(new MultipartFormMessageBodyReader())
                .registerInstances(getHostProvidedContexts())
                .registerInstances(
                        new AuthenticatedResourceFilter(userManager, darkFeatureManager, scopesRequestCache),
                        new AntiSniffingResponseFilter(httpContext),
                        new WebSudoResourceFilter(salWebSudoResourceContext),
                        new XsrfResourceDynamicFeature(httpContext, xsrfRequestValidator, corsDefaultService),
                        new DarkFeatureResourceDynamicFeature(darkFeatureManager),
                        new CorsResourceDynamicFeature(corsDefaultService),
                        new CorsAcceptOptionsPreflightFilter(),
                        new ExpandFilter(createEntityExpanderResolver(osgiPlugin)),
                        new CorsAcceptOptionsPreflightFilter(),
                        new DeprecationFilter(),
                        new SecurityExceptionMapper(),
                        new NotFoundExceptionMapper(),
                        new ThrowableExceptionMapper(),
                        new UncaughtExceptionEntityWriter());
    }

    private EntityExpanderResolver createEntityExpanderResolver(ContainerManagedPlugin plugin) {
        List<EntityExpanderResolver> resolvers = Stream.of(
                        new PluginEntityExpanderResolver(plugin),
                        new ListWrapperEntityExpanderResolverImpl(),
                        new SelfExpandingEntityExpanderResolverImpl(),
                        new ListEntityExpanderResolver(),
                        new IndexedEntityExpanderResolver(),
                        new IdentityEntityExpanderResolver())
                .collect(Collectors.toList());
        return new ChainingEntityExpanderResolver(resolvers);
    }

    private Set<Object> getHostProvidedContexts() {
        List<ResourceConfigHostContext> nonDefaultContexts = resourceConfigHostContexts.stream()
                .filter(context -> !(context instanceof DefaultResourceConfigHostContext))
                .collect(Collectors.toList());

        if (nonDefaultContexts.size() > 1) {
            log.warn(
                    "Multiple exports of ResourceConfigHostContext found ({})."
                            + " This scenario isn't typically expected, only one is usually needed."
                            + " If multiple are present, they will be combined in no particular order.",
                    nonDefaultContexts);
        }

        return resourceConfigHostContexts.stream()
                .flatMap(context -> context.hostProvidedRestContext().stream())
                .collect(Collectors.toSet());
    }

    private static class CustomBinder extends AbstractBinder {

        private final OsgiPlugin osgiPlugin;

        public CustomBinder(OsgiPlugin osgiPlugin) {

            this.osgiPlugin = osgiPlugin;
        }

        @Override
        protected void configure() {
            // we need this factory only so Jersey will inject OsgiPlugin into SpringInTimeResolver
            bindFactory(new OsgiPluginFactory(osgiPlugin))
                    .to(OsgiPlugin.class)
                    .proxy(false)
                    .proxyForSameScope(false)
                    .in(Singleton.class);
            bind(SpringInTimeResolver.class).to(JustInTimeInjectionResolver.class);
            // we override the implementation of provider from jersey-media-jaxb
            bindFactory(XmlInputFactoryInjectionProvider.class)
                    .to(XMLInputFactory.class)
                    .in(Singleton.class);

            // we need this to be able to inject MultipartHandler
            // with @Context annotation to resource methods
            bindFactory(MultipartHandlerSupplier.class)
                    .to(MultipartHandler.class)
                    .in(Singleton.class);

            // we need to bind this provider to be able to inject file parts with
            // @MultipartFormParam annotation to resource methods
            bind(MultipartFormParamValueParamProvider.class)
                    .to(ValueParamProvider.class)
                    .in(Singleton.class);

            // we need to override the implementation of FilterUrlMappingsProvider
            bind(AtlassianFilterUrlMappingsProviderImpl.class)
                    .to(FilterUrlMappingsProvider.class)
                    .qualifiedBy(CustomAnnotationLiteral.INSTANCE)
                    .in(PerLookup.class);
        }
    }

    public static class OsgiPluginFactory implements Supplier<OsgiPlugin> {

        private final OsgiPlugin osgiPlugin;

        public OsgiPluginFactory(OsgiPlugin osgiPlugin) {
            this.osgiPlugin = osgiPlugin;
        }

        @Override
        public OsgiPlugin get() {
            return osgiPlugin;
        }
    }
}
