package com.atlassian.plugins.rest.v2.darkfeature;

import java.io.IOException;
import java.lang.reflect.AnnotatedElement;
import java.util.Optional;
import jakarta.annotation.Nonnull;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugins.rest.api.darkfeature.RequiresDarkFeature;
import com.atlassian.sal.api.features.DarkFeatureManager;

import static java.util.Objects.requireNonNull;

import static com.atlassian.plugins.rest.v2.util.ReflectionUtils.getAnnotation;

/**
 * Restricts access to resources based on the state of dark feature flags.
 * <p>
 * Used with the {@link com.atlassian.plugins.rest.api.darkfeature.RequiresDarkFeature} annotation to control access to
 * resources and methods based on the named dark feature keys.
 *
 * @see com.atlassian.plugins.rest.api.darkfeature.RequiresDarkFeature
 */
@Provider
public class DarkFeatureResourceFilter implements ContainerRequestFilter {
    private static final Logger log = LoggerFactory.getLogger(DarkFeatureResourceFilter.class);

    private final DarkFeatureManager darkFeatureManager;
    private final ResourceInfo resourceInfo;

    public DarkFeatureResourceFilter(
            @Nonnull final ResourceInfo resourceInfo, @Nonnull final DarkFeatureManager darkFeatureManager) {
        this.darkFeatureManager = requireNonNull(darkFeatureManager, "darkFeatureManager can't be null");
        this.resourceInfo = requireNonNull(resourceInfo, "method can't be null");
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        log.debug(
                "Applying dark feature filter to request {} {}",
                requestContext.getMethod(),
                requestContext.getUriInfo().getRequestUri());
        if (accessIsAllowed(resourceInfo.getResourceMethod()) && accessIsAllowed(resourceInfo.getResourceClass())) {
            log.debug("Dark feature check OK");
            return;
        }
        log.debug("Dark feature check failed. Refusing access to the resource.");

        throw new NotFoundException(requestContext.getUriInfo().getRequestUri().toString());
    }

    private boolean accessIsAllowed(AnnotatedElement annotatedElement) {
        if (annotatedElement == null) {
            return true;
        }
        final RequiresDarkFeature annotation = getAnnotation(RequiresDarkFeature.class, annotatedElement);
        return annotation == null || allFeaturesAreEnabled(annotation.value());
    }

    private boolean allFeaturesAreEnabled(String[] featureKeys) {
        for (String featureKey : featureKeys) {
            Optional<Boolean> darkFeatureEnabled = darkFeatureManager.isEnabledForCurrentUser(featureKey);
            if (!darkFeatureEnabled.isPresent() || Boolean.FALSE.equals(darkFeatureEnabled.get())) {
                return false;
            }
        }
        return true;
    }
}
