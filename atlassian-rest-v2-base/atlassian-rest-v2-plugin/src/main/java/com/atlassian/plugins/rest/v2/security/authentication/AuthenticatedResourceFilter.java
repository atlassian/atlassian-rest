package com.atlassian.plugins.rest.v2.security.authentication;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;
import jakarta.annotation.Priority;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.annotations.security.ScopesAllowed;
import com.atlassian.oauth2.scopes.api.ScopesRequestCache;
import com.atlassian.plugins.rest.api.security.annotation.AnonymousSiteAccess;
import com.atlassian.plugins.rest.api.security.exception.AuthenticationRequiredException;
import com.atlassian.plugins.rest.api.security.exception.AuthorizationException;
import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;

/**
 * This is a Container Request Filter that checks whether the current client has access to current resource or its method. If the
 * client doesn't  have access then an {@link AuthenticationRequiredException} is thrown.
 *
 * <p>
 * Resources can be marked as not needing authentication by using the
 * {@link AnonymousSiteAccess} annotation
 */
@Priority(Priorities.AUTHENTICATION)
@Provider
public class AuthenticatedResourceFilter implements ContainerRequestFilter {

    private static final Logger log = LoggerFactory.getLogger(AuthenticatedResourceFilter.class);

    public static final String DEFAULT_TO_LICENSED_ACCESS_FEATURE_KEY =
            "atlassian.rest.default.to.licensed.access.disabled";
    public static final String DEFAULT_TO_SYSADMIN_ACCESS_FEATURE_KEY =
            "atlassian.rest.default.to.sysadmin.access.enabled";

    private final UserManager userManager;
    private final DarkFeatureManager darkFeatureManager;
    private final ScopesRequestCache scopesRequestCache;

    @Context
    private ResourceInfo resourceInfo;

    public AuthenticatedResourceFilter(
            UserManager userManager, DarkFeatureManager darkFeatureManager, ScopesRequestCache scopesRequestCache) {
        this.userManager = userManager;
        this.darkFeatureManager = darkFeatureManager;
        this.scopesRequestCache = scopesRequestCache;
    }

    @Override
    public void filter(ContainerRequestContext requestContext) {
        AccessType accessType =
                AccessType.getAccessType(resourceInfo.getResourceClass(), resourceInfo.getResourceMethod());
        UserKey userKey = userManager.getRemoteUserKey();

        if (AccessType.EMPTY == accessType) {
            // DragonFly feature flag - unannotated resources should default to system admin access
            if (darkFeatureManager
                    .isEnabledForAllUsers(DEFAULT_TO_SYSADMIN_ACCESS_FEATURE_KEY)
                    .orElse(false)) {
                if (userManager.isSystemAdmin(userKey)) {
                    return;
                }
            } else if (darkFeatureManager
                    .isEnabledForAllUsers(DEFAULT_TO_LICENSED_ACCESS_FEATURE_KEY)
                    .orElse(false)) {
                return;
            } else if (userManager.isLicensed(userKey)) {
                return;
            }
        } else if (AccessType.SYSTEM_ADMIN_ONLY == accessType) {
            if (userManager.isSystemAdmin(userKey)) {
                return;
            }
        } else if (AccessType.ADMIN_ONLY == accessType) {
            if (userManager.isAdmin(userKey) || userManager.isSystemAdmin(userKey)) {
                return;
            }
        } else if (AccessType.LICENSED_ONLY == accessType) {
            if (userManager.isLicensed(userKey)) {
                return;
            }
        } else if (AccessType.UNLICENSED_SITE_ACCESS == accessType) {
            if (userManager.isLicensed(userKey) || userManager.isLimitedUnlicensedUser(userKey)) {
                return;
            }
        } else if (AccessType.ANONYMOUS_SITE_ACCESS == accessType) {
            if ((userKey == null && userManager.isAnonymousAccessEnabled())
                    || (userManager.isLicensed(userKey) || userManager.isLimitedUnlicensedUser(userKey))) {
                return;
            }
        } else if (AccessType.UNRESTRICTED_ACCESS == accessType) {
            return;
        }

        String[] scopesAllowed = getScopesAllowed(resourceInfo);
        if (scopesAllowed != null && Arrays.stream(scopesAllowed).anyMatch(scopesRequestCache::isScopePermitted)) {
            return;
        }

        log.trace(
                "Resource class={} method={} accessType={} cannot be accessed by the current user={}",
                resourceInfo.getResourceClass(),
                resourceInfo.getResourceMethod(),
                accessType,
                userKey);

        throw mapToException(userKey, accessType);
    }

    public String[] getScopesAllowed(ResourceInfo resourceInfo) {
        Method resourceMethod = resourceInfo.getResourceMethod();
        Optional<ScopesAllowed> scopesAllowedAnnotation = Stream.of(resourceMethod)
                .filter(Objects::nonNull)
                .map(annotatedElement -> annotatedElement.getAnnotation(ScopesAllowed.class))
                .filter(Objects::nonNull)
                .findFirst();
        if (scopesAllowedAnnotation.isPresent()) {
            return scopesAllowedAnnotation.get().requiredScope();
        }

        // Check if the annotation is present but is from a different class loader, log an error
        Optional<Annotation> annotationFromDifferentClassLoader = Stream.of(resourceMethod)
                .filter(Objects::nonNull)
                .flatMap(annotatedElement -> Arrays.stream(annotatedElement.getAnnotations()))
                .filter(annotation -> annotation.annotationType().getName().equals(ScopesAllowed.class.getName()))
                .findFirst();
        if (annotationFromDifferentClassLoader.isPresent()) {
            Class<?> resourceClass = resourceInfo.getResourceClass();
            log.error(
                    "Resource class={} method={} has ScopesAllowed annotation from a different class loader, "
                            + "ignoring it. Please ensure that atlassian-annotations dependency is added with a "
                            + "provided scope in the plugin's pom.xml",
                    resourceClass,
                    resourceMethod);
        }
        return new String[0];
    }

    private static SecurityException mapToException(UserKey userKey, AccessType accessType) {
        if (userKey == null) {
            return new AuthenticationRequiredException();
        }
        if (accessType == AccessType.SYSTEM_ADMIN_ONLY) {
            return new AuthorizationException(
                    "Client must be authenticated as a system administrator to access this resource.");
        }
        if (accessType == AccessType.ADMIN_ONLY) {
            return new AuthorizationException(
                    "Client must be authenticated as an administrator to access this resource.");
        }
        if (accessType == AccessType.LICENSED_ONLY) {
            throw new AuthorizationException(
                    "Client must be authenticated as a licensed user to access this resource.");
        }
        return new AuthorizationException("Client must have sufficient permissions to access this resource.");
    }
}
