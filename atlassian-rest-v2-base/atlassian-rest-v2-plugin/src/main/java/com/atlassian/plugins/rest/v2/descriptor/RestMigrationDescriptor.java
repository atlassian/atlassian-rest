package com.atlassian.plugins.rest.v2.descriptor;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;

/**
 * This module descriptor exists only to avoid warns/error related to the presence of `rest-migration` tag
 */
public class RestMigrationDescriptor extends AbstractModuleDescriptor<Object> {
    public RestMigrationDescriptor(ModuleFactory moduleFactory) {
        super(moduleFactory);
    }

    @Override
    public Object getModule() {
        return null;
    }
}
