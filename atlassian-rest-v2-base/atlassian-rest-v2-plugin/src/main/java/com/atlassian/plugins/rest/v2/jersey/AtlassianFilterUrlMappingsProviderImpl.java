package com.atlassian.plugins.rest.v2.jersey;

import java.util.Collections;
import java.util.List;
import jakarta.servlet.FilterConfig;

import org.glassfish.jersey.servlet.spi.FilterUrlMappingsProvider;

/**
 * This class provides a custom implementation of the {@link FilterUrlMappingsProvider} interface.
 * It is intended to have a custom implementation so that
 * {@code org.glassfish.jersey.servlet.ServletContainer.init(jakarta.servlet.FilterConfig)}
 * will not pick the default {@code FilterUrlMappingsProviderImpl} implementation.
 * <p>
 * Important: This class is bound within {@code ResourceConfigFactory} to ensure it
 * successfully supersedes the default implementation of {@code FilterUrlMappingsProviderImpl}.
 *
 * @since v8.0
 */
public class AtlassianFilterUrlMappingsProviderImpl implements FilterUrlMappingsProvider {
    @Override
    public List<String> getFilterUrlMappings(FilterConfig filterConfig) {
        return Collections.singletonList("*");
    }
}
