package com.atlassian.plugins.rest.v2.security.websudo;

import java.lang.reflect.Method;
import jakarta.annotation.Priority;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.ext.Provider;

import com.atlassian.plugins.rest.api.security.exception.WebSudoRequiredException;
import com.atlassian.sal.api.websudo.WebSudoNotRequired;
import com.atlassian.sal.api.websudo.WebSudoRequired;

/**
 * This is a request filter that checks whether the resource requires or opts out of WebSudo protection.
 * The check is based on annotations ({@link WebSudoRequired} and
 * {@link WebSudoNotRequired}) that can be applied on the method, class and package level.
 * <p/>
 * Annotations on more specific elements override annotations applied to more general elements (in ascending order from
 * specific to general):
 * <ul>
 * <li>Method level</li>
 * <li>Class level</li>
 * <li>Package level</li>
 * </li>
 */
@Priority(Priorities.AUTHENTICATION)
@Provider
public class WebSudoResourceFilter implements ContainerRequestFilter {
    private final SalWebSudoResourceContext salWebSudoResourceContext;

    @Context
    private ResourceInfo resourceInfo;

    public WebSudoResourceFilter(final SalWebSudoResourceContext salWebSudoResourceContext) {
        this.salWebSudoResourceContext = salWebSudoResourceContext;
    }

    @Override
    public void filter(ContainerRequestContext requestContext) {
        if (requiresWebSudo() && salWebSudoResourceContext.shouldEnforceWebSudoProtection()) {
            throw new WebSudoRequiredException("This resource requires WebSudo.");
        }
    }

    private boolean requiresWebSudo() {
        final Method resourceMethod = resourceInfo.getResourceMethod();
        if (null != resourceMethod && resourceMethod.getAnnotation(WebSudoRequired.class) != null) {
            return true;
        }
        if (null != resourceMethod && resourceMethod.getAnnotation(WebSudoNotRequired.class) != null) {
            return false;
        }

        Class<?> resourceClass = resourceInfo.getResourceClass();
        if (resourceClass.isAnnotationPresent(WebSudoRequired.class)) {
            return true;
        }
        if (resourceClass.isAnnotationPresent(WebSudoNotRequired.class)) {
            return false;
        }

        final Package resourcePackage = resourceInfo.getResourceClass().getPackage();
        if (resourcePackage.getAnnotation(WebSudoRequired.class) != null) {
            return true;
        }
        if (resourcePackage.getAnnotation(WebSudoNotRequired.class) != null) {
            return false;
        }
        return false;
    }
}
