package com.atlassian.plugins.rest.v2.descriptor;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import jakarta.annotation.Nonnull;

import org.osgi.framework.ServiceRegistration;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugin.servlet.filter.FilterLocation;
import com.atlassian.plugins.rest.v2.RestApiContext;
import com.atlassian.plugins.rest.v2.jersey.ResourceConfigFactory;
import com.atlassian.plugins.rest.v2.servlet.RestServletModuleManager;
import com.atlassian.plugins.rest.v2.version.ApiVersion;
import com.atlassian.plugins.rest.v2.version.InvalidVersionException;
import com.atlassian.plugins.rest.v2.xml.ComplementingElement;

import static java.util.Objects.requireNonNull;

public class RestModuleDescriptor extends AbstractModuleDescriptor<Object> {
    private final RestServletModuleManager servletModuleManager;
    /**
     * This is the context path of REST APIs.
     * <p>
     * Typically if the application lives at {@code http://localhost:9090/app} and the REST context path is
     * {@code /rest}, then APIs will be available at {@code http://localhost:9090/app/rest}
     */
    private final String restContext;

    private final ResourceConfigFactory resourceConfigFactory;

    private RestApiContext restApiContext;

    private ServiceRegistration<?> serviceRegistration;
    private RestServletFilterModuleDescriptor restServletFilterModuleDescriptor;
    private OsgiPlugin osgiPlugin;
    private Element element;

    public RestModuleDescriptor(
            ModuleFactory moduleFactory,
            RestServletModuleManager servletModuleManager,
            String restContext,
            ResourceConfigFactory resourceConfigFactory) {
        super(moduleFactory);
        this.servletModuleManager = requireNonNull(servletModuleManager, "servletModuleManager can't be null");
        this.restContext = requireNonNull(restContext, "restContext can't be null");
        this.resourceConfigFactory = resourceConfigFactory;
    }

    @Override
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) {
        super.init(plugin, element);

        this.restApiContext = new RestApiContext(
                restContext,
                parsePath(element),
                parseVersion(element),
                parsePackages(element),
                parseIndexBundledPluginsFlag(element));

        osgiPlugin = (OsgiPlugin) plugin;

        this.element = element;
    }

    private Element createCompanionRestServletFilterElement(Element element) {
        Map<String, String> newAttributes = Map.of(
                "location", FilterLocation.BEFORE_DISPATCH.name(), "key", element.attributeValue("key") + "-filter");
        return new ComplementingElement(
                element,
                newAttributes,
                "url-pattern",
                restApiContext.getContextlessPathToVersion() + RestApiContext.ANY_PATH_PATTERN);
    }

    private boolean parseIndexBundledPluginsFlag(Element element) {
        return Boolean.parseBoolean(element.attributeValue("index-bundled-jars"));
    }

    public RestApiContext getRestApiContext() {
        return restApiContext;
    }

    private String parsePath(Element element) {
        return element.attributeValue("path");
    }

    private Set<String> parsePackages(Element rootElement) {
        Set<String> packages = new HashSet<>();
        for (Element pkgElement : rootElement.elements("package")) {
            packages.add(pkgElement.getTextTrim());
        }
        return packages;
    }

    private ApiVersion parseVersion(Element element) {
        try {
            return new ApiVersion(element.attributeValue("version"));
        } catch (InvalidVersionException e) {
            // rethrowing the exception with more information
            throw new InvalidVersionException(plugin, this, e);
        }
    }

    /**
     * @return <code>null</code>, the REST module descriptor doesn't instansiate any module.
     */
    public Object getModule() {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (this == o) {
            return true;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }

        final RestModuleDescriptor that = (RestModuleDescriptor) o;
        return that.getCompleteKey().equals(getCompleteKey());
    }

    @Override
    public int hashCode() {
        return getCompleteKey().hashCode();
    }

    @Override
    public String toString() {
        return super.toString()
                + '/'
                + restContext
                + (restApiContext != null ? '/' + restApiContext.getApiPath() + '/' + restApiContext.getVersion() : "");
    }

    @Override
    public void disabled() {
        if (restServletFilterModuleDescriptor != null) {
            restServletFilterModuleDescriptor.disabled();
            restServletFilterModuleDescriptor = null;
        }

        restApiContext.disabled();

        if (serviceRegistration != null) {
            try {
                serviceRegistration.unregister();
            } catch (IllegalStateException ex) {
                // this has
            }
            serviceRegistration = null;
        }

        super.disabled();
    }

    @Override
    public void enabled() {
        super.enabled();
        restServletFilterModuleDescriptor = new RestServletFilterModuleDescriptor(
                osgiPlugin, moduleFactory, servletModuleManager, restApiContext, resourceConfigFactory);
        restServletFilterModuleDescriptor.init(plugin, createCompanionRestServletFilterElement(element));
        restServletFilterModuleDescriptor.enabled();

        // dynamically register the servlet filter that serves the REST API requests
        serviceRegistration = osgiPlugin
                .getBundle()
                .getBundleContext()
                .registerService(
                        new String[] {
                            restServletFilterModuleDescriptor.getClass().getName(), ModuleDescriptor.class.getName(),
                        },
                        restServletFilterModuleDescriptor,
                        null);
    }
}
