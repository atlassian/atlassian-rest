package com.atlassian.plugins.rest.v2.security.antisniffing;

import java.io.IOException;
import jakarta.annotation.Priority;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.ext.Provider;

import com.atlassian.sal.api.web.context.HttpContext;

import static java.util.Objects.requireNonNull;

/**
 * This class adds a 'X-Content-Type-Options' header to responses to
 * prevent certain browsers from performing mime-type sniffing.
 *
 * @since 2.8.1
 */
@Priority(Priorities.HEADER_DECORATOR)
@Provider
public class AntiSniffingResponseFilter implements ContainerResponseFilter {

    private HttpContext httpContext;

    public static final String ANTI_SNIFFING_HEADER_NAME = "X-Content-Type-Options";
    public static final String ANTI_SNIFFING_HEADER_VALUE = "nosniff";

    public AntiSniffingResponseFilter(HttpContext httpContext) {
        this.httpContext = requireNonNull(httpContext, "httpContext can't be null");
        ;
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
            throws IOException {
        if (!httpContext.getResponse().containsHeader(ANTI_SNIFFING_HEADER_NAME)
                && !responseContext.getHeaders().containsKey(ANTI_SNIFFING_HEADER_NAME)) {
            responseContext.getHeaders().add(ANTI_SNIFFING_HEADER_NAME, ANTI_SNIFFING_HEADER_VALUE);
        }
    }
}
