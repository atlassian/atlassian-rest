package com.atlassian.plugins.rest.v2.expand.resolver;

import com.atlassian.plugins.rest.api.expand.expander.EntityExpander;
import com.atlassian.plugins.rest.api.expand.expander.SelfExpanding;
import com.atlassian.plugins.rest.api.internal.expand.resolver.SelfExpandingEntityExpanderResolver;
import com.atlassian.plugins.rest.v2.expand.expander.SelfExpandingExpander;

/**
 * @see SelfExpandingEntityExpanderResolver
 */
public class SelfExpandingEntityExpanderResolverImpl implements SelfExpandingEntityExpanderResolver {

    public <T> boolean hasExpander(T instance) {
        return hasExpander(instance.getClass());
    }

    public boolean hasExpander(Class<?> aClass) {
        return SelfExpanding.class.isAssignableFrom(aClass);
    }

    @SuppressWarnings("unchecked")
    public <T> EntityExpander<T> getExpander(T instance) {
        return (EntityExpander<T>) getExpander(instance.getClass());
    }

    public <T> EntityExpander<T> getExpander(Class<? extends T> aClass) {
        return hasExpander(aClass) ? (EntityExpander<T>) new SelfExpandingExpander() : null;
    }
}
