package com.atlassian.plugins.rest.v2.multipart.exception;

import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;

/**
 * Exception indicating the file size limit was exceeded
 *
 * @since 2.4
 */
public class FileSizeLimitExceededException extends WebApplicationException {
    private static final int PAYLOAD_TOO_LARGE = 413;

    private static int getStatusCode() {
        return PAYLOAD_TOO_LARGE;
    }

    public FileSizeLimitExceededException(String message) {
        super(Response.status(getStatusCode()).entity(message).build());
    }
}
