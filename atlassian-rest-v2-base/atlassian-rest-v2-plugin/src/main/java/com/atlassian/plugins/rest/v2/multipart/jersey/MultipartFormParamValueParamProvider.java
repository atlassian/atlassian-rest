package com.atlassian.plugins.rest.v2.multipart.jersey;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import jakarta.ws.rs.BadRequestException;

import org.glassfish.jersey.server.ContainerRequest;
import org.glassfish.jersey.server.model.Parameter;
import org.glassfish.jersey.server.spi.internal.ValueParamProvider;

import com.atlassian.plugins.rest.api.multipart.FilePart;
import com.atlassian.plugins.rest.api.multipart.MultipartForm;
import com.atlassian.plugins.rest.api.multipart.MultipartFormParam;

/**
 * Value param provider supporting injection of files with &#064;MultipartFormParam
 * annotation to resource methods.
 */
public class MultipartFormParamValueParamProvider implements ValueParamProvider {

    @Override
    public Function<ContainerRequest, ?> getValueProvider(Parameter parameter) {
        Class<?> rawType = parameter.getRawType();
        String sourceName = parameter.getSourceName();

        if (parameter.isAnnotationPresent(MultipartFormParam.class)) {
            if (FilePart.class == rawType) {
                return new FilePartParamProvider(sourceName);
            }
            if (List.class == rawType) {
                return new FileListPartParamProvider(sourceName);
            }
            if (Set.class == rawType) {
                return new FileSetPartParamProvider(sourceName);
            }
            if (Collection.class == rawType) {
                return new FilesPartParamProvider(sourceName);
            }
        }

        return null;
    }

    @Override
    public PriorityType getPriority() {
        return Priority.HIGH;
    }

    /**
     * ValueProvider similar to org.glassfish.jersey.media.multipart.internal.FormDataParamValueParamProvider (version 2.41).
     */
    private abstract static class ValueProvider<T> implements Function<ContainerRequest, T> {

        /**
         * Returns a {@code MultipartForm} entity from the request and stores it in the request context properties.
         *
         * @return a form data multi part entity.
         */
        MultipartForm getEntity(ContainerRequest request) {
            final String requestPropertyName = MultipartForm.class.getName();

            Object entity = request.getProperty(requestPropertyName);
            if (entity == null) {
                // read MultipartForm by using MultipartFormMessageBodyReader
                entity = request.readEntity(MultipartForm.class);
                if (entity == null) {
                    throw new BadRequestException("Entity is empty");
                }
                request.setProperty(requestPropertyName, entity);
            }

            return (MultipartForm) entity;
        }
    }

    private static class FilePartParamProvider extends ValueProvider<FilePart> {
        private final String paramName;

        public FilePartParamProvider(String paramName) {
            this.paramName = paramName;
        }

        @Override
        public FilePart apply(ContainerRequest request) {
            return getEntity(request).getFilePart(paramName);
        }
    }

    private static class FilesPartParamProvider extends ValueProvider<Collection<FilePart>> {
        private final String paramName;

        public FilesPartParamProvider(String paramName) {
            this.paramName = paramName;
        }

        @Override
        public Collection<FilePart> apply(ContainerRequest request) {
            return getEntity(request).getFileParts(paramName);
        }
    }

    private static class FileListPartParamProvider extends ValueProvider<List<FilePart>> {
        private final String paramName;

        public FileListPartParamProvider(String paramName) {
            this.paramName = paramName;
        }

        @Override
        public List<FilePart> apply(ContainerRequest request) {
            return new ArrayList<>(getEntity(request).getFileParts(paramName));
        }
    }

    private static class FileSetPartParamProvider extends ValueProvider<Set<FilePart>> {
        private final String paramName;

        public FileSetPartParamProvider(String paramName) {
            this.paramName = paramName;
        }

        @Override
        public Set<FilePart> apply(ContainerRequest request) {
            return new HashSet<>(getEntity(request).getFileParts(paramName));
        }
    }
}
