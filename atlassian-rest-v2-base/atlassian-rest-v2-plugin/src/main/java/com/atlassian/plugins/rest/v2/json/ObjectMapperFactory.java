package com.atlassian.plugins.rest.v2.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.module.jakarta.xmlbind.JakartaXmlBindAnnotationIntrospector;

/**
 * Factory class responsible for creating and configuring an {@link ObjectMapper} for use in JSON serialization and
 * deserialization with the Atlassian REST custom configuration.
 * <p/>
 * Since no additional visibility configuration for the {@link ObjectMapper}, exists, the default visibility settings are as
 * follows: getter: PUBLIC_ONLY, isGetter: PUBLIC_ONLY, setter: ANY, creator: ANY, field: PUBLIC_ONLY
 */
public class ObjectMapperFactory {
    public static ObjectMapper createObjectMapper() {
        return new ObjectMapper()
                .findAndRegisterModules()
                .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                .setAnnotationIntrospector(createJacksonJaxbAnnotationIntrospector());
    }

    /**
     * The method creates the {@link AnnotationIntrospector} that combines the capabilities of both Jackson and JAXB annotations.
     * The Jackson annotations are used primarily if both Jackson and JAXB annotations are present.
     *
     * @return {@link AnnotationIntrospector}
     */
    private static AnnotationIntrospector createJacksonJaxbAnnotationIntrospector() {
        final AnnotationIntrospector jacksonIntrospector = new JacksonAnnotationIntrospector();
        final AnnotationIntrospector jaxbIntrospector =
                new JakartaXmlBindAnnotationIntrospector(TypeFactory.defaultInstance());
        return AnnotationIntrospector.pair(jacksonIntrospector, jaxbIntrospector);
    }
}
