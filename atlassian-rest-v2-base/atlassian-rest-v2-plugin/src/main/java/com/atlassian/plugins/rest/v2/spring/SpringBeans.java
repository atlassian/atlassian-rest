package com.atlassian.plugins.rest.v2.spring;

import java.util.Collection;
import java.util.Hashtable;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import com.atlassian.oauth2.scopes.api.ScopesRequestCache;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.bridge.external.SpringHostContainer;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;
import com.atlassian.plugins.osgi.javaconfig.ServiceCollection;
import com.atlassian.plugins.rest.api.expand.EntityCrawler;
import com.atlassian.plugins.rest.api.internal.expand.resolver.ListWrapperEntityExpanderResolver;
import com.atlassian.plugins.rest.api.internal.expand.resolver.SelfExpandingEntityExpanderResolver;
import com.atlassian.plugins.rest.api.internal.security.cors.CorsDefaults;
import com.atlassian.plugins.rest.api.internal.spi.config.ResourceConfigHostContext;
import com.atlassian.plugins.rest.api.json.JaxbJsonMarshaller;
import com.atlassian.plugins.rest.api.util.RestUrlBuilder;
import com.atlassian.plugins.rest.v2.descriptor.RestMigrationDescriptor;
import com.atlassian.plugins.rest.v2.descriptor.RestModuleDescriptorFactory;
import com.atlassian.plugins.rest.v2.expand.DefaultEntityCrawler;
import com.atlassian.plugins.rest.v2.expand.resolver.ListWrapperEntityExpanderResolverImpl;
import com.atlassian.plugins.rest.v2.expand.resolver.SelfExpandingEntityExpanderResolverImpl;
import com.atlassian.plugins.rest.v2.jersey.ResourceConfigFactory;
import com.atlassian.plugins.rest.v2.json.DefaultJaxbJsonMarshaller;
import com.atlassian.plugins.rest.v2.sal.MarshallingRequestFactoryServiceFactory;
import com.atlassian.plugins.rest.v2.security.cors.CorsDefaultService;
import com.atlassian.plugins.rest.v2.security.websudo.SalWebSudoResourceContext;
import com.atlassian.plugins.rest.v2.servlet.DefaultRestServletModuleManager;
import com.atlassian.plugins.rest.v2.servlet.RestServletModuleManager;
import com.atlassian.plugins.rest.v2.util.urlbuilder.RestUrlBuilderImpl;
import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.sal.api.net.MarshallingRequestFactory;
import com.atlassian.sal.api.net.NonMarshallingRequestFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.web.context.HttpContext;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.xsrf.XsrfRequestValidator;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiServiceCollection;
import static com.atlassian.plugins.osgi.javaconfig.ServiceCollection.list;

@Configuration
public class SpringBeans {

    @Bean
    public ModuleFactory importModuleFactory() {
        return importOsgiService(ModuleFactory.class);
    }

    @Bean
    public PluginEventManager importPluginEventManager() {
        return importOsgiService(PluginEventManager.class);
    }

    @Bean
    public UserManager importUserManager() {
        return importOsgiService(UserManager.class);
    }

    @Bean
    public DarkFeatureManager importDarkFeatureManager() {
        return importOsgiService(DarkFeatureManager.class);
    }

    @Bean
    public PluginAccessor importPluginAccessor() {
        return importOsgiService(PluginAccessor.class);
    }

    @Bean
    public HttpContext importHttpContext() {
        return importOsgiService(HttpContext.class);
    }

    @Bean
    public XsrfRequestValidator impotXsrfRequestValidator() {
        return importOsgiService(XsrfRequestValidator.class);
    }

    @Bean
    public SpringHostContainer springHostContainer() {
        return new SpringHostContainer();
    }

    @Bean
    public RestServletModuleManager restServletModuleManager(PluginEventManager pluginEventManager) {
        return new DefaultRestServletModuleManager(pluginEventManager);
    }

    @Bean
    public ScopesRequestCache scopesRequestCache() {
        return importOsgiService(ScopesRequestCache.class);
    }

    @Bean
    public ResourceConfigFactory resourceConfigFactory(
            UserManager userManager,
            DarkFeatureManager darkFeatureManager,
            SalWebSudoResourceContext salWebSudoResourceContext,
            HttpContext httpContext,
            XsrfRequestValidator xsrfRequestValidator,
            CorsDefaultService corsDefaultService,
            Collection<ResourceConfigHostContext> resourceConfigHostContexts,
            ScopesRequestCache scopesRequestCache) {
        return new ResourceConfigFactory(
                userManager,
                darkFeatureManager,
                salWebSudoResourceContext,
                httpContext,
                xsrfRequestValidator,
                corsDefaultService,
                resourceConfigHostContexts,
                scopesRequestCache);
    }

    @Bean
    public RestModuleDescriptorFactory restModuleDescriptorFactory(
            HostContainer hostContainer,
            ModuleFactory moduleFactory,
            RestServletModuleManager restServletModuleManager,
            ResourceConfigFactory resourceConfigFactory) {
        return new RestModuleDescriptorFactory(
                hostContainer, moduleFactory, restServletModuleManager, resourceConfigFactory);
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportRestModuleDescriptorFactory(
            RestModuleDescriptorFactory restModuleDescriptorFactory) {
        return exportOsgiService(restModuleDescriptorFactory, as(ListableModuleDescriptorFactory.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportRestMigrationDescriptorFactory(HostContainer hostContainer) {
        SingleModuleDescriptorFactory<RestMigrationDescriptor> restMigrationModuleDescriptorFactory =
                new SingleModuleDescriptorFactory<>(hostContainer, "rest-migration", RestMigrationDescriptor.class);
        return exportOsgiService(restMigrationModuleDescriptorFactory, as(ListableModuleDescriptorFactory.class));
    }

    @Bean
    public JaxbJsonMarshaller jaxbJsonMarshaller() {
        return new DefaultJaxbJsonMarshaller();
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportJaxbJsonMarshaller(JaxbJsonMarshaller jaxbJsonMarshaller) {
        return exportOsgiService(jaxbJsonMarshaller, as(JaxbJsonMarshaller.class));
    }

    @Bean
    public WebSudoManager importWebSudoManager() {
        return importOsgiService(WebSudoManager.class);
    }

    @Bean
    public SalWebSudoResourceContext salWebSudoResourceContext(WebSudoManager webSudoManager, UserManager userManager) {
        return new SalWebSudoResourceContext(webSudoManager, userManager);
    }

    @Bean
    public CorsDefaultService corsDefaultService() {
        return new CorsDefaultService(importOsgiServiceCollection(list(CorsDefaults.class)));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportEntityCrawler() {
        return exportOsgiService(new DefaultEntityCrawler(), as(EntityCrawler.class));
    }

    // exporting ListWrapperEntityExpanderResolver and SelfExpandingEntityExpanderResolver to help Jira migrate
    // to REST v2
    @Bean
    public FactoryBean<ServiceRegistration> exportListWrapperEntityExpanderResolver() {
        return exportOsgiService(
                new ListWrapperEntityExpanderResolverImpl(), as(ListWrapperEntityExpanderResolver.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportSelfExpandingEntityExpanderResolver() {
        return exportOsgiService(
                new SelfExpandingEntityExpanderResolverImpl(), as(SelfExpandingEntityExpanderResolver.class));
    }

    @Bean
    public RestUrlBuilder restUrlBuilder() {
        return new RestUrlBuilderImpl();
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportRestUrlBuilder(RestUrlBuilder restUrlBuilder) {
        return exportOsgiService(restUrlBuilder, as(RestUrlBuilder.class));
    }

    @Bean
    public NonMarshallingRequestFactory nonMarshallingRequestFactory() {
        return importOsgiService(NonMarshallingRequestFactory.class);
    }

    @Bean
    public ServiceRegistration<?> marshallingRequestFactoryServiceFactory(
            NonMarshallingRequestFactory nonMarshallingRequestFactory,
            BundleContext bundleContext,
            PluginAccessor pluginAccessor) {
        MarshallingRequestFactoryServiceFactory marshallingRequestFactoryServiceFactory =
                new MarshallingRequestFactoryServiceFactory(pluginAccessor, nonMarshallingRequestFactory);
        return bundleContext.registerService(
                MarshallingRequestFactory.class, marshallingRequestFactoryServiceFactory, new Hashtable<>());
    }

    @Bean
    public Collection<ResourceConfigHostContext> importResourceConfigHostContexts() {
        // Instead of importing a single bean, we import a collection of ResourceConfigHostContext services to make it
        // optional. If the host application hasn't exported an implementation, this will simply return an empty
        // collection.
        return importOsgiServiceCollection(ServiceCollection.list(ResourceConfigHostContext.class));
    }
}
