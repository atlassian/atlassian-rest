package com.atlassian.plugins.rest.v2.exception.constant;

public class UncaughtExceptionEntityConstants {
    public static final String ATLASSIAN_REST_RESPONSE_STACKTRACES = "atlassian.rest.response.stacktraces";
    public static final String ATLASSIAN_REST_RESPONSE_STACKTRACES_PRESENT = "1";
    public static final String ATLASSIAN_REST_RESPONSE_STACKTRACES_ABSENT = "0";
    public static final String NO_STACKTRACE_MESSAGE =
            "Please contact your admin passing attached Log's referral number: ";

    private UncaughtExceptionEntityConstants() {}
}
