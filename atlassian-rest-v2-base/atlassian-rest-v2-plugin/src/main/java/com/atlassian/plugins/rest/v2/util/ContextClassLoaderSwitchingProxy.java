package com.atlassian.plugins.rest.v2.util;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.atlassian.plugins.rest.v2.ChainingClassLoader;

/**
 * InvocationHandler for a dynamic proxy that ensures all methods are executed with the {@link ChainingClassLoader}
 * as the context class loader
 */
public class ContextClassLoaderSwitchingProxy implements InvocationHandler {
    private final Object delegate;
    private final ClassLoader[] classLoaders;

    public ContextClassLoaderSwitchingProxy(final Object delegate, final ClassLoader... classLoaders) {
        this.delegate = delegate;
        this.classLoaders = classLoaders;
    }

    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
        ClassLoader oldClassLoader = Thread.currentThread().getContextClassLoader();
        ChainingClassLoader chainingClassLoader = new ChainingClassLoader(classLoaders);
        try {
            try {
                Thread.currentThread().setContextClassLoader(chainingClassLoader);
                return method.invoke(delegate, args);
            } catch (InvocationTargetException e) {
                throw e.getCause();
            }
        } finally {
            Thread.currentThread().setContextClassLoader(oldClassLoader);
        }
    }
}
