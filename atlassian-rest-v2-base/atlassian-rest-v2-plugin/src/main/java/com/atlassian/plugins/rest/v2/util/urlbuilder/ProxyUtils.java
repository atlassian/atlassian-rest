package com.atlassian.plugins.rest.v2.util.urlbuilder;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.loading.ClassLoadingStrategy;
import net.bytebuddy.implementation.InvocationHandlerAdapter;

import com.atlassian.plugins.rest.v2.ChainingClassLoader;

import static net.bytebuddy.matcher.ElementMatchers.any;

/**
 * Proxy creation utilities.
 */
public class ProxyUtils {

    private ProxyUtils() {}

    private static final Map<Class<?>, ByteBuddyProxyCreator> generatorCache = new ConcurrentHashMap<>();

    public static <T> T create(Class<T> clazz, InvocationHandler invocationHandler) {
        try {
            return (T) generatorCache
                    .computeIfAbsent(clazz, ByteBuddyProxyCreator::new)
                    .create(invocationHandler);
        } catch (Exception e) {
            throw new RuntimeException("Failed to construct class: " + e.getMessage(), e);
        }
    }
}

/**
 * This class is used to create a proxy for a specified class.
 * The proxy will delegate all method calls to a provided InvocationHandler.
 * It is cached per class.
 */
class ByteBuddyProxyCreator {
    private final Class<?> clazz;

    ByteBuddyProxyCreator(Class<?> clazz) {
        this.clazz = clazz;
    }

    public Object create(InvocationHandler invocationHandler) {
        try {
            Class<?> dynamicType = new ByteBuddy()
                    .subclass(clazz)
                    .method(any())
                    .intercept(InvocationHandlerAdapter.of(invocationHandler))
                    .make()
                    .load(
                            new ChainingClassLoader(ProxyUtils.class.getClassLoader(), clazz.getClassLoader()),
                            ClassLoadingStrategy.Default.INJECTION)
                    .getLoaded();

            return constructDynamicTypeInstance(dynamicType);
        } catch (Exception e) {
            throw new RuntimeException(
                    String.format("Failed to create dynamic type proxy for %s: %s", clazz.getName(), e.getMessage()),
                    e);
        }
    }

    private static Object constructDynamicTypeInstance(Class<?> dynamicType)
            throws InstantiationException, IllegalAccessException, InvocationTargetException {
        Constructor<?>[] constructors = dynamicType.getDeclaredConstructors();
        if (constructors.length == 0) {
            throw new RuntimeException("No constructors found for class: " + dynamicType.getName());
        }

        for (Constructor<?> constructor : constructors) {
            if ((constructor.getModifiers() & Modifier.PUBLIC) != 0) {
                int size = constructor.getParameterTypes().length;
                Object[] args = new Object[size];

                for (int i = 0; i < args.length; i++) {
                    args[i] = createEmptyValue(constructor.getParameterTypes()[i]);
                }

                return constructor.newInstance(args);
            }
        }
        throw new RuntimeException("No accessible constructor found for class: " + dynamicType.getName());
    }

    private static Object createEmptyValue(Class<?> aClass) {
        // Handle interfaces
        if (aClass.isInterface()) {
            return stubInterface(aClass);
        }

        // Handle primitive types and their boxed counterparts
        if (aClass == Long.TYPE || aClass == Long.class) {
            return 0L;
        } else if (aClass == Integer.TYPE || aClass == Integer.class) {
            return 0;
        } else if (aClass == Boolean.TYPE || aClass == Boolean.class) {
            return false;
        } else if (aClass == Character.TYPE || aClass == Character.class) {
            return '\0';
        } else if (aClass == Byte.TYPE || aClass == Byte.class) {
            return (byte) 0;
        } else if (aClass == Short.TYPE || aClass == Short.class) {
            return (short) 0;
        } else if (aClass == Float.TYPE || aClass == Float.class) {
            return 0.0f;
        } else if (aClass == Double.TYPE || aClass == Double.class) {
            return 0.0d;
            // Handle special classes
        } else if (aClass == String.class) {
            return "";
        } else if (aClass.isArray()) {
            return Array.newInstance(aClass.getComponentType(), 0);
        }

        // For any other type, return null
        return null;
    }

    private static Object stubInterface(Class<?> anInterface) {
        return Proxy.newProxyInstance(
                anInterface.getClassLoader(),
                new Class[] {anInterface},
                UnsupportedOperationInvocationHandler.INSTANCE);
    }
}

class UnsupportedOperationInvocationHandler implements InvocationHandler {
    public static final UnsupportedOperationInvocationHandler INSTANCE = new UnsupportedOperationInvocationHandler();

    public Object invoke(Object proxy, Method method, Object[] args) {
        throw new UnsupportedOperationException();
    }
}
