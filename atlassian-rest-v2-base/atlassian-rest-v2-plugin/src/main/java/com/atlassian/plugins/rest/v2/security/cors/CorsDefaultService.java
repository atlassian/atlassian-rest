package com.atlassian.plugins.rest.v2.security.cors;

import java.util.List;

import com.atlassian.plugins.rest.api.internal.security.cors.CorsDefaults;

/**
 * This class encapsulates the imported OSGi services related to {@link CorsDefaults} implementations from the plugins.
 * An instance of this class contains a list of {@link CorsDefaults} objects, each representing a default CORS configuration.
 * <p>
 * Typical usage is to create an instance of this class using imported OSGi services, as shown in the following example:
 *
 * <pre>
 * {@code
 * @Bean
 * public CorsDefaultService corsDefaultService() {
 *     return new CorsDefaultService(importOsgiServiceCollection(list(CorsDefaults.class)));
 * }
 * }
 * </pre>
 *
 * @see CorsDefaults
 */
public class CorsDefaultService {
    private final List<CorsDefaults> corsDefaults;

    public List<CorsDefaults> getCorsDefaults() {
        return corsDefaults;
    }

    public CorsDefaultService(List<CorsDefaults> corsDefaults) {
        this.corsDefaults = corsDefaults;
    }
}
