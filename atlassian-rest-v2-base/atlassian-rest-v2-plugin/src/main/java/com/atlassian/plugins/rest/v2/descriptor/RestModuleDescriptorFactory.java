package com.atlassian.plugins.rest.v2.descriptor;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;
import com.atlassian.plugins.rest.v2.jersey.ResourceConfigFactory;
import com.atlassian.plugins.rest.v2.servlet.RestServletModuleManager;

import static java.util.Objects.requireNonNull;

/**
 * Copy of {@link com.atlassian.plugins.rest.module.RestModuleDescriptorFactory}
 * Differences:
 * <ul>
 *     <li>`type` of module is changed to `rest2` to differentiate from the original one</li>
 *     <li>`rest context path` is always set to the same value (originally it was passed in constructor, but value was always set to the same value in spring configuration anyway)</li>
 * </ul>
 * This class is almost 100% the same as original one. It could be shared in both modules.
 */
public class RestModuleDescriptorFactory extends SingleModuleDescriptorFactory<RestModuleDescriptor> {
    private final RestServletModuleManager servletModuleManager;

    private final ModuleFactory moduleFactory;
    private final String restContextPath;
    private final ResourceConfigFactory resourceConfigFactory;

    public RestModuleDescriptorFactory(
            HostContainer hostContainer,
            ModuleFactory moduleFactory,
            RestServletModuleManager servletModuleManager,
            ResourceConfigFactory resourceConfigFactory) {
        super(requireNonNull(hostContainer, "hostContainer can't be null"), "rest", RestModuleDescriptor.class);
        this.moduleFactory = moduleFactory;
        this.servletModuleManager = requireNonNull(servletModuleManager, "servletModuleManager can't be null");
        this.restContextPath = "/rest";
        this.resourceConfigFactory = resourceConfigFactory;
    }

    @Override
    public ModuleDescriptor<Object> getModuleDescriptor(String type) {
        return hasModuleDescriptor(type)
                ? new RestModuleDescriptor(moduleFactory, servletModuleManager, restContextPath, resourceConfigFactory)
                : null;
    }
}
