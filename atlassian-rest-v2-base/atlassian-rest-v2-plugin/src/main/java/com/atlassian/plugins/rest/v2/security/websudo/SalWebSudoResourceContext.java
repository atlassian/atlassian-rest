package com.atlassian.plugins.rest.v2.security.websudo;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Objects;
import java.util.Optional;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugins.rest.v2.util.ServletUtils;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;

public class SalWebSudoResourceContext {
    private static final Logger log = LoggerFactory.getLogger(SalWebSudoResourceContext.class);
    private static final String BASIC_AUTHZ_TYPE_PREFIX = "Basic ";
    private static final String BEARER_AUTHZ_TYPE_PREFIX = "Bearer ";

    private final WebSudoManager webSudoManager;
    private final UserManager userManager;

    public SalWebSudoResourceContext(final WebSudoManager webSudoManager, final UserManager userManager) {
        this.webSudoManager = webSudoManager;
        this.userManager = userManager;
    }

    /**
     * Checks if WebSudo protection is required.
     * <ul>
     * <li>If clients authenticate using Basic-Auth WebSudo is not required.</li>
     * <li>If clients authenticate using a Bearer Token WebSudo is not required.</li>
     * <li>If the current request is already protected (or if WebSudo is disabled in the host application) WebSudo is not
     * required.</li>
     * </ul>
     *
     * @return true if resource need to be protected by WebSudo
     */
    public boolean shouldEnforceWebSudoProtection() {
        final HttpServletRequest req = ServletUtils.getHttpServletRequest();
        // If the servlet request is null (presumably because we are not running in a servlet container) there is no
        // point in
        // making use of WebSudo.
        if (null == req) {
            return false;
        }

        // We can skip web sudo if this is a request authenticated by BASIC-AUTH
        final String authHeader = req.getHeader("Authorization");

        // Check if the request can skip WebSudo due to valid Basic Auth or due to valid personal access token
        if (authHeader != null
                && (isBasicAuthValid(authHeader) || shouldDisableWebSudoForPersonalAccessTokens(req, authHeader))) {
            return false;
        }
        // WebSudo enforcement is required
        return !webSudoManager.canExecuteRequest(req);
    }

    private boolean shouldDisableWebSudoForPersonalAccessTokens(
            final HttpServletRequest request, final String authHeader) {
        final HttpSession session = request.getSession(false);
        return Objects.nonNull(session)
                && authHeader.startsWith(BEARER_AUTHZ_TYPE_PREFIX)
                && Objects.nonNull(session.getAttribute("is.pats.enabled"));
    }

    private boolean isBasicAuthValid(String authHeader) {
        if (isBasicAuthorizationHeader(authHeader)) {
            Optional<UserPassCredentials> credentials = decodeBasicAuthorizationCredentials(authHeader);
            return credentials
                    .map(credential -> validateCredentials(credential.getUsername(), credential.getPassword()))
                    .orElse(false);
        }
        return false;
    }

    private boolean isBasicAuthorizationHeader(String header) {
        return header != null && header.startsWith(BASIC_AUTHZ_TYPE_PREFIX);
    }

    private boolean validateCredentials(final String username, final String password) {
        return userManager.authenticate(username, password);
    }

    private Optional<UserPassCredentials> decodeBasicAuthorizationCredentials(String authHeader) {

        String base64Credentials =
                authHeader.substring(BASIC_AUTHZ_TYPE_PREFIX.length()).trim();
        try {
            String token = new String(Base64.getDecoder().decode(base64Credentials), StandardCharsets.UTF_8);
            String[] values = token.split(":", 2);
            if (values.length == 2 && !values[0].isEmpty() && !values[1].isEmpty()) {
                return Optional.of(new UserPassCredentials(values[0], values[1]));
            }
        } catch (IllegalArgumentException e) {
            log.warn("Provided BasicAuth token that is not valid base64 string: {}", e.getMessage());
        }
        return Optional.empty();
    }

    public static class UserPassCredentials {
        private final String username;
        private final String password;

        public UserPassCredentials(String username, String password) {
            this.username = username;
            this.password = password;
        }

        public String getUsername() {
            return this.username;
        }

        public String getPassword() {
            return this.password;
        }
    }
}
