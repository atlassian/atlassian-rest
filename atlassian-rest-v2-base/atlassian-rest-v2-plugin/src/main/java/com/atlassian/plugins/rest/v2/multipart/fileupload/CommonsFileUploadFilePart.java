package com.atlassian.plugins.rest.v2.multipart.fileupload;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import jakarta.mail.internet.MimeUtility;

import org.apache.commons.fileupload2.core.FileItem;

import com.atlassian.plugins.rest.api.multipart.FilePart;
import com.atlassian.plugins.rest.v2.multipart.exception.UnsupportedFileNameEncodingException;

import static java.util.Objects.requireNonNull;

public final class CommonsFileUploadFilePart implements FilePart {

    private final FileItem fileItem;
    private final String name;

    CommonsFileUploadFilePart(FileItem fileItem) {
        this.fileItem = requireNonNull(fileItem);
        try {
            if (fileItem.getName() == null) {
                name = null;
            } else {
                this.name = new File(MimeUtility.decodeText(fileItem.getName())).getName();
            }

        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedFileNameEncodingException(fileItem.getName());
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return fileItem.getInputStream();
    }

    @Override
    public String getContentType() {
        return fileItem.getContentType();
    }

    @Override
    public void write(final File file) throws IOException {
        try {
            fileItem.write(file.toPath());
        } catch (Exception e) {
            if (e instanceof IOException) {
                throw (IOException) e;
            } else {
                throw new IOException(e);
            }
        }
    }

    @Override
    public String getValue() {
        return fileItem.getString();
    }

    @Override
    public boolean isFormField() {
        return fileItem.isFormField();
    }

    @Override
    public long getSize() {
        return fileItem.getSize();
    }
}
