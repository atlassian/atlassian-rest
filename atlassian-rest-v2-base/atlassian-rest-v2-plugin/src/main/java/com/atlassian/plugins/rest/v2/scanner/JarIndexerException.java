package com.atlassian.plugins.rest.v2.scanner;

public class JarIndexerException extends RuntimeException {
    public JarIndexerException(String message) {
        super(message);
    }

    public JarIndexerException(String message, Throwable cause) {
        super(message, cause);
    }
}
