package com.atlassian.plugins.rest.v2.multipart.jersey;

import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyReader;
import jakarta.ws.rs.ext.Provider;

import org.apache.commons.fileupload2.core.RequestContext;
import org.glassfish.jersey.message.internal.AbstractMessageReaderWriterProvider;

import com.atlassian.plugins.rest.api.multipart.MultipartConfig;
import com.atlassian.plugins.rest.api.multipart.MultipartConfigClass;
import com.atlassian.plugins.rest.api.multipart.MultipartForm;
import com.atlassian.plugins.rest.v2.multipart.fileupload.CommonsFileUploadMultipartHandler;

/**
 * Reads a multipart form data object
 *
 * @since 2.4
 */
@Provider
@Consumes(MediaType.MULTIPART_FORM_DATA)
public class MultipartFormMessageBodyReader implements MessageBodyReader<MultipartForm> {

    @Context
    private ResourceInfo resourceInfo;

    public boolean isReadable(
            final Class<?> type, final Type genericType, final Annotation[] annotations, final MediaType mediaType) {
        return type.equals(MultipartForm.class);
    }

    public MultipartForm readFrom(
            final Class<MultipartForm> type,
            final Type genericType,
            final Annotation[] annotations,
            final MediaType mediaType,
            final MultivaluedMap<String, String> httpHeaders,
            final InputStream entityStream)
            throws WebApplicationException {
        CommonsFileUploadMultipartHandler handler = getMultipartHandler();

        return handler.getForm(new RequestContext() {
            public String getCharacterEncoding() {
                return AbstractMessageReaderWriterProvider.getCharset(mediaType).name();
            }

            public String getContentType() {
                return mediaType.toString();
            }

            public long getContentLength() {
                return -1;
            }

            public InputStream getInputStream() {
                return entityStream;
            }
        });
    }

    private CommonsFileUploadMultipartHandler getMultipartHandler() {
        Annotation[] annotations = resourceInfo.getResourceMethod().getAnnotations();

        for (Annotation annotation : annotations) {
            if (annotation instanceof MultipartConfigClass) {
                Class<? extends MultipartConfig> configClass = ((MultipartConfigClass) annotation).value();
                try {
                    MultipartConfig multipartConfig = configClass.newInstance();
                    return new CommonsFileUploadMultipartHandler(
                            multipartConfig.getMaxFileSize(),
                            multipartConfig.getMaxSize(),
                            multipartConfig.getMaxFileCount());
                } catch (InstantiationException | IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        return new CommonsFileUploadMultipartHandler();
    }
}
