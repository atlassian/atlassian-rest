package com.atlassian.plugins.rest.v2.expand;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.plugins.rest.api.expand.EntityCrawler;
import com.atlassian.plugins.rest.api.expand.ExpandContext;
import com.atlassian.plugins.rest.api.expand.annotation.Expandable;
import com.atlassian.plugins.rest.api.expand.expander.EntityExpander;
import com.atlassian.plugins.rest.api.expand.parameter.ExpandParameter;
import com.atlassian.plugins.rest.api.expand.resolver.EntityExpanderResolver;
import com.atlassian.plugins.rest.v2.util.ReflectionUtils;

import static com.atlassian.plugins.rest.v2.util.ReflectionUtils.getFieldValue;
import static com.atlassian.plugins.rest.v2.util.ReflectionUtils.setFieldValue;

/**
 * This allows for crawling the fields of any arbitrary object, looking for fields that should be expanded.
 */
public class DefaultEntityCrawler implements EntityCrawler {

    private final ConcurrentMap<Class, List<Field>> declaredFields = new ConcurrentHashMap<>();
    private final ConcurrentMap<Class, Optional<Field>> expandFields = new ConcurrentHashMap<>();

    public DefaultEntityCrawler() {}

    /**
     * Crawls an entity for fields that should be expanded and expands them.
     *
     * @param entity           the object to crawl, can be {@code null}.
     * @param expandParameter  the parameters to match for expansion
     * @param expanderResolver the resolver to lookup {@link EntityExpander} for fields to be expanded.
     */
    public void crawl(Object entity, ExpandParameter expandParameter, EntityExpanderResolver expanderResolver) {
        if (entity == null) {
            return;
        }

        final Collection<Field> expandableFields = getExpandableFields(entity);
        setExpandParameter(expandableFields, entity);
        expandFields(expandableFields, entity, expandParameter, expanderResolver);
    }

    private void setExpandParameter(Collection<Field> expandableFields, Object entity) {
        final Optional<Field> expand = expandFields.computeIfAbsent(entity.getClass(), this::getExpandFields);
        if (expand.isPresent() && !expandableFields.isEmpty()) {
            String expandValue = createExpandString(expandableFields);
            setFieldValue(expand.get(), entity, expandValue);
        }
    }

    private String createExpandString(Collection<Field> expandableFields) {
        return expandableFields.stream()
                .map(this::getExpandable)
                .map(Expandable::value)
                .distinct()
                .collect(Collectors.joining(","));
    }

    private Collection<Field> getExpandableFields(final Object entity) {
        return declaredFields.computeIfAbsent(entity.getClass(), this::getDeclaredFields).stream()
                .filter(field -> getExpandable(field) != null && getFieldValue(field, entity) != null)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    private void expandFields(
            Collection<Field> expandableFields,
            Object entity,
            ExpandParameter expandParameter,
            EntityExpanderResolver expanderResolver) {
        for (Field field : expandableFields) {
            final Expandable expandable = getExpandable(field);
            if (expandParameter.shouldExpand(expandable) && expanderResolver.hasExpander(field.getType())) {
                // we know the expander is not null, as per ExpanderResolver contract
                final EntityExpander<Object> entityExpander = expanderResolver.getExpander(field.getType());

                final ExpandContext<Object> context =
                        new DefaultExpandContext<>(getFieldValue(field, entity), expandable, expandParameter);
                setFieldValue(field, entity, entityExpander.expand(context, expanderResolver, this));
            }
        }
    }

    /**
     * Returns the expandable annotation with the properly set value. The value is defined as the first valid point in the following list:
     * <ol>
     * <li>the value of the {@link Expandable} annotation if it is set</li>
     * <li>the name of an {@link XmlElement} if the annotation is present on the field and its name is not {@code ##default}</li>
     * <li>the name of the field</li>
     * <ol>
     *
     * @param field the field to look up the Expandable for
     * @return {@code null} if the field is null, {@code null} if the field doesn't have an expandable annotation,
     * an expandable annotation with a properly set value.
     */
    Expandable getExpandable(final Field field) {
        if (field == null) {
            return null;
        }

        final Expandable expandable = field.getAnnotation(Expandable.class);
        if (expandable == null) {
            return null;
        }

        if (StringUtils.isNotEmpty(expandable.value())) {
            return expandable;
        }

        final XmlElement xmlElement = field.getAnnotation(XmlElement.class);
        if (xmlElement != null
                && StringUtils.isNotEmpty(xmlElement.name())
                && !StringUtils.equals("##default", xmlElement.name())) {
            return new ExpandableWithValue(xmlElement.name());
        }

        return new ExpandableWithValue(field.getName());
    }

    private List<Field> getDeclaredFields(Class cls) {
        return new LinkedList<>(ReflectionUtils.getDeclaredFields(cls));
    }

    private Optional<Field> getExpandFields(Class cls) {
        for (Field field : declaredFields.computeIfAbsent(cls, this::getDeclaredFields)) {
            if (field.getType().equals(String.class)) {
                final XmlAttribute annotation = field.getAnnotation(XmlAttribute.class);
                if (annotation != null && (field.getName().equals("expand") || "expand".equals(annotation.name()))) {
                    return Optional.of(field);
                }
            }
        }
        return Optional.empty();
    }

    private static class ExpandableWithValue implements Expandable {
        private final String value;

        public ExpandableWithValue(String value) {
            this.value = value;
        }

        public String value() {
            return value;
        }

        public Class<? extends Annotation> annotationType() {
            return Expandable.class;
        }
    }
}
