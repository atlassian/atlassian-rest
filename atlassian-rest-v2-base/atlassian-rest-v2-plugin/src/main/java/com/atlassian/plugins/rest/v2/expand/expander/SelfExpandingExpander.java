package com.atlassian.plugins.rest.v2.expand.expander;

import com.atlassian.plugins.rest.api.expand.expander.AbstractEntityExpander;
import com.atlassian.plugins.rest.api.expand.expander.EntityExpander;
import com.atlassian.plugins.rest.api.expand.expander.SelfExpanding;

/**
 * This is a general-purpose expander for atlassian-rest that delegates the
 * expand process to the entity that is to be expanded (instead of having that
 * knowledge in a separate {@link EntityExpander}.
 * As a result, this expander can be used for every entity that implements
 * {@link SelfExpanding}.
 *
 * @author Erik van Zijst
 * @since v1.0.7
 */
public class SelfExpandingExpander extends AbstractEntityExpander<SelfExpanding> {

    protected SelfExpanding expandInternal(SelfExpanding selfExpandingObject) {
        selfExpandingObject.expand();
        return selfExpandingObject;
    }
}
