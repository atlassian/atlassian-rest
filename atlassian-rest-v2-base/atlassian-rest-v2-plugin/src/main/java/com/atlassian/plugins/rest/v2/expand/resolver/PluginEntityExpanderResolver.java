package com.atlassian.plugins.rest.v2.expand.resolver;

import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugins.rest.api.expand.annotation.Expander;
import com.atlassian.plugins.rest.api.expand.expander.EntityExpander;

import static java.util.Objects.requireNonNull;

public class PluginEntityExpanderResolver extends AbstractAnnotationEntityExpanderResolver {
    private final ContainerManagedPlugin plugin;

    public PluginEntityExpanderResolver(ContainerManagedPlugin plugin) {
        this.plugin = requireNonNull(plugin);
    }

    protected final EntityExpander<?> getEntityExpander(Expander expander) {
        return plugin.getContainerAccessor().createBean(expander.value());
    }
}
