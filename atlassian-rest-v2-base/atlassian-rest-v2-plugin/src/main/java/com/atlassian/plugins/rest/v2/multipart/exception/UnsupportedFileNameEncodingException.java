package com.atlassian.plugins.rest.v2.multipart.exception;

import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;

/**
 * Exception indicating invalid encoding of the file name
 *
 * @since 2.8
 */
public class UnsupportedFileNameEncodingException extends WebApplicationException {
    public UnsupportedFileNameEncodingException(String rawFileName) {
        super(Response.status(Response.Status.BAD_REQUEST)
                .entity(String.format("The encoding of file name '%s' is invalid according to RFC 2047", rawFileName))
                .build());
    }
}
