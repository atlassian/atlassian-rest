package com.atlassian.plugins.rest.v2.jersey;

import java.io.IOException;
import java.net.URI;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.ws.rs.core.UriBuilder;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugins.rest.v2.RestApiContext;
import com.atlassian.util.profiling.Ticker;

import static java.util.Objects.requireNonNull;

import static com.atlassian.util.profiling.Metrics.metric;

/**
 * Copy of {@link com.atlassian.plugins.rest.module.RestDelegatingServletFilter.JerseyOsgiServletContainer}
 * Differences:
 * <ul>
 *     <li>extracted to separate file (originally it was an inner class)</li>
 *     <li>removed methods incompatible with new Jersey 2.x</li>
 * </ul>
 * This class is specific to Jersey 2.x. However, main method (`doFilter`) is essentially the same.
 * It would be possible to share generation of `baseUri` and `requestUri` between modules.
 */
public class JerseyOsgiServletContainer extends ServletContainer {
    private static final Logger log = LoggerFactory.getLogger(JerseyOsgiServletContainer.class);

    private static final String METRIC_NAME = "http.rest.request";

    private final OsgiPlugin plugin;
    private final RestApiContext restApiContext;

    public JerseyOsgiServletContainer(OsgiPlugin plugin, RestApiContext restApiContext, ResourceConfig resourceConfig) {
        // it looks that now we should provide all classes to scan at initialisation of Jersey context
        super(resourceConfig);
        this.plugin = requireNonNull(plugin, "plugin can't be null");
        this.restApiContext = requireNonNull(restApiContext, "restApiContext can't be null");
    }

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        // This is overridden so that we can change the base URI, and keep the context path the same
        String baseUriPath;
        if (request.getRequestURI().contains(restApiContext.getPathToLatest())) {
            baseUriPath = request.getContextPath() + restApiContext.getPathToLatest();
            // TODO Remove this when REST-159 is fixed
            // Added logging to help find the root cause of REST-159
            log.debug("Setting base uri for REST to 'latest'");
            log.debug("Incoming URI : {}", request.getRequestURI());
        } else {
            baseUriPath = request.getContextPath() + restApiContext.getPathToVersion();
        }
        final UriBuilder absoluteUriBuilder =
                UriBuilder.fromUri(request.getRequestURL().toString());

        final URI baseUri =
                absoluteUriBuilder.replacePath(baseUriPath).path("/").build();

        final URI requestUri = absoluteUriBuilder
                .replacePath(request.getRequestURI())
                .replaceQuery(request.getQueryString())
                .build();

        try (Ticker ignored = metric(METRIC_NAME)
                .fromPluginKey(plugin.getKey())
                .tag("path", restApiContext.getPathToVersion())
                .tag("action", request.getMethod())
                .withAnalytics()
                .startTimer()) {
            service(baseUri, requestUri, request, response);
        }
    }
}
