package com.atlassian.plugins.rest.v2.multipart.fileupload;

import java.util.Collection;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Stream;
import jakarta.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload2.core.DiskFileItemFactory;
import org.apache.commons.fileupload2.core.FileItem;
import org.apache.commons.fileupload2.core.FileUploadException;
import org.apache.commons.fileupload2.core.FileUploadFileCountLimitException;
import org.apache.commons.fileupload2.core.FileUploadSizeException;
import org.apache.commons.fileupload2.core.RequestContext;
import org.apache.commons.fileupload2.jakarta.servlet6.JakartaServletFileUpload;
import org.apache.commons.fileupload2.jakarta.servlet6.JakartaServletRequestContext;
import io.atlassian.util.concurrent.Lazy;

import com.atlassian.plugins.rest.api.multipart.FilePart;
import com.atlassian.plugins.rest.api.multipart.MultipartForm;
import com.atlassian.plugins.rest.api.multipart.MultipartHandler;
import com.atlassian.plugins.rest.v2.multipart.exception.FileCountLimitExceededException;
import com.atlassian.plugins.rest.v2.multipart.exception.FileSizeLimitExceededException;

import static java.util.stream.Collectors.toUnmodifiableSet;

public class CommonsFileUploadMultipartHandler implements MultipartHandler {

    public static final long NO_LIMIT = -1;
    public static final long DEFAULT_REQUEST_PART_LIMIT = 1000;
    private final JakartaServletFileUpload servletFileUpload;

    public CommonsFileUploadMultipartHandler() {
        this(NO_LIMIT, NO_LIMIT, DEFAULT_REQUEST_PART_LIMIT);
    }

    /**
     * @param maxFileSize  Max file size, where -1 indicates no limit
     * @param maxSize      Max request size, where -1 indicates no limit
     * @param maxFileCount Max file count, where -1 indicates no limit
     */
    public CommonsFileUploadMultipartHandler(long maxFileSize, long maxSize, long maxFileCount) {
        servletFileUpload = new JakartaServletFileUpload(new DiskFileItemFactory.Builder().get());
        servletFileUpload.setFileSizeMax(maxFileSize);
        servletFileUpload.setSizeMax(maxSize);
        servletFileUpload.setFileCountMax(maxFileCount);
    }

    @Override
    public FilePart getFilePart(HttpServletRequest request, String field) {
        return getForm(request).getFilePart(field);
    }

    private static class CommonsFileUploadMultipartForm implements MultipartForm {
        private final Collection<FileItem> fileItems;
        private final Supplier<Set<String>> fieldNames;

        private CommonsFileUploadMultipartForm(final Collection<FileItem> fileItems) {
            this.fileItems = fileItems;
            this.fieldNames = Lazy.supplier(
                    () -> fileItems.stream().map(FileItem::getFieldName).collect(toUnmodifiableSet()));
        }

        private Stream<FilePart> getFilePartsStream(String field) {
            return fileItems.stream()
                    .filter(item -> item.getFieldName().equals(field))
                    .map(CommonsFileUploadFilePart::new);
        }

        @Override
        public FilePart getFilePart(String field) {
            return getFilePartsStream(field).findFirst().orElse(null);
        }

        @Override
        public Collection<FilePart> getFileParts(String field) {
            return getFilePartsStream(field).toList();
        }

        @Override
        public Set<String> getFieldNames() {
            return fieldNames.get();
        }
    }

    @Override
    public MultipartForm getForm(HttpServletRequest request) {
        return getForm(new JakartaServletRequestContext(request));
    }

    public MultipartForm getForm(RequestContext request) {
        try {
            return new CommonsFileUploadMultipartForm(servletFileUpload.parseRequest(request));
        } catch (FileUploadException e) {
            if (e instanceof FileUploadFileCountLimitException) {
                throw new FileCountLimitExceededException(e.getMessage());
            }
            if (e instanceof FileUploadSizeException) {
                throw new FileSizeLimitExceededException(e.getMessage());
            }
            throw new RuntimeException(e);
        }
    }
}
