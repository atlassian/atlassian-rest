package com.atlassian.plugins.rest.v2.sal;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import jakarta.ws.rs.core.GenericEntity;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyReader;
import jakarta.ws.rs.ext.MessageBodyWriter;
import jakarta.ws.rs.ext.Provider;

import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.message.internal.MessageBodyFactory;
import org.glassfish.jersey.server.ApplicationHandler;
import org.glassfish.jersey.server.ResourceConfig;

import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugins.rest.v2.exception.jersey.EntityConversionException;
import com.atlassian.plugins.rest.v2.scanner.AnnotatedClassScanner;

/**
 * {@link MarshallingEntityHandler} is a service that un/marshalls the request payload, and is used by {@link MarshallingRequest}.
 */
public class MarshallingEntityHandler {

    private final MessageBodyFactory messageBodyFactory;

    /**
     * The constructor fetches all {@link MessageBodyWriter}s and {@link MessageBodyReader}s from Atlassian REST plugin Jersey context,
     * and from the plugin, that sends the client request. Then it registers them in {@link MessageBodyFactory}.
     *
     * @param plugin - consumer plugin
     */
    public MarshallingEntityHandler(final OsgiPlugin plugin) {
        // scan the consumer plugin for all its providers
        this(new AnnotatedClassScanner(plugin.getBundle(), false, Provider.class).scan(Collections.emptySet()));
    }

    /**
     * Package private constructor for unit testing.
     *
     * @param providersScan - classes annotated with {@link Provider}, scanned from the consumer plugin
     */
    MarshallingEntityHandler(Set<Class<?>> providersScan) {
        ResourceConfig config = new ResourceConfig().register(new MessageBodyWorkersBinder(providersScan));
        messageBodyFactory = new MessageBodyFactory(null);
        ApplicationHandler applicationHandler = new ApplicationHandler(config);
        messageBodyFactory.initialize(applicationHandler.getInjectionManager());
    }

    public String marshall(Object entity, MediaType mediaType, Charset charset) throws IOException {
        Type entityType;
        if (entity instanceof GenericEntity) {
            final GenericEntity genericEntity = (GenericEntity) entity;
            entityType = genericEntity.getType();
            entity = genericEntity.getEntity();
        } else {
            entityType = entity.getClass();
        }
        final Class entityClass = entity.getClass();
        final MessageBodyWriter messageBodyWriter =
                messageBodyFactory.getMessageBodyWriter(entityClass, entityType, new Annotation[] {}, mediaType);
        if (messageBodyWriter == null) {
            throw new RuntimeException("Unable to find a message body writer for " + entityClass);
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        messageBodyWriter.writeTo(
                entity,
                entityClass,
                entityType,
                new Annotation[] {},
                mediaType,
                new MultivaluedHashMap<String, Object>(),
                outputStream);
        return outputStream.toString(charset.name());
    }

    public <T> T unmarshall(
            Class<T> entityClass,
            MediaType mediaType,
            InputStream entityStream,
            Map<String, List<String>> responseHeaders)
            throws IOException {
        MessageBodyReader<T> reader =
                messageBodyFactory.getMessageBodyReader(entityClass, entityClass, new Annotation[0], mediaType);
        if (reader == null) {
            throw new RuntimeException("Unable to find a message body reader for " + entityClass);
        }
        MultivaluedMap<String, String> headers = new MultivaluedHashMap<>();
        headers.putAll(responseHeaders);
        try {
            return reader.readFrom(entityClass, entityClass, new Annotation[0], mediaType, headers, entityStream);
        } catch (IOException e) {
            throw new EntityConversionException(e);
        }
    }

    /**
     * {@code MessageBodyWorkersBinder} binds the providers of type {@link MessageBodyWriter} and {@link MessageBodyReader}.
     */
    private static class MessageBodyWorkersBinder extends AbstractBinder {
        private final Set<Class<?>> providerClasses;

        public MessageBodyWorkersBinder(Set<Class<?>> providerClasses) {
            this.providerClasses = providerClasses;
        }

        @Override
        protected void configure() {
            providerClasses.forEach(provider -> {
                if (MessageBodyWriter.class.isAssignableFrom(provider)) {
                    bind(provider).to(MessageBodyWriter.class);
                } else if (MessageBodyReader.class.isAssignableFrom(provider)) {
                    bind(provider).to(MessageBodyReader.class);
                }
            });
        }
    }

    /**
     * Package private method for unit testing
     */
    MessageBodyFactory getMessageBodyFactory() {
        return messageBodyFactory;
    }
}
