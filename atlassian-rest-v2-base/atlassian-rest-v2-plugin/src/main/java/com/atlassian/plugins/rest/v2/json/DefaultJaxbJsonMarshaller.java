package com.atlassian.plugins.rest.v2.json;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import jakarta.ws.rs.core.MediaType;

import org.glassfish.jersey.jackson.internal.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import org.glassfish.jersey.jackson.internal.jackson.jaxrs.json.JacksonJsonProvider;
import com.fasterxml.jackson.core.JsonEncoding;

import com.atlassian.plugins.rest.api.json.JaxbJsonMarshaller;
import com.atlassian.plugins.rest.api.json.JsonMarshallingException;

import static org.glassfish.jersey.jackson.internal.jackson.jaxrs.json.JacksonJaxbJsonProvider.DEFAULT_ANNOTATIONS;

public class DefaultJaxbJsonMarshaller implements JaxbJsonMarshaller {
    private final JacksonJsonProvider jsonProvider;

    public DefaultJaxbJsonMarshaller() {
        this.jsonProvider = new JacksonJaxbJsonProvider(ObjectMapperFactory.createObjectMapper(), DEFAULT_ANNOTATIONS);
    }

    /**
     * Package-private constructor for unit tests.
     *
     * @param jsonProvider - mocked object
     */
    DefaultJaxbJsonMarshaller(JacksonJsonProvider jsonProvider) {
        this.jsonProvider = jsonProvider;
    }

    public String marshal(Object jacksonJaxbBean) throws JsonMarshallingException {
        try {
            final ByteArrayOutputStream os = new ByteArrayOutputStream();
            jsonProvider.writeTo(
                    jacksonJaxbBean, jacksonJaxbBean.getClass(), null, null, MediaType.APPLICATION_JSON_TYPE, null, os);
            // The encoding used inside JacksonJsonProvider is always UTF-8
            return os.toString(JsonEncoding.UTF8.getJavaName());
        } catch (IOException e) {
            throw new JsonMarshallingException(e);
        }
    }
}
