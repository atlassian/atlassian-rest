package com.atlassian.plugins.rest.v2.multipart.jersey;

import java.util.function.Supplier;

import com.atlassian.plugins.rest.api.multipart.MultipartHandler;
import com.atlassian.plugins.rest.v2.multipart.fileupload.CommonsFileUploadMultipartHandler;

/**
 * Multipart handler supplier supporting injection of MultipartHandler with
 * &#064;Context  annotation to resource methods.
 */
public class MultipartHandlerSupplier implements Supplier<MultipartHandler> {

    @Override
    public MultipartHandler get() {
        return new CommonsFileUploadMultipartHandler();
    }
}
