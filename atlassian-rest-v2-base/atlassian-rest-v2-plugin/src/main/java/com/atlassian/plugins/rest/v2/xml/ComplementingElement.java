package com.atlassian.plugins.rest.v2.xml;

import java.util.*;

import com.atlassian.annotations.Internal;
import com.atlassian.plugin.module.Element;

import static java.util.Collections.emptyList;

@Internal
public class ComplementingElement implements Element {
    private final Element baseElement;
    private final Map<String, String> attributes;
    private final LeafElement childElement;
    private final String childElementName;

    public ComplementingElement(
            Element element, Map<String, String> attributes, String childElementName, String childElementValue) {
        this.baseElement = element;
        this.attributes = attributes;
        this.childElement = new LeafElement(childElementName, childElementValue);
        this.childElementName = childElementName;
    }

    @Override
    public String attributeValue(String s) {
        return attributes.getOrDefault(s, baseElement.attributeValue(s));
    }

    @Override
    public String attributeValue(String s, String s1) {
        return attributes.getOrDefault(s, baseElement.attributeValue(s, s1));
    }

    @Override
    public Element element(String s) {
        return childElementName.equals(s) ? childElement : baseElement.element(s);
    }

    @Override
    public List<Element> elements(String s) {
        return childElementName.equals(s) ? List.of(childElement) : baseElement.elements(s);
    }

    @Override
    public String getTextTrim() {
        return baseElement.getTextTrim();
    }

    @Override
    public String elementTextTrim(String s) {
        return baseElement.elementTextTrim(s);
    }

    @Override
    public String getText() {
        return baseElement.getText();
    }

    @Override
    public String getName() {
        return baseElement.getName();
    }

    @Override
    public List<Element> elements() {
        List<Element> list = new ArrayList<>(baseElement.elements());
        list.add(childElement);
        return list;
    }

    @Override
    public List<String> attributeNames() {
        List<String> list = new ArrayList<>(baseElement.attributeNames());
        list.addAll(attributes.keySet());
        return list;
    }

    /**
     * A class to represent a simplified leaf element containing no child elements or attributes
     * Eg: <elementName>elementValue</elementName>.
     */
    private static class LeafElement implements Element {
        private final String value;
        private final String name;

        public LeafElement(String childElementName, String childElementValue) {
            this.name = childElementName;
            this.value = childElementValue;
        }

        @Override
        public String getText() {
            return value;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getTextTrim() {
            return value == null ? null : value.trim();
        }

        @Override
        public String attributeValue(String s) {
            return null;
        }

        @Override
        public String attributeValue(String s, String s1) {
            return null;
        }

        @Override
        public Element element(String s) {
            return null;
        }

        @Override
        public List<Element> elements(String s) {
            return emptyList();
        }

        @Override
        public String elementTextTrim(String s) {
            return null;
        }

        @Override
        public List<Element> elements() {
            return null;
        }

        @Override
        public List<String> attributeNames() {
            return null;
        }
    }
}
