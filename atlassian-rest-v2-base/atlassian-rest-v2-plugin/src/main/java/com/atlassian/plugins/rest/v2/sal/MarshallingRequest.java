package com.atlassian.plugins.rest.v2.sal;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugins.rest.v2.exception.jersey.EntityConversionException;
import com.atlassian.plugins.rest.v2.exception.jersey.UnsupportedContentTypeException;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFilePart;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.sal.api.net.ReturningResponseHandler;

import static java.util.Objects.requireNonNull;

/**
 * Implementation of the {@link Request} with ability to set entities.
 */
public class MarshallingRequest implements Request<MarshallingRequest, MarshallingResponse> {
    private final Request delegateRequest;
    private final MarshallingEntityHandler marshallingEntityHandler;
    private final Plugin plugin;

    private Object entity;

    public MarshallingRequest(
            final Request delegateRequest, final MarshallingEntityHandler marshallingEntityHandler, Plugin plugin) {
        this.delegateRequest = delegateRequest;
        this.marshallingEntityHandler = marshallingEntityHandler;
        this.plugin = plugin;
    }

    public MarshallingRequest addBasicAuthentication(String hostname, String username, String password) {
        delegateRequest.addBasicAuthentication(hostname, username, password);
        return this;
    }

    @Override
    public MarshallingRequest setEntity(final Object entity) {
        this.entity = requireNonNull(entity);
        return this;
    }

    @Override
    public MarshallingRequest setConnectionTimeout(final int i) {
        delegateRequest.setConnectionTimeout(i);
        return this;
    }

    @Override
    public MarshallingRequest setSoTimeout(final int i) {
        delegateRequest.setSoTimeout(i);
        return this;
    }

    @Override
    public MarshallingRequest setUrl(final String s) {
        delegateRequest.setUrl(s);
        return this;
    }

    @Override
    public MarshallingRequest setRequestBody(final String s) {
        delegateRequest.setRequestBody(s);
        return this;
    }

    public MarshallingRequest setRequestBody(String requestBody, String contentType) {
        delegateRequest.setRequestBody(requestBody, contentType);
        return this;
    }

    @Override
    public MarshallingRequest setFiles(List<RequestFilePart> files) {
        delegateRequest.setFiles(files);
        return this;
    }

    @Override
    public MarshallingRequest addRequestParameters(final String... strings) {
        delegateRequest.addRequestParameters(strings);
        return this;
    }

    @Override
    public MarshallingRequest addHeader(final String s, final String s1) {
        delegateRequest.addHeader(s, s1);
        return this;
    }

    @Override
    public MarshallingRequest setHeader(final String s, final String s1) {
        delegateRequest.setHeader(s, s1);
        return this;
    }

    @Override
    public MarshallingRequest setFollowRedirects(final boolean follow) {
        delegateRequest.setFollowRedirects(follow);
        return this;
    }

    @Override
    public Map<String, List<String>> getHeaders() {
        return delegateRequest.getHeaders();
    }

    @Override
    public void execute(final ResponseHandler<? super MarshallingResponse> responseHandler) throws ResponseException {
        executeAndReturn((ReturningResponseHandler<MarshallingResponse, Void>) marshallingResponse -> {
            responseHandler.handle(marshallingResponse);
            return null;
        });
    }

    @Override
    public String execute() throws ResponseException {
        marshallEntity();
        return delegateRequest.execute();
    }

    @Override
    public <RET> RET executeAndReturn(final ReturningResponseHandler<? super MarshallingResponse, RET> responseHandler)
            throws ResponseException {
        marshallEntity();
        final Object result = delegateRequest.executeAndReturn((ReturningResponseHandler<Response, RET>) response -> {
            MarshallingResponse res = new MarshallingResponse(response, marshallingEntityHandler, plugin);
            return responseHandler.handle(res);
        });

        return (RET) result;
    }

    private void marshallEntity() {
        if (entity != null) {
            String contentType = getOrSetSingleHeaderValue(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML);
            getOrSetSingleHeaderValue(HttpHeaders.ACCEPT, contentType);
            Charset charset = StandardCharsets.UTF_8;
            MediaType type;
            try {
                type = MediaType.valueOf(contentType);
            } catch (IllegalArgumentException e) {
                throw new UnsupportedContentTypeException(e.getMessage(), e);
            }
            try {
                String body = marshallingEntityHandler.marshall(entity, type, charset);
                setRequestBody(body, contentType);
            } catch (IOException e) {
                throw new EntityConversionException(e);
            }
        }
    }

    /**
     * Retrieve the specified header value, or set it to the supplied defaultValue if the header is currently unset.
     */
    private String getOrSetSingleHeaderValue(final String headerName, final String defaultValue) {
        String value = defaultValue;
        List<String> headers = getHeaders().get(headerName);
        if (headers != null && !headers.isEmpty()) {
            if (headers.size() == 1) {
                value = headers.get(0);
            }
        } else {
            setHeader(headerName, defaultValue);
        }
        return value;
    }
}
