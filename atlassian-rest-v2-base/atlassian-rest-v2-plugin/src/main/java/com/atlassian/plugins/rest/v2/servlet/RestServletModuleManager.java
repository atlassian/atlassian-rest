package com.atlassian.plugins.rest.v2.servlet;

import com.atlassian.plugin.servlet.ServletModuleManager;

/**
 * The RestServletModuleManager interface extends the ServletModuleManager interface.
 * It provides an abstraction for managing RestServlet modules.
 *
 * <p>Classes implementing this interface should provide methods for adding,
 * removing, and managing RestServlet modules.
 *
 * <p>This interface is a part of an adapter or facade pattern,
 * serving as a link between RestServlet modules and the ServletModuleManager.
 *
 * @see ServletModuleManager
 */
public interface RestServletModuleManager extends ServletModuleManager {}
