package com.atlassian.plugins.rest.v2.exception;

import jakarta.annotation.Priority;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugins.rest.api.model.Status;

/**
 * A generic exception mapper that will map {@link NotFoundException not found exceptions}.
 *
 * @since 1.0
 */
@Priority(Priorities.USER)
@Provider
public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {
    private static final Logger log = LoggerFactory.getLogger(NotFoundExceptionMapper.class);

    @Context
    Request request;

    public Response toResponse(NotFoundException exception) {
        log.debug(
                "Not Found Exception in REST: {}: {}",
                exception.getResponse().getStatus(),
                exception.getResponse(),
                exception);

        return Status.notFound()
                .message(exception.getMessage())
                .responseBuilder()
                .type(Status.variantFor(request))
                .build();
    }
}
