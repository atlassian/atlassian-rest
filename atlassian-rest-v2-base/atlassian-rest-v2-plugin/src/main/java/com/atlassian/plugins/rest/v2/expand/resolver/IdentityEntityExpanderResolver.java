package com.atlassian.plugins.rest.v2.expand.resolver;

import com.atlassian.plugins.rest.api.expand.expander.AbstractEntityExpander;
import com.atlassian.plugins.rest.api.expand.expander.EntityExpander;
import com.atlassian.plugins.rest.api.expand.resolver.EntityExpanderResolver;

/**
 * The aim of IdentityExpanderResolver is to provide a default expander called IdentityExpander for an entity, which
 * couldn't be resolved by any other resolver.
 */
public class IdentityEntityExpanderResolver implements EntityExpanderResolver {
    private static final EntityExpander IDENTITY = new IdentityExpander();

    public boolean hasExpander(Class<?> type) {
        return true;
    }

    public <T> EntityExpander<T> getExpander(Class<? extends T> type) {
        return (EntityExpander<T>) IDENTITY;
    }

    private static class IdentityExpander extends AbstractEntityExpander<Object> {
        protected Object expandInternal(Object entity) {
            return entity;
        }
    }
}
