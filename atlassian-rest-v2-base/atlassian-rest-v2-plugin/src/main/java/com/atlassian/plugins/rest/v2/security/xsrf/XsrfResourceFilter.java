package com.atlassian.plugins.rest.v2.security.xsrf;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import jakarta.annotation.Priority;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.internal.guava.Cache;
import org.glassfish.jersey.internal.guava.CacheBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.http.method.Methods;
import com.atlassian.http.mime.BrowserUtils;
import com.atlassian.http.mime.UserAgentUtil;
import com.atlassian.http.mime.UserAgentUtilImpl;
import com.atlassian.http.url.SameOrigin;
import com.atlassian.plugins.rest.api.internal.security.cors.CorsDefaults;
import com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders;
import com.atlassian.plugins.rest.api.security.exception.XsrfCheckFailedException;
import com.atlassian.sal.api.web.context.HttpContext;
import com.atlassian.sal.api.xsrf.XsrfRequestValidator;

/**
 * Rejects requests that do not satisfy XSRF checks.
 * <p>
 * A request is rejected if it requires XSRF protection, but does not include the
 * no-check header or a valid XSRF protection token.
 * <p>
 * Also protects browsers against XSRF attacks where the origin of a request would not
 * otherwise be permitted by the same origin policy or CORS.
 *
 * @since 2.4
 */
@Priority(Priorities.AUTHORIZATION)
@Provider
public class XsrfResourceFilter implements ContainerRequestFilter {
    public static final String TOKEN_HEADER = "X-Atlassian-Token";
    public static final String NO_CHECK = "no-check";
    private static final Response.Status FAILURE_STATUS = Response.Status.FORBIDDEN;

    private static final Set<String> XSRFABLE_TYPES = Collections.unmodifiableSet(new HashSet<>(
            Arrays.asList(MediaType.APPLICATION_FORM_URLENCODED, MediaType.MULTIPART_FORM_DATA, MediaType.TEXT_PLAIN)));
    private static final Set<String> BROWSER_EXTENSION_ORIGINS =
            Collections.unmodifiableSet(new HashSet<>(Arrays.asList("chrome-extension", "safari-extension")));
    private static final Logger log = LoggerFactory.getLogger(XsrfResourceFilter.class);
    private static final Cache<String, Boolean> XSRF_NOT_ENFORCED_RESOURCE_CACHE =
            CacheBuilder.newBuilder().maximumSize(1000).build();

    private HttpContext httpContext;
    private XsrfRequestValidator xsrfRequestValidator;
    private List<CorsDefaults> corsDefaults;

    public void setHttpContext(final HttpContext httpContext) {
        this.httpContext = httpContext;
    }

    public void setXsrfRequestValidator(final XsrfRequestValidator xsrfRequestValidator) {
        this.xsrfRequestValidator = xsrfRequestValidator;
    }

    public void setCorsDefaults(List<CorsDefaults> corsDefaults) {
        this.corsDefaults = corsDefaults;
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        if (passesAllXsrfChecks(requestContext)) {
            return;
        }
        throw new XsrfCheckFailedException(FAILURE_STATUS);
    }

    private boolean passesAllXsrfChecks(final ContainerRequestContext requestContext) {
        final HttpServletRequest httpRequest = getRequestOrNull(httpContext);
        final String method = httpRequest != null && httpRequest.getMethod() != null
                ? httpRequest.getMethod()
                : requestContext.getMethod();
        final boolean isMethodMutative = Methods.isMutative(method);
        final boolean isPostRequest = isPostRequest(method);

        if (isMethodMutative && isLikelyToBeFromBrowser(requestContext)) {
            final boolean passesOriginChecks = passesAdditionalBrowserChecks(requestContext);
            if (isPostRequest && !passesOriginChecks) {
                return false;
            }
            if (!isPostRequest) {
                if (!passesOriginChecks) {
                    logXsrfFailureButNotBeingEnforced(requestContext, log);
                }
                return true;
            }
        }
        if (isXsrfable(method, requestContext.getMediaType())) {
            final boolean passes = passesStandardXsrfChecks(httpRequest) || hasDeprecatedHeaderValue(requestContext);
            if (passes) {
                return true;
            } else if (isMethodMutative && !isPostRequest) {
                logXsrfFailureButNotBeingEnforced(requestContext, log);
                return true;
            }
            log.warn(
                    "XSRF checks failed for request: {} , origin: {} , referrer: {}",
                    requestContext.getUriInfo().getRequestUri().toString().contains("?")
                            ? StringUtils.substringBefore(
                                    requestContext.getUriInfo().getRequestUri().toString(), "?")
                            : requestContext.getUriInfo().getRequestUri().toString(),
                    requestContext.getHeaderString(CorsHeaders.ORIGIN.value()),
                    getSanitisedReferrer(requestContext));
            return false;
        }
        return true;
    }

    public void logXsrfFailureButNotBeingEnforced(ContainerRequestContext requestContext, Logger logger) {
        final String key = requestContext.getUriInfo().getPath();
        if (key != null && XSRF_NOT_ENFORCED_RESOURCE_CACHE.getIfPresent(key) == null) {
            logger.warn(
                    "XSRF failure not being enforced for request: {} , origin: {} , referrer: {}, " + "method: {}",
                    requestContext.getUriInfo().getRequestUri().toString().contains("?")
                            ? StringUtils.substringBefore(
                                    requestContext.getUriInfo().getRequestUri().toString(), "?")
                            : requestContext.getUriInfo().getRequestUri().toString(),
                    requestContext.getHeaderString(CorsHeaders.ORIGIN.value()),
                    getSanitisedReferrer(requestContext),
                    requestContext.getMethod());
            XSRF_NOT_ENFORCED_RESOURCE_CACHE.put(key, Boolean.TRUE);
        }
    }

    private boolean passesStandardXsrfChecks(final HttpServletRequest httpServletRequest) {
        if (httpServletRequest == null) {
            return false;
        }
        return xsrfRequestValidator.validateRequestPassesXsrfChecks(httpServletRequest);
    }

    /**
     * Returns true if the provided origin is from a browser extension.
     *
     * @param origin the origin to check.
     * @return true if the provided origin is from a browser extension,
     * otherwise returns false.
     */
    private boolean isOriginABrowserExtension(String origin) {
        if (StringUtils.isEmpty(origin)) {
            return false;
        }
        try {
            final URI originUri = new URI(origin);
            return BROWSER_EXTENSION_ORIGINS.contains(originUri.getScheme()) && !originUri.isOpaque();
        } catch (URISyntaxException e) {
            return false;
        }
    }

    /**
     * Due to bugs in some browsers, it is possible for non-simple cross-domain HTTP requests
     * to be issued *without* the normal CORS preflight request being triggered. This meant
     * that both our CORS restrictions and our XSRF protections could be bypassed.
     */
    protected boolean passesAdditionalBrowserChecks(final ContainerRequestContext requestContext) {
        final String origin = requestContext.getHeaderString(CorsHeaders.ORIGIN.value());
        final String referrer = getSanitisedReferrer(requestContext);
        final URI uri = requestContext.getUriInfo().getRequestUri();

        if (isSameOrigin(referrer, uri)) {
            return true;
        }

        if (isSameOrigin(origin, uri)) {
            return true;
        }
        if (isOriginABrowserExtension(origin)) {
            return true;
        }
        final boolean requestContainsCredentials = containsCredentials(requestContext);
        final boolean requestAllowedViaCors = isAllowedViaCors(origin, requestContainsCredentials);

        if (requestAllowedViaCors) {
            return true;
        }
        if (requestContext.getMethod() != null && isPostRequest(requestContext.getMethod())) {
            log.warn(
                    "Additional XSRF checks failed for request: {} , "
                            + "origin: {} , referrer: {} , credentials in request: {} , "
                            + "allowed via CORS: {}",
                    uri.toString().contains("?") ? StringUtils.substringBefore(uri.toString(), "?") : uri.toString(),
                    origin,
                    referrer,
                    requestContainsCredentials,
                    requestAllowedViaCors);
        }
        return false;
    }

    boolean isXsrfable(final String method, MediaType mediaType) {
        return mediaType != null
                && (Methods.isMutative(method) && XSRFABLE_TYPES.contains(mediaTypeToString(mediaType)));
    }

    private boolean hasDeprecatedHeaderValue(final ContainerRequestContext requestContext) {
        final String tokenHeader = requestContext.getHeaderString(TOKEN_HEADER);

        if (tokenHeader == null) {
            return false;
        }

        final String normalisedTokenHeader = tokenHeader.toLowerCase(Locale.ENGLISH);

        if (normalisedTokenHeader.equals("nocheck")) {
            log.warn(
                    "Use of the 'nocheck' value for {} "
                            + "has been deprecated since rest 3.0.0. Please use a value of "
                            + "'no-check' instead.",
                    TOKEN_HEADER);
            return true;
        }
        return false;
    }

    private boolean isSameOrigin(final String uri, final URI origin) {
        try {
            return StringUtils.isNotEmpty(uri) && SameOrigin.isSameOrigin(new URI(uri), origin);
        } catch (MalformedURLException | URISyntaxException | IllegalArgumentException e) {
            return false;
        }
    }

    private boolean isAllowedViaCors(final String originUri, final boolean withCredentials) {
        if (originUri == null) {
            return false;
        }
        return corsDefaults.stream()
                .anyMatch(delegate -> delegate.allowsOrigin(originUri)
                        && (!withCredentials || delegate.allowsCredentials(originUri)));
    }

    private static boolean containsCredentials(final ContainerRequestContext requestContext) {
        return containsCookies(requestContext) || containsHttpAuthHeader(requestContext);
    }

    private static boolean containsCookies(final ContainerRequestContext requestContext) {
        return !requestContext.getCookies().isEmpty();
    }

    private static boolean containsHttpAuthHeader(final ContainerRequestContext requestContext) {
        return StringUtils.isNotEmpty(requestContext.getHeaderString("Authorization"));
    }

    private boolean isPostRequest(final String method) {
        return method.equals("POST");
    }

    private boolean isLikelyToBeFromBrowser(final ContainerRequestContext requestContext) {
        final String userAgent = requestContext.getHeaderString("User-Agent");
        final UserAgentUtil.BrowserFamily browserFamily = new UserAgentUtilImpl().getBrowserFamily(userAgent);
        if ((passesStandardXsrfChecks(getRequestOrNull(httpContext)) || hasDeprecatedHeaderValue(requestContext))
                && BrowserUtils.isIE(userAgent)) {
            return false;
        }
        return !browserFamily.equals(UserAgentUtil.BrowserFamily.UKNOWN);
    }

    private static HttpServletRequest getRequestOrNull(final HttpContext httpContext) {
        return (httpContext == null) ? null : httpContext.getRequest();
    }

    private static String mediaTypeToString(MediaType mediaType) {
        return mediaType.getType().toLowerCase(Locale.ENGLISH) + "/"
                + mediaType.getSubtype().toLowerCase(Locale.ENGLISH);
    }

    private static String getSanitisedReferrer(final ContainerRequestContext requestContext) {
        return StringUtils.substringBefore(requestContext.getHeaderString("Referer"), "?");
    }
}
