package com.atlassian.plugins.rest.v2.servlet;

import java.io.IOException;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.plugins.rest.v2.util.ServletUtils;

@UnrestrictedAccess
public class RestServletUtilsUpdaterFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
        // Do nothing
    }

    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        // This sucks, I hate relying on ThreadLocals but we have to do that in order to use the SAL UserManager API.
        ServletUtils.setHttpServletRequest(request);
        try {
            filterChain.doFilter(request, response);
        } finally {
            ServletUtils.setHttpServletRequest(null);
        }
    }

    /**
     * Simply calls {@link #doFilterInternal(HttpServletRequest, HttpServletResponse, FilterChain)}
     *
     * @see #doFilterInternal(HttpServletRequest, HttpServletResponse, FilterChain)
     */
    public final void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        doFilterInternal((HttpServletRequest) request, (HttpServletResponse) response, chain);
    }

    @Override
    public void destroy() {
        // Do nothing
    }
}
