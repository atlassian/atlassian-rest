package com.atlassian.plugins.rest.v2.sal;

import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceRegistration;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;
import com.atlassian.sal.api.net.NonMarshallingRequestFactory;

/**
 * A {@link ServiceFactory} for {@link DefaultMarshallingRequestFactory}. It's used to pass {@link Plugin} to the {@link DefaultMarshallingRequestFactory}.
 */
public class MarshallingRequestFactoryServiceFactory implements ServiceFactory {
    private final PluginAccessor pluginAccessor;
    private final NonMarshallingRequestFactory nonMarshallingRequestFactory;

    public MarshallingRequestFactoryServiceFactory(
            PluginAccessor pluginAccessor, NonMarshallingRequestFactory nonMarshallingRequestFactory) {
        this.pluginAccessor = pluginAccessor;
        this.nonMarshallingRequestFactory = nonMarshallingRequestFactory;
    }

    public Object getService(final Bundle bundle, final ServiceRegistration serviceRegistration) {
        final Plugin plugin = pluginAccessor.getPlugin(OsgiHeaderUtil.getPluginKey(bundle));

        if (!(plugin instanceof ContainerManagedPlugin)) {
            throw new IllegalStateException("Can't create RequestFactory for plugin " + plugin + " "
                    + plugin.getClass().getCanonicalName() + " as it is not a ContainerManagedPlugin");
        } else {
            return new DefaultMarshallingRequestFactory(nonMarshallingRequestFactory, plugin);
        }
    }

    public void ungetService(final Bundle bundle, final ServiceRegistration serviceRegistration, final Object o) {}
}
