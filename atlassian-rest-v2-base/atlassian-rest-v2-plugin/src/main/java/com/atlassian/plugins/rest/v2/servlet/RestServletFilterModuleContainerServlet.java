package com.atlassian.plugins.rest.v2.servlet;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.plugin.servlet.ServletModuleManager;
import com.atlassian.plugin.servlet.filter.ServletFilterModuleContainerFilter;

import static java.util.Objects.requireNonNull;

/**
 * Copy of {@link com.atlassian.plugins.rest.module.servlet.RestServletFilterModuleContainerServlet}
 * This class is 100% the same as original one. It could be shared by both modules.
 */
@UnrestrictedAccess
public class RestServletFilterModuleContainerServlet extends ServletFilterModuleContainerFilter {
    private final ServletModuleManager servletModuleManager;

    public RestServletFilterModuleContainerServlet(RestServletModuleManager servletModuleManager) {
        this.servletModuleManager = requireNonNull(servletModuleManager);
    }

    @Override
    protected ServletModuleManager getServletModuleManager() {
        return servletModuleManager;
    }
}
