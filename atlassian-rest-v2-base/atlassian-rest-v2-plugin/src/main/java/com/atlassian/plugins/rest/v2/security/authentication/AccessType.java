package com.atlassian.plugins.rest.v2.security.authentication;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;

import com.atlassian.plugins.rest.api.security.annotation.AdminOnly;
import com.atlassian.plugins.rest.api.security.annotation.AnonymousSiteAccess;
import com.atlassian.plugins.rest.api.security.annotation.LicensedOnly;
import com.atlassian.plugins.rest.api.security.annotation.SystemAdminOnly;
import com.atlassian.plugins.rest.api.security.annotation.UnlicensedSiteAccess;
import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

/**
 * An enum class to provide access information based on the annotation related functionality.
 * <p>
 *
 * @since 8.0.0
 */
enum AccessType {
    EMPTY,
    SYSTEM_ADMIN_ONLY(SystemAdminOnly.class, com.atlassian.annotations.security.SystemAdminOnly.class),
    ADMIN_ONLY(AdminOnly.class, com.atlassian.annotations.security.AdminOnly.class),
    LICENSED_ONLY(LicensedOnly.class, com.atlassian.annotations.security.LicensedOnly.class),
    UNLICENSED_SITE_ACCESS(UnlicensedSiteAccess.class, com.atlassian.annotations.security.UnlicensedSiteAccess.class),
    ANONYMOUS_SITE_ACCESS(AnonymousSiteAccess.class, com.atlassian.annotations.security.AnonymousSiteAccess.class),
    UNRESTRICTED_ACCESS(UnrestrictedAccess.class, com.atlassian.annotations.security.UnrestrictedAccess.class);

    // key: fully qualified annotation class name -> value: enum
    private static final Map<String, AccessType> VALID_ANNOTATIONS = Arrays.stream(AccessType.values())
            .filter(accessType -> accessType.annotationTypes != null)
            .flatMap(accessType -> Arrays.stream(accessType.annotationTypes)
                    .map(annotationType -> Pair.of(annotationType.getName(), accessType)))
            .collect(Collectors.toMap(Pair::getLeft, Pair::getRight));

    private final Class<? extends Annotation>[] annotationTypes;

    @SafeVarargs
    AccessType(Class<? extends Annotation>... annotationTypes) {
        this.annotationTypes = annotationTypes;
    }

    static AccessType getAccessType(Class<?> clazz, Method method) {
        AnnotatedElement packageElement = clazz != null ? clazz.getPackage() : null;

        return Stream.of(method, clazz, packageElement)
                .filter(Objects::nonNull)
                .map(AccessType::extractFromAnnotation)
                .filter(Objects::nonNull)
                .findFirst()
                .orElse(EMPTY);
    }

    /**
     * Returns the {@link AccessType} type associated with the annotation attached to the object.
     * <p>
     * Annotations are compared based on name rather than class,
     * because classes may be loaded by different {@link ClassLoader}s.
     * Class loaders are taken into account when comparing classes.
     *
     * @param annotatedElement annotated object to investigate
     * @return {@link AccessType} or {@code null} if there aren't any annotations.
     */
    private static AccessType extractFromAnnotation(AnnotatedElement annotatedElement) {
        Annotation[] annotations = annotatedElement.getAnnotations();
        for (Annotation annotation : annotations) {
            // We compare annotation FQNs instead of annotation classes because of class loaders.
            // Class loaders are taken into account when comparing class objects, so we compare string FQNs instead.
            String annotationType = annotation.annotationType().getName();
            if (VALID_ANNOTATIONS.containsKey(annotationType)) {
                return VALID_ANNOTATIONS.get(annotationType);
            }
        }
        return null;
    }
}
