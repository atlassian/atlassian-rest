package com.atlassian.plugins.rest.v2.util.urlbuilder;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.UriBuilder;

/**
 * Base class used for creating invokable REST resource.
 */
public abstract class ResourceInvokable implements InvocationHandler {
    protected Class<?> resourceClass;
    private final URI baseUri;

    protected ResourceInvokable(final Class<?> resourceClass, final URI baseUri) {
        this.resourceClass = resourceClass;
        this.baseUri = baseUri;
    }

    protected Map<String, Object> buildParamMap(final Method method, final Object[] args) {
        Map<String, Object> pathParamMap = new HashMap<>();

        Annotation[][] allParameterAnnotations = method.getParameterAnnotations();

        for (int i = 0; i < allParameterAnnotations.length; i++) {
            Annotation[] parameterAnnotations = allParameterAnnotations[i];

            for (Annotation annotation : parameterAnnotations) {
                if (annotation instanceof PathParam && args[i] != null) {
                    pathParamMap.put(((PathParam) annotation).value(), args[i]);
                }
            }
        }

        return pathParamMap;
    }

    protected URI getURI(final Method method, final Object[] args) {
        final UriBuilder builder = UriBuilder.fromUri(baseUri).path(resourceClass);
        if (method.getAnnotation(Path.class) != null) {
            builder.path(method);
        }
        return builder.buildFromMap(buildParamMap(method, args));
    }
}
