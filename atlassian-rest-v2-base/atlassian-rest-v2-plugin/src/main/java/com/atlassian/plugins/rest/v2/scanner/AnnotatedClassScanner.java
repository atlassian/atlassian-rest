package com.atlassian.plugins.rest.v2.scanner;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarFile;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.osgi.framework.Bundle;

/**
 * Search for Java classes in the specified OSGi bundle that are annotated or have at least one method annotated with
 * one or more of a set of annotations.
 */
public final class AnnotatedClassScanner {
    private static final String REFERENCE_PROTOCOL = "reference:";
    private static final String FILE_PROTOCOL = "file:";

    private final Bundle bundle;
    private final Set<String> annotations;

    private boolean indexBundledJars;

    public AnnotatedClassScanner(Bundle bundle, boolean indexBundledJars, Class<?>... annotations) {
        Validate.notNull(bundle);
        Validate.notEmpty(annotations, "You gotta scan for something!");
        this.indexBundledJars = indexBundledJars;
        this.bundle = bundle;
        this.annotations = getAnnotationSet(annotations);
    }

    public Set<Class<?>> scan(Collection<String> basePackages) {
        final File bundleFile = getBundleFile(bundle);
        if (!bundleFile.isFile() || !bundleFile.exists()) {
            throw new AnnotatedScannerException("Could not identify Bundle at location <" + bundle.getLocation() + ">");
        }
        try (JarFile jarFile = new JarFile(bundleFile)) {
            return new JarIndexer(jarFile, preparePackages(basePackages), indexBundledJars, annotations, bundle)
                    .scanJar();
        } catch (IOException e) {
            throw new AnnotatedScannerException(e.getMessage());
        }
    }

    File getBundleFile(Bundle bundle) {
        String bundleLocation = bundle.getLocation();
        // Felix supports the reference protocol that is basically a pointer
        // to the original jar file used to install the bundle.
        if (bundleLocation.startsWith(REFERENCE_PROTOCOL)) {
            bundleLocation = bundleLocation.substring(REFERENCE_PROTOCOL.length());
        }
        final File bundleFile;
        if (bundleLocation.startsWith(FILE_PROTOCOL)) {
            try {
                bundleFile = new File(URLDecoder.decode(new URL(bundleLocation).getFile(), "UTF-8"));
            } catch (MalformedURLException e) {
                throw new AnnotatedScannerException(
                        "Could not parse Bundle location as URL <" + bundleLocation + ">", e);
            } catch (UnsupportedEncodingException e) {
                throw new IllegalStateException(
                        "Obviously something is wrong with your JVM... It doesn't support UTF-8 !?!", e);
            }
        } else {
            bundleFile = new File(bundleLocation);
        }
        return bundleFile;
    }

    private Set<String> preparePackages(Collection<String> packages) {
        final Set<String> packageNames = new HashSet<>();
        for (String packageName : packages) {
            final String newPackageName = StringUtils.replaceChars(packageName, '.', '/');

            // make sure we have a trailing / to not confuse packages such as com.mycompany.package
            // and com.mycompany.package1 which would both start with com/mycompany/package once transformed
            if (!newPackageName.endsWith("/")) {
                packageNames.add(newPackageName + '/');
            } else {
                packageNames.add(newPackageName);
            }
        }

        return packageNames;
    }

    Set<String> getAnnotationSet(Class<?>... annotations) {
        final Set<String> formattedAnnotations = new HashSet<>();
        for (Class<?> cls : annotations) {
            formattedAnnotations.add("L" + cls.getName().replace(".", "/") + ";");
        }
        return formattedAnnotations;
    }

    public void setIndexBundledJars(boolean indexBundledJars) {
        this.indexBundledJars = indexBundledJars;
    }
}
