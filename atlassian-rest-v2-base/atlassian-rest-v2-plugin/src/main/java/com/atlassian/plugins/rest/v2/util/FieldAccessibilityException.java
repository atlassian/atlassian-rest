package com.atlassian.plugins.rest.v2.util;

import java.lang.reflect.Field;

public class FieldAccessibilityException extends RuntimeException {
    public FieldAccessibilityException(Field field, Object target, Throwable cause) {
        super("Could not access '" + field.getName() + "' from '" + target + "'", cause);
    }
}
