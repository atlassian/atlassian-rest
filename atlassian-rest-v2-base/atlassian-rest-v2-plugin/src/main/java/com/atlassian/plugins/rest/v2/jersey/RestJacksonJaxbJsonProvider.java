package com.atlassian.plugins.rest.v2.jersey;

import jakarta.annotation.Priority;
import jakarta.inject.Inject;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.core.Configuration;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.ext.Provider;
import jakarta.ws.rs.ext.Providers;

import org.glassfish.jersey.jackson.internal.DefaultJacksonJaxbJsonProvider;
import org.glassfish.jersey.jackson.internal.JacksonAutoDiscoverable;
import org.glassfish.jersey.jackson.internal.jackson.jaxrs.cfg.JaxRSFeature;

import com.atlassian.plugins.rest.v2.json.ObjectMapperFactory;

/**
 * Jersey context will always register {@link DefaultJacksonJaxbJsonProvider},
 * because of {@link JacksonAutoDiscoverable} registered as Java Service.
 * <p>
 * {@link DefaultJacksonJaxbJsonProvider} doesn't define any {@link Priority}, so it defaults to {@link Priorities#USER}.
 * <p>
 * Our implementation will always be called first,
 * because it has exactly the same capabilities as {@link DefaultJacksonJaxbJsonProvider}
 * and defines higher priority (lower values have precedence).
 */
@Priority(Priorities.ENTITY_CODER)
@Provider
public class RestJacksonJaxbJsonProvider extends DefaultJacksonJaxbJsonProvider {

    @Inject
    public RestJacksonJaxbJsonProvider(@Context Providers providers, @Context Configuration config) {
        super(providers, config);
        setMapper(ObjectMapperFactory.createObjectMapper());
        disable(JaxRSFeature.ALLOW_EMPTY_INPUT);
    }
}
