package com.atlassian.plugins.rest.v2.xml;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.function.Supplier;
import javax.xml.stream.XMLInputFactory;

/**
 * A service provider for XMLInputFactory to enhance protection against XXE attacks. It overrides the
 * {@link org.glassfish.jersey.jaxb.internal.XmlInputFactoryInjectionProvider} from jersey, because jersey impl doesn't
 * provide security for Collection type entities.
 */
public class XmlInputFactoryInjectionProvider implements Supplier<XMLInputFactory> {

    private static final InputStream EMPTY_INPUT_STREAM = new ByteArrayInputStream(new byte[0]);

    @Override
    public XMLInputFactory get() {
        XMLInputFactory xif = XMLInputFactory.newInstance();
        xif.setProperty(XMLInputFactory.SUPPORT_DTD, Boolean.FALSE);
        xif.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, Boolean.FALSE);
        // Disable dtd validation
        xif.setXMLResolver((publicID, systemID, baseURI, namespace) -> EMPTY_INPUT_STREAM);
        return xif;
    }
}
