package com.atlassian.plugins.rest.v2.darkfeature;

import jakarta.annotation.Nonnull;
import jakarta.annotation.Priority;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.DynamicFeature;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.FeatureContext;
import jakarta.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugins.rest.api.darkfeature.RequiresDarkFeature;
import com.atlassian.sal.api.features.DarkFeatureManager;

import static java.util.Objects.requireNonNull;

import static com.atlassian.plugins.rest.v2.util.ReflectionUtils.getAnnotation;

@Priority(Priorities.AUTHORIZATION)
@Provider
public class DarkFeatureResourceDynamicFeature implements DynamicFeature {
    private static final Logger log = LoggerFactory.getLogger(DarkFeatureResourceDynamicFeature.class);

    private final DarkFeatureManager darkFeatureManager;

    public DarkFeatureResourceDynamicFeature(@Nonnull final DarkFeatureManager darkFeatureManager) {
        this.darkFeatureManager = requireNonNull(darkFeatureManager);
    }

    @Override
    public void configure(ResourceInfo resourceInfo, FeatureContext context) {
        if (getAnnotation(RequiresDarkFeature.class, resourceInfo.getResourceMethod()) != null
                || getAnnotation(RequiresDarkFeature.class, resourceInfo.getResourceClass()) != null) {
            log.debug("RequiresDarkFeature annotation found - creating filter");
            context.register(new DarkFeatureResourceFilter(resourceInfo, darkFeatureManager));
            return;
        }

        log.debug("No RequiresDarkFeature annotation found - not creating filter");
    }
}
