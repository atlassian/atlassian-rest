package com.atlassian.plugins.rest.v2.security.cors;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import jakarta.annotation.Priority;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugins.rest.api.internal.security.cors.CorsDefaults;

import static java.util.stream.Collectors.toList;

import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS;
import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.ACCESS_CONTROL_ALLOW_HEADERS;
import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.ACCESS_CONTROL_ALLOW_METHODS;
import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN;
import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.ACCESS_CONTROL_EXPOSE_HEADERS;
import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.ACCESS_CONTROL_MAX_AGE;
import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.ACCESS_CONTROL_REQUEST_HEADERS;
import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.ACCESS_CONTROL_REQUEST_METHOD;
import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.CORS_PREFLIGHT_REQUESTED;
import static com.atlassian.plugins.rest.api.internal.security.cors.CorsHeaders.ORIGIN;

/**
 * A filter that handles Cross-Origin Resource Sharing preflight checks and response headers. Handles simple and preflight
 * requests.
 * <p>
 * See spec at <a href="http://www.w3.org/TR/cors">http://www.w3.org/TR/cors</a>
 *
 * @since 2.6
 */
@Priority(Priorities.HEADER_DECORATOR)
@Provider
public class CorsResourceFilter implements ContainerRequestFilter, ContainerResponseFilter {
    public static final String CORS_PREFLIGHT_FAILED = "Cors-Preflight-Failed";
    public static final String CORS_PREFLIGHT_SUCCEEDED = "Cors-Preflight-Succeeded";

    private static final Logger log = LoggerFactory.getLogger(CorsResourceFilter.class);

    private final String allowMethod;
    private final CorsDefaultService corsDefaultService;

    public CorsResourceFilter(String allowMethod, CorsDefaultService corsDefaultService) {
        this.allowMethod = allowMethod;
        this.corsDefaultService = corsDefaultService;
    }

    /**
     * Adds the appropriate <a href="http://www.w3.org/TR/cors/#resource-preflight-requests">
     * cors preflight </a>
     * response headers for cors preflight requests from a whitelisted origin.
     *
     * @param requestContext the request context.
     */
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        if (!requestContext.getPropertyNames().contains(CORS_PREFLIGHT_REQUESTED)) {
            return;
        }
        try {
            String origin = validateSingleOriginInWhitelist(corsDefaultService.getCorsDefaults(), requestContext);
            List<CorsDefaults> defaultsWithAllowedOrigin = allowsOrigin(corsDefaultService.getCorsDefaults(), origin);

            Response.ResponseBuilder response = Response.ok();
            validateAccessControlRequestMethod(allowMethod, requestContext);
            Set<String> allowedRequestHeaders = getAllowedRequestHeaders(defaultsWithAllowedOrigin, origin);
            validateAccessControlRequestHeaders(allowedRequestHeaders, requestContext);

            addAccessControlAllowOrigin(response, origin);
            conditionallyAddAccessControlAllowCredentials(response, origin, defaultsWithAllowedOrigin);
            addAccessControlMaxAge(response);
            addAccessControlAllowMethods(response, allowMethod);
            addAccessControlAllowHeaders(response, allowedRequestHeaders);

            requestContext.setProperty(CORS_PREFLIGHT_SUCCEEDED, "true");
            requestContext.abortWith(response.build());
        } catch (PreflightFailedException ex) {
            Response.ResponseBuilder response = Response.ok();
            requestContext.setProperty(CORS_PREFLIGHT_FAILED, "true");
            log.info("CORS preflight failed: {}", ex.getMessage());
            requestContext.abortWith(response.build());
        }
    }

    /**
     * Adds the appropriate cors response headers to the response of
     * <a href="http://www.w3.org/TR/cors/#resource-requests">cors requests</a>
     * from a whitelisted origin.
     *
     * @param requestContext  the request context.
     * @param responseContext the response context.
     */
    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
            throws IOException {
        if (requestContext.getProperty(CORS_PREFLIGHT_FAILED) != null
                || requestContext.getProperty(CORS_PREFLIGHT_SUCCEEDED) != null
                || extractOrigin(requestContext) == null) {
            return;
        }

        try {
            String origin = validateSingleOriginInWhitelist(corsDefaultService.getCorsDefaults(), requestContext);
            List<CorsDefaults> defaultsWithAllowedOrigin = allowsOrigin(corsDefaultService.getCorsDefaults(), origin);

            addAccessControlAllowOrigin(responseContext, origin);
            conditionallyAddAccessControlAllowCredentials(responseContext, origin, defaultsWithAllowedOrigin);
            addAccessControlExposeHeaders(
                    responseContext, getAllowedResponseHeaders(defaultsWithAllowedOrigin, origin));
        } catch (PreflightFailedException ex) {
            log.info("Unable to add CORS headers to response: {}", ex.getMessage());
        }
    }

    private void addAccessControlExposeHeaders(ContainerResponseContext response, Set<String> allowedHeaders) {
        response.getHeaders()
                .put(
                        ACCESS_CONTROL_EXPOSE_HEADERS.value(),
                        Collections.singletonList(String.join(", ", allowedHeaders)));
    }

    private void addAccessControlAllowHeaders(Response.ResponseBuilder response, Set<String> allowedHeaders) {
        response.header(ACCESS_CONTROL_ALLOW_HEADERS.value(), String.join(", ", allowedHeaders));
    }

    private void addAccessControlAllowMethods(Response.ResponseBuilder response, String allowMethod) {
        response.header(ACCESS_CONTROL_ALLOW_METHODS.value(), allowMethod);
    }

    private void addAccessControlMaxAge(Response.ResponseBuilder response) {
        response.header(ACCESS_CONTROL_MAX_AGE.value(), 60 * 60);
    }

    private void addAccessControlAllowOrigin(Response.ResponseBuilder response, String origin) {
        response.header(ACCESS_CONTROL_ALLOW_ORIGIN.value(), origin);
    }

    private void addAccessControlAllowOrigin(ContainerResponseContext responseContext, String origin) {
        responseContext.getHeaders().put(ACCESS_CONTROL_ALLOW_ORIGIN.value(), Collections.singletonList(origin));
    }

    private void conditionallyAddAccessControlAllowCredentials(
            Response.ResponseBuilder response, String origin, Iterable<CorsDefaults> defaultsWithAllowedOrigin) {
        if (anyAllowsCredentials(defaultsWithAllowedOrigin, origin)) {
            response.header(ACCESS_CONTROL_ALLOW_CREDENTIALS.value(), "true");
        }
    }

    private void conditionallyAddAccessControlAllowCredentials(
            ContainerResponseContext responseContext, String origin, Iterable<CorsDefaults> defaultsWithAllowedOrigin) {
        if (anyAllowsCredentials(defaultsWithAllowedOrigin, origin)) {
            responseContext
                    .getHeaders()
                    .put(ACCESS_CONTROL_ALLOW_CREDENTIALS.value(), Collections.singletonList("true"));
        }
    }

    private void validateAccessControlRequestHeaders(Set<String> allowedHeaders, ContainerRequestContext requestContext)
            throws PreflightFailedException {
        List<String> requestedHeaders = requestContext.getHeaders().get(ACCESS_CONTROL_REQUEST_HEADERS.value());
        requestedHeaders = requestedHeaders != null ? requestedHeaders : Collections.emptyList();
        Set<String> flatRequestedHeaders = new HashSet<>();
        for (String requestedHeader : requestedHeaders) {
            flatRequestedHeaders.addAll(
                    Arrays.asList(requestedHeader.toLowerCase(Locale.US).trim().split("\\s*,\\s*")));
        }
        Set<String> allowedHeadersLowerCase =
                allowedHeaders.stream().map(from -> from.toLowerCase(Locale.US)).collect(Collectors.toSet());
        Set<String> difference = new HashSet<>(flatRequestedHeaders);
        difference.removeAll(allowedHeadersLowerCase);
        if (!difference.isEmpty()) {
            throw new PreflightFailedException("Unexpected headers in CORS request: " + new ArrayList<>(difference));
        }
    }

    private void validateAccessControlRequestMethod(String allowMethod, ContainerRequestContext requestContext)
            throws PreflightFailedException {
        String requestedMethod = requestContext.getHeaderString(ACCESS_CONTROL_REQUEST_METHOD.value());
        if (!allowMethod.equals(requestedMethod)) {
            throw new PreflightFailedException("Invalid method: " + requestedMethod);
        }
    }

    private String validateSingleOriginInWhitelist(
            Iterable<CorsDefaults> defaults, ContainerRequestContext requestContext) throws PreflightFailedException {
        String origin = extractOrigin(requestContext);
        validateOriginAsUri(origin);

        List<CorsDefaults> allowsOriginResult = allowsOrigin(defaults, origin);
        if (allowsOriginResult.isEmpty()) {
            throw new PreflightFailedException("Origin '" + origin + "' not in whitelist");
        }
        return origin;
    }

    private void validateOriginAsUri(String origin) throws PreflightFailedException {
        try {
            final URI originUri = URI.create(origin);
            if (originUri.isOpaque() || !originUri.isAbsolute()) {
                throw new IllegalArgumentException("The origin URI must be absolute and not opaque.");
            }
        } catch (IllegalArgumentException ex) {
            throw new PreflightFailedException("Origin '" + origin + "' is not a valid URI");
        }
    }

    public static String extractOrigin(ContainerRequestContext requestContext) {
        return requestContext.getHeaderString(ORIGIN.value());
    }

    /**
     * Thrown if the preflight or simple cross-origin check process fails
     */
    private static class PreflightFailedException extends Exception {
        private PreflightFailedException(String message) {
            super(message);
        }
    }

    private static List<CorsDefaults> allowsOrigin(Iterable<CorsDefaults> delegates, final String uri) {
        return StreamSupport.stream(delegates.spliterator(), false)
                .filter(delegate -> delegate.allowsOrigin(uri))
                .collect(toList());
    }

    private static boolean anyAllowsCredentials(Iterable<CorsDefaults> delegatesWhichAllowOrigin, final String uri) {
        for (CorsDefaults defs : delegatesWhichAllowOrigin) {
            if (defs.allowsCredentials(uri)) {
                return true;
            }
        }
        return false;
    }

    private static Set<String> getAllowedRequestHeaders(List<CorsDefaults> delegatesWhichAllowOrigin, String uri) {
        Set<String> result = new HashSet<>();
        for (CorsDefaults defs : delegatesWhichAllowOrigin) {
            result.addAll(defs.getAllowedRequestHeaders(uri));
        }
        return result;
    }

    private static Set<String> getAllowedResponseHeaders(List<CorsDefaults> delegatesWithAllowedOrigin, String uri) {
        Set<String> result = new HashSet<>();
        for (CorsDefaults defs : delegatesWithAllowedOrigin) {
            result.addAll(defs.getAllowedResponseHeaders(uri));
        }
        return result;
    }
}
