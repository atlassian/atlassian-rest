package com.atlassian.plugins.rest.v2.sal;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import jakarta.ws.rs.core.MediaType;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugins.rest.v2.ChainingClassLoader;
import com.atlassian.plugins.rest.v2.exception.jersey.EntityConversionException;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;

import static com.google.common.collect.Maps.transformValues;
import static jakarta.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static jakarta.ws.rs.core.MediaType.APPLICATION_XML_TYPE;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Implementation of the {@link Response} with ability to get entities.
 */
public class MarshallingResponse implements Response {
    private final Response delegateResponse;
    private final MarshallingEntityHandler marshallingEntityHandler;
    private final Plugin plugin;

    public MarshallingResponse(
            Response delegateResponse, MarshallingEntityHandler marshallingEntityHandler, Plugin plugin) {
        this.delegateResponse = delegateResponse;
        this.marshallingEntityHandler = marshallingEntityHandler;
        this.plugin = plugin;
    }

    public <T> T getEntity(final Class<T> entityClass) throws ResponseException {
        final InputStream entityStream = getResponseBodyAsStream();
        final MediaType contentType = getContentType(APPLICATION_XML_TYPE);
        final Map<String, List<String>> unmarshallingHeaders = getUnmarshallingHeaders();

        final ClassLoader oldClassLoader = Thread.currentThread().getContextClassLoader();
        final ChainingClassLoader chainingClassLoader = getChainingClassLoader(plugin);
        try {
            Thread.currentThread().setContextClassLoader(chainingClassLoader);
            return marshallingEntityHandler.unmarshall(entityClass, contentType, entityStream, unmarshallingHeaders);
        } catch (IOException e) {
            throw new EntityConversionException(e);
        } finally {
            Thread.currentThread().setContextClassLoader(oldClassLoader);
        }
    }

    private Map<String, List<String>> getUnmarshallingHeaders() {
        return transformValues(getHeaders(), Collections::singletonList);
    }

    private MediaType getContentType(final MediaType defaultType) {
        final String headerValue = getHeader(CONTENT_TYPE);
        if (isNotBlank(headerValue)) {
            return MediaType.valueOf(headerValue);
        } else {
            return defaultType;
        }
    }

    public int getStatusCode() {
        return delegateResponse.getStatusCode();
    }

    public String getResponseBodyAsString() throws ResponseException {
        return delegateResponse.getResponseBodyAsString();
    }

    public InputStream getResponseBodyAsStream() throws ResponseException {
        return delegateResponse.getResponseBodyAsStream();
    }

    public String getStatusText() {
        return delegateResponse.getStatusText();
    }

    public boolean isSuccessful() {
        return delegateResponse.isSuccessful();
    }

    public String getHeader(final String name) {
        return delegateResponse.getHeader(name);
    }

    public Map<String, String> getHeaders() {
        return delegateResponse.getHeaders();
    }

    private ChainingClassLoader getChainingClassLoader(final Plugin plugin) {
        return new ChainingClassLoader(getClass().getClassLoader(), plugin.getClassLoader());
    }
}
