package com.atlassian.plugins.rest.v2;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.commons.lang3.Validate;

/**
 * A class loader that delegates to a list of class loaders. The order is important as classes and resources will be
 * loaded from the first classloader that can load them.
 */
public class ChainingClassLoader extends ClassLoader {

    /**
     * The list of classloader to delegate to.
     */
    private final List<ClassLoader> classLoaders;

    public ChainingClassLoader(ClassLoader... classLoaders) {
        Validate.noNullElements(classLoaders, "ClassLoader arguments cannot be null");
        this.classLoaders = Arrays.asList(classLoaders);
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        for (ClassLoader classloader : classLoaders) {
            try {
                return classloader.loadClass(name);
            } catch (ClassNotFoundException e) {
                // ignoring until we reach the end of the list since we are chaining
            }
        }
        throw new ClassNotFoundException(name);
    }

    @Override
    public Enumeration<URL> getResources(String name) throws IOException {
        return new ResourcesEnumeration(name, classLoaders);
    }

    @Override
    public URL getResource(String name) {
        for (ClassLoader classloader : classLoaders) {
            final URL url = classloader.getResource(name);
            if (url != null) {
                return url;
            }
        }
        return null;
    }

    @Override
    public InputStream getResourceAsStream(String name) {
        for (ClassLoader classloader : classLoaders) {
            final InputStream inputStream = classloader.getResourceAsStream(name);
            if (inputStream != null) {
                return inputStream;
            }
        }

        return null;
    }

    @Override
    public synchronized void setDefaultAssertionStatus(boolean enabled) {
        for (ClassLoader classloader : classLoaders) {
            classloader.setDefaultAssertionStatus(enabled);
        }
    }

    @Override
    public synchronized void setPackageAssertionStatus(String packageName, boolean enabled) {
        for (ClassLoader classloader : classLoaders) {
            classloader.setPackageAssertionStatus(packageName, enabled);
        }
    }

    @Override
    public synchronized void setClassAssertionStatus(String className, boolean enabled) {
        for (ClassLoader classloader : classLoaders) {
            classloader.setClassAssertionStatus(className, enabled);
        }
    }

    @Override
    public synchronized void clearAssertionStatus() {
        for (ClassLoader classloader : classLoaders) {
            classloader.clearAssertionStatus();
        }
    }

    private static final class ResourcesEnumeration implements Enumeration<URL> {
        private final List<Enumeration<URL>> resources;
        private final String resourceName;

        ResourcesEnumeration(String resourceName, List<ClassLoader> classLoaders) throws IOException {
            this.resourceName = resourceName;
            this.resources = new LinkedList<>();
            for (ClassLoader classLoader : classLoaders) {
                resources.add(classLoader.getResources(resourceName));
            }
        }

        public boolean hasMoreElements() {
            for (Enumeration<URL> resource : resources) {
                if (resource.hasMoreElements()) {
                    return true;
                }
            }

            return false;
        }

        public URL nextElement() {
            for (Enumeration<URL> resource : resources) {
                if (resource.hasMoreElements()) {
                    return resource.nextElement();
                }
            }
            throw new NoSuchElementException(resourceName);
        }
    }
}
