package com.atlassian.plugins.rest.v2.expand.resolver;

import java.util.LinkedList;
import java.util.List;

import com.atlassian.plugins.rest.api.expand.EntityCrawler;
import com.atlassian.plugins.rest.api.expand.ExpandContext;
import com.atlassian.plugins.rest.api.expand.expander.EntityExpander;
import com.atlassian.plugins.rest.api.expand.resolver.EntityExpanderResolver;
import com.atlassian.plugins.rest.v2.expand.DefaultExpandContext;

/**
 * An {@link EntityExpanderResolver} that can find {@link EntityExpander entity expanders} for {@link List}.
 * Doesn't support indexing.
 */
public class ListEntityExpanderResolver implements EntityExpanderResolver {

    private static final ListExpander EXPANDER = new ListExpander();

    public boolean hasExpander(Class<?> type) {
        return List.class.isAssignableFrom(type);
    }

    @SuppressWarnings("unchecked")
    public <T> EntityExpander<T> getExpander(Class<? extends T> type) {
        return List.class.isAssignableFrom(type) ? (EntityExpander<T>) EXPANDER : null;
    }

    private static class ListExpander implements EntityExpander<List> {
        public List expand(
                ExpandContext<List> context, EntityExpanderResolver expanderResolver, EntityCrawler entityCrawler) {
            final List list = new LinkedList();
            for (Object item : context.getEntity()) {
                final ExpandContext<Object> itemContext =
                        new DefaultExpandContext<>(item, context.getExpandable(), context.getEntityExpandParameter());
                final EntityExpander<Object> entityExpander =
                        item != null ? expanderResolver.getExpander(item.getClass()) : null;
                list.add(
                        entityExpander != null
                                ? entityExpander.expand(itemContext, expanderResolver, entityCrawler)
                                : item);
            }
            return list;
        }
    }
}
