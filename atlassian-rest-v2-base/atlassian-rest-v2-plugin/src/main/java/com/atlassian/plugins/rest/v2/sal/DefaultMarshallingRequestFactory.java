package com.atlassian.plugins.rest.v2.sal;

import java.lang.reflect.Proxy;

import io.atlassian.util.concurrent.LazyReference;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugins.rest.v2.ChainingClassLoader;
import com.atlassian.plugins.rest.v2.util.ContextClassLoaderSwitchingProxy;
import com.atlassian.sal.api.net.MarshallingRequestFactory;
import com.atlassian.sal.api.net.NonMarshallingRequestFactory;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;

/**
 * Implementation of the {@link MarshallingRequestFactory} which supplies the {@link Request} with ability to handle entities.
 * So if a plugin uses this request factory, it can attach an entity to the request, using the {@link Request#setEntity(Object)}
 * method.
 */
public class DefaultMarshallingRequestFactory implements MarshallingRequestFactory {
    private final RequestFactory<? extends Request> delegateRequestFactory;
    private final Plugin plugin;

    private final LazyReference<MarshallingEntityHandler> marshallingEntityHandlerReference =
            new LazyReference<MarshallingEntityHandler>() {
                @Override
                protected MarshallingEntityHandler create() {
                    ClassLoader oldClassLoader = Thread.currentThread().getContextClassLoader();
                    ChainingClassLoader chainingClassLoader = getChainingClassLoader(plugin);
                    try {
                        Thread.currentThread().setContextClassLoader(chainingClassLoader);
                        return new MarshallingEntityHandler((OsgiPlugin) plugin);
                    } finally {
                        Thread.currentThread().setContextClassLoader(oldClassLoader);
                    }
                }
            };

    public DefaultMarshallingRequestFactory(
            final NonMarshallingRequestFactory<? extends Request> delegateRequestFactory, Plugin plugin) {
        this.plugin = plugin;
        this.delegateRequestFactory = delegateRequestFactory;
    }

    public Request createRequest(final Request.MethodType methodType, final String s) {
        final Request delegateRequest = delegateRequestFactory.createRequest(methodType, s);
        final MarshallingRequest request =
                new MarshallingRequest(delegateRequest, marshallingEntityHandlerReference.get(), plugin);
        return (Request) Proxy.newProxyInstance(
                getClass().getClassLoader(),
                new Class[] {Request.class},
                new ContextClassLoaderSwitchingProxy(request, getChainingClassLoader(plugin)));
    }

    public boolean supportsHeader() {
        return delegateRequestFactory.supportsHeader();
    }

    private ChainingClassLoader getChainingClassLoader(final Plugin plugin) {
        return new ChainingClassLoader(getClass().getClassLoader(), plugin.getClassLoader());
    }
}
