package com.atlassian.plugins.rest.v2.version;

import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jakarta.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import static java.util.Objects.requireNonNull;

/**
 * Copy of {@link com.atlassian.plugins.rest.common.version.ApiVersion}
 * This class is 100% the same as original one. It could be shared by both modules.
 */
public class ApiVersion implements Comparable {
    public static final String NONE_STRING = "none";
    public static final ApiVersion NONE = new ApiVersion(NONE_STRING);

    private static final String DOT = ".";

    private static final Pattern VERSION_PATTERN =
            Pattern.compile("(\\d+)(?:\\.(\\d+))?(?:\\.(\\d+))?(?:\\.([\\w-]*))?");

    private final Integer major;

    private final Integer minor;

    private final Integer micro;

    private final String classifier;

    private static RuntimeException defaultVersionExceptionMapping(String version) {
        requireNonNull(version);
        throw new IllegalArgumentException(version);
    }

    public ApiVersion(String version) {
        this(version, new Function<String, RuntimeException>() {
            @Nullable
            @Override
            public RuntimeException apply(String input) {
                return defaultVersionExceptionMapping(input);
            }
        });
    }

    protected ApiVersion(String version, Function<String, RuntimeException> exceptionMapper) {
        if (version == null) {
            throw exceptionMapper.apply(version);
        }

        // forcing clients to specify a "version=none" string in the URL prevents then from accidentally going
        // versionless
        if (NONE_STRING.equals(version)) {
            major = minor = micro = null;
            classifier = null;
        } else {
            final Matcher matcher = VERSION_PATTERN.matcher(version);
            if (!matcher.matches()) {
                throw exceptionMapper.apply(version);
            }

            major = Integer.valueOf(matcher.group(1));
            minor = matcher.group(2) != null ? Integer.valueOf(matcher.group(2)) : null;
            micro = matcher.group(3) != null ? Integer.valueOf(matcher.group(3)) : null;
            classifier = matcher.group(4);
        }
    }

    public boolean isNone() {
        return this.equals(NONE);
    }

    public static boolean isNone(String version) {
        return NONE_STRING.equals(version);
    }

    public Integer getMajor() {
        return major;
    }

    public Integer getMinor() {
        return minor;
    }

    public Integer getMicro() {
        return micro;
    }

    public String getClassifier() {
        return classifier;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof ApiVersion)) {
            return false;
        }

        final ApiVersion version = (ApiVersion) o;
        return new EqualsBuilder()
                .append(this.major, version.major)
                .append(this.minor, version.minor)
                .append(this.micro, version.micro)
                .append(this.classifier, version.classifier)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(3, 41)
                .append(major)
                .append(minor)
                .append(micro)
                .append(classifier)
                .toHashCode();
    }

    @Override
    public int compareTo(Object o) {
        if (o == null) {
            return 1;
        }
        if (o == this) {
            return 0;
        }
        if (!(o instanceof ApiVersion)) {
            return 1;
        }

        final ApiVersion that = (ApiVersion) o;

        final int majorDifference = compare(this.major, that.major);
        if (majorDifference != 0) {
            return majorDifference;
        }
        final int minorDifference = compare(this.minor, that.minor);
        if (minorDifference != 0) {
            return minorDifference;
        }
        final int microDifference = compare(this.micro, that.micro);
        if (microDifference != 0) {
            return microDifference;
        }
        return compare(this.classifier, that.classifier);
    }

    private static <T extends Comparable<T>> int compare(T n, T m) {
        if (n == null && m == null) {
            return 0;
        }
        if (n == null) {
            return -1;
        }
        if (m == null) {
            return +1;
        }
        return n.compareTo(m);
    }

    @Override
    public String toString() {
        return isNone()
                ? NONE_STRING
                : major
                        + (minor != null ? DOT + minor : "")
                        + (micro != null ? DOT + micro : "")
                        + (StringUtils.isNotBlank(classifier) ? DOT + classifier : "");
    }
}
