package com.atlassian.plugins.rest.v2.expand.resolver;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.atlassian.plugins.rest.api.expand.EntityCrawler;
import com.atlassian.plugins.rest.api.expand.ExpandContext;
import com.atlassian.plugins.rest.api.expand.expander.EntityExpander;
import com.atlassian.plugins.rest.api.expand.listwrapper.ListWrapper;
import com.atlassian.plugins.rest.api.expand.resolver.EntityExpanderResolver;
import com.atlassian.plugins.rest.api.internal.expand.resolver.ListWrapperEntityExpanderResolver;

import static java.util.Objects.requireNonNull;

import static com.atlassian.plugins.rest.v2.util.ReflectionUtils.setFieldValue;

/**
 * @see ListWrapperEntityExpanderResolver
 */
public class ListWrapperEntityExpanderResolverImpl implements ListWrapperEntityExpanderResolver {

    static final ListWrapperEntityExpander EXPANDER = new ListWrapperEntityExpander();

    public boolean hasExpander(Class<?> type) {
        return ListWrapper.class.isAssignableFrom(requireNonNull(type));
    }

    public <T> EntityExpander<T> getExpander(Class<? extends T> type) {
        return ListWrapper.class.isAssignableFrom(type) ? (EntityExpander<T>) EXPANDER : null;
    }

    static class ListWrapperEntityExpander<T> implements EntityExpander<ListWrapper<T>> {
        public ListWrapper<T> expand(
                ExpandContext<ListWrapper<T>> context,
                EntityExpanderResolver expanderResolver,
                EntityCrawler entityCrawler) {
            final ListWrapper<T> entity = context.getEntity();

            final Set<Field> collectionFields = new HashSet<>();
            for (Class cls = entity.getClass(); cls != null; cls = cls.getSuperclass()) {
                final Field[] fields = cls.getDeclaredFields();
                for (Field field : fields) {
                    if (Collection.class.isAssignableFrom(field.getType())) {
                        collectionFields.add(field);
                    }
                }
            }
            if (collectionFields.isEmpty()) {
                throw new RuntimeException("Entity " + entity.getClass() + " has no collection field, cannot expand.");
            }
            if (collectionFields.size() > 1) {
                throw new RuntimeException("Entity " + entity.getClass()
                        + " has more than one collection field, cannot determine which collection to expand.");
            }

            setFieldValue(
                    collectionFields.iterator().next(),
                    entity,
                    entity.getCallback()
                            .getItems(context.getEntityExpandParameter().getIndexes(context.getExpandable())));

            entityCrawler.crawl(
                    entity,
                    context.getEntityExpandParameter().getExpandParameter(context.getExpandable()),
                    expanderResolver);
            return entity;
        }
    }
}
