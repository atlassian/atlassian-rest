# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [7.1.7]

### Changed
* [REST-451] Reinstate deleted method in CommonsFileUploadMultipartHandler to maintain backwards compatibility

## [7.1.6]

### Changed
* [REST-451] Fix backwards compatibility for injecting MarshallingRequestFactory

## [7.1.5]

### Security
* [REST-451] Added configuration option for multipart request part limit that defaults to 1000

## [7.1.4]

### Security
* [REST-439] Fixed AnonymousSiteAccess annotation allowing unlicensed users when anonymous access enabled

## [7.1.3]

### Security
* [CONFSERVER-75222]:Introduced new system property to increase jar indexer threshold limit and also hard coded file separator in JarIndexer
* [REST-439] Introduced new annotations to support correct REST resource security enforcement

## [6.2.2]

### Changed
* [BSP-4328] Bump spring beans version

## [6.2.0]

### Changed
* [ITAS-99] Add profiling metrics to a rest filter

## [6.1.14]

### Changed
* [BSP-4328] Bump spring beans version

## [6.1.12]

### Changed
* [BSP-3315] Remove redundant OSGi import of `org.apache.log4j` package

## [6.1.6] - Unreleased

### Fixed
* [BSP-1459] Replace felix framework 4.0.0 dependency and dependencyManagement with osgi.core 6.0.0, avoiding dependency
   management conflicts in consumers of the com.atlassian.jersey-library bom this project provides.

## [6.1.5] 

### Fixed
* [RAID-1901] (Forward-port from 6.0.7) Fix `JerseyResponse` to remove case-sensitive extraction of Content-Type header

## [6.0.12]

### Changed
* [BSP-4328] Bump spring beans version

## [6.0.11]

### Changed
* [BSP-3315] Remove redundant OSGi import of `org.apache.log4j` package

## [6.0.7] 

### Fixed
* [RAID-1901] Fix `JerseyResponse` to remove case-sensitive extraction of Content-Type header

## [6.1.4]

### Security
* [BSP-1263]: updated jackson to version '1.9.13-atlassian-5' containing security fixes

## [6.0.6] - 2020-05-18

### Security
* [BSP-1263]: updated jackson to version '1.9.13-atlassian-5' containing security fixes

## [6.0.0]

### Changed
* [REST-421]: Updated dependency versions to make this library Platform 5 and Java 11 compatible

## [3.5.4] - Unreleased

### Security
* [BSP-1263]: updated jackson to version '1.9.13-atlassian-5' containing security fixes

## [3.4.15]

### Security
* [BSP-1263]: updated jackson to version '1.9.13-atlassian-5' containing security fixes

## [3.5.0 - Unreleased]

### Added
* [REST-420]: Updates for compatibility with new plugin framework ServletModuleManager methods for working with
DispatcherType enums.

## [3.2.21] - 2020-05-12

### Security
* [BSP-1263]: updated jackson to version '1.9.13-atlassian-5' containing security fixes

## [3.2.20] - 2019-11-04

### Fixed
* [REST-428]: fixed jar scanning throwing exceptions on 'module-info.class'

## [3.2.19] - 2019-10-29

### Security
* [BSP-610]: updated jackson to version '1.9.13-atlassian-4' containing security fixes

## [3.2.18] - 2018-05-22

### Fixed
* [REST-417]: fixed urls containing IPv6 IP addresses not being handled correctly

[3.2.18]: https://bitbucket.org/atlassian/atlassian-rest/branches/compare/atlassian-rest-parent-3.2.18%0Datlassian-rest-parent-3.2.17
[REST-417]: https://ecosystem.atlassian.net/browse/REST-417
[REST-439]: https://ecosystem.atlassian.net/browse/REST-439
[REST-451]: https://ecosystem.atlassian.net/browse/REST-451
