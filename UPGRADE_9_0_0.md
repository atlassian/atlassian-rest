# Atlassian REST 9.0 upgrade guide

## JavaX -> Jakarta

Jakarta 10 changed java package namespaces from `javax.*` to `jakarta.*`. 
This means that all imports pointing to `javax.*` packages are now `jakarta.*`. 
This change affects the following classes:

### Public classes

| Class name                                                                   |
|------------------------------------------------------------------------------|
| `com.atlassian.plugins.rest.api.expand.listwrapper.AbstractPagedListWrapper` |
| `com.atlassian.plugins.rest.api.model.Link`                                  |
| `com.atlassian.plugins.rest.api.model.Status`                                |
| `com.atlassian.plugins.rest.api.multipart.MultipartHandler`                  |
| `com.atlassian.plugins.rest.api.security.exception.XsrfCheckFailedException` |

### Private classes

| Class name                                                                               |
|------------------------------------------------------------------------------------------|
| `com.atlassian.plugins.rest.processors.DeprecationAnnotationProcessor`                   |
| `com.atlassian.plugins.rest.processors.EnricherUtils`                                    |
| `com.atlassian.plugins.rest.v2.darkfeature.DarkFeatureResourceDynamicFeature`            |
| `com.atlassian.plugins.rest.v2.darkfeature.DarkFeatureResourceFilter`                    |
| `com.atlassian.plugins.rest.v2.descriptor.RestModuleDescriptor`                          |
| `com.atlassian.plugins.rest.v2.descriptor.RestServletFilterModuleDescriptor`             |
| `com.atlassian.plugins.rest.v2.exception.NotFoundExceptionMapper`                        |
| `com.atlassian.plugins.rest.v2.exception.SecurityExceptionMapper`                        |
| `com.atlassian.plugins.rest.v2.exception.ThrowableExceptionMapper`                       |
| `com.atlassian.plugins.rest.v2.exception.UncaughtExceptionEntityWriter`                  |
| `com.atlassian.plugins.rest.v2.exception.entity.UncaughtExceptionEntity`                 |
| `com.atlassian.plugins.rest.v2.expand.DefaultEntityCrawler`                              |
| `com.atlassian.plugins.rest.v2.expand.ExpandFilter`                                      |
| `com.atlassian.plugins.rest.v2.filter.DeprecationFilter`                                 |
| `com.atlassian.plugins.rest.v2.filter.ExtensionJerseyFilter`                             |
| `com.atlassian.plugins.rest.v2.jersey.AtlassianFilterUrlMappingsProviderImpl`            |
| `com.atlassian.plugins.rest.v2.jersey.JerseyOsgiServletContainer`                        |
| `com.atlassian.plugins.rest.v2.jersey.ResourceConfigFactory`                             |
| `com.atlassian.plugins.rest.v2.jersey.RestJacksonJaxbJsonProvider`                       |
| `com.atlassian.plugins.rest.v2.jersey.SpringInTimeResolver`                              |
| `com.atlassian.plugins.rest.v2.json.DefaultJaxbJsonMarshaller`                           |
| `com.atlassian.plugins.rest.v2.multipart.exception.FileCountLimitExceededException`      |
| `com.atlassian.plugins.rest.v2.multipart.exception.FileSizeLimitExceededException`       |
| `com.atlassian.plugins.rest.v2.multipart.exception.UnsupportedFileNameEncodingException` |
| `com.atlassian.plugins.rest.v2.multipart.fileupload.CommonsFileUploadMultipartHandler`   |
| `com.atlassian.plugins.rest.v2.multipart.jersey.MultipartFormMessageBodyReader`          |
| `com.atlassian.plugins.rest.v2.multipart.jersey.MultipartFormParamValueParamProvider`    |
| `com.atlassian.plugins.rest.v2.sal.MarshallingEntityHandler`                             |
| `com.atlassian.plugins.rest.v2.sal.MarshallingRequest`                                   |
| `com.atlassian.plugins.rest.v2.sal.MarshallingResponse`                                  |
| `com.atlassian.plugins.rest.v2.security.antisniffing.AntiSniffingResponseFilter`         |
| `com.atlassian.plugins.rest.v2.security.authentication.AuthenticatedResourceFilter`      |
| `com.atlassian.plugins.rest.v2.security.cors.CorsAcceptOptionsPreflightFilter`           |
| `com.atlassian.plugins.rest.v2.security.cors.CorsResourceDynamicFeature`                 |
| `com.atlassian.plugins.rest.v2.security.cors.CorsResourceFilter`                         |
| `com.atlassian.plugins.rest.v2.security.websudo.SalWebSudoResourceContext`               |
| `com.atlassian.plugins.rest.v2.security.websudo.WebSudoResourceFilter`                   |
| `com.atlassian.plugins.rest.v2.security.xsrf.XsrfResourceDynamicFeature`                 |
| `com.atlassian.plugins.rest.v2.security.xsrf.XsrfResourceFilter`                         |
| `com.atlassian.plugins.rest.v2.servlet.DefaultRestServletModuleManager`                  |
| `com.atlassian.plugins.rest.v2.servlet.RestDelegatingServletFilter`                      |
| `com.atlassian.plugins.rest.v2.servlet.RestSeraphFilter`                                 |
| `com.atlassian.plugins.rest.v2.servlet.RestServletUtilsUpdaterFilter`                    |
| `com.atlassian.plugins.rest.v2.util.ReflectionUtils`                                     |
| `com.atlassian.plugins.rest.v2.util.ServletUtils`                                        |
| `com.atlassian.plugins.rest.v2.util.urlbuilder.ResourceInvokable`                        |
| `com.atlassian.plugins.rest.v2.version.ApiVersion`                                       |
