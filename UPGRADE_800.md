<!-- TOC -->
* [REST v2 Migration Guide](#rest-v2-migration-guide)
  * [Features breakdown](#features-breakdown)
    * [Basic features](#basic-features)
  * [Details on Feature Migration](#details-on-feature-migration)
  * [REST v2 Request Factory migration guide](#rest-v2-request-factory-migration-guide)
<!-- TOC -->

# REST v2 Migration Guide

## Features breakdown

### Basic features

| Feature        | Migration steps                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
|----------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| RequestFactory | Request Factory migration guide [REST v2 Request Factory migration guide](#rest-v2-request-factory-migration-guide)


## Details on Feature Migration

### REST v2 Request Factory migration guide

REST v2 changed the way it exports the request marshaling service bean.


| **REST v1 bean interface**                            | **REST v2 bean interface**                            |
|-------------------------------------------------------|-------------------------------------------------------|
| `com.atlassian.sal.api.net.RequestFactory`            | `com.atlassian.sal.api.net.MarshallingRequestFactory` |
| `com.atlassian.sal.api.net.MarshallingRequestFactory` | `com.atlassian.sal.api.net.MarshallingRequestFactory` |

<br>REST will no longer export the default implementation of the RequestFactory. Plugins that use this functionality need to change their RequestFactory imports:
`com.atlassian.sal.api.net.RequestFactory` → `com.atlassian.sal.api.net.MarshallingRequestFactory`
